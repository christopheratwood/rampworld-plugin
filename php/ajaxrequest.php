<?php
date_default_timezone_set('Europe/London');
function remove_size()
{
    if (isset($_REQUEST['s'])) {
        $sid = $_REQUEST['s'];
    } else {
        die();
    }

    require_once __DIR__.'/../../RWCommerce/classes/ProductVariant.php';

    $productv = new ProductVariant();
    echo $productv->removeSize($sid);
    exit();
}
add_action( 'wp_ajax_remove_size', 'remove_size' );
function get_member_by_id()
{
    if (isset($_REQUEST['memberID'])) {
        $memberID = $_REQUEST['memberID'];
    } else {
        die();
    }
    require_once 'classes/Member.php';

    $member = new Member();
    $member->getMemberEntry($memberID);
    echo json_encode($member->members);
    die();
}
add_action( 'wp_ajax_get_member_by_id', 'get_member_by_id' );


function get_member_by_custom()
{
    
    if (isset($_REQUEST['field_one_opt'])) {
        $field_one_opt = sanitize_text_field($_REQUEST['field_one_opt']);
    } else {
        $field_one_opt = null;
    }
    if (isset($_REQUEST['field_one_input'])) {
        $field_one_input = sanitize_text_field($_REQUEST['field_one_input']);
    } else {
        $field_one_input =  null;
    }
    if (isset($_REQUEST['field_two_opt'])) {
        $field_two_opt = sanitize_text_field($_REQUEST['field_two_opt']);
    } else {
        $field_two_opt = null;
    }
    if (isset($_REQUEST['field_two_input'])) {
        $field_two_input = sanitize_text_field($_REQUEST['field_two_input']);
    } else {
        $field_two_input = null;
    }

    require_once 'classes/Member.php';
    $member = new Member();
    $member->getMembersEntry($field_one_opt, $field_one_input, $field_two_opt, $field_two_input);


    if( $member->is_booking ) {
        echo json_encode( $member->bookings);
    }else {
        echo json_encode($member->members);
    }
    exit();
}
add_action( 'wp_ajax_get_member_by_custom', 'get_member_by_custom' );
function get_full_booking_details() {
    if ( isset( $_REQUEST['bookingID'] ) ) {
        require_once 'classes/Booking.php';
        $booking = new Booking();

        $booking->getAllMembers( intval($_REQUEST['bookingID'] ) );
        echo json_encode( array(
            'details' => $booking->details,
            'members' => $booking->members));
    }else {
        echo json_encode( array() );
    }
    exit();
}   
add_action( 'wp_ajax_get_full_booking_details', 'get_full_booking_details' );

function get_todays_sessions()
{
    require_once 'classes/Session.php';
    $session = new Session();
    $session->getTodaysSessions();
    echo json_encode($session->sessionTimes);
    die();
}
add_action( 'wp_ajax_get_todays_sessions', 'get_todays_sessions' );
/**
 * Entry functions
 *
 */


 /**
    * Set member entry
    *
    * @return void
    */
function set_member_entry()
{

    if (isset($_REQUEST['memberID'])) {
        $memberID = sanitize_text_field($_REQUEST['memberID']);
    } else {
        die();
    }
    if (isset($_REQUEST['timeOut'])) {
        $end = sanitize_text_field($_REQUEST['timeOut']);
    } else {
        die();
    }
    if (isset($_REQUEST['verify'])) {
        $verify = $_REQUEST['verify'];
    } else {
        die();
    }
    require_once 'classes/Entry.php';
    $entry = new Entry();
    if ($verify === true) {
        require_once 'classes/Member.php';
        $member = new Member();
        if ($member->verify($memberID) === false) {
            echo implode(', ', $member->error);
            die();
        }
    }
    if ($entry->add($memberID, $end, $verify)) {
        echo "passed";
        exit();
    } else {
        echo impolde(', ', $entry->error);
        exit();
    }
}
add_action('wp_ajax_set_member_entry', 'set_member_entry');


function set_new_weekpass()
{

    if (isset($_REQUEST['memberID'])) {
        $memberID = intval(sanitize_text_field($_REQUEST['memberID']));
    } else {
        die();
    }
    if (isset($_REQUEST['week'])) {
        $week = intval(sanitize_text_field($_REQUEST['week']));
    } else {
        die();
    }
    if (isset($_REQUEST['time'])) {
        $time = sanitize_text_field($_REQUEST['time']);
    } else {
        $time = null;
    }
    if (isset($_REQUEST['verify'])) {
        $verify = boolval($_REQUEST['verify']);
    } else {
        die;
    }

    if (is_int( $memberID )) {
        require_once 'classes/PaidMembership.php';
        require_once 'classes/Entry.php';

        if ($verify === true) {
            require_once 'classes/Member.php';
            $member = new Member();
            if ($member->verify($memberID) === false) {
                echo implode(', ', $member->error);
                die();
            }
        }
        $pm = new PaidMembership();

        if ($pm->create($memberID, array(
            'type' => 1,
            'start_date' => date('Y-m-d', strtotime('monday this week')),
        ), true) === true ) {
            $entry = new Entry();
            if ($entry->add($memberID, $time, false) === true) {
                echo 'passed';
                die();
            } else {
                echo implode(', ', $entry->error);
                die();
            }
        } else {
            echo implode(', ', $pm->error);
            die();
        }
    } else {
        echo "Member ID not an number.";
        die();
    }
}
add_action('wp_ajax_set_new_weekpass', 'set_new_weekpass');


function set_member_entry_pp()
{

    if (isset($_REQUEST['memberID'])) {
        $memberID = sanitize_text_field($_REQUEST['memberID']);
    } else {
        die();
    }
    if (isset($_REQUEST['timeOut'])) {
        $end = sanitize_text_field($_REQUEST['timeOut']);
    } else {
        die();
    }
    if (isset($_REQUEST['ppid'])) {
        $pp = intval($_REQUEST['ppid']);
    } else {
        die();
    }
    if (isset($_REQUEST['verify'])) {
        $verify = $_REQUEST['verify'];
    } else {
        die();
    }

    require_once 'classes/Entry.php';
    require_once 'classes/Booking.php';
    $entry = new Entry();
    $booking = new Booking();

    if ($verify) {
        require_once 'classes/Member.php';
        $member = new Member();

        if (!$member->verify($memberID)) {
            echo implode(', ', $member->error);
            exit();
        }
    }
    if ($entry->add($memberID, $end)) {
        if ($booking->update_used($pp)) {
                echo "passed";
                die();
        } else {
            echo implode(', ', $booking->error);
            die();
        }
    } else {
        echo implode(', ', $entry->error);
        die();
    }
}
add_action('wp_ajax_set_member_entry_pp', 'set_member_entry_pp');

function set_member_entry_pass()
{

    if (isset($_REQUEST['memberID'])) {
        $memberID = sanitize_text_field($_REQUEST['memberID']);
    } else {
        die();
    }
    if (isset($_REQUEST['ppid'])) {
        $pp = intval($_REQUEST['ppid']);
    } else {
        die();
    }
    if (isset($_REQUEST['verify'])) {
        $verify = $_REQUEST['verify'];
    } else {
        die();
    }

    require_once 'classes/Entry.php';
    require_once 'classes/Booking.php';
    $entry = new Entry();
    $booking = new Booking();

    if ($verify) {
        require_once 'classes/Member.php';
        $member = new Member();

        if (!$member->verify($memberID)) {
            echo implode(', ', $member->error);
            exit();
        }
    }
    if ($entry->add($memberID, $end)) {
        if ($booking->update_used($pp)) {
                echo "passed";
                die();
        } else {
            echo implode(', ', $booking->error);
            die();
        }
    } else {
        echo implode(', ', $entry->error);
        die();
    }
}
add_action('wp_ajax_set_member_entry_pp', 'set_member_entry_pp');


function set_member_entry_booking_pass()
{

    if (isset($_REQUEST['memberID'])) {
        $memberID = sanitize_text_field($_REQUEST['memberID']);
    } else {
        die();
    }
    if (isset($_REQUEST['passid'])) {
        $passid = intval($_REQUEST['passid']);
    } else {
        die();
    }
    if (isset($_REQUEST['verify'])) {
        $verify = $_REQUEST['verify'];
    } else {
        die();
    }
    if (isset($_REQUEST['timeOut'])) {
        $end = sanitize_text_field($_REQUEST['timeOut']);
    } else {
        die();
    }

    require_once 'classes/Entry.php';
    require_once 'classes/Booking.php';
    $entry = new Entry();
    $booking = new Booking();

    if ($verify) {
        require_once 'classes/Member.php';
        $member = new Member();

        if (!$member->verify($memberID)) {
            echo implode(', ', $member->error);
            exit();
        }
    }
    if ($entry->add($memberID, $end)) {
        if ($booking->update_used($passid, 'pass')) {
                echo "passed";
                die();
        } else {
            echo implode(', ', $booking->error);
            die();
        }
    } else {
        echo implode(', ', $entry->error);
        die();
    }
}
add_action('wp_ajax_set_member_entry_booking_pass', 'set_member_entry_booking_pass');

function remove_entry()
{

    if (isset($_REQUEST['id'])) {
        $id = sanitize_text_field($_REQUEST['id']);
    } else {
        die();
    }
    require_once 'classes/Entry.php';
    $entry = new Entry();

    echo $entry->delete($id);
    die();
}
add_action('wp_ajax_remove_entry', 'remove_entry');
function get_stats()
{
    $days = [];
    
    if (isset($_REQUEST['days'])) {
        for ($i = 0; $i < count($_REQUEST['days']); $i++) {
            $days[] = sanitize_text_field($_REQUEST['days'][$i]);
        }
    } else {
        die();
    }
    if (isset($_REQUEST['duration'])) {
        $duration = sanitize_text_field($_REQUEST['duration']);
    } else {
        die();
    }


    if (isset($_REQUEST['mode'])) {
        $mode = $_REQUEST['mode'];

        if ($mode == 'entry') {
            $where = array();
            $add = false;
            $orMain = false;
            for ($i = 0; $i < count( $days ); $i++) {
                if ($where != '') {
                    $add = true;
                }
                if (isset($days['weekday']) && ( isset($days['1']) || isset($days['2']) || isset($days['3']) || isset($days['4']) ||isset($days['5'])) && ($days[$i] == '1' || $days[$i] == '2' || $days[$i] == '3' || $days[$i] == '4' || $days[$i] == '5')) {
                    break;
                } elseif (isset($days['weekend']) && ( isset($days['6']) || isset($days['0'])   ) && ($days[$i] == '6' || $days[$i] == '0')) {
                    break;
                } elseif (isset($days['all']) && ( isset($days['weekdays']) && isset($days['weekend'])) && ($days[$i] == 'weekday' || $days[$i] == 'weekend')) {
                    break;
                }

                if ($days[$i] == 'weekday') {
                    $or = false;
                    for ($j = 1; $j < 6; $j++) {
                        if ($or == false) {
                            $where[0] .= 'DAYOFWEEK (`entry_date`) = \'' . $j . '\'';
                            $or = true;
                        } else {
                            $where[0] .= ' OR DAYOFWEEK (`entry_date`) = \'' . $j . '\'';
                        }
                    }
                } elseif ($days[$i] == 'weekend') {
                    $where['weekend'] .= 'DAYOFWEEK (`entry_date`) = \'6\' OR  DAYOFWEEK (`entry_date`) = \'0\'';
                } elseif ($days[$i] == 'all') {
                    $or = false;
                    for ($j = 0; $j < 7; $j++) {
                        if ($or == false) {
                            $where[0] .= 'DAYOFWEEK (`entry_date`) = \'' . $j . '\'';
                            $or = true;
                        } else {
                            $where[0] .= ' OR DAYOFWEEK (`entry_date`) = \'' . $j . '\'';
                        }
                    }
                } else {
                    if ($orMain) {
                        $where[$i]  = 'OR DAYOFWEEK (`entry_date`) = \'' . $days[$i] . '\'';
                    } else {
                        $where[$i]  = 'DAYOFWEEK (`entry_date`) = \'' . $days[$i] . '\'';
                        $orMain = true;
                    }
                }
            }
            
            for ($i = 0; $i < count( $days ); $i++) {
                $dayName = jddayofweek($days[$i], 1);
                
                
                if (isset($days['weekday']) && ( isset($days['1']) || isset($days['2']) || isset($days['3']) || isset($days['4']) ||isset($days['5'])) && ($days[$i] == '1' || $days[$i] == '2' || $days[$i] == '3' || $days[$i] == '4' || $days[$i] == '5')) {
                    break;
                } elseif (isset($days['weekend']) && ( isset($days['6']) || isset($days['0'])   ) && ($days[$i] == '6' || $days[$i] == '0')) {
                    break;
                } elseif (isset($days['all']) &&
                     ( (isset($days['weekdays']) && isset($days['weekend']) ) ||
                      ( isset($days['0']) || isset($days['1']) || isset($days['2']) || isset($days['3']) || isset($days['4']) || isset($days['5']) || isset($days['6'])) ) && ($days[$i] == 'weekday' || $days[$i] == 'weekend') ) {
                    break;
                } elseif ($days[$i] == 'weekday' || $days[$i] == 'weekend' || $days[$i] == 'all') {
                    switch ($duration) {
                        case '1w':
                            $where[ 0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '2w':
                            $where[ 0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-2 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '3w':
                            $where[ 0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-3 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '1m':
                            $where[ 0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 month") ) . '\' and CURRENT_DATE)';
                            break;
                        case '3m':
                            $where[ 0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-3 month") ) . '\' and CURRENT_DATE)';
                            break;
                        case '1y':
                            $where[0 ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 year") ) . '\' and CURRENT_DATE)';
                            break;
                        case 'lt':
                            $where[ 0 ] .= '';
                            break;
                        default:
                            $where[ 0 ] .= '  AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 week") ) . '\' and CURRENT_DATE)';
                            break;
                    }
                } else {
                    switch ($duration) {
                        case '24h':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '1w':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '2w':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-2 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '3w':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-3 week") ) . '\' and CURRENT_DATE)';
                            break;
                        case '1m':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 month") ) . '\' and CURRENT_DATE)';
                            break;
                        case '3m':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-3 month") ) . '\' and CURRENT_DATE)';
                            break;
                        case '1y':
                            $where[ $days[$i] ] .= ' AND (`entry_date` BETWEEN \'' . date('Y-m-d,', strtotime("-1 year") ) . '\' and CURRENT_DATE)';
                            break;
                        default:
                            $where[ $days[$i] ] .= 'ok';
                            break;
                    }
                }
            }
            $wh = '';
            for ($i=0; $i < count($where); $i++) {
                $wh .= $where[$i];
            }
            $db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);


            $res = $db->get_results('SELECT `entry_time`, `exit_time`, `entry_date` FROM rwc_members_entries WHERE '.$wh.'', ARRAY_A);
            
            echo json_encode( $res );
        }
    } else {
    }
}

add_action('wp_ajax_get_stats', 'get_stats');



/* function set_new_weekpass_verify() {
	
		if(isset($_REQUEST['memberID'])) $memberID = intval(sanitize_text_field($_REQUEST['memberID'])); else die();
		if(isset($_REQUEST['time'])) $time = sanitize_text_field($_REQUEST['time']); else $time = null;
		require_once 'classes/Entry.php';
		require_once 'classes/Member.php';
		require_once 'classes/PaidMembership.php';		
		$entry = new Entry();
		$member = new Member();
		$pm = new PaidMembership();
	
		$inits = wp_get_current_user()->user_firstname .' '. wp_get_current_user()->user_lastname;
		if($member->verify($memberID, $inits) ) {
			$r = $pm->add($memberID, 1, date('Y-m-d', strtotime('monday this week')),  date('Y-m-d', strtotime('sunday this week')));
			if($r === true) {
				if($entry->add($memberID,  $time) ) {
					echo 'passed';
					exit();
				} else {
					echo 'Failed to save entry';
					exit();
				}
			} else {
				echo json_encode($r);
				exit();
			}
		} else{
			echo 'Failed to verify';
			exit();
		}
	}
	add_action('wp_ajax_set_new_weekpass_verify', 'set_new_weekpass_verify'); */


/* function remove_pass() {
	echo "hello";
	if(isset($_REQUEST['id'])) $id = sanitize_text_field($_REQUEST['id']); else die();
	echo "hello2";

	//validation
	
	$db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	
	if($db->update('rwc_paid_membership' ,array('premature'=> date("Y-m-d H:i:s")), array('paid_membership_id' => $id), array('%s'), array('%d') )) {
		echo 'passed';
		die();
	} else {
		echo  'Server error. Check with admin.';
		die();
	}

	
}
add_action('wp_ajax_remove_pass', 'remove_pass'); */
function get_passes()
{

    
    $db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    
    echo json_encode($db->get_results("SELECT * FROM rwc_membership_types ORDER BY display_name", ARRAY_A));
    die();
}
add_action('wp_ajax_get_passes', 'get_passes');
/* 
function set_new_pass() {

	if(isset($_REQUEST['memberID'])) $memberID = intval(sanitize_text_field($_REQUEST['memberID'])); else die();
	if(isset($_REQUEST['mtype'])) $mtype = intval(sanitize_text_field($_REQUEST['mtype'])); else die();
	if(isset($_REQUEST['duration'])) $duration = sanitize_text_field($_REQUEST['duration']); else die();
	if(isset($_REQUEST['start_date'])) $start_date = sanitize_text_field($_REQUEST['start_date']); else die();

	if(is_int( $memberID )) {
		$db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);

		$inits = ucfirst(substr(wp_get_current_user()->user_firstname, 0, 1)) . ucfirst(substr(wp_get_current_user()->user_lastname, 0, 1));
		$prev = $db->get_results("SELECT `date`, start_date, end_date FROM rwc_paid_membership WHERE member_id = {$memberID} AND start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE ORDER BY start_date LIMIT 1", ARRAY_A);
		if(empty($prev)) {
			if($db->insert('rwc_paid_membership' , array('member_id' => $memberID, 'membership_type' => $mtype, 'start_date' => date('Y-m-d', strtotime($start_date)), 'end_date' => date('Y-m-d', strtotime($start_date .' + '.$duration.' week')), 'staff_init' => $inits) ) ) {
				echo 'passed';
				die();
			} else {
				echo  $db->last_query;
				die();
			}
	 	} else {
	 		echo 'Sorry, Member already has membership.Purchased on '.$prev[0]['date'].', Start Date '.$prev[0]['start_date'].', End Date '.$prev[0]['end_date'];
	 		die();
	 	}
			


	} else {
		echo "Member ID not an number.";
		die();
	}
	 	
}
add_action('wp_ajax_set_new_pass', 'set_new_pass'); */

function update_member_entry()
{
    
    if (isset($_REQUEST['session_id']) && !empty($_REQUEST['session_id'])) {
        $session_id = intval(sanitize_text_field($_REQUEST['session_id']));
    } else {
        echo 'No session identifier found.';
        die();
    }
    if (isset($_REQUEST['exit_time'])) {
        $exit_time = sanitize_text_field($_REQUEST['exit_time']);
    } else {
        echo 'No new session time selected.';
        die();
    }


    if (is_int( $session_id )) {
        $db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);

        if ($db->update('rwc_members_entries', array('exit_time' => $exit_time), array('entry_id' => $session_id), array('%s'), array('%d') ) === false) {
            echo 'error';
            die();
        } else {
            echo 'passed';
            die();
        }
    } else {
        echo 'No session identifier found.';
        die();
    }
}
add_action('wp_ajax_update_member_entry', 'update_member_entry');


function change_booking_participant()
{
    print_r($_REQUEST);
    if (isset($_REQUEST['ppid'])) {
        $ppid = intval($_REQUEST['ppid']);
    } else {
        die();
    }
    if (isset($_REQUEST['t'])) {
        $type = $_REQUEST['t'];
    }
    if (isset($_REQUEST['prev'])) {
        $prev = intval($_REQUEST['prev']);
    } else {
        die();
    }
    if (isset($_REQUEST['mid'])) {
        $mid = intval($_REQUEST['mid']);
    } else {
        die();
    }
    
    $db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    if ($type == 'pp') {
        if ($db->update('rwc_prepaid_sessions', array('member_id' => $mid), array('prepaid_id' => $ppid), array('%d'), array('%d') )) {
            $db->insert('rwc_log', array('date'=> date('Y-m-d H:i:s'), 'content' => 'Changed member from '.$prev.' to '.$mid.'', 'record_id' => $ppid), array('%s','%s', '%d'));
            echo 'passed';
            die();
        } else {
            echo  $db->last_query;
            die();
        }
    } elseif ($type == 'pm') {
        if ($db->update('rwc_paid_membership', array('member_id' => $mid), array('paid_membership_id' => $ppid), array('%d'), array('%d') )) {
            $db->insert('rwc_log', array('date'=> date('Y-m-d H:i:s'), 'content' => 'Changed member from '.$prev.' to '.$mid.'', 'record_id' => $ppid), array('%s','%s', '%d'));
            echo 'passed';
            die();
        } else {
            echo  $db->last_query;
            die();
        }
    } else {
        echo "Paid Session ID not an number.";
        die();
    }
}
add_action('wp_ajax_change_booking_participant', 'change_booking_participant');


function get_booking_details()
{

    if (isset($_REQUEST['ppid'])) {
        $ppid = intval($_REQUEST['ppid']);
    } else {
        die();
    }
    if (isset($_REQUEST['memberID'])) {
        $memberid = intval($_REQUEST['memberID']);
    } else {
        die();
    }



    if (is_int( $ppid )) {
        require_once 'classes/Booking.php';
        $booking = new Booking();
        echo json_encode($booking->getDetails($ppid, $memberid));
        die();
    } else {
        echo "Paid Session ID not an number.";
        die();
    }
}
add_action('wp_ajax_get_booking_details', 'get_booking_details');



function verify_member_json()
{
    if (isset($_REQUEST['mid'])) {
        $member_id = intval($_REQUEST['mid']);
    } else {
        die();
    }

    require_once 'classes/Member.php';
    $member = new Member();
    $member->getViewMember($member_id);

    echo json_encode($member->members);
    die();
}
add_action('wp_ajax_verify_member_json', 'verify_member_json');

function set_member_entry_verify()
{

    if (isset($_REQUEST['memberID'])) {
        $id = sanitize_text_field($_REQUEST['memberID']);
    } else {
        die();
    }
    if (isset($_REQUEST['timeOut'])) {
        $end = sanitize_text_field($_REQUEST['timeOut']);
    } else {
        die();
    }


    require_once 'classes/Entry.php';
    require_once 'classes/Member.php';
    $entry = new Entry();
    $member = new Member();

    $inits = ucfirst(substr(wp_get_current_user()->user_firstname, 0, 1)) . ucfirst(substr(wp_get_current_user()->user_lastname, 0, 1));
    if ($member->verify($id, $inits)) {
        if ($entry->add($id, $end)) {
            echo 'passed';
            exit();
        } else {
            echo $entry->error;
            exit();
        }
    } else {
        echo 'Could Not verify';
        exit();
    }
}
add_action('wp_ajax_set_member_entry_verify', 'set_member_entry_verify');

function update_check_message() {
    require_once 'classes/PremisesCheck.php';
    $check = new PremisesCheck();
    echo $check->updateDismissed($_POST['checkID']);
    exit();
}
add_action('wp_ajax_update_check_message', 'update_check_message');