<?php

date_default_timezone_set('Europe/London');
class PremisesCheck
{
    private $_db;
    public $error;
    public $checks = array();
		public $count = null;
		public $last_session = '';
		public $first_session = '';
    public $opening = array(
        'sections' => array('bowl' => 5, 'jump_boxes' => 5, 'foam_resi' => 5, 'street_area' => 5, 'spine_mini_pump_track' => 5, 'cctv' => 2, 'reception' => 12, 'spectators' => 7, 'toilets' => 7, 'thursday' => 4 , 'fireChecks' => 4),
        'values' => array('bowl' => '', 'jump_boxes' => '', 'foam_resi' => '', 'street_area' => '', 'spine_mini_pump_track' => '', 'cctv' => '', 'reception' => '', 'spectators' => '', 'toilets' => '', 'thursday' => '', 'fireChecks' => '', 'comments' => '' )
    );
    public $closing = array(
        'sections' => array('stock' => 3, 'cleaning' => 12, 'park_check' => 4, 'till' => 2, 'power_off' => 7, 'final' => 4),
        'values' => array('stock' => '', 'cleaning' => '', 'park_check' => '', 'till' => '', 'power_off' => '', 'final' => '', 'comments' => '' )
    );
    public function __construct()
    {
        
        $this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    }
    public function getAll($date, $start, $limit)
    {

        if ($date == null) {
            $this->checks = $this->_db->get_results($this->_db->prepare('SELECT
				DATE_FORMAT(b.date, "%%Y-%%m-%%d") date,
				(c.`closing_check_id`),
				(o.`opening_check_id`)
			FROM
			(
				SELECT
					(CURRENT_DATE + interval 1 day)  - INTERVAL a.`opening_check_id` day AS date
				FROM rwc_opening_checks a
			) b
			LEFT JOIN rwc_closing_checks c ON date(b.date) = date(c.date)
			LEFT JOIN rwc_opening_checks o ON date(b.date) = date(o.date)  
			GROUP BY b.date
			ORDER BY b.date DESC
			LIMIT %d, %d', array($start, $limit)), ARRAY_A);
            
            
            $this->count = $this->_db->get_results('SELECT
            count(b.date) as count FROM (SELECT (CURRENT_DATE + interval 1 day)  - INTERVAL a.`opening_check_id` day AS date FROM rwc_opening_checks a) b', ARRAY_A)[0]['count'];
            //$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE '.$searchField.' LIKE \'%%%s%%\' AND '.$searchField2.' LIKE \'%%%s%%\'',array($searchQuery, $searchQuery2)), ARRAY_A)[0]['count'];
        } else {
            $this->checks = $this->_db->get_results($this->_db->prepare('SELECT
							DATE_FORMAT(o.date, "%%Y-%%m-%%d") date,
							(c.`closing_check_id`),
							(o.`opening_check_id`)
						FROM rwc_opening_checks as o
						LEFT JOIN rwc_closing_checks c  ON date(o.date)  = date(c.date)
						WHERE date(c.date) = %s OR date(o.date)= %s LIMIT 1
		    ', array($date,$date )), ARRAY_A);

            $this->count = $this->_db->get_results($this->_db->prepare('SELECT 1 as count FROM rwc_opening_checks as o
						LEFT JOIN rwc_closing_checks c  ON date(o.date)  = date(c.date)
						WHERE date(c.date) = %s OR date(o.date)= %s', array($date, $date)), ARRAY_A)[0]['count'];
        }
    }
    
    public function get($type, $date)
    {
        if ($type == 'opening') {
            $this->checks = $this->_db->get_results($this->_db->prepare('(SELECT opening_check_id,date as datetime, DATE_FORMAT(date, "%%W the %%D of %%M %%Y") as date,bowl, jump_boxes, foam_resi, street_area, spine_mini_pump_track, cctv, reception, spectators, toilets, thursday, fireChecks, comments, last_updated_time, last_updated_user, updated_amount FROM rwc_opening_checks WHERE DATE(date - interval 1 HOUR) = %s) UNION (SELECT null,null,null, null,null,null,null,null,null, null, null,null,null,null,null,null,null,null) LIMIT 1', array($date)), ARRAY_A);
        } elseif ($type == 'closing') {
            $this->checks = $this->_db->get_results($this->_db->prepare('(SELECT closing_check_id, date as datetime, DATE_FORMAT(date, "%%W the %%D of %%M %%Y") as date, stock, cleaning, park_check, till, power_off, final, comments, last_updated_time, last_updated_user, updated_amount FROM rwc_closing_checks WHERE DATE(date - interval 1 HOUR) = %s)UNION 
			(SELECT null,null,null, null,null,null,null,null,null, null,null, null, null)
			 LIMIT 1', array($date)), ARRAY_A);
        }
    }
    public function completedOpeningChecks()
    {
        $res = $this->_db->get_results('SELECT count(1) as completed FROM rwc_opening_checks WHERE DATE(`date`) = \''.date('Y-m-d').'\' LIMIT 1', ARRAY_A);
        return (!empty($res) && $res[0]['completed'] == "1") ? true: false;
    }
    public function completedClosingChecks()
    {
        $res = $this->_db->get_results('SELECT count(1) as completed FROM rwc_closing_checks WHERE DATE(`date`) = \''.date('Y-m-d', strtotime("- 30 minute")).'\' LIMIT 1', ARRAY_A);
        return (!empty($res) && $res[0]['completed'] == "1") ? true: false;
    }
    public function openingWarning()
    {
        $date = date('w');
        if (isHoliday) {
					$holiday = $this->_db->get_results('SELECT holiday_id FROM rwc_holidays WHERE CURDATE() between start_date AND end_date AND visible = 1', ARRAY_A);
					if (!empty($holiday )) {
						$this->first_session =  $this->_db->get_results($this->_db->prepare('SELECT end_time from rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ORDER BY end_time ASC LIMIT 1', array($holiday[0]['holiday_id'], $date)), ARRAY_A)[0]['end_time'];
					}
        } else {
					$this->first_session = $this->_db->get_results($this->_db->prepare('SELECT end_time FROM rwc_sessions WHERE day = %d ORDER BY end_time ASC LIMIT 1', array($date)), ARRAY_A)[0]['end_time'];
				}
				if(date('H:i', strtotime('+ 30 minutes')) > $this->first_session) {
					$res = $this->_db->get_results('SELECT count(1) as completed FROM rwc_opening_checks WHERE DATE(`date`) = \''.date('Y-m-d').'\' LIMIT 1', ARRAY_A);
					return (!empty($res) && $res[0]['completed'] == "1") ? false: true;
				} else {
					return false;
				}
		
		}
		public function closingWarning()
    {
			$date = date('w');
			if (isHoliday) {
				$holiday = $this->_db->get_results('SELECT holiday_id FROM rwc_holidays WHERE CURDATE() between start_date AND end_date AND visible = 1', ARRAY_A);
				if (!empty($holiday )) {
					$this->last_session =  $this->_db->get_results($this->_db->prepare('SELECT end_time from rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ORDER BY end_time DESC LIMIT 1', array($holiday[0]['holiday_id'], $date)), ARRAY_A)[0]['end_time'];
				}
			} else {
				$this->last_session = $this->_db->get_results($this->_db->prepare('SELECT end_time FROM rwc_sessions WHERE day = %d ORDER BY end_time DESC LIMIT 1', array($date)), ARRAY_A)[0]['end_time'];
			}
			//echo $this->last_session;
			
			if(date('H:i', strtotime('- 1 hour')) > $this->last_session) {
				$res = $this->_db->get_results('SELECT count(1) as completed FROM rwc_closing_checks WHERE DATE(`date`) = \''.date('Y-m-d', strtotime("- 30 minute")).'\' LIMIT 1', ARRAY_A);
        return (!empty($res) && $res[0]['completed'] == "1") ? false: true;
			} else {
				return false;
			}
    }
    public function add($mode, $data)
    {
        return $this->_commit('create', (($mode == 'opening')? 'rwc_opening_checks': 'rwc_closing_checks'), $data);
    }
    public function update($mode, $id, $data)
    {
        return $this->_commit('update', (($mode == 'opening')? 'rwc_opening_checks': 'rwc_closing_checks'), $data, array((($mode == 'opening')? 'opening_check_id': 'closing_check_id') => intval($id)));
    }

    private function _commit($mode, $table, $data, $where = null)
    {
        if ($mode == "create") {
            if ($this->_db->insert($table, $data) === false) {
                $this->error = $this->_db->last_error;
                return false;
            } else {
                return true;
            }
        } elseif ($mode == "update") {
            if ($this->_db->update($table, $data, $where) === false) {
                $this->error = $this->_db->last_error;
                return false;
            } else {
                return true;
            }
        }
    }
    public function getCheck($type)
    {
        if ($type == 'opening') {
            $day = date("w");
            if ($day == 4) {
                $res =  $this->_db->get_results('SELECT `opening_check_id`,`bowl`,`jump_boxes`,`foam_resi`,`street_area`,`spine_mini_pump_track`,`cctv`,`reception`,`spectators`,`toilets`,`thursday`, `comments`, `last_updated_user` FROM '.database_prefix.'opening_checks WHERE date = \''.date('Y-m-d').'\' LIMIT 1"', ARRAY_A);
                return (!empty($res))? $res: false;
            } elseif ($day == 6 || $day == 0) {
                $res =  $this->_db->get_results('SELECT `opening_check_id`,`bowl`,`jump_boxes`,`foam_resi`,`street_area`,`spine_mini_pump_track`,`cctv`,`reception`,`spectators`,`toilets`, `fireChecks`, `comments`, `last_updated_user` FROM '.database_prefix.'opening_checks WHERE date = \''.date('Y-m-d').'\' LIMIT 1', ARRAY_A);
                return (!empty($res))? $res: false;
            } else {
                $res =  $this->_db->get_results('SELECT `opening_check_id`,`bowl`,`jump_boxes`,`foam_resi`,`street_area`,`spine_mini_pump_track`,`cctv`,`reception`,`spectators`,`toilets`,`comments`, `last_updated_user` FROM '.database_prefix.'opening_checks WHERE date = \''.date('Y-m-d').'\' LIMIT 1', ARRAY_A);
                return (!empty($res))? $res: false;
            }
        } elseif ($type == 'closing') {
            return $this->_db->get_results($this->_db->prepare("SELECT * FROM rwc_closing_checks WHERE date = %d LIMIT 1", array(date('Y-m-d'))), ARRAY_A);
        }
    }

    public function addOpening($data, $date)
    {

        $day = date('w');

        $this->_format('opening', $data);
        $current_user = wp_get_current_user();

        $check = $this->_db->get_results($this->_db->prepare('SELECT opening_check_id FROM rwc_opening_checks WHERE DATE(date) = %s', array($date)), ARRAY_A);
        if (count($check) != 0) {
            if ($day == 4) {
                if ($this->_db->update('rwc_opening_checks', array( 'bowl' => $this->opening['values']['bowl'], 'jump_boxes' => $this->opening['values']['jump_boxes'], 'foam_resi' => $this->opening['values']['foam_resi'], 'street_area' => $this->opening['values']['street_area'], 'spine_mini_pump_track' => $this->opening['values']['spine_mini_pump_track'], 'cctv' => $this->opening['values']['cctv'], 'reception' => $this->opening['values']['reception'], 'spectators' => $this->opening['values']['spectators'],'fireChecks' => $this->opening['values']['fireChecks'], 'toilets' => $this->opening['values']['toilets'],'thursday' => $this->opening['values']['thursday'], 'comments' => $this->opening['values']['comments'], 'last_updated_user' => $current_user->user_firstname.' '. $current_user->user_lastname  ), array('opening_check_id' => $check[0]['opening_check_id'])) == false) {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
                } else {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&success');
                }
            } else {
                if ($this->_db->update('rwc_opening_checks', array( 'bowl' => $this->opening['values']['bowl'], 'jump_boxes' => $this->opening['values']['jump_boxes'], 'foam_resi' => $this->opening['values']['foam_resi'], 'street_area' => $this->opening['values']['street_area'], 'spine_mini_pump_track' => $this->opening['values']['spine_mini_pump_track'], 'cctv' => $this->opening['values']['cctv'], 'reception' => $this->opening['values']['reception'], 'spectators' => $this->opening['values']['spectators'], 'toilets' => $this->opening['values']['toilets'],'fireChecks' => $this->opening['values']['fireChecks'], 'comments' => $this->opening['values']['comments'], 'last_updated_user' => $current_user->user_firstname.' '. $current_user->user_lastname  ), array('opening_check_id' => $check[0]['opening_check_id'])) == false) {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
                } else {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&success');
                }
            }
        } else {
            //new date
            
            if(date('Y-m-d', strtotime($date)) == date('Y-m-d')) { 
                $date = date('Y-m-d H:i:s');
            } else {
                $date  = $date . ' 23:59:59';
            }
            if ($day == 4) {
   
                if ($this->_db->insert('rwc_opening_checks', array('date' => $date, 'bowl' => $this->opening['values']['bowl'], 'jump_boxes' => $this->opening['values']['jump_boxes'], 'foam_resi' => $this->opening['values']['foam_resi'], 'street_area' => $this->opening['values']['street_area'], 'spine_mini_pump_track' => $this->opening['values']['spine_mini_pump_track'], 'cctv' =>
                 $this->opening['values']['cctv'], 'reception' => $this->opening['values']['reception'], 'spectators' => $this->opening['values']['spectators'], 'toilets' => $this->opening['values']['toilets'],'thursday' => $this->opening['values']['thursday'],
                    'comments' => $this->opening['values']['comments'], 'last_updated_user' => wp_get_current_user()->user_firstname. ' '. wp_get_current_user()->user_lastname)) == false) {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
                } else {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&success');
                }
            } else {
                if ($this->_db->insert('rwc_opening_checks', array('date' => $date, 'bowl' => $this->opening['values']['bowl'], 'jump_boxes' => $this->opening['values']['jump_boxes'], 'foam_resi' => $this->opening['values']['foam_resi'], 'street_area' => $this->opening['values']['street_area'], 'spine_mini_pump_track' => $this->opening['values']['spine_mini_pump_track'], 'cctv' => $this->opening['values']['cctv'], 'reception' => $this->opening['values']['reception'], 'spectators' => $this->opening['values']['spectators'], 'toilets' => $this->opening['values']['toilets'], 'fireChecks' => $this->opening['values']['fireChecks'], 'comments' => $this->opening['values']['comments'], 'last_updated_user' => wp_get_current_user()->user_firstname. ' '. wp_get_current_user()->user_lastname)) == false) {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
                } else {
                    wp_redirect($_SERVER['HTTP_REFERER'].'&success');
                }
            }
        }
    }
    
    public function addClosing($data, $date)
    {
        $this->_format('closing', $data);
        $current_user = wp_get_current_user();
        $is_dismissed = (strlen($this->closing['values']['comments']) > 0)? 0: 1;
        $check = $this->_db->get_results($this->_db->prepare('SELECT closing_check_id FROM rwc_closing_checks WHERE DATE(date) = %s', array($date)), ARRAY_A);
        if (count($check) != 0) {
            if ($this->_db->update('rwc_closing_checks', array('stock' => $this->closing['values']['stock'], 'cleaning' => $this->closing['values']['cleaning'], 'park_check' => $this->closing['values']['park_check'], 'till' => $this->closing['values']['till'], 'power_off' => $this->closing['values']['power_off'], 'final' => $this->closing['values']['final'], 'comments' => $this->closing['values']['comments'], 'last_updated_user' => $current_user->user_firstname.' '. $current_user->user_lastname, 'is_dismissed' => $is_dismissed), array('closing_check_id' => $check[0]['closing_check_id'])) === false) {
                wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
            } else {
               wp_redirect($_SERVER['HTTP_REFERER'].'&success');
            }
        } else {
            if(date('Y-m-d', strtotime($date)) == date('Y-m-d')) { 
                $date = date('Y-m-d H:i:s');
            } else {
                $date  = $date . ' 23:59:59';
            }
            
            if ($this->_db->insert('rwc_closing_checks', array('date'=> $date, 'stock' => $this->closing['values']['stock'], 'cleaning' => $this->closing['values']['cleaning'], 'park_check' => $this->closing['values']['park_check'], 'till' => $this->closing['values']['till'], 'power_off' => $this->closing['values']['power_off'], 'final' => $this->closing['values']['final'], 'comments' => $this->closing['values']['comments'], 'last_updated_user' => $current_user->user_firstname.' '. $current_user->user_lastname, 'is_dismissed' => $is_dismissed)) === false) {
                wp_redirect($_SERVER['HTTP_REFERER'].'&error=save');
            } else {
                wp_redirect($_SERVER['HTTP_REFERER'].'&success');
            }
        }
    }
    protected function _format($type, $data)
    {
        $countErrors = 0;
        if ($type == 'opening') {
            foreach ($this->opening['sections'] as $key => $value) {
                for ($i = 0; $i < $value; $i++) {
                    if (isset($data[$key."_".$i])) {
                        if (empty($data[$key ."_". $i]) && ( (isset($data[$key ."_". $i]) && $data[$key ."_". $i] != "0"))) {
                            $data[$key ."_". $i] = 0;
                            $this->opening['values'][$key] .= 0;
                        } elseif ($data[$key ."_". $i] == "1" || $data[$key ."_". $i] == "0" || $data[$key ."_". $i] == "n") {
                            $this->opening['values'][$key] .= $data[$key ."_". $i];
                        } else {
                            $errors[] = $key ." Item {$i}: Not valid option.";
                            $countErrors++;
                        }
                    } else {
                        $data[$key ."_". $i] = 0;
                        $this->opening['values'][$key] .= $data[$key ."_". $i];
                    }
                }
                $this->closing['values']['comments'] = (isset($data['comments'])) ?$data['comments']: '' ;
            }
            if (empty($errors) && $countErrors != 0) {
                wp_redirect($_SERVER['HTTP_REFERER'].'&error=keys');
            }
        } else {
            foreach ($this->closing['sections'] as $key => $value) {
                for ($i = 0; $i < $value; $i++) {
                    if (isset($data[$key."_".$i])) {
                        if (empty($data[$key ."_". $i]) && ( (isset($data[$key ."_". $i]) && $data[$key ."_". $i] != "0"))) {
                            $data[$key ."_". $i] = 0;
                            $this->closing['values'][$key] .= 0;
                        } elseif ($data[$key ."_". $i] == "1" || $data[$key ."_". $i] == "0" || $data[$key ."_". $i] == "n") {
                            $this->closing['values'][$key] .= $data[$key ."_". $i];
                        } else {
                            $errors[] = $key ." Item {$i}: Not valid option.";
                            $countErrors++;
                        }
                    } else {
                        $data[$key ."_". $i] = 0;
                        $this->closing['values'][$key] .= $data[$key ."_". $i];
                    }
                }
            }

            $this->closing['values']['comments'] = (isset($data['comments'])) ?$data['comments']: '' ;
            

        }

        if (empty($errors) && $countErrors != 0) {
            wp_redirect($_SERVER['HTTP_REFERER'].'&error=keys');
        }
    }

    public function checkMessage() {

        $date = date('Y-m-d', strtotime('yesterday'));
        $comments = $this->_db->get_results($this->_db->prepare('SELECT closing_check_id as id, date, comments as message, last_updated_user as member FROM rwc_closing_checks WHERE DATE(date) = %s AND is_dismissed = %d LIMIT %d', array(
            $date, 0, 1 
        )), ARRAY_A);
        if(count($comments) > 0) {
            return $comments[0];
        } else {
            return false;
        }
    }

    public function updateDismissed($checkID) {

        if(count($this->_db->get_results($this->_db->prepare("SELECT COUNT(date) FROM rwc_closing_checks WHERE closing_check_id = %d", array($checkID)), ARRAY_A)) > 0) {
            $this->_db->update('rwc_closing_checks',array(
                'is_dismissed'      => 1,
            ), array(
                'closing_check_id'  => $checkID
            ), array('%d'), array('%d'));
            return true;
        } else {
            return false;
        }
    }
}
