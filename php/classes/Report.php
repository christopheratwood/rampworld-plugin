<?php


require_once(__DIR__.'/../thirdparty/FPDF_Diag.php');

class Report extends FPDF_Diag {





    protected $textColour = array( 0, 0, 0 );
    protected $headerColour = array( 100, 100, 100 );
    protected $tableHeaderTopTextColour = array( 255, 255, 255 );
    protected $tableHeaderTopFillColour = array(33, 161, 225);
    protected $tableHeaderTopProductTextColour = array( 0, 0, 0 );
    protected $tableHeaderTopProductFillColour = array(33, 161, 225);
    protected $tableHeaderLeftTextColour = array( 99, 42, 57 );
    protected $tableHeaderLeftFillColour = array( 184, 207, 229 );
    protected $tableBorderColour = array( 50, 50, 50 );
    protected $tableRowFillColour = array(33, 161, 225);
    protected $reportName = "2009 Widget Sales Report";
    protected $reportNameYPos = 160;
    protected $logoFile = "http://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png";
    protected $logoXPos = 80;
    protected $logoYPos = 108;
    protected $logoWidth = 50;
    protected $columnLabels = array( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" );
    protected $rowLabels = array( "SupaWidget", "WonderWidget", "MegaWidget", "HyperWidget" );
    protected $sessions = array();
    protected $chartXPos = 20;
    protected $chartYPos = 250;
    protected $chartWidth = 160;
    protected $chartHeight = 80;
    protected $chartXLabel = "Product";
    protected $chartYLabel = "2009 Sales";
    protected $chartYStep = 20000;
    protected $data;
    protected $genders;
    protected $ages;
    protected $expertise;
    protected $discipline;
    protected $terms;
    protected $colors = array();
    public $pdf;
    public $start_date;
    public $end_date;
    public $all;
    private $author;

    private $holiday;
    protected $headers = array();

    private $formatted_sessions = array();
    private $days_totals = array('Monday' => 0, 'Tuesday' => 0, 'Wednesday' => 0, 'Thursday' => 0, 'Friday' => 0, 'Saturday' => 0, 'Sunday' => 0);
    private $days_hours = array('Monday' => 0, 'Tuesday' => 0, 'Wednesday' => 0, 'Thursday' => 0, 'Friday' => 0, 'Saturday' => 0, 'Sunday' => 0);
    private $days_avg = array('Monday' => 0, 'Tuesday' => 0, 'Wednesday' => 0, 'Thursday' => 0, 'Friday' => 0, 'Saturday' => 0, 'Sunday' => 0);

    protected $member_detailss;
    protected $memberships ;
    protected $entries;
    protected $notes;
 

    var $B=0;
    var $I=0;
    var $U=0;
    var $HREF='';
    var $ALIGN='';

    function WriteHTML($html)  {
        //HTML parser
        $html = str_replace('</p>', '</p><br />', $html);
        $html = str_replace('</li>', '</li><br>', $html);
        $html = str_replace('</ul>', '</ul><br />', $html);
        $html = str_replace('&amp;', '&', $html);

        $html = str_replace('&nbsp;', ' ', $html);
        $str=array(
            '<br />' => '<br>',
            '<hr />' => '<hr>',
            '</div>' => '',
            '</ul>' => '',
            '[r]' => '<red>',
            '[/r]' => '</red>',
            '[l]' => '<blue>',
            '<p>' => '<b>',
            '</p>' => '</b>',
            '<li>' => chr(187). '  ',
            '</li>' => '',
            '&#8220;' => '"',
            '&#8221;' => '"',
            '&#8222;' => '"',
            '&#8230;' => '...',
            '&#8217;' => '\''
        );
        foreach ($str as $_from => $_to) $html = str_replace($_from,$_to,$html);
       
        $a=preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF, $e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0, 4, $e, 0, 1, 'C');
                else
                    $this->Write(3, $e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e, 1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ', $e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag, $prop);
                }
            }
        }
    }

    function OpenTag($tag, $prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag, true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x, $y, $x+$Width, $y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag, false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B', 'I', 'U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('', $style);
    }

    function PutLink($URL, $txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }
    function Header () {
        
        $this->setFillColor($this->tableHeaderTopProductFillColour[0], $this->tableHeaderTopProductFillColour[1], $this->tableHeaderTopProductFillColour[2]);
        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
        $this->Rect(0, 0, 210, 20, 'F' );
        $this->SetFont( 'Arial', 'I', 12 );
        $this->Cell( 0, 0, $this->Image( $this->logoFile, $this->GetX(), $this->GetY() - 8, 33.78), 0, 0, 'L' );
        $this->Cell( 0, 0, 'Generated on '.date('d-m-Y'), 0, 0, 'R' );
        $this->Ln(15);
        
    }
    function Footer() {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->Cell(0,10,'Page '.$this->PageNo().' of {nb}',0,0,'C');
        $this->SetFont('Arial','I',6);
        $this->Cell(0, 10, 'Created by '. wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name .' @ '. date('H:i:s d-m-Y'), 0,0,'R');
        $this->AliasNbPages();
    }
    public function createStats( $start= null, $end=null, $all =null, $reissue = null) {
        require_once 'Statistics.php';
        $stats = new Statistics();
        $stats->getAges($start, $end, $all);
        $stats->getSessions($start, $end, $all);
        $stats->getGenders($start, $end, $all);
        $stats->getExpertise($start, $end, $all);
        $stats->getDisciplines($start, $end, $all);

        $this->data = $stats->data;
        $this->ages = $stats->ages;
        $this->genders= $stats->genders;
        $this->expertise = $stats->expertise;
        $this->discipline= $stats->discipline;

        print_r($this->data);
        print_r($this->ages);
        print_r($this->genders);
        $this->renderTable('stats');
        $this->render('Statistics', 1, $_SERVER['REQUEST_URI'], $reissue);

    }

    /**
     * Creates statistics from an advance input
     *
     * @param array $asi
     * @return void
     */
    public function createStatsAdvance( $asi ) {
        require_once 'Statistics.php';
        require_once 'AdvanceSearchLog.php';
        $stats = new Statistics();
        $asl = new AdvanceSearchLog();

        $asl->get(intval($asi));

        $stats->getAgesAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']));
        $stats->getGendersAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']));
        $stats->getSessionsAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']));
        $stats->getExpertiseAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']));
        $stats->getDisciplinesAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']));

        $this->data = $stats->data;
        $this->ages = $stats->ages;
        $this->genders= $stats->genders;
        $this->expertise = $stats->expertise;
        $this->discipline= $stats->discipline;
        print_r($this->expertise);
       
        $this->renderTable('stats');
        $this->render('Statistics', 1, $_SERVER['REQUEST_URI'], $reissue);

    }

    public function create_holiday($holiday, $data,$seshData, $ages, $gender, $member_details) {

        $this->data = $data;
        $this->sessions = $seshData;
        $this->member_details = $this->member_detailss;
        $this->gender = $gender;
        $this->ages = $ages;
        $this->holiday = $holiday;
       
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        //First Page
        $this->AddPage();
        // Logo
        $this->Image( $this->logoFile, $this->logoXPos, $this->logoYPos, $this->logoWidth );
        // Report Name
        

        //
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', 'I', 20 );
        $this->Ln( $this->reportNameYPos );
        $this->Cell( 0, 15, 'RampWorld Cardiff Holiday Report', 0, 0, 'C' );
        $this->SetFont( 'Arial', 'I', 14 );
        $this->Ln();
        $this->Cell( 0, 15, $this->holiday['display_name'], 0, 0, 'C' );
         $this->SetFont( 'Arial', 'I', 12 );
        $this->Ln();
        $this->Cell( 0, 15, date("d-m-Y", strtotime($this->holiday['start_date'])) .' - '. date("d-m-Y", strtotime($this->holiday['end_date'])), 0, 0, 'C' );
    
        $this->renderTable('holiday');


    }
    public function createMember($member_id, $reissue)  {

        require_once 'Member.php';
		require_once 'Entries.php';
		require_once 'PaidMembership.php';
		require_once 'PaidSession.php';
		require_once 'Note.php';
		require_once 'Terms.php';
        require_once 'Booking.php';
        require_once 'Purchases.php';

        $member = new Member();
		$entries = new Entries();
		$bookings = new Booking();
		$purchases = new Purchases();
		$terms = new Terms();
		
		$memberships = new PaidMembership();
		$session = new PaidSession();
		$notes = new Note();

		$member->get($member_id);
		$entries->getMember($member_id, null, null, 'ASC');
		$memberships->getMember($member_id);
		$notes->getMember($member_id);
        $bookings->getMember($member_id);
		$terms->getByDate($member->members[0]['registered']);

        $this->author = wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name;

        $this->member_details = $member;
        $this->sessions = $session->sessions;
        $this->memberships = $memberships->memberships;
        $this->entries = $entries->entries;
        $this->bookings = $bookings->bookings;
        $this->notes = $notes->notes;
        $this->terms = $terms->terms;

        $this->renderTable('member');

        $this->render('member', 1, $_SERVER['REQUEST_URI'], $reissue);

    }
    public function create_consent(array $member_details)  {
        

        $this->member_details = $this->member_details_details[0];

        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        //First Page
        $this->AddPage();
        // Logo
        $this->Image( $this->logoFile, $this->logoXPos, $this->logoYPos, $this->logoWidth );

        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', 'I', 20 );
        $this->Ln( $this->reportNameYPos );

        $this->Cell( 0, 15, 'RampWorld Cardiff Member Report', 0, 0, 'C' );
        $this->SetFont( 'Arial', 'I', 14 );
        $this->Ln();
        
        $this->Cell( 0, 15, 'Consent Form Report', 0, 0, 'C' );


        $this->AddPage();

        $this->renderTable('consent');

    }


    public function createDuration($start, $end=null, $forename = null, $surname = null, $membership_id = null, $page = null, $limit = null) {
        $this->author = wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name;
		require_once 'Session.php';
		$session = new Session();

        echo $start.$end;
		$session->getTotal($start, $end, $forename, $surname, $membership_id, $page, 9999);
        $this->sessions = $session->sessions;
        $this->member_details = $session->sessionsCount;

        $this->renderTable('sessions');
        die();
        //$this->render('sessions');

    }
    public function createHoliday($start, $end) {
        $this->author = wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name;
		require_once 'Session.php';
		$session = new Session();

        echo $start.$end;
		$session->getTotalHoliday($start, $end);
        $this->sessions = $session->sessions;
        $this->member_details = $session->sessionsCount;

        $this->renderTable('sessions');
        $this->render('sessions');

    }

    public function createDurationAdvance($asi) {
        require_once 'AdvanceSearchLog.php';
        require_once 'Session.php';

        $asl = new AdvanceSearchLog();
        $session = new Session();
        

        $asl->get(intval($asi));

        $session->getTotalAdvance($asl->logs[0]['query'], $asl->logs[0]['join'], explode('|', $asl->logs[0]['bindings']), $asl->logs[0]['report_start_date'], $asl->logs[0]['report_end_date']);
        $this->sessions = $session->sessions;
        $this->member_details = $session->sessionsCount;
        echo '<pre>';print_r($this->member_details); echo '</pre>';
        $this->author = wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name;

        $this->renderTable('sessions');
        //$this->render('sessions');

    }
    public function createTerms($tandcid) {
        require_once 'Terms.php';

        $terms = new Terms();
        

        $terms->get(intval($tandcid));
        $this->terms = $terms->terms;

        $this->renderTable('terms');
        $this->render('Terms and Conditions V'.$terms->terms[0]['version_number']);

    }

    protected function renderTable($mode) { 
        $col1=array(31,161,225);
        $col2=array(255,102,102);
        $col3=array(204,255,102);
        $col4=array(88,107,164);
        $col5=array(255,193,94);
        $col6=array(28,124,84);

        switch ($mode) {
            case 'member':

                $genders = array('m' => 'Male', 'f' => 'Female', 'o' => 'Other');
                $gender = (isset($genders[$this->member_details->members[0]['gender']])) ? $genders[$this->member_details->members[0]['gender']] :'Non-Specified';


                $types = array('1' => 'Ban', '2' => 'Warning/ staff notice');
                //Participants members
                $this->AddPage();
                $this->setTableHeading();
                /* Personal inforamtion and Account detials */
                $this->Cell( 90, 10, 'Personal Details', 1, 0, 'L' );
                $this->Cell(10, 10,'',0,0,'L');
                $this->Cell( 90, 10, 'Account Details', 1, 0, 'L' );
                /* END Personal inforamtion and Account detials */
                $this->setContent();
                $this->Cell(10, 10,'',0,0,'L');
                $this->Ln();


                /* Member number */
                $this->Cell( 35, 8, 'Membership number', 1, 0, 'L' );
            
                $this->Cell( 55, 8, $this->member_details->members[0]['member_id'], 1, 0, 'L' );
                /*END  Member number */

                $this->Cell(10, 10,'',0,0,'L');
                /* Member number */
                $this->Cell( 35, 8, 'Registration date', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['registered'], 1, 0, 'L' );
                /*END  Member number */
                $this->Ln();
                
                /* Member name */
                $this->Cell( 35, 8, 'Name', 1, 0, 'L' );

                $this->Cell( 55, 8, ucwords(strtolower($this->member_details->members[0]['forename']. ' '. $this->member_details->members[0]['surname'])), 1, 0, 'L' );
                /*END  Member number */

                $this->Cell(10, 8,'',0,0,'L');
                /* Member number */
                $this->Cell( 35, 8, 'Terms and Conditions V.', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->terms[0]['version_number'], 1, 0, 'L' );
                /*END  Member number */
                $this->Ln();

                 /* Gender */
                $this->Cell( 35, 8, 'Gender', 1, 0, 'L' );

                $this->Cell( 55, 8, $gender, 1, 0, 'L' );
                /*END  Member number */

                $this->Cell(10, 8,'',0,0,'L');
                /* Member number */
                $this->Cell( 35, 8, 'Number of entries', 1, 0, 'L' );

                $this->Cell( 55, 8, count($this->entries), 1, 0, 'L' );
                /*END  Member number */
                $this->Ln();

                /* Contact number */
                $this->Cell( 35, 8, 'Phone number', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['number'], 1, 0, 'L' );
                /*END  Member number */

                $this->Cell(10, 8,'',0,0,'L');
                /* Member number */
                $this->Cell( 35, 8, 'Number of bookings', 1, 0, 'L' );

                $this->Cell( 55, 8, count($this->bookings), 1, 0, 'L' );
                /*END  Member number */
                $this->Ln();

                /* Dob */
                $this->Cell( 35, 8, 'Phone number', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['dob'], 1, 0, 'L' );
                /*END  Member number */

                $this->Cell(10, 8,'',0,0,'L');
                /* Member number */
                $this->Cell( 35, 8, 'Last updated', 1, 0, 'L' );


                $this->Cell( 55, 8, (($this->member_details->members[0]['last_updated_time'] == '')?'-': 'By '.$this->member_details->members[0]['last_updated_user'].' on '. $this->member_details->members[0]['last_updated_time']), 1, 0, 'L' );
                /*END  Member number */
                $this->Ln(15);

                //Address and consent 
                
                $this->setTableHeading();
                /* Personal inforamtion and Account detials */
                $this->Cell( 90, 10, 'Address Details', 1, 0, 'L' );
                $this->Cell(10, 10,'',0,0,'L');
                $this->Cell( 90, 10, 'Consent Details (If applicable)', 1, 0, 'L' );
                /* END Personal inforamtion and Account detials */
                $this->setContent();
                $this->Ln();

                /* line one */
                $this->Cell( 35, 8, 'Line one', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['address_line_one'], 1, 0, 'L' );
                /*END  Line one */

                $this->Cell(10, 8,'',0,0,'L');
                /* Member number */
                 if(isset($this->member_details->consent[0])) {
                    $this->Cell( 35, 8, 'Consent name', 1, 0, 'L' );
                
                    $this->Cell( 55, 8, strtolower($this->member_details->consent[0]['name']) , 1, 0, 'L' );
                } else {
                    $this->Cell( 35, 8, '', 0, 0, 'L' );
                
                    $this->Cell( 55, 8, '' , 0, 0, 'L' );
                }

                /*END  Member number */
                $this->Ln();
                
                /* line two */
                $this->Cell( 35, 8, 'Line two', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['address_line_two'], 1, 0, 'L' );
                /*END  line twor */

                $this->Cell(10, 8,'',0,0,'L');
                if(isset($this->member_details->consent[0])) {
                    $this->Cell( 35, 8, 'Consent number', 1, 0, 'L' );
                
                    $this->Cell( 55, 8, $this->member_details->consent[0]['number'] , 1, 0, 'L' );
                } else {
                    $this->Cell( 35, 8, '', 0, 0, 'L' );
                
                    $this->Cell( 55, 8, '' , 0, 0, 'L' );
                }
                /* consent  number */
               
                /*END  consent number */
                $this->Ln();

                /* city */
                $this->Cell( 35, 8, 'City', 1, 0, 'L' );
               
                $this->Cell( 55, 8, $this->member_details->members[0]['address_city'], 1, 0, 'L' );
                /*END  city */

                $this->Cell(10, 8,'',0,0,'L');
                /* consent  email */
                if(isset($this->member_details->consent[0])) {
                    $this->Cell( 35, 8, 'Consent email', 1, 0, 'L' );
                
                    $this->Cell( 55, 8, strtolower($this->member_details->consent[0]['email']) , 1, 0, 'L' );
                } else {
                    $this->Cell( 35, 8, '', 0, 0, 'L' );
                
                    $this->Cell( 55, 8, '' , 0, 0, 'L' );
                }
                /*END  consent email */
                $this->Ln();

                /* county */
                $this->Cell( 35, 8, 'County', 1, 0, 'L' );

                $this->Cell( 55, 8, $this->member_details->members[0]['address_county'], 1, 0, 'L' );
                /*END  line county */

                $this->Cell(10, 8,'',0,0,'L');
                /* consent  line */
                if(isset($this->member_details->consent[0])) {
                    $this->Cell( 35, 8, 'Consent address', 1, 0, 'L' );
                
                    $this->Cell( 55, 8, $this->member_details->consent[0]['address_line_one'] , 1, 0, 'L' );
                } else {
                    $this->Cell( 35, 8, '', 0, 0, 'L' );
                
                    $this->Cell( 55, 8, '' , 0, 0, 'L' );
                }

                /*END  consent line */
                $this->Ln();

                /* Postcode */
                $this->Cell( 35, 8, 'Postcode', 1, 0, 'L' );

                $this->Cell( 55, 8, strtoupper($this->member_details->members[0]['address_postcode']), 1, 0, 'L' );
                $this->Cell(10, 8,'',0,0,'L');
                if(isset($this->member_details->consent[0])) {
                    $this->Cell( 35, 8, 'Consent postcode', 1, 0, 'L' );
                
                    $this->Cell( 55, 8, strtoupper($this->member_details->consent[0]['address_postcode']) , 1, 0, 'L' );
                } else {
                    $this->Cell( 35, 8, '', 0, 0, 'L' );
                
                    $this->Cell( 55, 8, '' , 0, 0, 'L' );
                }

                /*END  consent line */
                

                /* Start system notes */
                $this->Ln(15);
                $this->setTableHeading();
                $this->Cell( 190, 10, 'System notes', 1, 0, 'L' );
                $this->setContent();
                $this->Ln();
                foreach($this->notes as $note) {
                    $this->Cell( 35, 8, 'Start Date', 1, 0, 'L' );
                    $this->Cell( 55, 8, $note['start_date'], 1, 0, 'L' );
                    $this->Cell( 100, 24, $note['comments'], 1, 0, 'L' );
                    $this->Ln(8);
                    $this->Cell( 35, 8, 'End Date', 1, 0, 'L' );
                    $this->Cell( 55, 8, $note['end_date'], 1, 0, 'L' );
                    $this->Ln(8);
                    $this->Cell( 35, 8, 'Type', 1, 0, 'L' );
                    $this->Cell( 55, 8, $types[$note['type']], 1, 0, 'L' );
                    $this->Ln(16);

                }
                /* End system notes */

                /* Start sessions */
                $cutOff = 39;
                $y = 0;
                $x = 0;
                $this->AddPage();

                $currentExitTime = null;  

                $this->setTableHeading();
                $this->Cell( 30, 10, 'Ent #', 1, 0, 'C' );
                $this->Cell( 40, 10, 'Entry date', 1, 0, 'C' );
                $this->Cell( 40, 10, 'Entry Time', 1, 0, 'C' );
                $this->Cell( 40, 10, 'Exit Time', 1, 0, 'C' );
                $this->Cell( 40, 10, 'Premature', 1, 0, 'C' );
                $this->Ln();
                foreach ($this->entries as $entry) {


                    
                    

                    

                    if(($x % 39) == 0)
                        $y++;
                    

                
                        
                    if($y > 1 && $x % $cutOff == 0){

                        

                        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                        $this->SetFont( 'Arial', 'B', 6 );

                        $this->Cell( 150, 6, 'Total: ', 1, 0, 'R' );
                        $this->Cell( 40, 6, 'Cont\'d', 1, 0, 'C' );
                        $this->AddPage();
                        
                        $this->setTableHeading();  

                        $this->Cell( 30, 10, 'Ent #', 1, 0, 'C' );
                        $this->Cell( 40, 10, 'Entry date', 1, 0, 'C' );
                        $this->Cell( 40, 10, 'Entry Time', 1, 0, 'C' );
                        $this->Cell( 40, 10, 'Exit Time', 1, 0, 'C' );
                        $this->Cell( 40, 10, 'Premature', 1, 0, 'C' );
    
                        $this->Ln();
                    
                    }

                    
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', '', 6 );

                    $this->Cell( 30, 6, $entry['entry_id'], 1, 0, 'C' );
                    $this->Cell( 40, 6, $entry['entry_date'], 1, 0, 'C' );
                    $this->Cell( 40, 6, $entry['entry_time'], 1, 0, 'C' );
                    $this->Cell( 40, 6, $entry['exit_time'], 1, 0, 'C' );
                    $this->Cell( 40, 6, $entry['premature_exit'], 1, 0, 'C' );
                    $this->Ln(6); 
                    $x++;
                    
                }
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'B', 6 );

                $this->Cell( 150, 6, 'Total: ', 1, 0, 'R' );
                $this->Cell( 40, 6, count($this->entries), 1, 0, 'C' );

                break;
            case 'stats' :

                $this->AddPage();

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                //Total members

                $this->Write(10, 'Total Amount of Customers.');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );

                $this->Write(5, 'Below is a breakdown of the amount of customers that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
            
                 $this->Ln( 10 );
                // Remaining header cells
                $this->setTableHeading();

                $this->Cell( 27, 10, 'Monday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Tuesday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Wednesday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Thursday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Friday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Saturday', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Sunday', 1, 0, 'C' );
                $this->Ln();
                $this->setContent();
                
                $this->Cell( 27, 10, $this->data[0]['Monday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Tuesday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Wednesday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Thursday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Friday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Saturday'] , 1, 0, 'C' );
                $this->Cell( 27, 10, $this->data[0]['Sunday'] , 1, 0, 'C' );
                $this->Ln();
                //percentages
                $this->Cell( 27, 10, intval(($this->data[0]['Monday'] / $this->data[0]['total'] )* 100)  ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Tuesday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Wednesday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Thursday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Friday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Saturday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->Cell( 27, 10, intval(($this->data[0]['Sunday'] / $this->data[0]['total'] )* 100) ."%", 1, 0, 'C' );
                $this->SetFont( 'Arial', 'B', 8 );
                $this->Ln();
                $this->Cell( 162, 10, 'Total: ', 0, 0, 'R', 0);
                $this->Cell( 27, 10, $this->data[0]['total'], 1, 0, 'C', 0 );

                $this->Ln( 15 );

                // Create the table data rows
                
                //Gender members
                $this->setHeading();
                $this->Write(10, 'Gender');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );


                $this->Write(5, 'Below is a range of customer\'s gender that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
               
               
                $this->Ln( 10 );
                // Remaining header cells
                $this->setTableHeading();
                
                $this->Cell( 27, 10, 'Male', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Female', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Other', 1, 0, 'C' );

                $this->Ln( 10 );
                $this->setContent();                
    
                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Male']) ), 1, 0, 'C' );
                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Female'] ) ), 1, 0, 'C' );
                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Other'] ) ), 1, 0, 'C' );

                $this->Ln(10);

                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Male'] / $this->genders[0]['total'] * 100, 1 ) ).'%', 1, 0, 'C');
                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Female'] / $this->genders[0]['total']  * 100, 1 ) ).'%', 1, 0, 'C');
                $this->Cell( 27, 10, ( number_format( $this->genders[0]['Other'] / $this->genders[0]['total']  * 100, 1 ) ).'%', 1, 0, 'C');

                $this->setXY(100, 118);
                $gender_array = array('Male' => $this->genders[0]['Male'], 'Female' => $this->genders[0]['Female'], 'Other' => $this->genders[0]['Other']);
                
                
                $col7=array(145,166,255);
                $this->PieChart(100, 35,$gender_array , '%l (%p)', array($col1,$col2,$col3));
                $this->Ln(15);

                //Total members
                $this->setHeading();
                $this->Write(10, 'Age Range');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );


                $this->Write(5, 'Below is a breakdown of the age range of customers that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
               
                 $this->Ln( 10 );
                // Remaining header cells
                $this->setTableHeading();
                $this->Cell( 27, 10, 'Age range', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Total', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Percentage', 1, 0, 'C' );
                $this->Ln();

                $this->setContent();
                $this->Cell( 27, 10, '< 7', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['Under 7'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['Under 7'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, '7 - 10', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['7 - 10'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['7 - 10'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, '11 - 13', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['11 - 13'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['11 - 13'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();
                
                $this->Cell( 27, 10, '14 - 17', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['14 - 17'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['14 - 17'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, '18 - 21', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['18 - 21'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['18 - 21'] / $this->ages[0]['total'] ) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, '22 - 30', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['22 - 30'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['22 - 30'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, '31+', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->ages[0]['31+'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->ages[0]['31+'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->setXY(100, 200);
                $age_array = array('Under 7' => $this->ages[0]['Under 7'], "7 - 10" => $this->ages[0]['7 - 10'], "11 - 13" => $this->ages[0]['11 - 13'],  "14 - 17" => $this->ages[0]['14 - 17'],  "18 - 21" => $this->ages[0]['18 - 21'],  "22 - 30" => $this->ages[0]['22 - 30'],  "31 and over" => $this->ages[0]['31+']);
                arsort($age_array);
              
                $this->PieChart(100, 35, $age_array , '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7));


                $this->AddPage();

                //Total members
                $this->setHeading();
                $this->Write(10, 'Expertise');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );


                $this->Write(5, 'Below is a breakdown of the expertise range of the customers that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
               
                $this->Ln( 10 );
                // Remaining header cells
                $this->setTableHeading();
                $this->Cell( 27, 10, 'Expertise', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Total', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Percentage', 1, 0, 'C' );
                $this->Ln();

                $this->setContent();
                $this->Cell( 27, 10, 'Beginner', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Beginner'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Beginner'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Novice', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Novice'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Novice'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Experienced', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Experienced'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Experienced'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();
                
                $this->Cell( 27, 10, 'Advanced', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Advanced'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Advanced'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Expert', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Expert'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Expert'] / $this->ages[0]['total'] ) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Professional', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->expertise[0]['Professional'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->expertise[0]['Professional'] / $this->ages[0]['total']) * 100).'%', 1, 0, 'C' );
        
                $this->setXY(100, 67);

                arsort($this->expertise[0]);
              
                $this->PieChart(100, 35, $this->expertise[0] , '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6));
                
                //Total members
                $this->setHeading();
                $this->Ln(50);
                $this->Write(10, 'Disciplines');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );


                $this->Write(5, 'Below is a breakdown of the Disciplines.', 'FJ');
               
                $this->Ln( 10 );
                $discipline_total = $this->discipline[0]['Scooter'] + $this->discipline[0]['BMX'] + $this->discipline[0]['MTB'] +  $this->discipline[0]['Skateboard'] + $this->discipline[0]['Inline'] + $this->discipline[0]['Spectator']+ $this->discipline[0]['Other'];
                // Remaining header cells
                $this->setTableHeading();
                $this->Cell( 27, 10, 'Discipline', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Total', 1, 0, 'C' );
                $this->Cell( 27, 10, 'Percentage', 1, 0, 'C' );
                $this->Ln();

                $this->setContent();
                $this->Cell( 27, 10, 'Scooter', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['Scooter'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['Scooter'] / $discipline_total) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'BMX', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['BMX'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['BMX'] / $discipline_total) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'MTB', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['MTB'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['MTB'] / $discipline_total) * 100).'%', 1, 0, 'C' );
                $this->Ln();
                
                $this->Cell( 27, 10, 'Skateboard', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['Skateboard'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['Skateboard'] / $discipline_total) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Inline', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['Inline'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['Inline'] / $discipline_total ) * 100).'%', 1, 0, 'C' );
                $this->Ln();

                $this->Cell( 27, 10, 'Spectator', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['Spectator'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['Spectator'] / $discipline_total) * 100).'%', 1, 0, 'C' );
                $this->Ln();
                
                $this->Cell( 27, 10, 'Other', 1, 0, 'C' );
                $this->Cell( 27, 10, $this->discipline[0]['Other'], 1, 0, 'C' );
                $this->Cell( 27, 10, number_format(($this->discipline[0]['Other'] / $discipline_total) * 100).'%', 1, 0, 'C' );
        
                $this->setXY(100, 177);

                arsort($this->discipline[0]);
              
                $this->PieChart(100, 35, $this->discipline[0] , '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7));
                
                
                
                
                // Create the table data rows


                
                break;
            case 'sessions':
                //Session break down

                $this->AddPage();
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Write(10, 'Session Breakdown.');
                $this->Ln( );
                $this->SetFont( 'Arial', 'I', 9 );
 
                $this->Write(5, 'Below is a breakdown each amount of customer of each session using the digital registration form and entry system.', 'FJ');


                $this->setTableHeading();
                
                               
                /**
                 * render every day stats with chart
                 */
                for ( $i=1; $i < count($this->sessions) + 1; $i++ ) {

                   
                    
                    if(!empty($this->sessions[$i]['sessions'])) {
                        $this->SetFont( 'Arial', 'I', 11);
                        $page_height = 279.4; // mm (portrait letter)
                        $bottom_margin = 10; // mm
                      
                        // mm until end of page (less bottom margin of 20mm)
                        $space_left = $page_height - $this->GetY(); // space left on page
                        $space_left -= $bottom_margin; // less the bottom margin

                        if(21 + (8 * count($this->sessions[$i]['sessions']) )> $space_left )
                            $this->AddPage();
                        else
                            $this->Ln(10);    
                        $this->Write(5, $this->sessions[$i]['day'], 'FJ');
                        $this->Ln( 10 );
                        $startY = $this->GetY();
                        $currentY = 0;

                        $this->setTableHeading();
                        $this->Cell( 27, 10, 'Session time', 1, 0, 'C' );
                        $this->Cell( 27, 10, 'Total', 1, 0, 'C' );
                        $this->Cell( 27, 10, 'Percentage', 1, 0, 'C' );

        
                        $this->setContent(); 
                        $total =0;
                        foreach($this->sessions[$i]['sessions'] as $session => $v)
                            $total += $v;
                        $this->Ln();


                        foreach ($this->sessions[$i]['sessions'] as $key => $value) {
                            if(!empty($value)){
                                $this->Cell( 27, 8, $key, 1, 0, 'C' );
                                $this->Cell( 27, 8, $value, 1, 0, 'C' );
                                $this->Cell( 27, 8, number_format(($value / $total ) *100). '%', 1, 0, 'C' );
                                $this->Ln();
                            }
                            $currentY = $this->GetY();
                        }
                        $this->setXY(100, number_format($startY + 8));                        
                        
                        $col7=array(145,166,255);
                        arsort($this->sessions[$i]['sessions']);
                        $this->PieChart(100, 35,$this->sessions[$i]['sessions']  , '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7));
                        
                        $this->setY($currentY);

                    }

                }

                
                //memebrs entry
                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Members Entries');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                
                $this->Write(5, 'Below is a list of all members that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
                
                $this->Ln( 10 );
                // Remaining header cells
                
                $cutOff = 38;      
                foreach ($this->member_details as $key=> $value) {
                    $this->AddPage();
                    $y = 0;
                    $x = 0;
                    $currentExitTime = null;  
                    

                    $this->SetFont( 'Arial', 'I', 10 );
                    $this->Write(5, $value['date']. ' - Session '.$value['members'][0]['exit_time'], 'FJ');
                    $this->Ln();

                    $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                    $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                    $this->SetFont( 'Arial', 'I', 8 );

                    $this->Cell( 15, 8, 'Ent #', 1, 0, 'C', true );
                    $this->Cell( 15, 8, 'Mem #', 1, 0, 'C', true );
                    $this->Cell( 27, 8, 'Forename', 1, 0, 'C', true );
                    $this->Cell( 27, 8, 'Surname', 1, 0, 'C', true );
                    $this->Cell( 27, 8, 'Number', 1, 0, 'C', true );
                    $this->Cell( 27, 8, 'Entry date', 1, 0, 'C', true );
                    $this->Cell( 24, 8, 'Entry Time', 1, 0, 'C', true );
                    $this->Cell( 15, 8, 'Exit Time', 1, 0, 'C', true );
                    $this->Cell( 15, 8, 'Premature', 1, 0, 'C', true );
                    $this->Ln();

                    
                    
                    $i = 0;
                    foreach ($value['members'] as $this->member_details => $memvalue) {
                        $i++;
                        if(($x % $cutOff) == 0)
                            $y++;
                        if($currentExitTime != null && $memvalue['exit_time'] != $currentExitTime) {

                            $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                            $this->SetFont( 'Arial', 'B', 6 );

                            $this->Cell( 177, 6, 'Total: ', 1, 0, 'R' );
                            $this->Cell( 15, 6, $i, 1, 0, 'C' );
                            $this->AddPage();
                            $this->SetFont( 'Arial', 'I', 10 );
                            $this->Write(5, $value['date']. ' - Session '.$memvalue['exit_time'], 'FJ');
                            $this->Ln();
                           
                            $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                            $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                            $this->SetFont( 'Arial', 'I', 8 );       

                            $this->Cell( 15, 8, 'Ent #', 1, 0, 'C', true );
                            $this->Cell( 15, 8, 'Mem #', 1, 0, 'C', true );
                            $this->Cell( 27, 8, 'Forename', 1, 0, 'C', true );
                            $this->Cell( 27, 8, 'Surname', 1, 0, 'C', true );
                            $this->Cell( 27, 8, 'Number', 1, 0, 'C', true );
                            $this->Cell( 27, 8, 'Entry date', 1, 0, 'C', true );
                            $this->Cell( 24, 8, 'Entry Time', 1, 0, 'C', true );
                            $this->Cell( 15, 8, 'Exit Time', 1, 0, 'C', true );
                            $this->Cell( 15, 8, 'Premature', 1, 0, 'C', true );
                            $this->Ln();
                            $x = 0;
                            $i = 0;
                        } else {
                            
                            if($y > 1 && $x % $cutOff === 0){
                                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                                $this->SetFont( 'Arial', 'B', 6 );

                                $this->Cell( 177, 6, 'Total: ', 1, 0, 'R' );
                                $this->Cell( 15, 6, 'Cont\'d', 1, 0, 'C' );
                                $this->AddPage();

                                $this->SetFont( 'Arial', 'I', 10 );
                                $this->Write(5, $value['date']. ' - Session '.$memvalue['exit_time'], 'FJ');
                                $this->Ln();
                                
                                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                                $this->SetFont( 'Arial', 'I', 8 );       

                                $this->Cell( 15, 8, 'Ent #', 1, 0, 'C', true );
                                $this->Cell( 15, 8, 'Mem #', 1, 0, 'C', true );
                                $this->Cell( 27, 8, 'Forename', 1, 0, 'C', true );
                                $this->Cell( 27, 8, 'Surname', 1, 0, 'C', true );
                                $this->Cell( 27, 8, 'Number', 1, 0, 'C', true );
                                $this->Cell( 27, 8, 'Entry date', 1, 0, 'C', true );
                                $this->Cell( 24, 8, 'Entry Time', 1, 0, 'C', true );
                                $this->Cell( 15, 8, 'Exit Time', 1, 0, 'C', true );
                                $this->Cell( 15, 8, 'Premature', 1, 0, 'C', true );
                                $this->Ln();
                            
                            }
                        }
                        $currentExitTime = $memvalue['exit_time'];
                        $x++;
                        
                        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                        $this->SetFont( 'Arial', '', 6 );

                        $this->Cell( 15, 6, $memvalue['entry_id'], 1, 0, 'C' );
                        $this->Cell( 15, 6, $memvalue['member_id'], 1, 0, 'C' );
                        $this->Cell( 27, 6, $memvalue['forename'], 1, 0, 'C' );
                        $this->Cell( 27, 6, $memvalue['surname'], 1, 0, 'C' );
                        $this->Cell( 27, 6, $memvalue['number'], 1, 0, 'C' );
                        $this->Cell( 27, 6, $memvalue['entry_date'], 1, 0, 'C' );
                        $this->Cell( 24, 6, $memvalue['entry_time'], 1, 0, 'C' );
                        $this->Cell( 15, 6, $memvalue['exit_time'], 1, 0, 'C' );
                        $this->Cell( 15, 6, $memvalue['premature'], 1, 0, 'C' );
                        $this->Ln(6); 
                        
                    }
                    $x = 1;
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', '', 6 );

                    $this->Cell( 177, 8, 'Total: ', 1, 0, 'R' );
                    $this->Cell( 15, 8, $i, 1, 0, 'C' );

                    
                }

                

                
                break;
            case 'consent':
                $genders = array('m' => 'Male', 'f' => 'Female', 'o' => 'Other');
                $gender = (isset($genders[$this->member_details->members[0]['gender']])) ? $genders[$this->member_details->members[0]['gender']] :'Non-Specified';
               
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Participants members

                $this->Write(10, 'Participant Details');
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Member ID', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, $this->member_details->members[0]['member_id'], 1, 0, 'L' );
                $this->Ln();


                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                $this->Cell( 50, 10, 'Member Forename', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                $this->Cell( 140, 10, $this->member_details->members[0]['forename'], 1, 0, 'L');
                $this->Ln();


                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
              
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Member Surname', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, $this->member_details->members[0]['surname'], 1, 0, 'L' );
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                $this->Cell( 50, 10, 'Member Gender', 1, 0, 'L', true);

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
               
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, $gender, 1, 0, 'L' );
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Member Number', 1, 0, 'L', true);

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                $this->Cell( 140, 10, $this->member_details->members[0]['number'], 1, 0, 'L' );
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Member Email', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, $this->member_details->members[0]['email'], 1, 0, 'L' );
               


                //Participants Address
                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                

                $this->Write(10, 'Participant Address');
                $this->Ln();

                //Participants members
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Address Line One', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, html_entity_decode(ucwords($this->member_details->members[0]['address_line_one']), ENT_QUOTES, 'UTF-8'), 1, 0, 'L' );
                $this->Ln();


                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                $this->Cell( 50, 10, 'Address Line Two', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, html_entity_decode(ucwords($this->member_details->members[0]['address_line_two']), ENT_QUOTES, 'UTF-8'), 1, 0, 'L');
                $this->Ln();


                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
              
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'City', 1, 0, 'L', true );

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, html_entity_decode(ucwords($this->member_details->members[0]['address_city']), ENT_QUOTES, 'UTF-8') , 1, 0, 'L' );
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                $this->Cell( 50, 10, 'County', 1, 0, 'L', true);

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
               
                $this->SetFont( 'Arial', '', 10 );
                $this->Cell( 140, 10, html_entity_decode(ucwords($this->member_details->members[0]['address_county']), ENT_QUOTES, 'UTF-8'), 1, 0, 'L' );
                $this->Ln();

                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                $this->Cell( 50, 10, 'Postcode', 1, 0, 'L', true);

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                $this->Cell( 140, 10, html_entity_decode(ucwords($this->member_details->members[0]['address_postcode']), ENT_QUOTES, 'UTF-8'), 1, 0, 'L' );
                

                $this->Ln(15);


                //emergency Details
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                

                $this->Write(10, 'Participant Emergency Details');
                $this->Ln();
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );
                for($i = 0; $i < count($this->member_details->members[0]['emergency_details']); $i++) {
                    $this->Cell( 45, 10, 'Name', 1, 0, 'C', true);
                    $this->Cell( 45, 10, 'Number', 1, 0, 'C', true);
                    if($i < count($this->member_details->members[0]['emergency_details']) - 1)
                        $this->Cell( 5, 10, '', 0, 0, 'C');
                }
                $this->Ln();


                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );
                for($i = 0; $i < count($this->member_details->members[0]['emergency_details']); $i++) {
                    $this->Cell( 45, 10, $this->member_details->members[0]['emergency_details'][$i]['name'], 1, 0, 'L');
                    $this->Cell( 45, 10, $this->member_details->members[0]['emergency_details'][$i]['emergency_number'], 1, 0, 'L');
                    if($i < count($this->member_details->members[0]['emergency_details']) - 1)
                        $this->Cell( 5, 10, '', 0, 0, 'L');
                    
                }
                $this->Ln(15);
                //Medical and staff notes Details
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Participant Medical Notes');
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );
                $this->Ln();
                $this->MultiCell( 190, 10, $this->member_details->members[0]['medical_notes'], 0, 'J', false);
                

                if(count($this->member_details->members[0]['staff_history']) > 0) {
                    $this->Ln(15);
                    //Medical and staff notes Details
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'I', 16 );
                    $this->Write(10, 'Staff Notes');
                    $this->Ln();
                    $text = '';


                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', '', 10 );
                    for ($i=0; $i < count($this->member_details->members[0]['staff_history']); $i++) { 
                        if($this->member_details->members[0]['staff_history'][$i]['type'] == 1)
                            $type = 'Ban';
                        else
                            $type = 'Custom Note';
                    
                        $text .= $this->member_details->members[0]['staff_history'][$i]['start_date']. ' - ' . $this->member_details->members[0]['staff_history'][$i]['end_date'] . ' - '. $type.' - ' .$this->member_details->members[0]['staff_history'][$i]['comments'] ."\n";
                    }
                    $this->MultiCell( 190, 10, $text, 0, 'J', false);

                   

                }
                $this->Ln(15);
                //Medical and staff notes Details
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Terms and Conditions At the time of registration');
               
                $this->SetFont( 'Arial', 'B', 12 );
                $this->Ln();
                $this->Write( 10, 'Version Number '. $this->member_details->members[0]['signup_terms'][0]['version_number']);
                $this->Ln();
                $this->Write( 10, 'Content');
                $this->Ln();
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'B', 7 );
                $this->WriteHTML($this->member_details->members[0]['signup_terms'][0]['content']);


                if(count($this->member_details->members[0]['current_terms']) > 0) {
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'I', 16 );
                    $this->Write(10, 'Current Terms and Conditions');
                   
                    $this->SetFont( 'Arial', 'B', 12 );
                    $this->Ln();
                    $this->Write( 10, 'Version Number '. $this->member_details->members[0]['current_terms'][0]['version_number']);
                    $this->Ln();
                    $this->Write( 10, 'Content');
                    $this->Ln();
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'B', 7 );
                    $this->WriteHTML($this->member_details->members[0]['current_terms'][0]['content']);   
                }




                if(count($this->member_details->members[0]['terms']) > 0) {
                    $this->Ln(15);
                    //Medical and staff notes Details
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'I', 16 );
                    $this->Write(10, 'Terms and Conditions Modifications ('.count($this->member_details->members[0]['terms']).' Alterations)');
                    $this->Ln();
                    $text = '';

                    $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                    $this->SetFont( 'Arial', 'I', 10 );
              
                    $this->Cell( 30, 10, 'Version Number', 1, 0, 'C', true);
                    $this->Cell( 35, 10, 'Publish Date', 1, 0, 'C', true);
                    $this->Cell( 15, 10, 'Notified', 1, 0, 'C', true);
                    $this->Cell( 55, 10, 'Notified Date', 1, 0, 'C', true);
                    $this->Cell( 55, 10, 'Email Address', 1, 0, 'C', true);
                    $this->Ln();
                

                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', '', 9 );
                    for ($i=0; $i < count($this->member_details->members[0]['terms']); $i++) { 

                        $this->Cell(30, 10, $this->member_details->members[0]['terms'][$i]['version_number'],1, 0, 'C');
                        $this->Cell(35, 10, $this->member_details->members[0]['terms'][$i]['publish_date'],1, 0, 'C');
                        $this->Cell(15, 10, (($this->member_details->members[0]['terms'][$i]['has_emailed_suceed'] == '1') ? "Yes": "No"),1, 0, 'C');
                        $this->Cell(55, 10, ($this->member_details->members[0]['terms'][$i]['has_emailed_suceed'] == '0') ? "Attempted @ {$this->member_details->members[0]['terms'][$i]['date']}": $this->member_details->members[0]['terms'][$i]['date'],1, 0, 'C');
                        $this->Cell(55, 10, $this->member_details->members[0]['terms'][$i]['email'],1, 0, 'C');
                        $this->Ln();
                    }
                    

                   

                }

                break;
            case 'holiday':

                $this->AddPage();
                $this->SetDrawColor( $this->tableBorderColour[0], $this->tableBorderColour[1], $this->tableBorderColour[2] );
        
                $this->SetFont( 'Arial', 'I', 10 );
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Total Amount of Customers.');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
   
                $this->Write(5, 'Below is a breakdown of the amount of customers that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
            
                $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                for ( $i=0; $i < count($this->columnLabels) ; $i++ ) {
                  $this->Cell( 27, 10, $this->columnLabels[$i], 1, 0, 'C', true );
                }

                $this->Ln( 10 );

                // Create the table data rows
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                for($i = 1; $i <= count($this->data) ; $i++) {

                    $this->Cell( 27, 10, ( $this->data[$i]  ), 1, 0, 'C' );
                }

                $this->Ln();
                $this->SetX(145);
                $this->SetFont( 'Arial', 'I', 12 );
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );

                $this->Cell( 27, 10, 'Total: ', 1, 0, 'C', true, 'R' );
                 $this->SetFont( 'Arial', 'I', 10 );
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->Cell( 27, 10, array_sum($this->data), 1, 0, 'C', false, 'R' );

                //Gender members
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Gender');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );

                $this->Write(5, 'Below is a range of customer\'s gender that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');

               
                $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                
                $this->Cell( 30, 10, 'Male', 1, 0, 'C', true );
                $this->Cell( 30, 10, 'Female', 1, 0, 'C', true );
                $this->Cell( 30, 10, 'Other', 1, 0, 'C', true );


                $this->Ln( 10 );

                // Create the table data rows

                $this->fill = false;
                $this->row = 0;
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFillColor( $this->tableRowFillColour[0], $this->tableRowFillColour[1], $this->tableRowFillColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Male']) ), 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Female'] ) ), 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Other'] ) ), 1, 0, 'C', $this->fill );

                $this->Ln(10);
                $count = $this->gender[0]['Male'] + $this->gender[0]['Female'] + $this->gender[0]['Other'];

                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Male'] / $count * 100, 1 ) ).'%', 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Female'] / $count * 100, 1 ) ).'%', 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Other'] / $count * 100, 1 ) ).'%', 1, 0, 'C', $this->fill );
                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Age Range');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );

                if($this->all)
                    $this->Write(5, 'Below is a breakdown of the age range of customers that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
                else
                    $this->Write(5, 'Below is a breakdown of the age range of customers that attended RampWorld Cardiff between '.$this->start_date.' and '. $this->end_date.' using the digital registration form and entry system.', 'FJ');

                 $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                foreach ($this->ages as $key => $value) {
                   $this->Cell( 27, 10, $key, 1, 0, 'C', true );
                }

                $this->Ln( 10 );

                // Create the table data rows

                $this->fill = false;
                $this->row = 0;
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFillColor( $this->tableRowFillColour[0], $this->tableRowFillColour[1], $this->tableRowFillColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                foreach ($this->ages as $key => $value) {
                   $this->Cell( 27, 10, $value, 1, 0, 'C' );
                }

                $this->AddPage();
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Session Breakdown.');
                $this->Ln( );
                $this->SetFont( 'Arial', 'I', 10 );
                
                $this->Write(5, 'Below is a breakdown each amount of customer of each session using the digital registration form and entry system.', 'FJ');
                
                $this->Ln( 10 );
                

                for ( $i=1; $i < count($this->sessions) + 1; $i++ ) {

                    $this->SetFont( 'Arial', 'I', 12 );
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );

                    $this->Write(5, $this->sessions[$i]['day'], 'L');
                    $this->Ln(8);

                    foreach ($this->sessions[$i]['sessions'] as $key => $value) {
                       
                        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                        $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                        $this->SetFont( 'Arial', 'I', 10 );
                        $this->Cell( (180 / count($this->sessions[$i]['sessions'])), 7, $key, 1, 0, 'C', true );
                    }
                    $this->Ln(7);
                    foreach ($this->sessions[$i]['sessions'] as $key => $value) {
                       
                        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                        
                        $this->SetFont( 'Arial', '', 10 );

                        $this->Cell( (180 / count($this->sessions[$i]['sessions'])), 7, $value, 1, 0, 'C' );
                    }
                    $this->Ln(10);
                   
                 
                }
                
                //memebrs entry
                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Members Entries');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Write(5, 'Below is a list of all members that attended RampWorld Cardiff using the digital registration form and entry system.', 'FJ');
                
                
                $this->Ln( 10 );
                // Remaining header cells
                
                
                foreach ($this->member_details as $key=> $value) {
                    $this->AddPage();
                    $y = 0;
                    $x = 0;
                    

                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'I', 14 );

                    $this->Write(5, $value['date'], 'FJ');
                    $this->Ln(8);

                    $this->Write(5, 'Number of Entries: '.$value['total'], 'FJ');
                    $this->Ln(10);

                    $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                    $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                    $this->SetFont( 'Arial', 'I', 12 );

                    $this->Cell( 27, 10, 'Member ID', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Forename', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Surname', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Number', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Entry date', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Entry Time', 1, 0, 'C', true );
                    $this->Cell( 27, 10, 'Exit Time', 1, 0, 'C', true );
                    $this->Ln();

                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', '', 10 );
                    

                    foreach ($value['members'] as $this->member_details => $memvalue) {
                        if(($x % 23) == 0)
                            $y++;
                        $x++;
                        if(($y == 1 && $x == 23) || ($y > 1 && $x == 24) ) {
                            $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                            $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                            $this->SetFont( 'Arial', 'I', 12 );

                            $this->Cell( 27, 10, 'Member ID', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Forename', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Surname', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Number', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Entry date', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Entry Time', 1, 0, 'C', true );
                            $this->Cell( 27, 10, 'Exit Time', 1, 0, 'C', true );
                            $this->Ln();
                            $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                            $this->SetFont( 'Arial', '', 10 );
                            $x = 0;
                        }
                        
                        //echo $y. '<br>';
                        foreach ($memvalue as $fieldname => $fieldvalue)
                           $this->Cell( 27, 10, $fieldvalue, 1, 0, 'C' );
                        $this->Ln(10);
                    }
                    $this->Ln();
                }
                
                $this->Ln( 10 );
                break;

            case 'terms':
                
                $this->AddPage();
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                //Total members

                $this->Write(10, 'Terms and condition.');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Ln( 10 );
                // Remaining header cells
                $this->setTableHeading();

                $this->Cell( 37, 10, 'Version number', 1, 0, 'C' );
                $this->Cell( 37, 10, 'Publish date', 1, 0, 'C' );
                $this->Cell( 37, 10, 'Created date', 1, 0, 'C' );
                $this->Cell( 37, 10, 'Deactivation date', 1, 0, 'C' );
                $this->Cell( 37, 10, 'Staff member', 1, 0, 'C' );

                $this->Ln();
                $this->setContent();
                
                $this->Cell( 37, 8, $this->terms[0]['version_number'] , 1, 0, 'C' );
                $this->Cell( 37, 8, $this->terms[0]['publish_date'] , 1, 0, 'C' );
                $this->Cell( 37, 8, $this->terms[0]['created_date'] , 1, 0, 'C' );
                $this->Cell( 37, 8, $this->terms[0]['deactivation_date'] , 1, 0, 'C' );
                $this->Cell( 37, 8, $this->terms[0]['staff_member'] , 1, 0, 'C' );
                $this->Ln();
                //Total members
                
                $this->SetFont( 'Arial', 'I', 10 );

                $this->Write(5, 'Content', 'FJ');
            
                $this->Ln( 10 );
                $this->SetFont( 'Arial', 'I', 8 );

                $this->WriteHTML($this->terms[0]['content']);
            
                
                break;
            default:
                $this->SetDrawColor( $this->tableBorderColour[0], $this->tableBorderColour[1], $this->tableBorderColour[2] );
        
                $this->SetFont( 'Arial', 'I', 10 );
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Total Amount of Customers.');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                
                
                $this->Write(5, 'Below is a breakdown of the amount of customers that attended RampWorld Cardiff between '.$this->start_date.' and '. $this->end_date.' using the digital registration form and entry system.', 'FJ');
                 $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                for ( $i=0; $i < count($this->columnLabels) ; $i++ ) {
                  $this->Cell( 27, 10, $this->columnLabels[$i], 1, 0, 'C', true );
                }



                $this->Ln( 10 );

                // Create the table data rows

                
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                for($i = 1; $i < count($this->data) ; $i++) {

                    $this->Cell( 27, 10, ( $this->data[$i]  ), 1, 0, 'C', $this->fill );
                }

                $this->Ln();
                $this->SetX(145);
                $this->SetFont( 'Arial', 'I', 12 );
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );

                $this->Cell( 27, 10, 'Total: ', 1, 0, 'C', true, 'R' );
                 $this->SetFont( 'Arial', 'I', 10 );
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->Cell( 27, 10, array_sum($this->data), 1, 0, 'C', false, 'R' );

                //Gender members
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Gender');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Write(5, 'Below is a range of customer\'s gender that attended RampWorld Cardiff between '.$this->start_date.' and '. $this->end_date.' using the digital registration form and entry system.', 'FJ');
                 $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                
                $this->Cell( 30, 10, 'Male', 1, 0, 'C', true );
                $this->Cell( 30, 10, 'Female', 1, 0, 'C', true );
                $this->Cell( 30, 10, 'Non-Specified', 1, 0, 'C', true );
                

                $this->Ln( 10 );

                // Create the table data rows

                $this->fill = false;
                $this->row = 0;
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFillColor( $this->tableRowFillColour[0], $this->tableRowFillColour[1], $this->tableRowFillColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Male'] ) ), 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Female'] ) ), 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Non-Specified'] ) ), 1, 0, 'C', $this->fill );
                $this->Ln(10);
                $count = $this->gender[0]['Male'] + $this->gender[0]['Female'] + $this->gender[0]['Non-Specified'];

                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Male'] / $count * 100 ) ).'%', 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Female'] / $count * 100 ) ).'%', 1, 0, 'C', $this->fill );
                $this->Cell( 30, 10, ( number_format( $this->gender[0]['Non-Specified'] / $count * 100 ) ).'%', 1, 0, 'C', $this->fill );

                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Age Range');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Write(5, 'Below is a breakdown of the age range of customers that attended RampWorld Cardiff between '.$this->start_date.' and '. $this->end_date.' using the digital registration form and entry system.', 'FJ');
                 $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );

                foreach ($this->ages as $key => $value) {
                   $this->Cell( 27, 10, $key, 1, 0, 'C', true );
                }

                $this->Ln( 10 );

                // Create the table data rows

                $this->fill = false;
                $this->row = 0;
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFillColor( $this->tableRowFillColour[0], $this->tableRowFillColour[1], $this->tableRowFillColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                foreach ($this->ages as $key => $value) {
                   $this->Cell( 27, 10, $value, 1, 0, 'C' );
                }

                $this->AddPage();
                //Session break down

                
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );
                $this->Write(10, 'Session Breakdown.');
                $this->Ln( );
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Write(5, 'Below is a breakdown each amount of customer of each session between '.$this->end_date.' and '. $this->start_date.' using the digital registration form and entry system.', 'C');
                
               
                // Remaining header cells
                $this->Ln( 10 );
                

                for ( $i=1; $i < count($this->sessions) + 1; $i++ ) {

                    $this->SetFont( 'Arial', 'I', 14 );
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );

                    $this->Write(5, $this->sessions[$i]['day'], 'L');
                    $this->Ln(8);

                    foreach ($this->sessions[$i]['sessions'] as $key => $value) {
                       
                        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                        $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                        $this->SetFont( 'Arial', 'I', 12 );
                        $this->Cell( (180 / count($this->sessions[$i]['sessions'])), 10, $key, 1, 0, 'C', true );
                    }
                    $this->Ln(10);
                    foreach ($this->sessions[$i]['sessions'] as $key => $value) {
                       
                        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                        
                        $this->SetFont( 'Arial', '', 10 );

                        $this->Cell( (180 / count($this->sessions[$i]['sessions'])), 10, $value, 1, 0, 'C' );
                    }
                    $this->Ln(15);
                   
                 
                }

               

                // Create the table data rows

                

                //memebrs entry
                $this->Ln(15);
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                $this->Write(10, 'Members Entries');
                $this->Ln();
                $this->SetFont( 'Arial', 'I', 10 );
                $this->Write(5, 'Below is a list of all members that attended RampWorld Cardiff between '.$this->start_date.' and '. $this->end_date.' using the digital registration form and entry system.', 'FJ');
                 $this->Ln( 10 );
                // Remaining header cells
                $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                $this->SetFont( 'Arial', 'I', 12 );


                $this->Cell( 27, 10, 'Member ID', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Forename', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Surname', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Number', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Entry date', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Entry Time', 1, 0, 'C', true );
                $this->Cell( 27, 10, 'Exit Time', 1, 0, 'C', true );

                
                $this->Ln( 10 );

                // Create the table data rows

                $this->fill = false;
                $this->row = 0;
                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', '', 10 );

                foreach ($this->members as $key => $value) {
                    foreach ($value as $fieldname => $fieldvalue) {
                       $this->Cell( 27, 10, $fieldvalue, 1, 0, 'C' );
                    }
                    $this->Ln(10);
                   
                }
                break;
        }
        

    
        

    }
    private function _generate_sessions($mode) {
        $this->AddPage();
        switch ($mode) {
            case 'member':

                $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                $this->SetFont( 'Arial', 'I', 16 );

                //Total members

                
                
                $this->_process_sessions('member');


                $this->_create_total();
                $this->_create_hours();
                



                foreach($this->formatted_sessions as $key => $value) {
                   
                    $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                    $this->SetFont( 'Arial', 'I', 16 );
                   
                    $this->Ln(10);
                    $this->Write(10, $key);
                    $this->Ln(10);


                    foreach($value as $week => $week_value) {
                        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                        $this->SetFont( 'Arial', 'B', 13 );
                        $this->Write(10,'Week '. $week .':');
                        $this->Ln();
                        $this->SetFont( 'Arial', 'I', 13 );
                        $this->Write(10, 'Start Date: '.$week_value['start_date'].' - '.$week_value['end_date']);
                        $this->Ln(15);
                        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
                        $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
                        $this->SetFont( 'Arial', 'I', 12 );
                        $this->Cell( 54, 10, 'Entry date', 1, 0, 'C', true );
                        $this->Cell( 27, 10, 'Entry Time', 1, 0, 'C', true );
                        $this->Cell( 27, 10, 'Exit Time', 1, 0, 'C', true );
                        $this->Cell( 54, 10, 'Premature Exit Time', 1, 0, 'C', true );
                   
                        $this->Ln( 10 );

                        foreach ($week_value['sessions'] as $session => $sesh) {
                            
                            $this->SetFont( 'Arial', 'I', 10 );
                            $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
                            for ($j=0; $j < count($sesh['entries']); $j++) {
                                $this->Cell(54,10, date('l d-m-Y', strtotime($sesh['entries'][$j]['entry_date'])), 1, 0, 'C');
                                $this->Cell(27,10, $sesh['entries'][$j]['start_time'], 1, 0, 'C');
                                $this->Cell(27,10, $session, 1, 0, 'C');
                                $this->Cell(54,10, $sesh['entries'][$j]['premature_exit'], 1, 0, 'C');

                                $this->Ln(10);
                                
                            }


                        }
                        $this->SetFont( 'Arial', 'I', 13 );
                        $this->Write(10, 'Average Time Spent:  ');
                        $this->SetFont( 'Arial', 'I', 13 );
                        $this->Write(10, date('H\h i\m', ($week_value['avg_duration'] / count($sesh['entries']))));
                        
                        $this->Ln(15);
                    }
                    # code...
                }

                break;
            
            
            default:
                # code...
                break;
        }
    }
    private function _create_total() {

        $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $this->Write(10, 'Total Amount of Visits.');
        $this->Ln();
        $this->SetFont( 'Arial', 'I', 10 );
        $this->Write(5, 'Below is a breakdown of the amount of visits', 'FJ');
        $this->Ln( 10 );
        // Remaining header cells
        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
        $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
        $this->SetFont( 'Arial', 'I', 12 );

        for ( $i=0; $i < count($days) ; $i++ ) {
          $this->Cell( 27, 10, $days[$i], 1, 0, 'C', true );
        }


        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', '', 10 );
        $this->Ln(10);
        foreach ($this->days_totals as $key => $value) {
           $this->Cell( 27, 10, $value, 1, 0, 'C' );
        }
        $this->Ln(10);
        foreach ($this->days_totals as $key => $value) {
           $this->Cell( 27, 10, number_format( $value / array_sum($this->days_totals) * 100 ).'%', 1, 0, 'C' );

        }

        $this->Ln();
        $this->SetX(145);
        $this->SetFont( 'Arial', 'I', 12 );
        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );

        $this->Cell( 27, 10, 'Total: ', 1, 0, 'C', true, 'R' );
        $this->SetFont( 'Arial', 'I', 10 );
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->Cell( 27, 10, array_sum($this->days_totals), 1, 0, 'C', false, 'R' );

    }
    private function _create_hours() {

        $days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $this->Write(10, 'Total Amount of hours.');
        $this->Ln();
        $this->SetFont( 'Arial', 'I', 10 );
        $this->Write(5, 'Below is a breakdown of the amount of hours for this member.', 'FJ');
        $this->Ln( 10 );
        // Remaining header cells
        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );
        $this->SetFillColor( $this->tableHeaderTopFillColour[0], $this->tableHeaderTopFillColour[1], $this->tableHeaderTopFillColour[2] );
        $this->SetFont( 'Arial', 'I', 12 );

        for ( $i=0; $i < count($days) ; $i++ ) {
          $this->Cell( 27, 10, $days[$i], 1, 0, 'C', true );
        }

        
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', '', 10 );
        $this->Ln(10);
        foreach ($this->days_hours as $key => $value) {
           $this->Cell( 27, 10, date('H\h i\m', $value), 1, 0, 'C' );
        }
        $this->_calc_avg_hours();

        

    }
    private function _calc_avg_hours() {

        $total = 0;
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', '', 10 );
        $this->Ln(10);

        foreach ($this->days_hours as $key => $value) {

            $total += $value;
            $count += $this->days_totals[$key];
            $this->Cell( 27, 10, date('H\h i\m', $value / $this->days_totals[$key]) , 1, 0, 'C' );
        }

        $this->Ln();
        $this->SetX(145);
        $this->SetFont( 'Arial', 'I', 12 );
        $this->SetTextColor( $this->tableHeaderTopTextColour[0], $this->tableHeaderTopTextColour[1], $this->tableHeaderTopTextColour[2] );

        $this->Cell( 27, 10, 'Average: ', 1, 0, 'C', true, 'R' );
        $this->SetFont( 'Arial', 'I', 10 );
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->Cell( 27, 10, date('H\h i\m', $total/$count), 1, 0, 'C', false, 'R' );
       
    }
    private function _process_sessions($mode) {
        if($mode == 'member') {
            for ($i=0; $i < count($this->member_details->members[0]['entries']) ; $i++) { 
                
                $date =new DateTime( $this->member_details->members[0]['entries'][$i]['entry_date']);

                if(!isset( $this->formatted_sessions[$date->format('Y')][$date->format('W')])) {

                     $this->formatted_sessions[$date->format('Y')][$date->format('W')] =array(

                        'total' => 1,
                        'start_date' => date('d-m-Y', strtotime('monday this week',strtotime( $this->member_details->members[0]['entries'][$i]['entry_date']))),
                        'end_date' =>date('d-m-Y', strtotime('sunday this week',strtotime( $this->member_details->members[0]['entries'][$i]['entry_date']))),
                        'avg_duration' => strtotime($this->member_details->members[0]['entries'][$i]['duration']),
                        'sessions' => array());

                } else {
                    $this->formatted_sessions[$date->format('Y')][$date->format('W')]['total']++;
                    $this->formatted_sessions[$date->format('Y')][$date->format('W')]['avg_duration'] += strtotime($this->member_details->members[0]['entries'][$i]['duration']);
                    
                }
               

                if(!isset( $this->formatted_sessions[$date->format('Y')][$date->format('W')]['sessions'][$this->member_details->members[0]['entries'][$i]['exit_time']])) {
                     $this->formatted_sessions[$date->format('Y')][$date->format('W')]['sessions'][$this->member_details->members[0]['entries'][$i]['exit_time']] = array(
                            'total'=> 1,
                            'entries' => array(
                                array(

                                'start_time' => $this->member_details->members[0]['entries'][$i]['entry_time'],
                                'premature_exit' => $this->member_details->members[0]['entries'][$i]['premature_exit'],
                                'entry_date' => $this->member_details->members[0]['entries'][$i]['entry_date'] )
                            ))
                        ;

                } else {
                    $this->formatted_sessions[$date->format('Y')][$date->format('W')]['sessions'][ $this->member_details->members[0]['entries'][$i]['exit_time']]['total']++;

                    $this->formatted_sessions[$date->format('Y')][$date->format('W')]['sessions'][ $this->member_details->members[0]['entries'][$i]['exit_time']]['entries'][] = array(
                        'start_time' => $this->member_details->members[0]['entries'][$i]['entry_time'],
                        'premature_exit' => $this->member_details->members[0]['entries'][$i]['premature_exit'],
                        'entry_date' => $this->member_details->members[0]['entries'][$i]['entry_date'] );
                }
                

                ksort($this->formatted_sessions);
                $this->days_totals[date('l', strtotime( $this->member_details->members[0]['entries'][$i]['entry_date']))]++;
                $this->days_hours[date('l', strtotime( $this->member_details->members[0]['entries'][$i]['entry_date']))] += strtotime( $this->member_details->members[0]['entries'][$i]['exit_time']) - strtotime( $this->member_details->members[0]['entries'][$i]['entry_time']);
                $this->days_avg[date('l', strtotime( $this->member_details->members[0]['entries'][$i]['entry_date']))]++;
    
            }
        } else {


        }

       echo '<pre>';print_r($this->formatted_sessions);echo '</pre>';
    }
    private function addAvgDuration($year, $week, $duration) {
        
        $this->formatted_sessions[$year][$week]['avg_duration'] = date('H:i:s', strtotime($this->formatted_sessions[$year][$week]['avg_duration']) + strtotime($duration));
        

    }
    public function render($name, $report, $uri, $reissue) {
        if(!$reissue) {
            require_once 'ReportLog.php';
            $reportLog = new ReportLog();
            $reportLog->add($report, wp_get_current_user()->first_name. ' '. wp_get_current_user()->last_name, $uri );
        }
        ob_end_clean();
        $this->Output('I', $name.'.pdf');
    }
    private function setHeading() {
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', 'I', 12 );
    }
    private function setTableHeading() {
        $this->SetTextColor( 0, 0, 0 );
        $this->SetFont( 'Arial', 'B', 10 );
    }
    private function setContent() {
        $this->SetTextColor( $this->textColour[0], $this->textColour[1], $this->textColour[2] );
        $this->SetFont( 'Arial', '', 8 );
    }

}
