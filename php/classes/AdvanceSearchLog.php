<?php

class AdvanceSearchLog
{

	private $_db;
	public $logs = array();
	public $asi = null;
	public function __construct()
	{
			$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function get($asi= null, $uri = null)
	{

		if($asi != null) {
			$this->logs = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_advance_search_log WHERE advance_search_id  = %d LIMIT 1', array($asi)), ARRAY_A);
		
		} else if($uri != null) {
			$this->logs = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_advance_search_log WHERE uri  = %s LIMIT 1', array($uri)), ARRAY_A);
		}
	} 
	public function getAll($limit)
	{
			$this->logs = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_advance_search_log ORDER BY created_time DESC LIMIT %d', array($limit)), ARRAY_A);
	}
			
	public function add($query, $join, $bindings, $uri)
	{
		$current_user = wp_get_current_user();
		$previous = $this->_db->get_results($this->_db->prepare('SELECT advance_search_id FROM rwc_advance_search_log WHERE uri  = %s LIMIT 1', array($uri)), ARRAY_A);
		
	
		if ($previous == null) {
			$pre = explode('&', $uri);
			$post = array();
			foreach($pre as $p) {
				if(strpos($p, 'report') !== false ){
					$parts = explode('=', $p);
					$post[$parts[0]] = $parts[1];
				}
				
			}
			if(isset($post['report_start']) && isset($post['report_end'])) {
				$this->_db->insert( 'rwc_advance_search_log', array(
					'query'         		=> $query,
					'join'      				=> $join,
					'bindings'					=> $bindings,
					'uri'           		=> $uri,
					'report_start_date'	=> $post['report_start'],
					'report_end_date'		=> $post['report_end'],
					'created_time'  		=> date('Y-m-d H:i:s'),
					'created_user'		  => $current_user->first_name .' '. $current_user->last_name
				), array('%s', '%s', '%s', '%s', '%s', '%s', '%s'));
			} else {
				$this->_db->insert( 'rwc_advance_search_log', array(
					'query'         => $query,
					'join'      		=> $join,
					'bindings'			=> $bindings,
					'uri'           => $uri,
					'created_time'  => date('Y-m-d H:i:s'),
					'created_user'  => $current_user->first_name .' '. $current_user->last_name
				), array('%s', '%s', '%s', '%s', '%s', '%s', '%s'));
			
			}
			$this->asi = $this->_db->insert_id;
		} else {
			$this->_db->update('rwc_advance_search_log', array(
				'last_accessed_time'	=> date('Y-m-d H:i:s'),
				'last_accessed_user'	=> $current_user->first_name .' '. $current_user->last_name
			), array(
				'advance_search_id'	=> intval($previous[0]['advance_search_id'])
			), array(
				'%s', '%s'
			), array(
				'%d'
			));
			$this->asi = $previous[0]['advance_search_id'];
		}
	}
}
