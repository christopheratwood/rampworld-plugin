<?php
date_default_timezone_set('Europe/London');
class PaidMembership {
	public 	$data,
			$types,
			$processed,
			$html = '';
	private $_db,
			$_processed;
	public $passes = array();
	public $memberships = array();
	public function __construct() {
		
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function getTypesAsTable() {
		if($this->_processed == null or $this->_processed == false)
			$this->getTypesAsArray(false); 
		for($i = 0; $i < count($this->types); $i++)
			$this->html .= '<tr id="'.$this->types[$i]['membership_type_id'].'" data-no-action="true"><td data-original="'.$this->types[$i]['display_name'].'" data-type="text"> <input type="text" id="membership_'.$this->types[$i]['membership_type_id'].'_dn" value="'.$this->types[$i]['display_name'].'" class="rwc_new"></td><td data-original="'.$this->types[$i]['duration'].'" data-type="number"><input type="number" id="membership_'.$this->types[$i]['membership_type_id'].'_duration" value="'.$this->types[$i]['duration'].'"  class="rwc_new"></td></tr><tr><td colspan="2"><small>'.((!empty($this->types[$i]['description']))? $this->types[$i]['description']: '').'</small></tr>';
		return $this->html;
	}
	public function getTypes() {
		$this->types = $this->_db->get_results('SELECT * FROM rwc_membership_types', ARRAY_A);
	}
	public function getTypeID($dn) {
		return $this->_db->get_results($this->_db->prepare('SELECT membership_type_id FROM rwc_membership_types WHERE  LOWER(display_name) = %s LIMIT 1',array($dn)), ARRAY_A);
	}
	public function getCountActive($type) {
		$this->count = $this->_db->get_results($this->_db->prepare("SELECT count(mem.`member_id`) as 'total' FROM rwc_paid_membership AS paid LEFT JOIN rwc_members AS mem ON paid.`member_id` = mem.`member_id` WHERE end_date > CURRENT_DATE AND paid.`premature` IS NULL AND paid.`membership_type` = %d", array($type)),ARRAY_A);
		return $this->count[0]['total'];
	}
	public function add($member_id, $type, $start_date, $end_date= null) {
		if($end_date == null) {
			
			$duration = $this->_db->get_results($this->_db->prepare("SELECT duration from rwc_membership_types WHERE membership_type_id = %d LIMIT 1", array($type)), ARRAY_A);
			$inits = wp_get_current_user()->user_firstname. ' ' . wp_get_current_user()->user_lastname;
			$prev = $this->_db->get_results($this->_db->prepare("SELECT `date`, start_date, end_date FROM rwc_paid_membership WHERE member_id = %d AND start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE ORDER BY start_date LIMIT 1",array($member_id)), ARRAY_A);

			if(empty($prev)) {
				if($this->_db->insert('rwc_paid_membership' , array('member_id' => $member_id, 'membership_type' => $type, 'start_date' => date('Y-m-d', strtotime($start_date)), 'end_date' => date('Y-m-d', strtotime($start_date .' + '.$duration.' week')), 'staff_init' => $inits) ) ) {
					return true;
					
				} else {
					return false;
					
				}
			} else {
				return 'Sorry, Member already has membership.Purchased on '.$prev[0]['date'].', Start Date '.$prev[0]['start_date'].', End Date '.$prev[0]['end_date'];
				
			}
		} else {
			$inits = ucfirst(substr(wp_get_current_user()->user_firstname, 0, 1)) . ucfirst(substr(wp_get_current_user()->user_lastname, 0, 1));
			$prev = $this->_db->get_results($this->_db->prepare("SELECT `date`, start_date, end_date FROM rwc_paid_membership WHERE member_id = %d AND start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE ORDER BY start_date LIMIT 1",array($member_id)), ARRAY_A);
	
			if(empty($prev)) {
				if($this->_db->insert('rwc_paid_membership' , array('member_id' => $member_id, 'membership_type' => $type, 'start_date' => date('Y-m-d', strtotime($start_date)), 'end_date' => date('Y-m-d', strtotime($end_date)), 'staff_init' => $inits) ) ) {
					return true;
					
				} else {
					return false;
					
				}
		
			} else {
				return 'Sorry, Member already has membership.Purchased on '.$prev[0]['date'].', Start Date '.$prev[0]['start_date'].', End Date '.$prev[0]['end_date'];
				
			}
		}
	}
	public function getMember($memberId, $start = null, $end = null) {
		if($start != null) {
			$s = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ',$start))))));
			$e = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ',$end))))));

			$this->memberships = $this->_db->get_results($this->_db->prepare('SELECT paid_membership_id, rwc_membership_types.display_name, date, start_date, end_date, premature FROM rwc_paid_membership LEFT JOIN rwc_membership_types ON rwc_paid_membership.membership_type = rwc_membership_types.membership_type_id WHERE member_id = %d AND ((end_date BETWEEN %s AND %s ) OR end_date >= %s) ORDER BY date ASC, end_date ASC, start_date ASC', array(intval($memberId), $s->format('Y-m-d'), $e->format('Y-m-d'), $e->format('Y-m-d'))), ARRAY_A); 

	} else {
		$this->memberships = $this->_db->get_results($this->_db->prepare('SELECT paid_membership_id, rwc_membership_types.display_name, date, start_date, end_date, premature FROM rwc_paid_membership LEFT JOIN rwc_membership_types ON rwc_paid_membership.membership_type = rwc_membership_types.membership_type_id WHERE member_id = %d ORDER BY date DESC, end_date ASC, start_date ASC', array(intval($memberId))), ARRAY_A); 
		}
	}





	public function getAll($member_id = null, $searchField = null, $searchQuery = null, $searchField2 = null, $searchQuery2 = null, $start_dt=null, $end_dt = null, $type = null, $start_no, $limit) {
		$query = '';
		$bindings = array();

		if($searchField != null){
			switch($searchField) {
				case 'date of birth':
					$searchField = 'dob';
					break;
				case 'phone number':
					$searchField = 'number';
					break;
				case 'address line 1':
					$searchField = 'address_line_one';
					break;
				case 'postcode':
					$searchField = 'address_postcode';
					break;
			}
			if($query == "")
				$query .= 'WHERE '.$searchField.' LIKE \'%%%s%%\'';
			else
				$query .= ' AND '.$searchField.' LIKE \'%%%s%%\'';

			$bindings[] = $searchQuery;
		}
		if($searchField2 != null){
			switch($searchField2) {
				case 'date of birth':
					$searchField2 = 'dob';
					break;
				case 'phone number':
					$searchField2 = 'number';
					break;
				case 'address line 1':
					$searchField2 = 'address_line_one';
					break;
				case 'postcode':
					$searchField2 = 'address_postcode';
					break;
			}
			if($query == "")
				$query .= 'WHERE '.$searchField2.' LIKE \'%%%s%%\'';
			else
				$query .= ' AND '.$searchField2.' LIKE \'%%%s%%\'';
			
			$bindings[] = $searchQuery2;
		}

		if($member_id != null){
			if($query == "")
				$query .= 'WHERE pass.member_id = %s';
			else
				$query .= ' AND pass.member_id = %s';

			$bindings[] = $member_id;

		}
		if($start_dt != null && $end_dt != null){
			if($query == "")
				$query .= 'WHERE pass.start_date >= %s AND pass.end_date <= %s';
			else
				$query .= ' AND pass.start_date >= %s AND pass.end_date <= %s';

				$bindings[] = $start_dt;
				$bindings[] = $end_dt;

		}

		if($type != null) {
			$id = $this->getTypeID($type);
			if(!empty($id)) {
				$id =  $id[0]['membership_type_id'];
				if($query == "")
					$query .= 'WHERE pass.membership_type = %d';
				else
					$query .= ' AND pass.membership_type = %d';

				$bindings[] = $id;
			}
				
		}

		
		$bindings[] = $start_no;
		$bindings[] = $limit;
		if($member_id == null && $searchField == null && $searchField2 == null && $start_dt == null && $type == null) {
			$this->passes = $this->_db->get_results($this->_db->prepare('SELECT pass.*, mem.member_id, mem.forename, mem.surname, type.display_name as type FROM rwc_paid_membership as pass LEFT JOIN rwc_members as mem ON pass.member_id = mem.member_id LEFT JOIN rwc_membership_types as type ON pass.membership_type = type.membership_type_id WHERE end_date >= CURDATE() AND premature IS NULL ORDER BY end_date ASC LIMIT %d, %d', array($start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results('SELECT count(*) as count FROM rwc_paid_membership WHERE end_date >= CURDATE()', ARRAY_A)[0]['count'];
		} else {
			if($member_id == null && $searchField == null && $searchField2 == null && $start_dt == null && $type != null) {
				$this->passes = $this->_db->get_results($this->_db->prepare('SELECT pass.*, mem.member_id, mem.forename, mem.surname, type.display_name as type FROM rwc_paid_membership as pass LEFT JOIN rwc_members as mem ON pass.member_id = mem.member_id LEFT JOIN rwc_membership_types as type ON pass.membership_type = type.membership_type_id WHERE pass.membership_type = %d AND end_date >= CURDATE() AND premature IS NULL ORDER BY end_date ASC LIMIT %d, %d', array($id, $start_no, $limit)), ARRAY_A);
				$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_paid_membership WHERE membership_type = %d AND end_date >= CURDATE()',array($id)), ARRAY_A)[0]['count'];
			} else{
				$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_paid_membership as pass LEFT JOIN rwc_members as mem ON pass.member_id = mem.member_id  '.$query.'', $bindings), ARRAY_A)[0]['count'];

				$bindings[] = $start_dt;
				$bindings[] = $end_dt;
				$this->passes = $this->_db->get_results($this->_db->prepare('SELECT pass.*, mem.member_id, mem.forename, mem.surname, type.display_name as type FROM rwc_paid_membership as pass LEFT JOIN rwc_members as mem ON pass.member_id = mem.member_id LEFT JOIN rwc_membership_types as type ON pass.membership_type = type.membership_type_id '.$query.' ORDER BY end_date ASC LIMIT %d, %d', $bindings), ARRAY_A);

				
			}
		}
		
	}

	public function get($pmid) {
		$this->getTypes();
		$this->details = $this->_db->get_results($this->_db->prepare('SELECT paid_membership_id, pass.member_id, membership_type, DATE_FORMAT(date, "%%H:%%i:%%s %%d-%%m-%%Y") as date, start_date, end_date, staff_init, premature,
		transaction_id, cost, refunded, used_on, pass.last_updated_time, pass.last_updated_user, pass.updated_amount, forename, surname FROM rwc_paid_membership as pass LEFT JOIN rwc_members as mem ON pass.member_id = mem.member_id WHERE paid_membership_id = %d', array(intval($pmid))), ARRAY_A);
		if(!empty($this->details))
			$this->entries = $this->_db->get_results($this->_db->prepare('SELECT entry_id, entry_time, exit_time, entry_date FROM rwc_members_entries WHERE member_id = %d AND entry_date BETWEEN %s AND %s ORDER BY entry_date DESC, exit_time ASC', array($this->details[0]['member_id'], $this->details[0]['start_date'], $this->details[0]['end_date'])), ARRAY_A);

	}
	function delete($pmid) {
		if(!current_user_can('administrator')){
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&error=incorrect+permissions&member='.$member_id);
		} else {
			$current_user = wp_get_current_user();

			if($this->_db->query($this->_db->prepare('UPDATE rwc_paid_membership SET premature = %s, last_updated_user = %s, updated_amount = updated_amount + 1 WHERE paid_membership_id = %d', array(date('Y-m-d H:i:s'), $current_user->first_name . ' '. $current_user->last_name, intval($pmid)))) === false) {
				wp_redirect($_SERVER['HTTP_REFERER'].'&delete=error');
			} else {
				wp_redirect($_SERVER['HTTP_REFERER'].'&delete=success');
			}
		}
	}

	function update($pmid, $data) {

		$current_user = wp_get_current_user();
		//update main
		
		if($this->_db->update('rwc_paid_membership', array(
			'start_date'					=> $data['start_date'],
			'end_date'						=> $data['end_date'],
			'last_updated_time'		=> date('Y-m-d H:i:s'),
			'last_updated_user'		=> $current_user->user_firstname.' '. $current_user->user_lastname,
			'updated_amount'			=> intval($data['updated_amount']) + 1

		),array(
			'paid_membership_id' 	=> intval($pmid)
		), array('%s', '%s', '%s', '%s', '%d'), array('%d')) === false) {
			return false;
			
		}
		
	}

	function create($member, $data, $entry = false) {
		if(isset($data['type']) && isset($data['start_date'])) {

			$duration = $this->_db->get_var($this->_db->prepare('SELECT duration FROM rwc_membership_types WHERE membership_type_id = %d', array(intval($data['type']))));
			if(!empty($duration)) {
				if($this->_db->insert('rwc_paid_membership', array(
					'member_id'	=> intval($member),
					'membership_type'		=> intval($data['type']),
					'date'							=> date('Y-m-d H:i:s'),
					'start_date'				=> date('Y-m-d', strtotime($data['start_date'])),
					'end_date'					=> date('Y-m-d', strtotime($data['start_date'] .' + '.$duration.' week')),
					'staff_init'				=> wp_get_current_user()->user_firstname. ' ' . wp_get_current_user()->user_lastname
				), array(
					'%d','%d','%s','%s','%s','%s'
				)) === false){
					$this->errors = array('Failed to save');
					return false;
		
				} else{
					if($entry === false)
						wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fsingle&pmid='.$this->_db->insert_id);
					else
						return true;
					
				}

		} else {
			$this->errors = array('No duration available');
			return false;
		}
	}
}

}