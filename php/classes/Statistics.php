<?php
date_default_timezone_set('Europe/London');
class Statistics
{
    private $_db;
    public $total_new__data;
    public $total_new__labels;
    public $total_entries__data;
    public $total_entries__labels;
    public $names;
    public $ages = array();
    public $genders = array();
    public $data = array();
    public $expertise = array();
    public $discipline = array();

    public function __construct()
    {
        
        $this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    }
    public function getTotalNewMembers()
    {
        $datas = array();
        $labels = array();

        $this->total_new__data = $this->_db->get_results('SELECT COUNT(*) AS total FROM rwc_members WHERE registered > CURRENT_DATE - INTERVAL 1 Month GROUP BY DATE_FORMAT(registered, "%d-%m") order by registered ASC', ARRAY_A);

        $this->total_new__labels = $this->_db->get_results('SELECT DATE_FORMAT(registered, "%d-%m") AS date FROM rwc_members WHERE registered > CURRENT_DATE - INTERVAL 1 Month GROUP BY date order by registered ASC', ARRAY_A);
    

        foreach ($this->total_new__data as $data => $value) {
            array_push($datas, $value['total']);
        }
        foreach ($this->total_new__labels as $data => $value) {
            array_push($labels, $value['date']);
        }
        $this->total_new__data = json_encode($datas);
        $this->total_new__labels = json_encode($labels);
    }
    public function getTotalEntries()
    {
        $datas = array();
        $labels = array();

        $this->total_entries__data = $this->_db->get_results('SELECT COUNT(*) AS total FROM rwc_members_entries WHERE entry_date > CURRENT_DATE - INTERVAL 1 Month GROUP BY DATE_FORMAT(entry_date, "%d-%m") order by entry_date ASC', ARRAY_A);

        $this->total_entries__labels = $this->_db->get_results('SELECT DATE_FORMAT(entry_date, "%d-%m") AS date FROM rwc_members_entries WHERE entry_date > CURRENT_DATE - INTERVAL 1 Month GROUP BY date order by entry_date ASC', ARRAY_A);
        


        foreach ($this->total_entries__data as $data => $value) {
            array_push($datas, $value['total']);
        }
        foreach ($this->total_entries__labels as $data => $value) {
            array_push($labels, $value['date']);
        }
        $this->total_entries__data = json_encode($datas);
        $this->total_entries__labels = json_encode($labels);
    }
    public function getTotalMembersOnPremises()
    {
        $numOfEntries = $this->_db->get_results( 'SELECT count(*) as total FROM `rwc_members_entries` WHERE`rwc_members_entries`.`entry_date` = CURRENT_DATE AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE AND premature_exit IS NULL AND member_id != 2', ARRAY_A );
        return $numOfEntries[0]['total'];
    }
    public function getTotalPaidSessionsToday()
    {
        return intval($this->_db->get_results( 'SELECT count(*) as total FROM `rwc_prepaid_sessions` WHERE `session_end_time` >= NOW() AND used_on iS NULL AND refunded is NULL AND session_date = CURRENT_DATE ', ARRAY_A )[0]['total']);
    }
    public function getTotalPaidPassesToday()
    {
        return intval($this->_db->get_results('SELECT  count(paid_membership_id) as total
		FROM rwc_paid_membership as pm
		LEFT JOIN rwc_members_entries ent ON pm.member_id = ent.member_id AND ent.entry_date = CURRENT_DATE AND premature is null
		WHERE CURDATE() BETWEEN pm.start_date AND pm.end_date AND ent.entry_date = CURRENT_DATE AND premature is null and refunded is null
		 ', ARRAY_A)[0]['total']);
    }
    public function getAges($start = null, $end = null, $all = null)
    {
        if ($all == null) {
            $this->ages = $this->_db->get_results('SELECT 
				SUM((datediff(now(), dob) / 365.25) >= 31) as "31+",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 22 AND 31) as "22 - 30",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 18 AND 22) as "18 - 21",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 14 AND 18) as "14 - 17",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 11 AND 14 ) as "11 - 13",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 7 AND 11 ) as "7 - 10",
				SUM((datediff(now(), dob) / 365.25) < 7)  as"Under 7",
				COUNT(`rwc_members`.member_id) as "total"
			FROM rwc_members', ARRAY_A);
        } else {
            $s = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $start))))));
            $e = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $end))))));
            $this->ages = $this->_db->get_results('SELECT 
                SUM((datediff(now(), dob) / 365.25) >= 31) as "31+",
                SUM((datediff(now(), dob) / 365.25) BETWEEN 22 AND 31) as "22 - 30",
                SUM((datediff(now(), dob) / 365.25) BETWEEN 18 AND 22) as "18 - 21",
                SUM((datediff(now(), dob) / 365.25) BETWEEN 14 AND 18) as "14 - 17",
                SUM((datediff(now(), dob) / 365.25) BETWEEN 11 AND 14 ) as "11 - 13",
                SUM((datediff(now(), dob) / 365.25) BETWEEN 7 AND 11 ) as "7 - 10",
                SUM((datediff(now(), dob) / 365.25) < 7)  as"Under 7",
                COUNT(`rwc_members`.member_id) as "total"
            FROM rwc_members
            LEFT JOIN rwc_members_entries ON rwc_members.member_id = rwc_members_entries.member_id
            WHERE TIMESTAMP(entry_date, entry_time) BETWEEN \''.$s->format('Y-m-d H:i').'\' AND \''.$e->format('Y-m-d H:i').'\'' , ARRAY_A);

        }
    }


    public function getAgesAdvance($query = null, $join = null, $bindings = null)
    {
        $this->ages = $this->_db->get_results($this->_db->prepare('SELECT 
				SUM((datediff(now(), dob) / 365.25) >= 31) as "31+",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 22 AND 31) as "22 - 30",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 18 AND 22) as "18 - 21",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 14 AND 18) as "14 - 17",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 11 AND 14 ) as "11 - 13",
				SUM((datediff(now(), dob) / 365.25) BETWEEN 7 AND 11 ) as "7 - 10",
				SUM((datediff(now(), dob) / 365.25) < 7)  as"Under 7",
				COUNT(mem.member_id) as "total"
			FROM rwc_members as mem
			 	'.$join.' ' . $query, $bindings), ARRAY_A);
    }
    public function getGenders($start = null, $end = null, $all = null)
    {
        if ($all == null) {
            $this->genders = $this->_db->get_results('SELECT
				SUM(gender = "m" ) as "Male",
				SUM(gender = "f")  as"Female",
				SUM(gender = "o") as "Other",
				COUNT(member_id) as "total"
			FROM `rwc_members` ', ARRAY_A);
        } else {
            $s = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $start)))));
            $e = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $end)))));

            $this->genders = $this->_db->get_results($this->_db->prepare('SELECT
                SUM(gender = "m" ) as "Male",
                SUM(gender = "f")  as"Female",
                SUM(gender = "o") as "Other",
                COUNT(`rwc_members`.member_id) as "total"
            FROM `rwc_members`
            LEFT JOIN rwc_members_entries ON rwc_members.member_id = rwc_members_entries.member_id

            WHERE TIMESTAMP(entry_date, entry_time) BETWEEN %s AND %s',
            array($s->format('Y-m-d H:i'), $e->format('Y-m-d H:i'))), ARRAY_A);
        }
    }

    public function getGendersAdvance($query = null, $join = null, $bindings = null)
    {

        $this->genders = $this->_db->get_results($this->_db->prepare('SELECT
			SUM(gender = "m" ) as "Male",
			SUM(gender = "f")  as"Female",
			SUM(gender = "o") as "Other",
			COUNT(mem.member_id) as "total"
			FROM `rwc_members` as mem
			'.$join.' ' . $query, $bindings), ARRAY_A);
    }
    public function getSessions($start = null, $end = null, $all = null)
    {
        if ($all == null) {
            $this->data = $this->_db->get_results('SELECT
				SUM(DAYOFWEEK(`entry_date`) = 2) as "Monday",
				SUM(DAYOFWEEK(`entry_date`) = 3) as "Tuesday",
				SUM(DAYOFWEEK(`entry_date`) = 4) as "Wednesday",
				SUM(DAYOFWEEK(`entry_date`) = 5) as "Thursday",
				SUM(DAYOFWEEK(`entry_date`) = 6) as "Friday",
				SUM(DAYOFWEEK(`entry_date`) = 7) as "Saturday",
				SUM(DAYOFWEEK(`entry_date`) = 1) as "Sunday",
				COUNT(entry_id) as "total"
			FROM `rwc_members_entries` ', ARRAY_A);
        } else {
            $s = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $start))))));
            $e = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $end))))));

            $this->data = $this->_db->get_results($this->_db->prepare('SELECT
                SUM(DAYOFWEEK(`entry_date`) = 2) as "Monday",
                SUM(DAYOFWEEK(`entry_date`) = 3) as "Tuesday",
                SUM(DAYOFWEEK(`entry_date`) = 4) as "Wednesday",
                SUM(DAYOFWEEK(`entry_date`) = 5) as "Thursday",
                SUM(DAYOFWEEK(`entry_date`) = 6) as "Friday",
                SUM(DAYOFWEEK(`entry_date`) = 7) as "Saturday",
                SUM(DAYOFWEEK(`entry_date`) = 1) as "Sunday",
                COUNT(entry_id) as "total"
            FROM `rwc_members_entries`
            WHERE TIMESTAMP(entry_date, entry_time) BETWEEN %s AND %s',
            array($s->format('Y-m-d H:i'), $e->format('Y-m-d H:i'))), ARRAY_A);
        }
    }

    public function getSessionsAdvance($query = null, $join = null, $bindings = null)
    {
        $this->data = $this->_db->get_results($this->_db->prepare('SELECT
			SUM(DAYOFWEEK(`entry_date`) = 2) as "Monday",
			SUM(DAYOFWEEK(`entry_date`) = 3) as "Tuesday",
			SUM(DAYOFWEEK(`entry_date`) = 4) as "Wednesday",
			SUM(DAYOFWEEK(`entry_date`) = 5) as "Thursday",
			SUM(DAYOFWEEK(`entry_date`) = 6) as "Friday",
			SUM(DAYOFWEEK(`entry_date`) = 7) as "Saturday",
			SUM(DAYOFWEEK(`entry_date`) = 1) as "Sunday",
			COUNT(entry_id) as "total"
			FROM rwc_members_entries as ent
			LEFT JOIN rwc_members as mem ON ent.member_id = mem.member_id ' . $query, $bindings), ARRAY_A);
    }

    public function getExpertise($start = null, $end = null, $all = null)
    {
        if ($all == null) {
            $this->expertise = $this->_db->get_results('SELECT
				SUM(expertise = 0) as "Beginner",
				SUM(expertise = 1) as "Novice",
				SUM(expertise = 2) as "Experienced",
				SUM(expertise = 3) as "Advanced",
				SUM(expertise = 4) as "Expert",
				SUM(expertise = 5) as "Professional"
			FROM `rwc_members` as mem ', ARRAY_A);
        } else {
                $s = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $start)))));
                $e = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $end)))));

                $this->expertise = $this->_db->get_results($this->_db->prepare('SELECT
                    SUM(expertise = 0) as "Beginner",
                    SUM(expertise = 1) as "Novice",
                    SUM(expertise = 2) as "Experienced",
                    SUM(expertise = 3) as "Advanced",
                    SUM(expertise = 4) as "Expert",
                    SUM(expertise = 5) as "Professional"
				FROM `rwc_members` as mem
                LEFT JOIN rwc_members_entries as ent ON mem.member_id = ent.member_id
				WHERE TIMESTAMP(entry_date, entry_time) BETWEEN %s AND %s',
                array($s->format('Y-m-d H:i'), $e->format('Y-m-d H:i'))), ARRAY_A);
        }
    }

    public function getExpertiseAdvance($query = null, $join = null, $bindings = null)
    {
        $this->expertise = $this->_db->get_results($this->_db->prepare('SELECT
			SUM(expertise = 0) as "Beginner",
			SUM(expertise = 1) as "Novice",
			SUM(expertise = 2) as "Experienced",
			SUM(expertise = 3) as "Advanced",
			SUM(expertise = 4) as "Expert",
			SUM(expertise = 5) as "Professional"
			FROM rwc_members as mem
			'.$join.' ' . $query, $bindings), ARRAY_A);
    }

    public function getDisciplines($start = null, $end = null, $all = null)
    {
        if ($all == null) {
            $this->discipline = $this->_db->get_results('SELECT
                SUM(discipline LIKE \'%%SMX%%\') as "Scooter",
                SUM(discipline LIKE \'%%BMX%%\') as "BMX",
                SUM(discipline LIKE \'%%skateboard%%\') as "Skateboard",
                SUM(discipline LIKE \'%%inline%%\') as "Inline",
                SUM(discipline LIKE \'%%MTB%%\') as "MTB",
                SUM(discipline LIKE \'%%spectator%%\') as "Spectator",
                SUM(discipline NOT LIKE \'%%SMX%%\' AND discipline NOT LIKE \'%%BMX%%\' AND discipline NOT LIKE \'%%skateboard%%\' AND discipline NOT LIKE \'%%inline%%\' AND discipline NOT LIKE \'%%spectator%%\' AND discipline NOT LIKE \'%%spectator%%\') as "Other"
                FROM rwc_members as mem', ARRAY_A);
        } else {
            $s = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $start)))));
            $e = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(str_replace('%3A', ':', str_replace('+', ' ', $end)))));

            $this->discipline = $this->_db->get_results($this->_db->prepare('SELECT
                SUM(discipline LIKE \'%%SMX%%\') as "Scooter",
                SUM(discipline LIKE \'%%BMX%%\') as "BMX",
                SUM(discipline LIKE \'%%skateboard%%\') as "Skateboard",
                SUM(discipline LIKE \'%%inline%%\') as "Inline",
                SUM(discipline LIKE \'%%MTB%%\') as "MTB",
                SUM(discipline LIKE \'%%spectator%%\') as "Spectator",
                SUM(discipline NOT LIKE \'%%SMX%%\' AND discipline NOT LIKE \'%%BMX%%\' AND discipline NOT LIKE \'%%skateboard%%\' AND discipline NOT LIKE \'%%inline%%\' AND discipline NOT LIKE \'%%spectator%%\' AND discipline NOT LIKE \'%%spectator%%\') as "Other"
            FROM `rwc_members` as mem
            LEFT JOIN rwc_members_entries as ent ON mem.member_id = ent.member_id
            WHERE TIMESTAMP(entry_date, entry_time) BETWEEN %s AND %s',
            array($s->format('Y-m-d H:i'), $e->format('Y-m-d H:i'))), ARRAY_A);
        }
    }
    public function getDisciplinesAdvance($query = null, $join = null, $bindings = null)
    {
        $this->discipline = $this->_db->get_results($this->_db->prepare('SELECT
			SUM(discipline LIKE \'%%SMX%%\') as "Scooter",
			SUM(discipline LIKE \'%%BMX%%\') as "BMX",
			SUM(discipline LIKE \'%%skateboard%%\') as "Skateboard",
			SUM(discipline LIKE \'%%inline%%\') as "Inline",
			SUM(discipline LIKE \'%%MTB%%\') as "MTB",
			SUM(discipline LIKE \'%%spectator%%\') as "Spectator",
			SUM(discipline NOT LIKE \'%%SMX%%\' AND discipline NOT LIKE \'%%BMX%%\' AND discipline NOT LIKE \'%%skateboard%%\' AND discipline NOT LIKE \'%%inline%%\' AND discipline NOT LIKE \'%%spectator%%\' AND discipline NOT LIKE \'%%spectator%%\') as "Other"
			FROM rwc_members as mem
			'.$join.' ' . $query, $bindings), ARRAY_A);
    }
}
