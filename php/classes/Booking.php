<?php
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Sale;
use PayPal\Api\Payment;


date_default_timezone_set('Europe/London');
class Booking {
	private $_db;
    public $details = null;
    public $participants = null;
    private $_api;
    public $error_message;
    public $isMembership = false;
    public $isUsed = false;
    public $bookings = array();
    public $members = array();
    
    public $count = 0;

	public function __construct() {
		
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
    public function getAll($search_field = null, $search_value = null, $start_no = null, $limit = null) {

        if($search_field == null) {
            $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND pm.start_date IS NOT NULL GROUP BY b.id)  ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $start_no, $limit)), ARRAY_A);

            $this->count =  count($this->_db->get_results('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.start_date IS NOT NULL GROUP BY b.id)', ARRAY_A));
           

        } else {
            $search_field = str_replace('+', ' ',$search_field);
            if($search_field == 'booking reference'){
				$this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND  b.id = %d GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.complete IS NOT NULL AND b.id = pm.transaction_id WHERE b.id = %d GROUP BY b.id)', array( intval($search_value), intval($search_value))), ARRAY_A));

                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND b.id = %d GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND b.id = %d GROUP BY b.id)  ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
                echo $this->_db->last_query;
            }
            if($search_field == 'booking email'){
				$this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));

                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND email = %s GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
                echo $this->_db->last_query;
            } else if($search_field == 'booking name'){
                $this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));

                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
                
                
			} else if($search_field == 'member name') {
                $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE  CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));
                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
            } else if($search_field == 'member id') {
                $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE m.member_id = %d GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id)', array( intval($search_value), intval($search_value))), ARRAY_A));
                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete IS NOT NULL AND m.member_id = %d GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND m.member_id = %d GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( intval($search_value), intval($search_value), $start_no, $limit)), ARRAY_A);


			} else if ($search_field == 'booking date') {

                $search_value = DateTime::createFromFormat('d/m/Y', date('d/m/Y', strtotime($search_value)));

                $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) UNION 
                (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE  b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id)', array( $search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $search_value->format('Y-m-d'))), ARRAY_A));
                $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL AND b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) UNION 
                (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete is NOT null AND pm.date IS NOT NULL AND pm.start_date IS NOT NULL AND b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value->format('Y-m-d'), $search_value->format('Y-m-d') ,$search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $start_no, $limit)), ARRAY_A);

			}
            
                
        }

        
        
    }
    public function getDetails($pid, $mid) {
        $res = $this->_db->get_results($this->_db->prepare('SELECT session_start_time, session_end_time FROM rwc_prepaid_sessions WHERE session_date = curdate() AND prepaid_id = %d AND member_id = %d LIMIT 1', array(intval($pid), intval($mid))), ARRAY_A);

        if(count($res) == 0) {
            return false;

        } else {
            return $res[0];
        }
    }
    public function get( $bid ) {

        $this->participants =  $this->_db->get_results($this->_db->prepare(
            'SELECT ps.prepaid_id as prepaid_id, mem2.member_id as member_id,
            CONCAT(mem2.`forename`, " ", mem2.surname) as name,
            ps.used_on as used,
            ps.refunded,
            ps.cost,
            ps.used_on
            FROM rwc_prepaid_sessions as ps
            LEFT JOIN rwc_members as mem2 ON ps.member_id = mem2.member_id
            WHERE
                ps.transaction_id = %d
            ORDER by ps.session_date ASC ', array($bid)
        ), ARRAY_A);
        
        if(count($this->participants) == 0) {
            $this->details =  $this->_db->get_results($this->_db->prepare('SELECT id, complete, rwc_transactions_paypal.email, name, rwc_transactions_paypal.number, session_cost, booking_cost,  app_version, rwc_transactions_paypal.refunded, purchased, CONCAT(rwc_members.`forename`, " ", rwc_members.surname) as member_name, rwc_members.number as member_number, rwc_members.email as member_email, DATE_FORMAT(rwc_paid_membership.date , "%%Y-%%m-%%d") as session_date, DATE_FORMAT(rwc_paid_membership.end_date , "%%Y-%%m-%%d") as end_date , CONCAT(rwc_paid_membership.start_date, " - ", rwc_paid_membership.end_date) as times, rwc_transactions_paypal.refunded_total FROM rwc_transactions_paypal LEFT JOIN rwc_paid_membership ON rwc_transactions_paypal.id = rwc_paid_membership.transaction_id LEFT JOIN rwc_members ON rwc_transactions_paypal.member_id = rwc_members.member_id WHERE rwc_transactions_paypal.id = %d GROUP BY rwc_transactions_paypal.id', array($bid)), ARRAY_A);


            $this->isMembership = true;
            $this->participants = $this->_db->get_results($this->_db->prepare(
                'SELECT mem.paid_membership_id , mem2.member_id,
                CONCAT(mem2.`forename`, " ", mem2.surname) as name,
                mem.used_on as used,
                mem.refunded,
                mem.used_on,
                mem.cost
                FROM rwc_paid_membership as mem
                LEFT JOIN rwc_members as mem2 ON mem.member_id = mem2.member_id
                WHERE
                    mem.transaction_id = %d
            
                ORDER by mem.used_on ASC ', array($bid)
            ), ARRAY_A);

        } else {
            
            $this->details =  $this->_db->get_results($this->_db->prepare('SELECT id, complete, rwc_transactions_paypal.email, name, rwc_transactions_paypal.number, session_cost, booking_cost, app_version, rwc_transactions_paypal.refunded, purchased, CONCAT(rwc_members.`forename`, " ", rwc_members.surname) as member_name, rwc_members.number as member_number, rwc_members.email as member_email, rwc_prepaid_sessions.session_date , CONCAT(rwc_prepaid_sessions.session_start_time, " - ", rwc_prepaid_sessions.session_end_time) as times, rwc_transactions_paypal.refunded_total FROM rwc_transactions_paypal LEFT JOIN rwc_prepaid_sessions ON rwc_transactions_paypal.id = rwc_prepaid_sessions.transaction_id LEFT JOIN rwc_members ON rwc_transactions_paypal.member_id = rwc_members.member_id WHERE rwc_transactions_paypal.id = %d GROUP BY rwc_transactions_paypal.id', array($bid)), ARRAY_A);
        }
        $this->details =  $this->details[0];
        
        foreach($this->participants as $p) {
            if($p['used_on'] != null){
                $this->isUsed = true;
                break;
            }
        }
        
        return $this->participants;
        return true;
    }
    public function getMember( $member_id ){
        $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT prepaid_id as id, transaction_id, date_format(session_date, "%%d-%%m-%%Y") as date, concat(date_format(session_start_time, "%%H:%%i")," - ", date_format(session_end_time, "%%H:%%i")) as session, used_on, refunded, "false" as isPass FROM rwc_prepaid_sessions WHERE member_id = %d ) UNION (SELECT paid_membership_id  as id, transaction_id, date_format(date, "%%d-%%m-%%Y") as date, concat(start_date, " - ", end_date) as session, used_on, refunded, "true" as isPass FROM rwc_paid_membership WHERE member_id = %d AND staff_init = "BOOK")  ORDER BY date DESC ', array($member_id, $member_id)), ARRAY_A);
    }
    public function getAllMembers( $bookingID ) {
        $this->details = $this->_db->get_results(
            $this->_db->prepare(
                'SELECT book.name, book.email, book.number
                FROM rwc_transactions_paypal as book
                WHERE book.id = %d LIMIT 1'
            , array( $bookingID ))
        , ARRAY_A)[0];

        $this->members = $this->_db->get_results(
            $this->_db->prepare(
                'SELECT pre.prepaid_id as pre_session_id, paid.paid_membership_id as paid_pass_id, mem.member_id, mem.forename, mem.surname, mem.`staff_verification`, TIMESTAMPDIFF(YEAR,mem.dob,CURDATE()) AS dob, mem.medical_notes, pre.session_end_time, pre.session_start_time,
                    CASE
                        WHEN mem.expertise = 0 THEN "Beginner" 
                        WHEN mem.expertise = 1 THEN "Novice"
                        WHEN mem.expertise = 2 THEN "Experienced"
                        WHEN mem.expertise = 3 THEN "Advanced"
                        WHEN mem.expertise = 4 THEN "Expert"
                        WHEN mem.expertise = 5 THEN "Professional"
                        END AS expertise,
                    CASE
                        WHEN CURRENT_DATE() BETWEEN notes.`start_date` AND notes.end_date THEN notes.comments
                        ELSE NULL
                        END AS system_notes
                FROM rwc_transactions_paypal as book
                LEFT JOIN rwc_prepaid_sessions as pre ON book.id = pre.transaction_id AND pre.session_date = CURDATE() AND pre.used_on IS NULL
                LEFT JOIN rwc_paid_membership as paid ON book.id = paid.transaction_id AND paid.end_date >= CURDATE() AND paid.used_on IS NULL
                LEFT JOIN rwc_members as mem ON pre.member_id = mem.member_id OR paid.member_id = mem.member_id
                LEFT JOIN rwc_members_notes as notes ON mem.member_id = notes.member_id AND CURRENT_DATE() BETWEEN notes.`start_date` AND notes.end_date
                WHERE 
                    (paid.`paid_membership_id` IS NOT NULL OR pre.transaction_id IS NOT NULL)  
                    AND book.id = %d
                ORDER BY mem.forename ASC,
                    mem.surname ASC,
                    dob ASC'
            , array( intval( $bookingID ) ) )
        , ARRAY_A );
        $this->details['session_start_time'] = $this->members[0]['session_start_time'];
        $this->details['session_end_time'] = $this->members[0]['session_end_time'];
    }
    private function setApi() {
        $this->_api = new ApiContext(
           new OAuthTokenCredential('AcVKOqjrlhvyTTUUI28bDTI3Xpl4hebkY8K_OOMlGULIlL1W4ZUx7DdEQpzvTfzEWaEvDidPGZb1_fg6', 'EI-tIDO9DdP17acuT35BZQ7wAJlIgeX2wcTOwNuwZWuP7KHgcRwZaUqcosVkq8teWAa4OWaoNQvolQJN')
        );
        $this->_api->setConfig(array(
            'mode'                      => 'sandbox',
            'http.ConnectionTimeOut'    => 30,
            'log.LogEnabled'            => false,
            'log.FileName'              => 'paypal_log.txt',
            'log.LogLevel'              => 'DEBUG',
            'validation.level'          => 'log'

        ));
    }
    private function _getSaleId($pppid) {
        $payments = Payment::get($pppid, $this->_api);
        $payments->getTransactions();
        $obj = $payments->toJSON();//I wanted to look into the object
        $paypal_obj = json_decode($obj);//I wanted to look into the object
        $transaction_id = $paypal_obj->transactions[0]->related_resources[0]->sale->id;
        return $transaction_id;
    }
    public function refund($tid) {

        require __DIR__.'/../../../../themes/rampworld/modules/vendor/autoload.php';
        $this->setApi();

        //get_amount
        $refence = $this->_db->get_results($this->_db->prepare('SELECT `session_cost`, `payment_id` FROM rwc_transactions_paypal WHERE id = %d AND refunded IS NULL', $tid), ARRAY_A);

        if(count($refence) < 1) {
            $refunded = $this->_db->get_results($this->_db->prepare('SELECT date_format(`refunded`, "%H:%i %d-%m-%Y") as refunded FROM rwc_transactions_paypal WHERE id = %d', $tid), ARRAY_A);
            $this->error_message = 'This transaction has already been refunded on the '.$refunded[0]['refunded'];
            return false;
        } else {
            //check if any other been refuned
            $check =  $ref = $this->_db->get_results($this->_db->prepare('SELECT 1 FROM rwc_prepaid_sessions WHERE transaction_id = %d AND refunded IS NOT NULL', $tid), ARRAY_A);
            if(count($check) > 0) {
                $this->error_message = 'One or more transaction has already been refunded.';
                return false;
            } else {

                $amount = new Amount();
                $amount->setTotal($refence[0]['session_cost'])
                    ->setCurrency('GBP');
                
                $refund = new Refund();
                $refund->setAmount($amount);

                $sale = new Sale();
                $sale->setId($this->_getSaleId($refence[0]['payment_id']));

                try {
                    $refundedSale = $sale->refund($refund, $this->_api);
                    $this->_db->update('rwc_transactions_paypal', array('refunded' => date('Y-m-d H:i:s'), 'refunded_total' => $refence[0]['session_cost']), array('id' => $tid));
                    $this->_db->update('rwc_prepaid_sessions', array('refunded' => date('Y-m-d H:i:s')), array('transaction_id' => $tid));
                    $this->_db->update('rwc_paid_membership', array('refunded' => date('Y-m-d H:i:s')), array('transaction_id' => $tid));
                    
                    return true;
                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    $this->error_message = json_decode($ex->getData())->message;
                    return false;

                } catch (Exception $ex) {
                    $this->error_message = $ex;
                    return false;
                }
            }
        }
       
        
    }
    public function refundParticipant($ppid, $type) {

        require __DIR__.'/../../../../themes/rampworld/modules/vendor/autoload.php';
        $this->setApi();

        //get_amount

        switch($type) {
            case 'session' :
                $reference = $this->_db->get_results($this->_db->prepare('SELECT `cost`, `id`, `payment_id`, rwc_transactions_paypal.refunded_total as refunded_total FROM rwc_prepaid_sessions LEFT JOIN rwc_transactions_paypal ON rwc_prepaid_sessions.transaction_id = rwc_transactions_paypal.id WHERE prepaid_id = %d AND rwc_prepaid_sessions.refunded IS NULL AND rwc_prepaid_sessions.used_on IS NULL LIMIT 1', $ppid), ARRAY_A);
                if(empty($reference)) {
                    $refunded = $this->_db->get_results($this->_db->prepare('SELECT date_format(`refunded`, "%H:%i %d-%m-%Y") as refunded, date_format(`used_on`, "%H:%i %d-%m-%Y") as used_on FROM rwc_prepaid_sessions WHERE prepaid_id = %d', $ppid), ARRAY_A);

                    if(strlen($refunded[0]['used_on']) != null)
                        $this->error_message = 'Unable to refund participant as they have already attended on the '.$refunded[0]['used_on'].'. Please contact customer service if this options is still required.';
                    else
                        $this->error_message = 'This transaction has already been refunded on the '.$refunded[0]['refunded'];
                    
                    $this->error_message = 'This transaction has already been refunded on the '.$refunded[0]['refunded'];
                    return false;
                } else {
                    $amount = new Amount();
                    $amount->setTotal($reference[0]['cost'])
                        ->setCurrency('GBP');
                    
                    $refund = new Refund();
                    $refund->setAmount($amount);

                    $sale = new Sale();
                    $sale->setId($this->_getSaleId($reference[0]['payment_id']));

                    try {
                        $refundedSale = $sale->refund($refund, $this->_api);
                        $this->_db->update('rwc_transactions_paypal', array('refunded' => date('Y-m-d H:i:s'), 'refunded_total' => number_format($reference[0]['refunded_total'] + $reference[0]['cost'], 2)), array('id' => $reference[0]['id']));
                        $this->_db->update('rwc_prepaid_sessions', array('refunded' => date('Y-m-d H:i:s')), array('prepaid_id' => $ppid));
                        
                        return true;
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                        $this->error_message = json_decode($ex->getData())->message;
                        return false;

                    } catch (Exception $ex) {
                        $this->error_message = $ex;
                        return false;
                    }
                }
                break;
            case 'membership' :
                $reference = $this->_db->get_results($this->_db->prepare('SELECT `cost`, `id`, `paid_membership_id`, payment_id, rwc_transactions_paypal.refunded_total as refunded_total FROM rwc_paid_membership LEFT JOIN rwc_transactions_paypal ON rwc_paid_membership.transaction_id = rwc_transactions_paypal.id WHERE paid_membership_id = %d AND rwc_paid_membership.refunded IS NULL AND rwc_paid_membership.used_on IS NULL LIMIT 1', $ppid), ARRAY_A);
                if(empty($reference)) {
                    $refunded = $this->_db->get_results($this->_db->prepare('SELECT date_format(`refunded`, "%H:%i %d-%m-%Y") as refunded, date_format(`used_on`, "%H:%i %d-%m-%Y") as used_on FROM rwc_paid_membership WHERE paid_membership_id = %d', $ppid), ARRAY_A);
                    if(strlen($refunded[0]['used_on']) != null)
                        $this->error_message = 'Unable to refund participant as they have already attended on the '.$refunded[0]['used_on'].'. Please contact customer service if this options is still required.';
                    else
                        $this->error_message = 'This transaction has already been refunded on the '.$refunded[0]['refunded'];
                    return false;
                } else {
                    $amount = new Amount();
                    $amount->setTotal($reference[0]['cost'])
                        ->setCurrency('GBP');
                    
                    $refund = new Refund();
                    $refund->setAmount($amount);

                    $sale = new Sale();
                    $sale->setId($this->_getSaleId($reference[0]['payment_id']));

                    try {
                        $refundedSale = $sale->refund($refund, $this->_api);
                        $this->_db->update('rwc_transactions_paypal', array('refunded' => date('Y-m-d H:i:s'), 'refunded_total' => number_format($reference[0]['refunded_total'] + $reference[0]['cost'], 2)), array('id' => $reference[0]['id']));
                        $this->_db->update('rwc_paid_membership', array('refunded' => date('Y-m-d H:i:s')), array('paid_membership_id' => $ppid));
                        
                        return true;
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                        $this->error_message = json_decode($ex->getData())->message;
                        return false;

                    } catch (Exception $ex) {
                        $this->error_message = $ex;
                        return false;
                    }
                }
                break;
        }
        
    }
    public function update_used($ppid, $type = 'session') {
        if( $type == 'session' ){
            return $this->_db->update('rwc_prepaid_sessions', array('used_on' => date('Y-m-d H:i:s')), array('prepaid_id' => intval($ppid)));
        } else {
            return $this->_db->update('rwc_paid_membership', array('used_on' => date('Y-m-d H:i:s')), array('paid_membership_id' => intval($ppid)));
        }
    }
}