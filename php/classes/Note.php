<?php
class Note {
	public $errors;
	private $_db;
	public $notes = array();
	public $members = array();


	public function __construct() {
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}

	/**
	 * Undocumented function
	 *
	 * @param int $nid
	 * @param array $data
	 * @return boolean
	 */
	public function update($nid, $data) {
		$current_user = wp_get_current_user();
		if($this->_db->update('rwc_members_notes',
			array(
				'start_date'	=> sanitize_text_field($data['start_date']),
				'end_date'		=> sanitize_text_field($data['end_date']),
				'type'				=> intval($data['type']),
				'comments'		=> sanitize_text_field($data['comments']),
				'last_updated_by' => $current_user->user_firstname.' '. $current_user->user_lastname,
				'updated_amount'	=> intval($data['updated_amount'] + 1)
			), array(
				'note_id' => $nid
			), array(
				'%s','%s','%d','%s','%s','%d'
			), array(
				'%d'
			)) === false) {
				return false;
			} else {
				return true;
			}
	}
	public function add($mid, $data) {
		require_once 'Member.php';
		$member = new Member();
		$member->get(intval($mid));
		if(count($member->members) == 0) {
			wp_redirect($_SERVER['HTTP_REFERER']. '&error=incorrect+member');
		} else {
			$current_user = wp_get_current_user();
			if($this->_db->insert('rwc_members_notes', array(
				'member_id'				=> intval($data['mid']),
				'start_date'			=> sanitize_text_field($data['start_date']),
				'end_date'				=> sanitize_text_field($data['end_date']),
				'type'						=> intval($data['type']),
				'comments'				=> sanitize_text_field($data['comments']),
				'last_updated_by' => $current_user->user_firstname.' '. $current_user->user_lastname,

			), array(
				'%d','%s','%s','%d','%s','%s'
			)) === false) {
				wp_redirect($_SERVER['HTTP_REFERER'].'&error=couldnt+save');
				die();
			} else {
				return true;
				//wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fview%2Fall&success&member='.$mid);	
			}
		}
	}

	public function getMember($memberID) {
		$this->notes = $this->_db->get_results($this->_db->prepare('SELECT note_id, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, type, comments, updated_amount, created, last_updated_time, last_updated_by FROM rwc_members_notes WHERE member_id = %d ORDER BY start_date DESC, end_date DESC', array($memberID)), ARRAY_A);
		$this->members = $this->_db->get_results($this->_db->prepare('SELECT forename, surname, dob, email, number FROM rwc_members WHERE member_id = %d LIMIT 1', array($memberID)), ARRAY_A);
	}
	public function delete($nid) {
		if(!current_user_can('administrator')){
			wp_redirect($_SERVER['HTTP_REFERER'].'&errors=incorrect+permissions');
		} else {
			$details = $this->_db->get_results( $this->_db->prepare('SELECT member_id FROM rwc_members_notes WHERE note_id = %d', array(intval($nid))), ARRAY_A);
			if(count($details) > 0) {
				if($this->_db->delete('rwc_members_notes', array(
					'note_id' 	=> intval($nid)
				), array(
					'%d'
				)) === false) {
					wp_redirect($_SERVER['HTTP_REFERER'].'&errors=couldnt+remove');
				} else {
					
					wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fview%2Fall&member='.$details[0]['member_id'].'&removed');
				}
			} else {
				wp_redirect($_SERVER['HTTP_REFERER'].'&errors=couldnt+remove');
			}
		}
	}
}