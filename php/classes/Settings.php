<?php
date_default_timezone_set('Europe/London');
class Settings {

	private $_db;
	public $html;


	public function __construct() {
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function getSettings($category_id) {
		$isEmpty = 0;
		if(is_array($category_id)) {
			for($i = 0; $i < count($category_id); $i++){
				$settings_raw = $this->_db->get_results($this->_db->prepare('SELECT * FROM '.database_prefix.'dashboard_settings WHERE category = %d',array($category_id[$i])), ARRAY_A);

		   		if(!empty($settings_raw)) {
		   			
		   			for($i = 0; $i < count($settings_raw); $i++) {
		   				$this->html .= '<tr id="'.$settings_raw[$i]['setting_id'].'" data-no-action="true">
		   					<td><p class="medium">'.$settings_raw[$i]['display_name'].'</p></td>';

	   					switch ($settings_raw[$i]['accepted_type']) {
	   						case 1:
	   							//number
								$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="number" > <input type="number" class="rwc_new" id="setting_'.$settings_raw[$i]['setting_id'].'_value" value="'.$settings_raw[$i]['value'].'"></td>';

	   							break;
	   						case 2:
	   							//boolean
								$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="boolean"><select id="setting_'.$settings_raw[$i]['setting_id'].'_value">';

								$this->html .= ($settings_raw[$i]['value'] == 'true')? '<option selected value="true">True</option><option value="false">False</option></select>' : '<option selected value="false">False</option><option value="true">True</option></select></td>';

	   							break;
	   						case 3:
	   							$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="text"><input type="text" class="rwc_new" id="setting_'.$settings_raw[$i]['setting_id'].'_value" value="'.$settings_raw[$i]['value'].'" data-type="text"></td>';
	   							break;
	   						case 4:
	   							$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="day">
	   											<select id="setting_'.$settings_raw[$i]['setting_id'].'_value"><option selected '.($settings_raw[$i]['value'] == '1')?"selected":''.'  value="1">Monday</option><option selected '.($settings_raw[$i]['value'] == '2')?"selected":''.'  value="2">Tuesday</option><option selected '.($settings_raw[$i]['value'] == '3')?"selected":''.'  value="3">Wednesday</option><option selected '.($settings_raw[$i]['value'] == '4')?"selected":''.'  value="4">Thursday</option><option selected '.($settings_raw[$i]['value'] == '5')?"selected":''.'  value="5">Friday</option><option selected '.($settings_raw[$i]['value'] == '6')?"selected":''.'  value="6">Sunday</option><option selected '.($settings_raw[$i]['value'] == '0')?"selected":''.'  value="0">Sunday</option></select></td>';
	   						default:
	   							# text
	   							$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="text"><input type="text" class="rwc_new"  id="setting_'.$settings_raw[$i]['setting_id'].'_value" value="'.$settings_raw[$i]['value'].'" data-type="text"></td>';
	   							break;
	   					}
	   					$this->html .= '</tr>';
		   				if(!empty($settings_raw[$i]['description']))
		   					$this->html .= '<tr><td colspan="2"><small>'.$settings_raw[$i]['description'].'</small></tr>';

		   			}

		   			
		   		} else {
		   			$isEmpty++;
		   		}
			}
			if($isEmpty < count($category_id))
				return true;
			else
				return false;
		} else {
			$settings_raw = $this->_db->get_results($this->_db->prepare('SELECT * FROM '.database_prefix.'dashboard_settings WHERE category = %d',array($category_id)), ARRAY_A);

	   		if(!empty($settings_raw)) {
	   			
	   			for($i = 0; $i < count($settings_raw); $i++) {
	   				$this->html .= '<tr id="'.$settings_raw[$i]['setting_id'].'" data-no-action="true">
	   					<td><p class="medium">'.$settings_raw[$i]['display_name'].'</p></td>';

	   					switch ($settings_raw[$i]['accepted_type']) {
	   						case 1:
	   							//number
								$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="number" > <input type="number" id="setting_'.$settings_raw[$i]['setting_id'].'_value" value="'.$settings_raw[$i]['value'].'"></td>';

	   							break;
	   						case 2:
	   							//boolean
								$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="boolean"><select id="setting_'.$settings_raw[$i]['setting_id'].'_value">';

								$this->html .= ($settings_raw[$i]['value'] == 'true')? '<option selected value="true">True</option><option value="false">False</option></select>' : '<option selected value="false">False</option><option value="true">True</option></select></td>';

	   							break;
	   						
	   						default:
	   							# text
	   							$this->html .= '<td data-original="'.$settings_raw[$i]['value'].'" data-type="text"><input type="text" id="setting_'.$settings_raw[$i]['setting_id'].'_value" value="'.$settings_raw[$i]['value'].'" data-type="text"></td>';
	   							break;
	   					}
	   					$this->html .= '</tr>';
	   				if(!empty($settings_raw[$i]['description']))
	   					$this->html .= '<tr><td colspan="2"><small>'.$settings_raw[$i]['description'].'</small></tr>';

	   			}

	   			return true;
	   		} else {
	   			return false;
	   		}
		}
	}
	public function isHire() {
		$res = $this->_db->get_results($this->_db->prepare('SELECT 1 FROM rwc_dashboard_settings WHERE cmd_name = %s LIMIT 1', array("enable_hire")), ARRAY_A);
		return $res[0];
	}
}