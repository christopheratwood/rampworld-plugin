<?php
date_default_timezone_set('Europe/London');
class PaidSession {

	private $_db,
			$_processed;
	public $sessions = array();
	public function __construct() {
		
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function getMember($memberId, $start, $end) {
		$s = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ',$start))))));
		$e = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ',$end))))));

		$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT prepaid_id, transaction_id, session_date, session_start_time, session_end_time FROM rwc_prepaid_sessions WHERE member_id = %d AND session_date BETWEEN %s AND %s ORDER BY session_date ASC, session_end_time ASC, session_start_time ASC', array($memberId, $s->format('Y-m-d'), $e->format('Y-m-d'))), ARRAY_A); 
	}
}