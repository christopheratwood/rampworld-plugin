<?php
date_default_timezone_set('Europe/London');
class Holiday {
	private $_db;
	public $holidays = array();
	public $count = null;
	public $sessions = array();
	public function __construct() {
		
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function getAll($holiday_id = null, $start = null, $end = null, $start_no, $limit) {
	

		if($holiday_id == null && $start == null && $end == null) {
				$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, visible FROM rwc_holidays ORDER BY start_date ASC, end_date ASC LIMIT %d, %d', array($start_no, $limit)), ARRAY_A);
				$this->count = $this->_db->get_results('SELECT count(*) as count FROM rwc_holidays', ARRAY_A)[0]['count'];
		} else {

			if($holiday_id != null) {
				$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, visible FROM rwc_holidays  WHERE holiday_id = %d ORDER BY start_date ASC, end_date ASC LIMIT %d, %d', array($holiday_id, $start_no, $limit)), ARRAY_A);
				
				$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays WHERE holiday_id = %d',array($holiday_id)), ARRAY_A)[0]['count'];
			
			} else {

				if($start != null && $end == null ) {

					$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, visible FROM rwc_holidays  WHERE %s BETWEEN start_date AND end_date ORDER BY start_date ASC, end_date ASC LIMIT %d, %d', array($start, $start_no, $limit)), ARRAY_A);

					$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays WHERE %s BETWEEN start_date AND end_date',array($start)), ARRAY_A)[0]['count'];
			
				} else if($start == null  && $end != null ) {
					$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, visible FROM rwc_holidays  WHERE %s BETWEEN start_date AND end_date ORDER BY start_date ASC, end_date ASC LIMIT %d, %d', array($end, $start_no, $limit)), ARRAY_A);
					
					$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays WHERE %s BETWEEN start_date AND end_date',array($end)), ARRAY_A)[0]['count'];
				} else if($start != null  && $end != null ) {
						$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%d-%%m-%%Y") as start_date, DATE_FORMAT(end_date, "%%d-%%m-%%Y") as end_date, visible FROM rwc_holidays  WHERE %s BETWEEN start_date AND end_date AND %s BETWEEN start_date AND end_date ORDER BY start_date ASC, end_date ASC LIMIT %d, %d', array($start, $end, $start_no, $limit)), ARRAY_A);
					
						$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays WHERE %s BETWEEN start_date AND end_date AND %s BETWEEN start_date AND end_date',array($start, $end)), ARRAY_A)[0]['count'];
				}
			}
		}
		
	}

	public function get($holiday_id) {
		$this->holidays = $this->_db->get_results($this->_db->prepare('SELECT rwc_holidays.*, count(rwc_holidays_sessions.holiday_id) as sessions FROM rwc_holidays LEFT JOIN rwc_holidays_sessions ON rwc_holidays.holiday_id = rwc_holidays_sessions.holiday_id WHERE rwc_holidays.holiday_id = %d  ORDER BY start_date ASC, end_date ASC LIMIT 1', array($holiday_id)), ARRAY_A);
		
		if(count($this->holidays) > 0 && $this->holidays[0]['visible'] != 0)
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members_entries WHERE entry_date BETWEEN %s AND %s ',array($this->holidays[0]['start_date'], $this->holidays[0]['end_date'])), ARRAY_A)[0]['count'];
	}
	public function getList() {
		$this->holidays = $this->_db->get_results('SELECT holiday_id, display_name FROM rwc_holidays ORDER BY start_date DESC', ARRAY_A);
	}
	public function update($holiday_id, $data) {

		$current_user = wp_get_current_user();
		//update main
		
		if($this->_db->update('rwc_holidays', array(
			'display_name'				=> $data['display_name'],
			'start_date'					=> $data['start_date'],
			'end_date'						=> $data['end_date'],
			'visible'							=> intval($data['visible']),
			'last_updated_time'		=> date('Y-m-d H:i:s'),
			'last_updated_user'		=> $current_user->user_firstname.' '. $current_user->user_lastname,
			'updated_amount'			=> intval($data['update_counter']) + 1

		),array(
			'holiday_id'						=> intval($holiday_id)
		), array('%s', '%s', '%s', '%d', '%s', '%s', '%d'), array('%d')) === false) {
			return false;
			
		}
	}
	public function delete($hid) {
		if(!current_user_can('administrator')){
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&hid='.$hid.'&error=incorrect+permissions');
		} else {
			$this->_db->delete('rwc_holidays_sessions', array('holiday_id' => $hid));
			$this->_db->delete('rwc_holidays', array('holiday_id' => $hid));
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall&success=deleted+holiday&hid='.$hid);


		}
	}
	public function create($data) {
		if(!is_array($data)) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fcreate&error=incorrect+format');
				die();
		} else {
			$overlap = $this->_db->get_results($this->_db->prepare('SELECT holiday_id FROM rwc_holidays WHERE %s BETWEEN start_date AND end_date OR  %s BETWEEN start_date AND end_date LIMIT 1', array($data['start_date'], $data['end_date'])), ARRAY_A);
			if(count($overlap) != 0) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fsingle&error=overlap&hid='.$overlap[0]['holiday_id']);
				die();
			}else {
				$this->_db->insert('rwc_holidays', array(
					'display_name'	=> $data['display_name'],
					'start_date'		=> $data['start_date'],
					'end_date'			=> $data['end_date'],
					'visible'				=> intval($data['visible'])
				), array('%s','%s','%s','%d'));			

				if($data['inherit'] == "1") {
					$start_f = DateTime::createFromFormat('Y-m-d',$data['start_date']);
					$end_f = DateTime::createFromFormat('Y-m-d',$data['end_date']);

					$id = $this->_db->insert_id;
							

					$str = '';
					$days = array();
					$where = '';

					if(!in_array($start_f->format( "w" ), $days)){
						array_push($days, $start_f->format( "w" ));
						$where .= (strlen($where) != 0)? ' OR ': '';
						$where .= 'day = '.$start_f->format('w');
					}
					if(!in_array($end_f->format( "w" ), $days)){
						array_push($days, $end_f->format( "w" ));
						$where .= (strlen($where) != 0)? ' OR ': '';
						$where .= 'day = '.$end_f->format('w');
					}

					$begin = new DateTime( $start_f->format('Y-m-d') );
					$end = new DateTime( $end_f->format('Y-m-d') );

					$interval = DateInterval::createFromDateString('6 hour');
					$period = new DatePeriod($begin, $interval, $end);

					foreach ( $period as $dt ) {
						if(!in_array($dt->format( "w" ), $days)){
							array_push($days, $dt->format( "w" ));
							$where .= (strlen($where) != 0)? ' OR ': '';
							$where .= 'day = '.$dt->format('w');
						}
					}
					$default_sessions = $this->_db->get_results('SELECT * FROM rwc_default_holiday_sessions WHERE '.$where.' ORDER BY day ASC, start_time ASC ', ARRAY_A);

					for($i = 0; $i < count($default_sessions); $i++) {
						if(strlen($str) != 0)
							$str .= ', ';
						$pr = (empty($default_sessions[$i]['private']) ||  $default_sessions[$i]['private'] == '0') ? 'null' : $default_sessions[$i]['private'];
						$beg = (empty($default_sessions[$i]['beginner']) ||  $default_sessions[$i]['beginner'] == '0') ? 'null' : $default_sessions[$i]['beginner'];
						$consec = (empty($default_sessions[$i]['consecutive']) ||  $default_sessions[$i]['consecutive'] == '0') ? 'null' : 
						$default_sessions[$i]['consecutive'];


						$str .= '('.$id.', \''.$default_sessions[$i]['start_time'].'\', \''.$default_sessions[$i]['end_time'].'\', '.$default_sessions[$i]['day'].', \''.$default_sessions[$i]['display_name'].'\' , \''.$default_sessions[$i]['restrictions'].'\',  '.$consec.', '.$beg.', '.$pr.')';
					}
					if($this->_db->query('INSERT INTO rwc_holidays_sessions (`holiday_id`, `start_time`, `end_time`, `day`, `display_name`, `restrictions`, `consecutive`, `beginner`, `private`) VALUES '.$str ) === false) {
						wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fcreate&error=couldnt+save');
					} else {
						wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fsingle&hid='.$id.'&success=created');

					}
				} else {
					wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fsingle&hid='.$id.'&success=created');
					die();
				}
			}
		}
	}
	public function getSessions($hid, $day, $start_no, $limit) {
		if($hid == null && $day == null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT session_id as holiday_session_id, null as holiday_id, start_time, end_time, day, display_name, restrictions, consecutive, beginner, private FROM rwc_sessions ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array( $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results('SELECT count(*) as count FROM rwc_sessions', ARRAY_A)[0]['count'];
		} elseif($hid == null && $day != null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT session_id as holiday_session_id, null as holiday_id, start_time, end_time, day, display_name, restrictions, consecutive, beginner, private FROM rwc_sessions WHERE day = %d ORDER BY day ASC, start_time DESC, end_time ASC LIMIT %d, %d', array( intval($day), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_sessions WHERE day = %d',array(intval($day))), ARRAY_A)[0]['count'];
		} elseif($hid != null && $day == null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_holidays_sessions WHERE holiday_id = %d ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array(intval($hid), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays_sessions WHERE holiday_id = %d ',array($hid)), ARRAY_A)[0]['count'];

		} elseif($hid != null && $day != null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array($hid, intval($day), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d',array($hid, $day)), ARRAY_A)[0]['count'];
		}
	}
	public function getHolidayID() {
		$res = $this->_db->get_results("SELECT `holiday_id` FROM `rwc_holidays` WHERE start_date <= CURDATE() AND end_date >= Curdate() LIMIT 1", ARRAY_A);
		return (!empty($res))? $res[0]['holiday_id'] : null;
	}
	public function getDates() {
		return $this->_db->get_results('SELECT * FROM `rwc_holidays` WHERE end_date >= CURDATE()', ARRAY_A);
	}
	public function getDatesTable() {
		$dates = $this->getDates();
		if(!empty($dates)) {
			$table = '<table class="clean large" id="holiday_dates"><thead>
				<tr data-no-action="true"><th>Display Name</th><th>Start Date</th><th>End Date</th><th>Visible</th></tr></thead><tbody>';
			foreach($dates as $date){
				$table .= '<tr data-date-id="'.$date['holiday_id'].'" data-holiday>
				<td data-label="Display Name" data-name="'.$date['display_name'].'">'.$date['display_name'].'</td>
				<td data-label="Start Date" data-date="'.$date['start_date'].'">'.date('l, jS \of F Y',strtotime($date['start_date'])).'</td>
				<td data-label="End Date"  data-date="'.$date['end_date'].'">'.date('l, jS \of F Y',strtotime($date['end_date'])).'</td>
				<td data-label="Visible">'.(($date['visible'] == "1")?"Yes":"No").'</td></tr>';
				
			}
			$table .= '</tbody></table>';
			return $table;
		} else {
			return false;
		}
	}
	public function all($json = false) {

		return (!$json) ? $this->_db->get_results('SELECT holiday_id, display_name, start_date, end_date FROM rwc_holidays', ARRAY_A): json_encode($this->_db->get_results('SELECT holiday_id, start_date, end_date FROM rwc_holidays', ARRAY_A));
	}

}