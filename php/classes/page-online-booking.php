<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @si
 */
get_header();



?>
<div class="page-title">
	<p>Online Booking</p>
</div>
<div id="rw_blog_wrapper"><!--white background-->
   
   <!-- Start main content -->
   <!--<div class="container main-content clearfix">-->
    
    <!--<div class="sixteen columns">-->
    <div class="rampworld_mainbody">
    	<div class="rampworld_mainbody_content">
			<div class="membership-container">
<div class="top">
<h1>Book your session!</h1>
<p class="medium"><span class="blue highlight">Brand new!</span> RampWorld Cardiff now offers the ability to pre book your sessions at RampWorld Cardiff!  The service is avilable to all RampWorld Cardiff members with a valid Unique membership number.  Your can register for the participant unique <a href="https://www.rampworldcardiff.co.uk/membership"  class="blue highlight">HERE</a></p>

</div>

<h1>Required information</h1>
<p class="medium">Please make sure you have the following information at hand before starting your booking. Thank you.</p>

<div class="list-group"><a class="list-group-item">
<h4 class="list-group-item-heading">Payment</h4>
<p class="list-group-item-text">This services uses PayPal as a payment gateway. This services does not require yourself to be a PayPal member.</p></a><a class="list-group-item">
<h4 class="list-group-item-heading">Membership Numbers</h4>
<p class="list-group-item-text">Before beginning, you MUST know all member's unique membership number. This is the code given upon completing the registration form.  A copy of you membership number should be sent to the primary email address.</p>

</a><a class="list-group-item">
<h4 class="list-group-item-heading">Membership Numbers</h4>
<p class="list-group-item-text">Before beginning, you MUST know all member's unique membership number. This is the code given upon completing the registration form.  A copy of you membership number should be sent to the primary email address.</p>

</a><a class="list-group-item">
<h4 class="list-group-item-heading">Non Refundable</h4>
<p class="list-group-item-text">If you purchase a session, the session will only be valid for the date and time provided at the booking stage. It is important to check the correct date (Located at the top of the page) and the session time.</p>

</a><a class="list-group-item">
<h4 class="list-group-item-heading">Booking Information</h4>
<p class="list-group-item-text">We do not keep any sensative information regarding your purchase. We do require a valid email including name and contact number.  We do recomended bring the emailed receipt when arriving</p>

</a><a class="list-group-item">
<h4 class="list-group-item-heading">Upon Arrival</h4>
<p class="list-group-item-text">Upon arrival, you will be asked for the membership numbers which you provided during the booking stage.  We do recomended bring the emailed receipt.</p>

</a></div>


<div class="bottom"><!--<a href="/book-your-session" class="btn btn-success">Ready? Book your session!</a>--></div>
</div>
</div>
</div>
<?php
get_footer();