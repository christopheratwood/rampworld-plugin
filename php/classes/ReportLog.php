<?php

class ReportLog {

	private $_db,
			$_processed;
	public $history = array();
	public $report_id  = null;
	public function __construct() {
		
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function getLatest($numberOfRecords) {
		$this->history = $this->_db->get_results($this->_db->prepare('SELECT display_name, staff, report_uri, date_generated FROM rwc_report_log LEFT JOIN rwc_report_types ON rwc_report_log.report_id = rwc_report_types.report_type_id ORDER BY date_generated DESC LIMIT %d',array($numberOfRecords)), ARRAY_A);

	}
	public function add($report, $staff, $uri) {
		$this->_db->insert('rwc_report_log', array(
			'report_id'	=> intval($report),
			'staff'			=> $staff,
			'report_uri'=> $uri
		), array('%d', '%s', '%s'));
	}
}