<?php
	use Dompdf\Dompdf;
	use Dompdf\Options;
date_default_timezone_set('Europe/London');
class Report {
	private $_db;

	public function __construct() {
		
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function createMember($memberId, $startDate, $endDate) {
		require_once 'Member.php';
		require_once 'Entries.php';
		require_once 'PaidMembership.php';
		require_once 'PaidSession.php';
		require_once 'Notes.php';
		require_once 'Terms.php';
		$member = new Member();
		$entries = new Entries();
		$memberships = new PaidMembership();
		$session = new PaidSession();
		$notes = new  notes();
		$terms = new  terms();

		$member->get($memberId);
		$entries->getMember($memberId, $startDate, $endDate);
		$memberships->getMember($memberId, $startDate, $endDate);
		$session->getMember($memberId, $startDate, $endDate);
		$notes->getMember($memberId);
		$terms->getByDate($member->members[0]['registered']);
		print_r($notes->notes);
		require_once __DIR__.'/../../assets/templates/gui/reports/html/member.php';
		$this->render('Member '.$memberId.' Report', $template);
		   
	}
	public function createDuration($start, $end=null, $forename = null, $surname = null, $membership_id = null, $page = null, $limit = null) {
		require_once 'Session.php';
		$session = new Session();

		$session->getTotal($start, $end, $forename, $surname, $membership_id, $page, $limit);

		require_once __DIR__.'/../../assets/templates/gui/reports/html/duration.php';
	 	if($end == null)
			$this->render('Activity '.$start, $template);
		else 
			$this->render('Activity betwwen '.$start.'-'.$end, $template); 
	}
	public function render($fileName, $html) {

	ini_set('memory_limit', '512M');
	set_time_limit(0);
	

		$option = new Options();
		$option->setIsRemoteEnabled(true);
		$dompdf = new DOMPDF($option);

		$dompdf->load_html($html);
		
		$dompdf->render();

		$invnoabc = $fileName.'.pdf';

		ob_end_clean();
		$dompdf->stream($invnoabc);
		
	}
}