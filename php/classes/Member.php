<?php
date_default_timezone_set('Europe/London');
class Member
{
    public $numOfResults ;
    public $members = array();
    public $emergency = array();
    public $consent = array();
    public $consentIsPresent,
            $count;
    public $asi = null;
    private $_db;
    private $_member_details;
    private $_results;
    private $_database_prefix = "rwc_";
    public $error = array();

    public $bookings = array();
    public $is_booking = false;


    public function __construct()
    {
        
        $this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    }
        
    public function getMemberEntry($member_id)
    {
        $this->members = $this->_db->get_results($this->_db->prepare('
			SELECT mem.`member_id`,
			mem.`forename`,
			mem.`surname`,
			mem.`dob`,
			mem.`discipline`,
			mem.`medical_notes`,
			mem.`expertise`,
			mem.`additional_verification`,
            mem.`staff_verification`,
            notes.comments as note_comment,
			CASE
                WHEN CURRENT_DATE() BETWEEN paid.`start_date` AND paid.`end_date`
                    AND paid.`premature` IS NULL THEN type.`display_name`
                ELSE NULL
			END AS has_membership,
			CASE
                WHEN entries.`entry_date` = CURRENT_DATE()
                    AND entries.`premature_exit` IS NULL
                    AND entries.`exit_time` >= CURRENT_TIME() THEN (CONCAT(date_format(entries.`entry_time`, "%%l:%%i %%p"), ",", date_format(entries.`exit_time`, "%%l:%%i %%p")))
                ELSE NULL
            END AS alreadyOnPremises,
            CASE WHEN entries.`entry_date` = CURRENT_DATE()
                AND entries.`premature_exit` IS NULL
                AND entries.`exit_time` >= CURRENT_TIME() THEN entries.entry_id
            ELSE NULL
            END AS currentSession,
			CASE
                WHEN
                    mem.`member_id` = pp.`member_id`
                    AND pp.refunded IS NULL
                    AND pp.used_on IS NULL
                    AND pp.session_date = CURRENT_DATE()
                    AND (pp.session_end_time > CURRENT_TIME() OR pp.session_end_time = "00:00:00")
                    AND pp.transaction_id IS NOT NULL THEN pp.transaction_id
                WHEN 
                    mem.member_id = paid.member_id
                    AND paid.refunded IS NULL
                    AND paid.used_on IS NULL
                    AND paid.end_date >= CURRENT_DATE()
                    AND paid.transaction_id IS NOT NULL THEN paid.transaction_id
                ELSE NULL
            END AS has_booking,
			CASE
                WHEN CURRENT_DATE <= ban.end_date AND (ban.type = 1 OR ban.type = 2) THEN ban.comments
                ELSE NULL
			END AS is_banned
			FROM `rwc_members` AS mem
			LEFT JOIN `rwc_paid_membership` AS paid ON mem.`member_id` = paid.`member_id`
				AND paid.`premature` IS NULL
				AND CURRENT_DATE() BETWEEN paid.`start_date` AND paid.`end_date`
			LEFT JOIN `rwc_prepaid_sessions` AS pp ON mem.`member_id` = pp.`member_id`
				AND pp.refunded IS NULL
				AND pp.used_on IS NULL
				AND pp.session_date = CURRENT_DATE()
				AND (pp.session_end_time > CURRENT_TIME() OR pp.session_end_time = "00:00:00")
				AND pp.transaction_id IS NOT NULL
			LEFT JOIN `rwc_members_entries` AS entries ON mem.`member_id` = entries.`member_id`
				AND entries.`entry_date` = CURRENT_DATE()
				AND entries.`premature_exit` IS NULL
				AND entries.`exit_time` >= CURRENT_TIME()
			LEFT JOIN `rwc_members_notes` AS notes ON mem.`member_id` = notes.`member_id`
                AND CURRENT_DATE() BETWEEN notes.`start_date` AND notes.end_date
            LEFT JOIN `rwc_members_notes` AS ban ON mem.`member_id` = ban.`member_id` AND
                CURRENT_DATE() BETWEEN ban.`start_date` AND ban.end_date AND (ban.type = 1 OR ban.type = 2)
            LEFT JOIN rwc_membership_types AS type ON paid.`membership_type` = type.`membership_type_id`
            LEFT JOIN rwc_transactions_paypal AS book ON ( pp.transaction_id = book.id OR paid.transaction_id = book.id)
			WHERE mem.`member_id` = %d
			ORDER BY mem.`member_id` ASC, notes.created DESC
            LIMIT 1', array( $member_id)), ARRAY_A);

        //$this->_addNotes();
    }
    public function getMembersEntry($field_1, $input_1, $field_2 = null, $input_2 = null)
    {
        $remap = array('forename'=> '`mem`.forename','surname'=> '`mem`.surname','date of birth'=> '`mem`.dob','phone number'=> '`mem`.number','address line 1'=> '`mem`.address_line_one','postcode'=> '`mem`.postcode', 'booking name' => '`book`.name', 'booking number' => '`book`.id', 'booking email' => '`book`.email' );
        $inputs_values= array();
        if( strpos($field_1, "booking") !== false || strpos($field_2, "booking") !== false ) {
            $this->is_booking = true;
        
            if ($input_1 != null) {

                if($input_1 != 'booking number') {
                    $query .= ' AND ' . $remap[$field_1] . ' LIKE  \'%%%s%%\'';
                } else {
                    $query .= ' AND ' . $remap[$field_1] . ' = %d';
                }
                $inputs_values[] = $input_1;
            }
            if ($input_2 != null) {
                if($input_1 != 'booking number') {
                    $query .= ' AND ' . $remap[$field_2] . ' LIKE  \'%%%s%%\'';
                } else {
                    $query .= ' AND ' . $remap[$field_2] . ' = %d';
                }
                
                $inputs_values[] = $input_2;
            }
            $this->bookings = $this->_db->get_results($this->_db->prepare('
            SELECT book.`id` as booking_id,
                book.`name`,
                book.`email`,
                book.`number`,
                book.purchased,
                book.refunded,
                SUM(book.session_cost + book.booking_cost) as total,
                count(paid.transaction_id) as countPaid,
                count(pre.transaction_id) as countSession,
                paid.paid_membership_id
            FROM `rwc_transactions_paypal` AS book
            left JOIN rwc_paid_membership as paid ON book.id = paid.`transaction_id` AND paid.end_date >= CURDATE() AND paid.used_on IS NULL
            
            left JOIN rwc_prepaid_sessions as pre ON book.id = pre.`transaction_id` AND pre.session_date = CURDATE() AND pre.used_on IS NULL
            WHERE
                book.complete = 1 AND  (paid.`paid_membership_id` IS NOT NULL OR pre.transaction_id IS NOT NULL)  
                    '.$query.'
                GROUP BY book.id  ORDER BY book.`id` ASC LIMIT 500', $inputs_values), ARRAY_A);

        } else {
            $query = '';
            

            if ($input_1 != null) {
                $query .= ' WHERE ' . $remap[$field_1] . ' LIKE  \'%%%s%%\'';
                $inputs_values[] = $input_1;
            }
            if ($input_2 != null) {
                if ($query == '') {
                    $query .= ' WHERE ' . $remap[$field_2].' LIKE  \'%%%s%%\'';
                } else {
                    $query .= ' AND '. $remap[$field_2].' LIKE  \'%%%s%%\'';
                }
                $inputs_values[] = $input_2;
            }
                    
            $this->members = $this->_db->get_results($this->_db->prepare('
                SELECT mem.`member_id`,
                mem.`forename`,
                mem.`surname`,
                mem.`dob`,
                mem.`discipline`,
                mem.`medical_notes`,
                mem.`expertise`,
                mem.`additional_verification`,
                mem.`staff_verification`,
                notes.comments as note_comment,
                CASE
                    WHEN CURRENT_DATE() BETWEEN paid.`start_date` AND paid.`end_date`
                        AND paid.`premature` IS NULL THEN type.`display_name`
                    ELSE NULL
                END AS has_membership,
                CASE
                    WHEN entries.`entry_date` = CURRENT_DATE()
                        AND entries.`premature_exit` IS NULL
                        AND entries.`exit_time` >= CURRENT_TIME() THEN (CONCAT(date_format(entries.`entry_time`, "%%l:%%i %%p"), ",", date_format(entries.`exit_time`, "%%l:%%i %%p")))
                    ELSE NULL
                END AS alreadyOnPremises,
                CASE WHEN entries.`entry_date` = CURRENT_DATE()
                    AND entries.`premature_exit` IS NULL
                    AND entries.`exit_time` >= CURRENT_TIME() THEN entries.entry_id
                    ELSE NULL
                END AS currentSession,
                CASE
                    WHEN mem.`member_id` = pp.`member_id`
                        AND pp.refunded IS NULL
                        AND pp.used_on IS NULL
                        AND pp.session_date = curdate()
                        AND pp.session_end_time > CURRENT_TIME()
                        AND pp.transaction_id IS NOT NULL THEN pp.transaction_id
                    ELSE NULL
                END AS has_booking,
                CASE
                    WHEN CURRENT_DATE <= ban.end_date AND (ban.type = 1 OR ban.type = 2)  THEN ban.comments
                    ELSE NULL
                END AS is_banned
                FROM `rwc_members` AS mem
                LEFT JOIN `rwc_paid_membership` AS paid ON mem.`member_id` = paid.`member_id`
                    AND paid.`premature` IS NULL
                    AND CURRENT_DATE() BETWEEN paid.`start_date` AND paid.`end_date`
                LEFT JOIN `rwc_prepaid_sessions` AS pp ON mem.`member_id` = pp.`member_id`
                    AND pp.refunded IS NULL
                    AND pp.used_on IS NULL
                    AND pp.session_date = curdate()
                    AND pp.session_start_time <= CURRENT_TIME() + INTERVAL 30 MINUTE
                    AND pp.session_end_time > CURRENT_TIME()
                    AND pp.transaction_id IS NOT NULL
                LEFT JOIN `rwc_members_entries` AS entries ON mem.`member_id` = entries.`member_id`
                    AND entries.`entry_date` = CURRENT_DATE()
                    AND entries.`premature_exit` IS NULL
                    AND entries.`exit_time` >= CURRENT_TIME()
                LEFT JOIN `rwc_members_notes` AS notes ON mem.`member_id` = notes.`member_id`
                    AND CURRENT_DATE() BETWEEN notes.`start_date` AND notes.end_date
                LEFT JOIN `rwc_members_notes` AS ban ON mem.`member_id` = ban.`member_id` AND
                    CURRENT_DATE() BETWEEN ban.`start_date` AND ban.end_date AND (ban.type = 1 OR ban.type = 2)
                LEFT JOIN rwc_membership_types AS type ON paid.`membership_type` = type.`membership_type_id`
                LEFT JOIN rwc_transactions_paypal AS book ON ( pp.transaction_id = book.id OR paid.transaction_id = book.id)
                '.$query.'
                GROUP BY mem.member_id  ORDER BY mem.`member_id` ASC, UNIX_TIMESTAMP(notes.created) DESC LIMIT 500', $inputs_values), ARRAY_A);
            //$this->_addNotes();
            //echo $this->_db->last_query;
        }
    }
    public function getViewMember($member_id)
    {
                
        $this->members = $this->_db->get_results($this->_db->prepare('SELECT `forename`, `surname`,  TIMESTAMPDIFF(YEAR,dob,CURDATE()) AS age, CASE WHEN gender = "m" THEN "Male"
		WHEN gender = "f" THEN "Female" 
		ELSE "Other" END AS gender, `email`, DATE_FORMAT(dob, "%%d-%%m-%%Y") as dob, `number`, `address_line_one`, `address_line_two`, `address_city`, `address_county`, `address_postcode`, `consent_id`, `discipline`, `expertise`, `medical_notes`, `registered`, `staff_verification` FROM `rwc_members` WHERE `member_id` = %d', array(intval($member_id))), ARRAY_A);

        if (count($this->members) > 0) {
            $this->members[0]['member_id'] = $member_id;
            $this->members[0]['emergency'] = $this->_db->get_results($this->_db->prepare('SELECT name, emergency_number from '.$this->_database_prefix.'members_emergency where member_id = %d', array(intval($member_id))), ARRAY_A);
            if ($this->members[0]['consent_id'] != 0) {
                $this->members[0]['consent'] = $this->_db->get_results($this->_db->prepare('SELECT name, number, address_line_one, address_postcode, email from '.$this->_database_prefix.'members_consent where consent_id = %d ', array(intval($this->members[0]['consent_id']))), ARRAY_A);
                $this->members[0]['consent'] = $this->members[0]['consent'][0];
                $this->consentIsPresent = true;
            } else {
                $this->members[0]['consent'] = array();
            }
        }
    }
    public function getMembersAsJSON($fc1q, $fc2q, $and, $dif, $fc1d, $fc2d)
    {
                
                $this->_member_details = $this->_db->get_results('SELECT 
									mem.`member_id`, 
									mem.`forename`, 
									mem.`surname`, 
									mem.`dob`, 
									mem.`discipline`, 
									mem.`medical_notes`, 
									mem.`expertise`, 
									mem.`additional_verification`, 
									mem.`staff_verification`, 
									settings.`value`, 
									CASE WHEN paid.`start_date` <= CURDATE() 
									AND paid.`end_date` >= CURDATE() 
									AND paid.`premature` IS NULL THEN type.`display_name` ELSE 0 END AS has_membership, 
									CASE WHEN entries.`entry_date` = CURRENT_DATE() 
									AND entries.`premature_exit` IS NULL AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute  THEN 1 ELSE 0 END AS alreadyOnPremises,
									CASE when mem.`member_id` = pp.`member_id` AND pp.refunded IS null AND pp.used_on IS NULL AND pp.session_date = curdate() AND pp.session_end_time > CURRENT_TIME() AND pp.transaction_id IS NOT NULL THEN pp.prepaid_id ELSE null END as ppSession
								FROM 
									`rwc_dashboard_settings` as settings, 
									`rwc_members` as mem 
									LEFT JOIN `rwc_paid_membership` as paid ON mem.`member_id` = paid.`member_id` 
									AND paid.`premature` IS NULL 
									AND paid.`end_date` >= CURDATE() 
									AND paid.`start_date` <= CURDATE() 
									LEFT JOIN `rwc_members_entries` as entries ON mem.`member_id` = entries.`member_id` 
									AND entries.`entry_date` = CURRENT_DATE() 
									AND entries.`premature_exit` IS NULL
										AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute
										LEFT JOIN rwc_membership_types as type ON paid.`membership_type` = type.`membership_type_id`
									LEFT JOIN `rwc_prepaid_sessions` as pp ON mem.`member_id` = pp.`member_id` 
									AND pp.refunded IS null 
									AND pp.used_on IS NULL
									AND pp.session_date = curdate()
									AND pp.session_start_time <= CURRENT_TIME() + INTERVAL 30 MINUTE
									AND pp.session_end_time > CURRENT_TIME()
									AND pp.transaction_id IS NOT NULL
								WHERE 
									settings.`cmd_name` = \'staff_verification\' and '.$fc1q. $and .$fc2q.'
								GROUP BY mem.`member_id`
								ORDER BY 
									mem.`member_id` ASC, '.$fc1d. $dif . $fc2d, ARRAY_A);
                
                
        $this->count = count($this->_member_details);
        if (count($this->_member_details) > 0) {
            $this->_addNotes();

                
            return json_encode($this->_member_details);
        } else {
            return json_encode(array());
        }
    }
    public function getMember($member_id)
    {


        $this->_member_details = $this->_db->get_results($this->_db->prepare('SELECT 
				mem.
				settings.`value`, 
				CASE WHEN paid.`start_date` <= CURDATE() 
				AND paid.`end_date` >= CURDATE() 
				AND paid.`premature` IS NULL THEN type.`display_name` ELSE 0 END AS has_membership, 
				CASE WHEN entries.`entry_date` = CURRENT_DATE() 
				AND entries.`premature_exit` IS NULL AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute  THEN 1 ELSE 0 END AS alreadyOnPremises,
				CASE when pp.member_id = mem.member_id  AND pp.refunded IS null && pp.used_on IS NULL AND pp.session_date = curdate() and pp.session_start_time >= CURRENT_TIME() - INTERVAL 30 minute AND pp.session_end_time > CURRENT_TIME() THEN pp.prepaid_id ELSE null END as ppSession
			FROM 
				`rwc_dashboard_settings` as settings, 
				`rwc_members` as mem,
				`rwc_prepaid_sessions` as pp
				LEFT JOIN `rwc_paid_membership` as paid ON mem.`member_id` = paid.`member_id` 
				AND paid.`premature` IS NULL 
				AND paid.`end_date` >= CURDATE() 
				AND paid.`start_date` <= CURDATE() 
				LEFT JOIN `rwc_members_entries` as entries ON mem.`member_id` = entries.`member_id` 
				AND entries.`entry_date` = CURRENT_DATE() 
				AND entries.`premature_exit` IS NULL
					AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute
					LEFT JOIN rwc_membership_types as type ON paid.`membership_type` = type.`membership_type_id`
				
			WHERE 
				settings.`cmd_name` = %s and mem.`member_id` = %d
			ORDER BY 
				mem.`member_id` ASC LIMIT 1', array("staff_verification", $member_id)), ARRAY_A);

        if (count($this->_member_details) > 0) {
            $this->_addNotes();
                        
            return $this->_member_details;
        } else {
            return false;
        }
    }
    public function verify($member_id)
    {
        $current_user = wp_get_current_user();
        $user = $current_user->first_name .' '. $current_user->last_name;
        if ($this->_db->update('rwc_members', array('staff_verification' => $user), array('member_id' => $member_id) ) === false) {
            $this->error[] = $this->_db->last_error;
            return false;
        } else {
            return true;
        }
    }

        /* pagination */
    public function getAll($member_id = null, $searchField = null, $searchQuery = null, $searchField2 = null, $searchQuery2 = null, $start_no, $limit)
    {
        if ($searchField != null) {
            switch ($searchField) {
                case 'date of birth':
                    $searchField = 'dob';
                    break;
                case 'phone number':
                    $searchField = 'number';
                    break;
                case 'address line 1':
                    $searchField = 'address_line_one';
                    break;
                case 'postcode':
                    $searchField = 'address_postcode';
                    break;
            }
        }
        if ($searchField2 != null) {
            switch ($searchField2) {
                case 'date of birth':
                    $searchField2 = 'dob';
                    break;
                case 'phone number':
                    $searchField2 = 'number';
                    break;
                case 'address line 1':
                    $searchField2 = 'address_line_one';
                    break;
                case 'postcode':
                    $searchField2 = 'address_postcode';
                    break;
            }
        }

        if ($member_id == null && $searchField == null && $searchField2 == null) {
            $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob  FROM rwc_members ORDER BY member_id LIMIT %d, %d', array($start_no, $limit)), ARRAY_A);
            $this->count = $this->_db->get_results('SELECT count(*) as count FROM rwc_members', ARRAY_A)[0]['count'];
        } else {
            if ($member_id != null && $searchField == null && $searchField2 == null) {
                $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members WHERE member_id = %d ORDER BY member_id LIMIT %d, %d', array($member_id, $start_no, $limit)), ARRAY_A);
                                
                $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE member_id = %d', array($member_id)), ARRAY_A)[0]['count'];
            } elseif ($member_id == null && $searchField != null && $searchField2 == null) {
                $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members WHERE '.$searchField.' LIKE \'%%%s%%\' ORDER BY member_id LIMIT %d, %d', array( $searchQuery, $start_no, $limit)), ARRAY_A);
                        
                $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE '.$searchField.' LIKE \'%%%s%%\'', array( $searchQuery)), ARRAY_A)[0]['count'];
            } elseif ($member_id == null && $searchField == null  && $searchField2 != null) {
                        $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members WHERE '.$searchField2.' LIKE \'%%%s%%\' ORDER BY member_id LIMIT %d, %d', array( $searchQuery2, $start_no, $limit)), ARRAY_A);

                        $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE '.$searchField2.' LIKE \'%%%s%%\'', array($searchQuery2)), ARRAY_A)[0]['count'];
            } elseif ($member_id == null && $searchField != null  && $searchField2 != null) {
                        $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members
					WHERE '.$searchField.' LIKE \'%%%s%%\' AND '.$searchField2.' LIKE \'%%%s%%\' ORDER BY member_id LIMIT %d, %d', array($searchQuery, $searchQuery2, $start_no, $limit)), ARRAY_A);
                        $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE '.$searchField.' LIKE \'%%%s%%\' AND '.$searchField2.' LIKE \'%%%s%%\'', array($searchQuery, $searchQuery2)), ARRAY_A)[0]['count'];
            } elseif ($member_id != null && $searchField != null  && $searchField2 != null) {
                        $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members
					WHERE member_id  = %d AND '.$searchField.' LIKE \'%%%s%%\' AND '.$searchField2.' LIKE \'%%%s%%\' ORDER BY member_id LIMIT %d, %d', array($member_id, $searchQuery, $searchQuery2, $start_no, $limit)), ARRAY_A);
                        $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE member_id  = %d AND '.$searchField.' LIKE \'%%%s%%\' AND '.$searchField2.' LIKE \'%%%s%%\'', array($member_id, $searchQuery, $searchQuery2)), ARRAY_A)[0]['count'];
            } elseif ($member_id != null && $searchField == null  && $searchField2 != null) {
                        $this->members = $this->_db->get_results($this->_db->prepare('SELECT member_id, forename, surname, number, dob FROM rwc_members
					WHERE member_id  = %d AND '.$searchField2.' LIKE \'%%%s%%\' ORDER BY member_id LIMIT %d, %d', array($member_id, $searchQuery2, $start_no, $limit)), ARRAY_A);
                        $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members WHERE member_id  = %d AND '.$searchField2.' LIKE \'%%%s%%\'', array($member_id, $searchQuery2)), ARRAY_A)[0]['count'];
            }
        }
    }
    public function get($member_id)
    {
        $this->members = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members WHERE member_id = %d LIMIT 1', array($member_id)), ARRAY_A);
        $this->emergency = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members_emergency WHERE member_id = %d ', array($member_id)), ARRAY_A);
        if ($this->members[0]['consent_id'] != '0') {
            $this->consent =   $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members_consent WHERE consent_id = %d ', array($this->members[0]['consent_id'])), ARRAY_A);
        }
    }
    public function update($member_id, $data)
    {

        $current_user = wp_get_current_user();
        //update main
        if ($data['cid'] == '0' &&  strlen($data['consent_name']) > 0) {
            if ($this->_db->insert('rwc_members_consent', array(
                    'name'                              => $data['consent_name'],
                    'number'                            => $data['consent_number'],
                    'address_line_one'      => $data['consent_address_one'],
                    'address_postcode'      => $data['consent_postcode'],
                    'email'                                 => $data['consent_email']
            ), array('%s', '%s', '%s', '%s', '%s')) === false) {
                return false;
            } else {
                $data['cid'] = $this->_db->insert_id;
            }
        } elseif ($data['cid'] != 0) {
            if ($this->_db->update('rwc_members_consent', array(
                    'name'                              => $data['consent_name'],
                    'number'                            => $data['consent_number'],
                    'address_line_one'      => $data['consent_address_one'],
                    'address_postcode'      => $data['consent_postcode'],
                    'email'                 => $data['consent_email'],
            ), array(
                    'consent_id'         => $data['cid']
            ), array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'), array('%d')) === false) {
                return false;
            }
        }
        if ($this->_db->update('rwc_members', array(
                'forename'                      => $data['forename'],
                'surname'                           => $data['surname'],
                'gender'                            => $data['gender'],
                'dob'                                   => $data['dob'],
                'email'                                 => $data['email'],
                'number'                            => $data['number'],
                'address_line_one'      => htmlspecialchars($data['address_line_one']),
                'address_line_two'      => htmlspecialchars($data['address_line_two']),
                'address_city'              => htmlspecialchars($data['address_city']),
                'address_county'            => htmlspecialchars($data['address_county']),
                'address_postcode'      => $data['address_postcode'],
                'consent_id'                    => intval($data['cid']),
                'discipline'                    => implode(',', $data['discipline']),
                'expertise'                         => intval($data['expertise']),
                'medical_notes'                 => $data['medical_notes'],
                'last_updated_time'         => date('Y-m-d H:i:s'),
                'last_updated_user'         => $current_user->user_firstname.' '. $current_user->user_lastname,
                'updated_amount'            => intval($data['update_counter']) + 1

        ), array(
                'member_id'                         => intval($member_id)
        ), array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d', '%s', '%s','%s','%d'), array('%d')) === false) {
            return false;
        }


        if ($this->_db->update('rwc_members_emergency', array(
                'name'                  => $data['emergency_name_one'],
                'emergency_number'      => $data['emergency_number_one']
        ), array(
                'emergency_id'              => intval($data['eoid'])
        ), array('%s', '%s'), array('%d')) === false) {
            return false;
        }

        if ($data['etid'] == "0" && ( strlen($data['emergency_name_two']) > 0 && strlen($data['emergency_number_two']) >0 )) {
            if ($this->_db->insert('rwc_members_emergency', array(
                    'member_id'                     => intval($member_id),
                    'name'                          => $data['emergency_name_one'],
                    'emergency_number'  => $data['emergency_number_one']
            ), array('%d', '%s', '%s')) === false) {
                return false;
            }
        } elseif ($data['etid'] != "0") {
            if ($this->_db->update('rwc_members_emergency', array(
                    'name'                              => $data['emergency_name_two'],
                    'emergency_number'      => $data['emergency_number_two']
            ), array(
                    'emergency_id'              => intval($data['etid'])
            ), array('%s', '%s'), array('%d')) === false) {
                echo $this->_db->last_query;
                die();
                return false;
            }
        }
    }
    public function delete($member_id)
    {
        if (!current_user_can('administrator')) {
            wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&error=incorrect+permissions&member='.$member_id);
        } else {
            $member = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members WHERE member_id = %d LIMIT 1', array($member_id)), ARRAY_A);
            if (count($member) == 0) {
                wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&error=incorrect+member&member='.$member_id);
            } else {
                $emergency = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members_emergency WHERE member_id = %d', array($member_id)), ARRAY_A);

                if ($member[0]['consent_id'] != 0) {
                    $consent = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_members_consent WHERE consent_id = %d LIMIT 1', array($member[0]['consent_id'])), ARRAY_A);
                }

                if ($this->_db->insert('rwc_deleted_members', array(
                        'forename'                                  => $member[0]['forename'],
                        'surname'                                   => $member[0]['surname'],
                        'gender'                                        => $member[0]['gender'],
                        'dob'                                           => $member[0]['dob'],
                        'email'                                         => $member[0]['email'],
                        'number'                                        => $member[0]['number'],
                        'address_line_one'                  => $member[0]['address_line_one'],
                        'address_line_two'                  => $member[0]['address_line_two'],
                        'address_city'                          => $member[0]['address_city'],
                        'address_county'                        => $member[0]['address_county'],
                        'address_postcode'                  => $member[0]['address_postcode'],
                        'discipline'                                => $member[0]['discipline'],
                        'expertise'                                 => $member[0]['expertise'],
                        'medical_notes'                         => $member[0]['medical_notes'],
                        'registered'                                => $member[0]['registered'],
                        'additional_verification'   => $member[0]['additional_verification'],
                        'staff_verification'                => $member[0]['staff_verification'],
                        'last_updated_time'                 => $member[0]['last_updated_time'],
                        'last_updated_user'                 => $member[0]['last_updated_user'],
                        'updated_amount'                        => $member[0]['updated_amount'],
                        'name'                                          => (($member[0]['consent_id'] != 0)? $consent[0]['name']:''),
                        'consent_number'                        => (($member[0]['consent_id'] != 0)? $consent[0]['number']:''),
                        'consent_address_line_one'  => (($member[0]['consent_id'] != 0)? $consent[0]['address_line_one']:''),
                        'consent_address_postcode'  => (($member[0]['consent_id'] != 0)? $consent[0]['address_postcode']:''),
                        'consent_email'                         => (($member[0]['consent_id'] != 0)? $consent[0]['email']:''),
                        'emergency_name_one'                => $emergency[0]['name'],
                        'emergency_number_one'          => $emergency[0]['emergency_number'],
                        'emergency_name_two'                => (isset($emergency[1]['name'])? $emergency[1]['name']:''),
                        'emergency_number_two'          => (isset($emergency[1]['emergency_number'])? $emergency[1]['emergency_number']:'')
                )) !== false ) {
                    $this->_db->delete('rwc_members_emergency', array('member_id' => $member_id));
                    $this->_db->delete('rwc_members_consent', array('consent_id' => $member[0]['consent_id']));
                    $this->_db->delete('rwc_members', array('member_id' => $member_id));
                                        
                    wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&success=deleted+member&member='.$member_id);
                } else {
                    echo '<pre>'; print_r(array(
                        'forename'                                  => $member[0]['forename'],
                        'surname'                                   => $member[0]['surname'],
                        'gender'                                        => $member[0]['gender'],
                        'dob'                                           => $member[0]['dob'],
                        'email'                                         => $member[0]['email'],
                        'number'                                        => $member[0]['number'],
                        'address_line_one'                  => $member[0]['address_line_one'],
                        'address_line_two'                  => $member[0]['address_line_two'],
                        'address_city'                          => $member[0]['address_city'],
                        'address_county'                        => $member[0]['address_county'],
                        'address_postcode'                  => $member[0]['address_postcode'],
                        'discipline'                                => $member[0]['discipline'],
                        'expertise'                                 => $member[0]['expertise'],
                        'medical_notes'                         => $member[0]['medical_notes'],
                        'registered'                                => $member[0]['registered'],
                        'additional_verification'   => $member[0]['additional_verification'],
                        'staff_verification'                => $member[0]['staff_verification'],
                        'last_updated_time'                 => $member[0]['last_updated_time'],
                        'last_updated_user'                 => $member[0]['last_updated_user'],
                        'updated_amount'                        => $member[0]['updated_amount'],
                        'name'                                          => (($member[0]['consent_id'] != 0)? $consent[0]['name']:''),
                        'consent_number'                        => (($member[0]['consent_id'] != 0)? $consent[0]['number']:''),
                        'consent_address_line_one'  => (($member[0]['consent_id'] != 0)? $consent[0]['address_line_one']:''),
                        'consent_address_postcode'  => (($member[0]['consent_id'] != 0)? $consent[0]['address_postcode']:''),
                        'consent_email'                         => (($member[0]['consent_id'] != 0)? $consent[0]['email']:''),
                        'emergency_name_one'                => $emergency[0]['name'],
                        'emergency_number_one'          => $emergency[0]['emergency_number'],
                        'emergency_name_two'                => (isset($emergency[1]['name'])? $emergency[1]['name']:''),
                        'emergency_number_two'          => (isset($emergency[1]['emergency_number'])? $emergency[1]['emergency_number']:'')
                    ));echo '</pre>';
                    echo $this->_db->last_error;
                    echo $this->_db->last_query;
                    die();
                    wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&error=could+not+backup&member='.$member_id.'+'.$this->_db->last_error);
                }
            }
        }
    }

    public function renderMember($mode = null)
    {
        for ($i = 0; $i < count($this->_member_details); $i++) {
            $this->_member_details[$i]['status'] = $this->_getMemberStatus($i);
            $this->_setExpertise($i);
            $this->_member_details[$i]['formattedNotes'] = $this->_createSystemNotes($i);

            $this->_results = '<tr data-id="0" data-member="'.$this->_member_details[$i]['member_id'].'" class="'.(($mode != null)? 'loaded':'').' '.$this->_member_details[$i]['status']['class'].'" '. (($this->_member_details[$i]['status']['staff_verification'] != null) ? $this->_member_details[$i]['status']['staff_verification'] : '').' '. (($this->_member_details[$i]['status']['title'] != null) ? $this->_member_details[$i]['status']['title']: '').' '.(($mode != null)? 'data-type="'.$mode.'"':'').'><td data-label="Member Number">'.$this->_member_details[$i]['member_id'].'</td><td data-label="Forename">'.$this->_member_details[$i]['forename'].'</td><td data-label="Surname">'.$this->_member_details[$i]['surname'].'</td><td data-label="Age">'.$this->getAge($i).'</td><td data-label="Expertise">'.$this->_member_details[$i]['expertise'].'</td><td data-label="Discipline">'.$this->_member_details[$i]['discipline'].'</td><td data-label="Medical Notes">'.$this->_member_details[$i]['medical_notes'].'</td><td data-label="System Notes">'.$this->_member_details[$i]['formattedNotes'].'</td></tr>';
        }

        return $this->_results;
    }
    public function getAge($index)
    {
        $date = new DateTime($this->_member_details[$index]['dob']);
        $now = new DateTime();
        return $now->diff($date)->y;
    }
    private function _noResults()
    {
        return '<tr><td colspan="7">No Results</td></tr>';
    }
/**
 * Undocumented function
 *
 * @param array $membership_id
 * @param array $forename
 * @param array $surname
 * @param string $start_age
 * @param string $end_age
 * @param array $gender
 * @param array $dob
 * @param array $email
 * @param array $number
 * @param array $address_line_one
 * @param array $address_line_two
 * @param array $address_city
 * @param array $address_county
 * @param array $address_postcode
 * @param string $registered_start_date
 * @param string $registered_end_date
 * @param string $report_start_date
 * @param string $report_end_date
 * @param array $age_groups
 * @param array $disciplines
 * @param array $expertise
 * @param int $start_no
 * @param int $limit
 * @return void
 */
    public function advance(array $membership_id = null, array $forename, array $surname, $start_age, $end_age, array $gender, array $dob, array $email, array $number, array $address_line_one, array $address_line_two, array $address_city, array $address_county, array $address_postcode, $registered_start_date, $registered_end_date, $report_start_date, $report_end_date, array $age_groups, array $disciplines, array $expertise, $start_no, $limit, $uri)
    {
        require_once 'AdvanceSearchLog.php';
        $asl = new AdvanceSearchLog();

        $asl->get(null, $uri);
        if (count($asl->logs) > 0) {
            $bindings = explode('|', $asl->logs[0]['bindings']);
            $query = $asl->logs[0]['query'];
            $join = $asl->logs[0]['join'];
        } else {
            $bindings = array();
            $query = '';
            $join = '';
                
            $elements = array(array('mem.member_id', $membership_id, '%d'), array('forename', $forename, '%s'), array('surname', $surname, '%s'), array('gender', $gender, '%s'), array('dob', $dob, '%s'), array('email', $email, '%s'), array('number', $number, '%s'), array('address_line_one', $address_line_one, '%s'),array('address_line_two', $address_line_two, '%s'), array('address_city', $address_city, '%s'), array('address_county', $address_county, '%s'), array('address_postcode', $address_postcode, '%s'), array('discipline', $disciplines, '%s'), array('expertise', $expertise, '%d'));
            foreach ($elements as $e => $val) {
                if (!empty($val[1])) {
                    if (strlen($query) > 0) {
                        $query .= ' AND (';
                    } else {
                        $query = ' WHERE (';
                    }
                
                    
                    for ($i = 0; $i < count($val[1]); $i++) {
                        if ($val[2] == '%s') {
                            if ($i != 0) {
                                $query .= ' OR '.$val[0].' LIKE \'%%%s%%\'';
                            } else {
                                $query .= ' '.$val[0].' LIKE \'%%%s%%\'';
                            }
                            $bindings[] = trim($val[1][$i]);
                        } else {
                            if ($i != 0) {
                                $query .= ' OR '.$val[0].' = ' . $val[2];
                            } else {
                                $query .= ' '.$val[0].' = ' . $val[2];
                            }
                            $bindings[] = intval(trim($val[1][$i]));
                        }
                    }
                    $query .= ')';
                }
            }
            if ($start_age != '') {
                if (strlen($query) > 0) {
                    $query .= ' AND (';
                } else {
                    $query = ' WHERE (';
                }
                $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d)';
                $bindings[] = intval($start_age);
                $bindings[] = intval($end_age);
            }
    
            if (!empty($age_groups)) {
                if (strlen($query) > 0) {
                    $query .= ' AND (';
                } else {
                    $query = ' WHERE (';
                }
    
                for ($i = 0; $i < count($age_groups); $i++) {
                    if ($i > 0) {
                        $query .= ' OR';
                    }
                    switch ($age_groups[$i]) {
                        case '7-':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) < %d';
                            $bindings[] = 7;
                            break;
                        case '7-10':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d';
                            $bindings[] = 7;
                            $bindings[] = 10;
                            break;
                        case '11-13':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d';
                            $bindings[] = 11;
                            $bindings[] = 13;
                            break;
                        case '14-17':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d';
                            $bindings[] = 14;
                            $bindings[] = 17;
                            break;
                        case '18-21':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d';
                            $bindings[] = 18;
                            $bindings[] = 21;
                            break;
                        case '22-30':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN %d AND %d';
                            $bindings[] = 22;
                            $bindings[] = 30;
                            break;
                        case '30>':
                            $query .= ' TIMESTAMPDIFF(YEAR, dob, CURDATE()) > %d';
                            $bindings[] = 30;
                            
                            break;
                    }
                }
                $query .= ')';
            }
            if ($registered_start_date != '') {
                if (strlen($query) > 0) {
                    $query .= ' AND (';
                } else {
                    $query = ' WHERE (';
                }
                $query .= ' DATE(registered) BETWEEN %s AND %s)';
                $bindings[] = $registered_start_date;
                $bindings[] = $registered_end_date;
            }
            if ($report_start_date != '') {
                $join = ' LEFT JOIN rwc_members_entries ent ON mem.member_id = ent.member_id';
                if (strlen($query) > 0) {
                    $query .= ' AND (';
                } else {
                    $query = ' WHERE (';
                }
                $query .= ' ent.entry_date BETWEEN %s AND %s)';
                $bindings[] = $report_start_date;
                $bindings[] = $report_end_date;
            }
        }
        if ($uri != "") {
            $asl->add(
                $query, $join, implode('|', $bindings), $uri
            );

            $this->asi = $asl->asi;
        }
        /* echo $join;
        echo $query;
        echo '<pre>';
        print_r($bindings);
        echo '</pre>'; */

        $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members as mem
        '.$join. $query, $bindings), ARRAY_A)[0]['count'];

        $bindings[] = $start_no;
        $bindings[] = $limit;


        $this->members = $this->_db->get_results($this->_db->prepare('SELECT mem.member_id, forename, surname, number, dob FROM rwc_members as mem
        '.$join. $query.' GROUP BY mem.member_id ORDER BY mem.member_id LIMIT %d, %d', $bindings), ARRAY_A);
    }

    public function sendEmail($member_id) {
        require_once __DIR__.'/../../../../themes/rampworld/classes/thirdparty/Mailer.php';
       
        $personal = $this->_db->get_results($this->_db->prepare('
            SELECT forename, surname, dob,
            CASE WHEN gender = "m" THen "Male"
                WHEN gender = "f" THEN "Female"
                WHEN gender = "o" THEN "Other" END AS "gender", 
            mem.email, mem.number, mem.address_line_one, address_line_two,
            address_city, address_county, mem.address_postcode, discipline,
            CASE WHEN expertise = 0 THEN "Beginner"
                WHEN expertise = 1 THEN "Novice"
                WHEN expertise = 2 THEN "Experienced"
                WHEN expertise = 3 THEN "Advanced"
                WHEN expertise = 4 THEN "Expert"
                WHEN expertise = 5 THEN "Professional" END AS "expertise",
            mem.medical_notes,
            con.name as "consent_name", con.number as "consent_number", con.address_line_one as "consent_address_line_one",
            con.address_postcode as "consent_address_postcode", con.email as "consent_email"
        
        
            FROM rwc_members as mem
            LEFT JOIN rwc_members_consent as con ON mem.consent_id = con.consent_id
            WHERE member_id = %d
            LIMIT %d', array($member_id, 1)), ARRAY_A);
        $emergency = $this->_db->get_results($this->_db->prepare('
            SELECT name, emergency_number
            FROM rwc_members_emergency
            WHERE member_id = %d
            LIMIT %d', array(
                $member_id, 2
            )), ARRAY_A);
        
        $personal[0]['emergency_name_1'] = $emergency[0]['name'];
        $personal[0]['emergency_number_1'] = $emergency[0]['emergency_number'];
        
        $personal[0]['emergency_name_2'] = (isset($emergency[1]['name']) ) ? $emergency[1]['name']: '';
        $personal[0]['emergency_number_2'] = (isset($emergency[1]['emergency_number']) ) ? $emergency[1]['emergency_number']: '';
        $mailer = new Mailer($personal[0]);
        $mailer->create_member_reissue($member_id);
       

    }
}
