<?php
date_default_timezone_set('Europe/London');
class Entry {
	public $top = array("totals"=> array(), "head" => '', "body"=>'');
	public $html,
			$count,
			$error  =array();
	private $_data = array();
	private $_entries,
			$_db,
			$_sessions,
			$_member_id,
			$_exit_time,
			$_sid;

	public function __construct() {
		
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function add($member_id, $exit_time, $verify) {
		if($verify) {
			require_once 'Member.php';
			$member = new Member();
			if($member->verify($member_id) === false){
				$this->error = 'asd'.$member->error;
				return false;
			}
		}
		$this->_member_id = $member_id;
		$this->_exit_time = $exit_time;

		$check = $this->_db->get_results($this->_db->prepare("SELECT count(mem.`member_id`) as total, DATE_FORMAT(entries.`exit_time`, '%H:%i') as exit_time FROM rwc_members as mem left join `rwc_members_entries` as entries ON mem.`member_id` = entries.`member_id` WHERE mem.`member_id` = %d AND entries.`entry_date` = CURRENT_DATE() AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute AND entries.`premature_exit` IS NULL",array($member_id)), ARRAY_A);

		if(!empty($check) && $check[0]['total'] > 0) {

			$this->error[] = array('Member already on premises.  To extend their session, please come back after '. date('H:i', strtotime($check[0]['exit_time'] .' - 30 minute')));
			return false;
		} else {

			if($this->_db->insert('rwc_members_entries' , array('member_id' => $this->_member_id, 'entry_time' => Date('H:i:s'), 'exit_time' => $this->_exit_time, 'entry_date' => date('Y-m-d')), array("%d", "%s", "%s", "%s") ) === false) {

				$this->error[] = $this->_db->last_error;
				return false;
			} else {

				$this->_sid = $this->_db->insert_id;
				return true;
			}
		}

	}


	public function delete($entry_id) {

		return $this->_db->update('rwc_members_entries' , array('premature_exit' => date('Y-m-d H:i:s')), array('entry_id' => $entry_id), array('%s'), array('%d'));
	}


	
	public function addHire($equipment_type, $inits) {
		if($this->_db->insert('rwc_equipment_hire', array(
						'member_id' => $this->_member_id,
						'equipment_type' => $equipment_type,
						'session_id'	=> $this->_sid,
						'start_time' => date('H:i:s'),
						'end_time' => $this->_exit_time, 
						'date' => date('Y-m-d'),
						'staff_member' => $inits  )) == false ) {
			$this->error = $this->_db->last_error;
			return false;
		} else {
			return true;
		}
	}
	
	public function getListOfEntries() {
		require_once 'Holiday.php';
		$holiday = new Holiday();
		$holiday_id = $holiday->getHolidayID();


		if($holiday_id != null)

			$this->_sessions  = $this->_db->get_results($this->_db->prepare('SELECT `end_time` FROM `rwc_holidays_sessions` WHERE holiday_id = %d AND `day` = %s AND `end_time`> CURRENT_TIME ORDER BY day ASC, start_time ASC', array($holiday_id, date("w"))), ARRAY_A);
		else
			$this->_sessions  = $this->_db->get_results($this->_db->prepare('SELECT `end_time` FROM `rwc_sessions` WHERE `day` = %s AND `end_time`> CURRENT_TIME ORDER BY day ASC, start_time ASC', array(date("w"))), ARRAY_A);

		$this->_renderEntries();
		//get all entries for today and group by time out asc and time in asc
		
	}
	public function getListOfHire() {
		//get all entries for today and group by time out asc and time in asc
		$entries = $this->_db->get_results( 'SELECT `rwc_equipment_hire`.`member_id`, `rwc_equipment_hire`.`equipment_type`,  `rwc_equipment_hire`.`start_time`,  `rwc_equipment_hire`.`end_time`, `rwc_equipment_hire`.`staff_member`, `rwc_members`.`forename`, `rwc_members`.`surname` FROM `rwc_equipment_hire`  LEFT JOIN  `rwc_members`
			ON `rwc_equipment_hire`.`member_id`  =  `rwc_members`.`member_id` WHERE `date` = CURRENT_DATE AND `end_time` >= NOW() + INTERVAL 10 MINUTE ORDER BY `end_time`  ASC, `start_time` ASC', ARRAY_A );
	

		if( !empty( $this->_entries ) ) {
			for ( $i=0; $i < count( $entries ); $i++ ) {
				$this->_data[Date('H:i',strtotime( $this->_entries[$i]['end_time'] ) ) . ' Session'][] = $this->_entries[$i];
				$this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['end_time'] ) ) . ' Session'] = count( $this->_data[Date( 'H:i', strtotime( $this->_entries[$i]['end_time'] ) ) . ' Session'] );
			}
			foreach ( $this->_data as $key => $value ) {

				$this->html .= '<h1>'.$key.' ('. $this->top['totals'][$key] .')</h1><table class="clean large"><thead><th>Member ID</th><th>Forename</th><th>Surname</th><th>Equipment</th><th>Start Time</th><th>End Time</th><th>Staff Member</th></thead>';
				for ( $i=0; $i < count( $value ); $i++ ) {
					$hire = '';

					switch ($value[$i]['equipment_type']) {
						case '1':
							$hire = 'Scooter';
							break;
						case '2':
							$hire = 'BMX';
							break;
						case '3':
							$hire = 'Skateboard';
							break;
						default:
							$hire = 'Scooter';
							break;
					}
					$this->html .= '<tr>
								<td data-label="Member ID">'.$value[$i]['member_id'].'</td>
								<td data-label="Forename">'.$value[$i]['forename'].'</td>
								<td data-label="Surname">'.$value[$i]['surname'].'</td>
								<td data-label="Equipment">'.$hire.'</td>
								<td data-label="Start Time">'.$value[$i]['start_time'].'</td>
								<td data-label="End Time">'.$value[$i]['end_time'].'</td>
								<td data-label="Staff Member">'.$value[$i]['staff_member'].'</td></tr>';
				}
				$this->html .= '</table>';
			}
		} else {
			$this->html = '<p class="small">No Hire.</p>';
		}
	}
	private function _renderEntries() {
		$this->_getAllEntries();
		if( !empty( $this->_entries ) ) {
			for ( $i=0; $i < count( $this->_entries ); $i++ ) {
				if($this->_entries[$i]['paidMembership'] == '0') {
					$this->_data[Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session'][] = $this->_entries[$i];
					if(!isset($this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session']))
						$this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session'] = 1;
					else
						$this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session']++;
				} else {
					
					$this->_data[$this->_entries[$i]['paidMembership']][] = $this->_entries[$i];
				
					if(!isset($this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session']))
						$this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session'] = 1;
					else
						$this->top['totals'][Date('H:i',strtotime( $this->_entries[$i]['exit_time'] ) ) . ' Session'] += 1;
					
					$this->top['totals'][$this->_entries[$i]['paidMembership']] = count($this->_data[$this->_entries[$i]['paidMembership']] );


				}

			}
			$today = new DateTime(date('d-m-Y H:i:s')); 
			foreach ( $this->_data as $key => $value ) {
				
				$this->html .= '<h1>'.$key.' (<span id="session_total" style="display:inline-block !important; font-size:1em !important; line-height: 2em !important;">'. $this->top['totals'][$key] .'</span>)</h1><table class="clean large" data-exit-time="'. str_replace(' Session', '', $key).'"><thead><th>Forename</th><th>Surname</th><th>Time in</th><th>Age</th></thead>';
				for ( $i=0; $i < count( $value ); $i++ ) {
					$dob = DateTime::createFromFormat('Y-m-d',$value[$i]['dob']);
		    
			        $errors = DateTime::getLastErrors();
			        if (!empty($errors['warning_count'])) {
			     		$dob = 'Error.';
			        } else {
			        	$dob = $today->diff($dob);
						$dob = $dob->y;
			        }
					$this->html .= '<tr id="'.$value[$i]['entry_id'].'" data-member-id="'.$value[$i]['member_id'].'" data-remove-item="true" data-remove-type="session_entry">
							<td data-label="Forename">'.$value[$i]['forename'].'</td>
							<td data-label="Surname">'.$value[$i]['surname'].'</td>
							<td data-label="Entry Time">'.$value[$i]['entry_time'].'</td>
							<td data-label="Age">'.$dob.'</td></tr>';
				}
				$this->html .= '</table>';
			}
			
			
			for ($i = 0; $i < count($this->_sessions); $i++) {
				$this->top['head'] .= '<th>'.$this->_sessions[$i]['end_time'].' Session</th>';
	

				if(!isset($this->top['totals'][Date('H:i',strtotime($this->_sessions[$i]['end_time'])). ' Session']) || $this->top['totals'][Date('H:i',strtotime($this->_sessions[$i]['end_time'])). ' Session'] < 0) {
					$this->top['body'] .= '<td data-label="'.$this->_sessions[$i]['end_time'].' Session">0</td>';
				}else {
					$this->top['body'] .= '<td data-label="'.$this->_sessions[$i]['end_time'].' Session">'.$this->top['totals'][Date('H:i',strtotime($this->_sessions[$i]['end_time'])). ' Session'].' </td>';
				}
			


			}
		} else {
			
			for ($i = 0; $i < count($this->_sessions); $i++) {
				$this->top['head'] = '<th>'.$this->_sessions[$i]['end_time'].' Session</th>';
				$this->top['body'] = '<td data-label="'.$this->_sessions[$i]['end_time'].' Session">0</td>';

			}

			$this->html = '<h1>No Entries.</h1>';
		}
		
	}
	private function _getAllEntries() {
		$this->_entries = $this->_db->get_results( 'SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, `rwc_members_entries`.`exit_time`, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`dob`, CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE() AND `rwc_paid_membership`.`premature` IS NULL THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
		FROM `rwc_members_entries` LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id` LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id` AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE() AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE() AND `rwc_paid_membership`.`premature` IS NULL LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
		WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE AND `rwc_members_entries`.`premature_exit` IS NULL 
		GROUP BY `rwc_members_entries`.`entry_id`
		ORDER BY paidMembership ASC, `exit_time` ASC, `entry_time` ASC   ', ARRAY_A );
		
		$this->count = count($this->_entries);
		
	}

}