<?php

class Terms {
	
	public $incre,
			$prefix,
			$version,
			$next_version,
			$last_error,
			$publish_date,
			$old_version,
			$errors = array(),
			$insert_id;
	public $terms = array();
	public $currentTerms = array();
	public $update_data = array('total' => 0, 'failed' => 0, 'success' => 0, 'databaseAdd' => 0, 'errorDatabaseAdd' => 0);
	
	private $_current_terms,

			$_emails,
			$_rightPadding,
			$_db,
			$_start_time,
			$_end_time;



	public function __construct() {
		
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
		
		$this->_setMeta();
		$this->_setTerms();
		$this->_setVersions();
		
		$this->_rightPadding = strlen(substr(strrchr($this->incre, "."), 1));
		
		$this->_setNextVersion();

	}

	public function getAll() {
		$this->terms = $this->_db->get_results('SELECT * FROM rwc_terms_and_conditions ORDER BY publish_date DESC, version_number ASC', ARRAY_A);
	}
	public function get($tandc_id) {
		$this->terms = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_terms_and_conditions WHERE tandc_id = %d',array(intval($tandc_id))), ARRAY_A);
	}
	public function getCurrentTerms( $attribute ) {
		if($attribute == null)
			return $this->_current_terms;
		else if($attribute == 'content')
			return $this->_current_terms['content'];
		else
			return $this->_current_terms['version_number'];
	}
	
	public function update($content, $publish_date, $version_number, $notify) {
		
		if ($this->_db->insert('rwc_terms_and_conditions', array(
			'publish_date'	=> $publish_date,
			'content'				=> $content,
			'active'				=> 1,
			'version_number'=> $version_number,
			'notified'			=> 0,
			'staff_member'	=> wp_get_current_user()->user_firstname . ' ' . wp_get_current_user()->user_lastname)) == false) {

			$this->last_error = $this->_db->last_error;
			return false;
		} else {
			$this->insert_id = $this->_db->insert_id;
			$this->old_version = $this->version;
			$this->version = $data['version_number'];
			$this->_current_terms['content'] = $data['content'];
			
			$this->_setNextVersion();
			$this->publish_date = $publish_date;
				
			if( $this->_deativateTerms($publish_date))
				return true;
			else
				return false;

			

		}
	}
	public function updateDB() {
		return $this->_db->insert('rwc_change_log', array('tandc_id' => intval($this->_insert_id),'member_id' => intval($this->_emails[$i]['member_id']),'email' => $this->_emails[$i]['email'],'has_emailed_suceed' => 0, 'date' => date("Y-m-d H:i:s")), array('%d', '%d', '%s', '%d','%s'));
	}
	public function sendEmails() {
		
		
		$this->_getFakeData();
		$this->_start_time = new DateTime();

		$review = false;
		require_once '../vendor/autoload.php';
		$mail = new PHPMailer(true);

		//setting vars
		$content = $this->_current_terms['content'];
		$versionNumber = $this->prefix. $this->version;
		$publishDate = date('l \t\h\e jS \o\f F Y', strtotime($this->publish_date));
		
		require_once __DIR__.'/../../templates/email/tandc.php';


		$mail->debug = 0;

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  	// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'newMember.rampworldcardiff@gmail.com';                 // SMTP username
		$mail->Password = 'uUNb4bs<:D`QLFA/`BU`#*';                           // SMTP password
		                    // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;
		$mail->SMTPKeepAlive = true;

		$mail->setFrom('newMember.rampworldcardiff@gmail.com', 'No Reply - RampWorld Cardiff');
		
			
		$mail->isHTML(true);
		$mail->Subject = 'We have changed our Terms and Conditions.';
		$mail->Body = $email ;

		for($i = 0; $i < 10; $i++) {
			$mail->addAddress($this->_emails[0]['email'], ucwords(strtolower($this->_emails[0]['forename']. " " .$this->_emails[0]['surname']))); 
			try {
				$mail->send();
				$mail->clearAddresses();
				$mail->ClearAllRecipients();

				$this->update_data['success']++;
				if( $this->_db->insert('rwc_change_log', array('tandc_id' => intval($this->_insert_id),'member_id' => intval($this->_emails[0]['member_id']),'email' => $this->_emails[0]['email'], 'date' => date("Y-m-d H:i:s"), "has_emailed_suceed" => 1), array('%d', '%d', '%s', '%s', '%d')) != false)
					$this->update_data['databaseAdd']++;
				else
					$this->update_data['errorDatabaseAdd']++;
			} catch (phpmailerException $e) {
				$this->update_data['failed']++;
				if( $this->_db->insert('rwc_change_log', array('tandc_id' => intval($this->_insert_id),'member_id' => intval($this->_emails[0]['member_id']),'email' => $this->_emails[0]['email'],'has_emailed_suceed' => 2, 'date' => date("Y-m-d H:i:s")), array('%d', '%d', '%s', '%d','%s')) != false)
					$this->update_data['databaseAdd']++;
				else
					$this->update_data['errorDatabaseAdd']++;

			} catch (Exception $e) {
				$this->update_data['failed']++;
				if( $this->_db->insert('rwc_change_log', array('tandc_id' => intval($this->_insert_id),'member_id' => intval($this->_emails[0]['member_id']),'email' => $this->_emails[0]['email'], 'has_emailed_suceed' => 0, 'date' => date("Y-m-d H:i:s")), array('%d', '%d', '%s','%d', '%s')) != false)
					$this->update_data['databaseAdd']++;
				else
					$this->update_data['errorDatabaseAdd']++;
			}
		}
		$mail->SmtpClose();
		$this->_end_time = new DateTime();
		return true;
	}

	public function check($data) {
		$erros = array();
		if($data['version_number'] <= $this->version) 
			$this->errors[] = "Version Number must be greater than previous version ({$this->prefix}{$this->version}";
	
		if($data['content'] === $this->_current_terms['content']) 
			$this->errors[] = "Content must be different.";
		
		$date = date('Y-m-d H:i:s');
		if( $data['publish_date'] <= $date)
			$this->errors[] = "Publish date must be in the future.";

		if(empty($this->errors))
			return true;
		else
			return false;

	}
	public function getPublishDate() {
		return date('l \t\h\e jS \o\f F Y \a\t g.m A', strtotime($this->publish_date));
	}
	public function getOldVersion() {
		return $this->prefix . $this->old_version;
	}

	public function getStat($type) {
		return $this->update_data[$type];
	}

	public function getDuration() {
		$diff = $this->_start_time->diff($this->_end_time);
		return "{$diff->h}h, {$diff->i}m, {$diff->s}s ";
	}
	public function getByDate($date) {
		$this->terms = $this->_db->get_results($this->_db->prepare('SELECT tandc_id, content, version_number, active, publish_date, deactivation_date FROM rwc_terms_and_conditions WHERE publish_date <= %s ORDER BY DATEDIFF(publish_date, %s) ASC LIMIT 1', array($date, $date)), ARRAY_A);
	}

	private function _setNextVersion() {
		$this->next_version = number_format(($this->version + $this->incre), $this->_rightPadding);
	}
	private function _setMeta() {
		
		$incr = $this->_db->get_results('SELECT `value` from `rwc_dashboard_settings` where cmd_name = \'tandc_incre\' LIMIT 1', ARRAY_A);
		$this->incre =  (count($incr) > 0) ? $incr[0]['value']: "0.1";

		$prefix = $this->_db->get_results('SELECT `value` from `rwc_dashboard_settings` where  cmd_name = \'tandcs_prefix\' LIMIT 1', ARRAY_A);
		$this->prefix =  (count($prefix) > 0) ? $prefix[0]['value']: "V";

	}
	private function _setTerms() {

		$this->currentTerms  = $this->_db->get_results('SELECT tandc_id, content, version_number, active, publish_date, deactivation_date  FROM `rwc_terms_and_conditions` WHERE deactivation_date IS NULL AND date(publish_date) <= CURRENT_DATE() ORDER BY publish_date DESC, tandc_id DESC LIMIT 1', ARRAY_A);
	
		$this->publish_date =  (count($this->currentTerms) > 0) ? $this->currentTerms[0]['publish_date']: null;
	}
	private function _setVersions() {
		
		$version = $this->_current_terms['version_number'];
		$this->version =  $version;


	}
	private function _getEmailData() {

		$this->_emails = $this->_db->get_results('SELECT `member_id`, `email`, `forename`, `surname` FROM rwc_members', ARRAY_A);
		$this->update_data['total'] = count($this->_emails);
	}
	private function _getFakeData(){
		$this->_emails = array( 0 => array(
							'member_id' => "1", 'email' => "christopheratwood2@gmail.com", 'forename' => 'Chris', 'surname' => 'atwood'),
								1 => array(
								'member_id' => "1", 'email' => "chrisatwood50@gmail.com", 'forename' => 'Chris', 'surname' => 'atwood')
			);
		$this->update_data['total'] = count($this->_emails);
	}
	private function _deativateTerms( $date) {


		if( $this->_db->query($this->_db->prepare(" UPDATE rwc_terms_and_conditions  SET deactivation_date = %s WHERE deactivation_date IS NULL AND tandc_id != %d", array( $date, $this->_insert_id) )) == false)
			return false;
		else
			return true; 
	}

}