<?php
date_default_timezone_set('Europe/London');
class Entries
{
    private $_db;

    public $count = 0;
    public $entries = array();
    public $sessions = array();
		public $topMembers = array();
    public function __construct()
    {
        
        $this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    }

    public function getAll($forename = null, $surname = null, $membership_id = null, $start_dt = null, $end_dt = null, $start_no = null, $limit = 9999)
    {
            

        if ($forename == null && $surname == null && $membership_id == null && $start_dt == null) {
            $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`,`rwc_members`.`expertise`, `rwc_members_entries`.`premature_exit` as premature,
						CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
							AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
							AND `rwc_paid_membership`.`premature` IS NULL
							THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
						FROM `rwc_members_entries`
						LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
						LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
							AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
							AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
							AND `rwc_paid_membership`.`premature` IS NULL
						LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
						WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
							AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
							AND `rwc_members_entries`.`premature_exit` IS NULL 
						GROUP BY `rwc_members_entries`.`entry_id`
						ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results('SELECT COUNT(*)
						FROM `rwc_members_entries`
						LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
						LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
							AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
							AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
							AND `rwc_paid_membership`.`premature` IS NULL
						LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
						WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
							AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
							AND `rwc_members_entries`.`premature_exit` IS NULL', ARRAY_A)[0]['COUNT(*)'];
        } else {
            $forname = ($forename != null) ? str_replace('+', ' ', $forename) : null;
            $surname = ($surname != null) ? str_replace('+', ' ', $surname) : null;
            $membership_id = ($membership_id != null) ? str_replace('+', ' ', $membership_id) : null;
            $start_dt = ($start_dt != null) ? str_replace('%3A', ':', $start_dt) : null;
            $end_dt = ($end_dt != null) ? str_replace('%3A', ':', $end_dt) : null;

            /* forename, not(surname and membership id)*/
            if ($forename != null && $surname == null && $membership_id == null && $start_dt == null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND `rwc_members`.forename LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($forename, $start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND `rwc_members`.forename LIKE \'%%%s%%\'
							', array($forename)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname != null && $membership_id == null && $start_dt == null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`,`rwc_members_entries`.`premature_exit` as premature,  `rwc_members`.`expertise`,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.surname LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array( $surname, $start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.member_id LIKE \'%%%s%%\'
							', array( $surname)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname == null && $membership_id == null && $start_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL 
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $start_no,
                $limit
                    )), ARRAY_A);


                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL 
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1]
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname == null && $membership_id != null && $start_dt == null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
								AND `rwc_members`.member_id = %d
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array( $membership_id,$membership_id, $start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
								AND `rwc_members`.member_id = %d
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.member_id = %d
							', array( $membership_id,$membership_id)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname != null && $membership_id == null  && $start_dt == null) {
                /* forename surname, not( membership id)*/
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
								CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
									AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
									AND `rwc_paid_membership`.`premature` IS NULL
									THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
								FROM `rwc_members_entries`
								LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
								LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
									AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
									AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
									AND `rwc_paid_membership`.`premature` IS NULL
								LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
								WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
									AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
									AND `rwc_members_entries`.`premature_exit` IS NULL 
									AND `rwc_members`.forename LIKE \'%%%s%%\'
									AND `rwc_members`.surname LIKE \'%%%s%%\'
								GROUP BY `rwc_members_entries`.`entry_id`
								ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($forename, $surname, $start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
								FROM `rwc_members_entries`
								LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
								LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
									AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
									AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
									AND `rwc_paid_membership`.`premature` IS NULL
								LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
								WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
									AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
									AND `rwc_members_entries`.`premature_exit` IS NULL 
									AND `rwc_members`.forename LIKE \'%%%s%%\'
									AND `rwc_members`.surname LIKE \'%%%s%%\'
								', array($forename, $surname)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname == null && $membership_id != null && $start_dt == null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.forename LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($forename, $membership_id, $start_no, $limit)), ARRAY_A);
                        
                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.forename LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id` ', array($forename, $membership_id)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname == null && $membership_id == null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s 
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` BETWEEN %s AND %s
								AND `rwc_members_entries`.`entry_time` >= %s
								AND `rwc_members_entries`.`exit_time` <= %s
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname != null && $membership_id != null && $start_dt == null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.surname LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($surname, $membership_id, $start_no, $limit)), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL 
								AND `rwc_members`.surname LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d
							', array($surname, $membership_id)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname != null && $membership_id == null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.surname  LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $surname,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.surname  LIKE \'%%%s%%\'
							', array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $surname
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname == null && $membership_id != null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE 
							 TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $membership_id,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` BETWEEN %s AND %s
								AND `rwc_members_entries`.`entry_time` >= %s
								AND `rwc_members_entries`.`exit_time` <= %s
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[1],
                $membership_id
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname != null && $membership_id != null && $start_dt == null) {
                /* forename surname membership id*/
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND `rwc_members`.forename LIKE \'%%%s%%\'
								AND `rwc_members`.surname LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d 
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', array($forename, $surname, $membership_id, $start_no, $limit)), ARRAY_A);

                $this->count =  $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= CURRENT_DATE()
								AND `rwc_paid_membership`.`end_date` >= CURRENT_DATE()
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE `rwc_members_entries`.`entry_date` = CURRENT_DATE
								AND `rwc_members_entries`.`exit_time` >= NOW() - INTERVAL 10 MINUTE
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND `rwc_members`.forename LIKE \'%%%s%%\'
								AND `rwc_members`.surname LIKE \'%%%s%%\'
								AND `rwc_members`.member_id = %d 
							', array($forename, $surname, $membership_id)), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename == null && $surname != null && $membership_id != null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
								AND rwc_members.surname  LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $membership_id,
                $surname,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
								AND rwc_members.surname  LIKE \'%%%s%%\'
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $membership_id,
                $surname
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname != null && $membership_id == null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
								AND rwc_members.surname  LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename,
                $surname,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
								AND rwc_members.surname  LIKE \'%%%s%%\'
								
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename,
                $surname
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname == null && $membership_id != null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s 
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
								AND rwc_members.member_id = %d
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename,
                $membership_id,
                $start_no,
                $limit
                )), ARRAY_A);

                $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.forename  LIKE \'%%%s%%\'
								AND rwc_members.member_id = %d
							',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $forename,
                $membership_id
                )), ARRAY_A)[0]['COUNT(*)'];
            } elseif ($forename != null && $surname != null && $membership_id != null && $start_dt != null && $end_dt != null) {
                $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT `rwc_members_entries`.`entry_id`, `rwc_members_entries`.`entry_time`, DATE_FORMAT(`rwc_members_entries`.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(`rwc_members_entries`.`exit_time`, "%%H:%%i") as exit_time, `rwc_members`.`member_id`, `rwc_members`.`forename`, `rwc_members`.`surname`, `rwc_members`.`number`, `rwc_members`.`dob`, `rwc_members`.`expertise`,`rwc_members_entries`.`premature_exit` as premature,
							CASE WHEN `rwc_paid_membership`.`start_date` <= %s 
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
								THEN `rwc_membership_types`.`display_name` ELSE 0 END AS paidMembership 
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
								AND rwc_members.surname  LIKE \'%%%s%%\'
								AND rwc_members.forename  LIKE \'%%%s%%\'
							GROUP BY `rwc_members_entries`.`entry_id`
							ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d',
                array(
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $end_dt)[0],
                explode('T', $start_dt)[0],
                explode('T', $start_dt)[1],
                explode('T', $end_dt)[0],
                explode('T', $end_dt)[1],
                $membership_id,
                $surname,
                $forename,
                $start_no,
                $limit
                    )), ARRAY_A);
                    echo $this->_db->last_query;
                    $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
							FROM `rwc_members_entries`
							LEFT JOIN `rwc_members` ON `rwc_members_entries`.`member_id` = `rwc_members`.`member_id`
							LEFT JOIN `rwc_paid_membership` ON `rwc_members`.`member_id` = `rwc_paid_membership`.`member_id`
								AND `rwc_paid_membership`.`start_date` <= %s
								AND `rwc_paid_membership`.`end_date` >= %s
								AND `rwc_paid_membership`.`premature` IS NULL
							LEFT JOIN `rwc_membership_types` ON `rwc_paid_membership`.`membership_type` = `rwc_membership_types`.`membership_type_id`
							WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)
								AND `rwc_members_entries`.`premature_exit` IS NULL
								AND rwc_members.member_id = %d
								AND rwc_members.surname  LIKE \'%%%s%%\'
								AND rwc_members.forename  LIKE \'%%%s%%\'
							',
                    array(
                    explode('T', $start_dt)[0],
                    explode('T', $end_dt)[0],
                    explode('T', $start_dt)[0],
                    explode('T', $start_dt)[1],
                    explode('T', $end_dt)[0],
                    explode('T', $end_dt)[1],
                    $membership_id,
                    $surname,
                    $forename
                    )), ARRAY_A)[0]['COUNT(*)'];
            }
        }
    }

    public function getAllAdvance($query, $join, $bindings, $start, $end)
    {
        echo '0';
        $query .= ' AND TIMESTAMP(ent.`entry_date`, ent.`exit_time`) BETWEEN %s AND %s AND ent.`premature_exit` IS NULL';
        $bindings[] = $start;
        $bindings[] = $end;

        $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT ent.`entry_id`, ent.`entry_time`, DATE_FORMAT(ent.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(ent.`exit_time`, "%%H:%%i") as exit_time, mem.`member_id`, mem.`forename`, mem.`surname`, mem.`number`, mem.`dob`, ent.premature_exit as premature
		FROM rwc_members_entries as ent
		LEFT JOIN rwc_members AS mem ON ent.`member_id` = mem.`member_id`
		'.$query.'
		GROUP BY ent.`entry_id`
		ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC', $bindings), ARRAY_A);

        $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
		FROM rwc_members_entries as ent
		LEFT JOIN rwc_members AS mem ON ent.`member_id` = mem.`member_id`
		'.$query.'
		', $bindings), ARRAY_A)[0]['COUNT(*)'];

    }
        
    public function getAllHoliday($start, $end)
    {
        $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT ent.`entry_id`, ent.`entry_time`, DATE_FORMAT(ent.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(ent.`exit_time`, "%%H:%%i") as exit_time, mem.`member_id`, mem.`forename`, mem.`surname`, mem.`number`, mem.`dob`, ent.premature_exit as premature
			FROM rwc_members_entries as ent
			LEFT JOIN rwc_members AS mem ON ent.`member_id` = mem.`member_id`
			where date(ent.entry_date) BETWEEN %s AND %s
			GROUP BY ent.`entry_id`
			ORDER BY `entry_date` DESC, `exit_time` ASC, `entry_time` ASC', array($start, $end)), ARRAY_A);
    
        $this->count = $this->_db->get_results($this->_db->prepare('SELECT COUNT(*)
			FROM rwc_members_entries as ent
			LEFT JOIN rwc_members AS mem ON ent.`member_id` = mem.`member_id`
			where date(ent.entry_date) BETWEEN %s AND %s
			', array($start, $end)), ARRAY_A)[0]['COUNT(*)'];
    
        echo $this->_db->last_query;
    }

    public function get()
    {
    }
    public function getMember($memberID, $start = null, $end = null, $order = 'DESC')
    {
        if ($start != null) {
            $s = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $start))))));
            $e = DateTime::createFromFormat('m/d/Y H:i A', date('m/d/Y H:i A', strtotime(str_replace('%2F', '/', str_replace('%3A', ':', str_replace('+', ' ', $end))))));

            $this->entries = $this->_db->get_results($this->_db->prepare('SELECT entry_id, entry_time, exit_time, entry_date, premature_exit FROM rwc_members_entries WHERE member_id = %d AND entry_date BETWEEN %s AND %s ORDER BY entry_date '.$order.', exit_time ASC, entry_time ASC', array($memberID, $s->format('Y-m-d'), $e->format('Y-m-d'))), ARRAY_A);
        } else {
            $this->entries = $this->_db->get_results($this->_db->prepare('SELECT entry_id, entry_time, exit_time, DATE_FORMAT(entry_date, "%%d-%%m-%%Y") as entry_date, premature_exit FROM rwc_members_entries WHERE member_id = %d ORDER BY entry_id '.$order.', exit_time ASC, entry_time ASC', array($memberID)), ARRAY_A);
        }
    }
    public function getSessions()
    {
    }
    public function getOverview()
    {
        $hid = $this->_db->get_results('SELECT holiday_id FROM rwc_holidays WHERE CURRENT_DATE BETWEEN start_date AND end_date', ARRAY_A);

        if (count($hid) == 0) {
            return $this->_db->get_results('SELECT
					DATE_FORMAT(sess.end_time, \'%H:%i\') as end_time,
					SUM(discipline LIKE \'%SMX%\') as "Scooter",
					SUM(discipline LIKE \'%BMX%\') as "BMX",
					SUM(discipline LIKE \'%skateboard%\') as "Skateboard",
					SUM(discipline LIKE \'%inline%\') as "Inline",
					SUM(discipline LIKE \'%MTB%\') as "MTB",
					SUM(discipline LIKE \'%spectator%\') as "Spectator",
					SUM(discipline NOT LIKE \'%SMX%\' AND discipline NOT LIKE \'%BMX%\' AND discipline NOT LIKE \'%skateboard%\' AND discipline NOT LIKE \'%inline%\' AND discipline NOT LIKE \'%spectator%\' AND discipline NOT LIKE \'%spectator%\') as "Other",
					count(ent.entry_id) as "total"
					FROM rwc_sessions as sess
				LEFT JOIN  rwc_members_entries as ent ON sess.end_time = ent.exit_time AND ent.entry_date = CURRENT_DATE AND ent.exit_time >= CURRENT_TIME
				LEFT JOIN rwc_members as mem ON ent.member_id = mem.member_id
				WHERE sess.end_time >= CURRENT_TIME
				AND ent.entry_date= CURRENT_DATE
				GROUP BY sess.end_time', ARRAY_A);
        } else {
        }
		}
	public function getTop($number) {
		$this->topMembers = $this->_db->get_results( $this->_db->prepare('
			SELECT count(ent.member_id) as visits, mem.member_id, CONCAT(UCASE(MID(mem.forename,1,1)),LCASE(MID(mem.forename,2))) AS forename, mem.surname, mem.email, 
			FLOOR(SUM((TIME_TO_SEC(ent.exit_time) - TIME_TO_SEC(ent.entry_time)) / 3600)) AS hours 
			from rwc_members_entries as ent
			LEFT JOIN rwc_members as mem ON ent.member_id = mem.member_id
			WHERE ent.member_id = 12
			group by ent.member_id
			order by visits desc, hours DESC
			LIMIT %d', array($number)), ARRAY_A);
	}
}