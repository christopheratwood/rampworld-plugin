<?php
date_default_timezone_set('Europe/London');
class MembershipType {
	public 	$data,
			$types,
			$processed,
			$html = '';
	private $_db,
			$_processed;
	public $passes = array();
	public $memberships = array();
	public function __construct() {
		
		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}

	public function getAll() {
		$this->types = $this->_db->get_results('SELECT  type.*, CountTable.Cnt
			FROM rwc_membership_types type
			LEFT JOIN
				(SELECT  membership_type, COUNT(1) Cnt
					FROM    rwc_paid_membership
					GROUP BY membership_type) CountTable ON type.membership_type_id = CountTable.membership_type
			', ARRAY_A);
	}

	public function add($name, $duration) {
		$inits = wp_get_current_user()->user_firstname . ' '.wp_get_current_user()->user_lastname;

		
		if($this->_db->insert('rwc_membership_types' , array('display_name' => $name, 'duration' => $duration), array('%s', '%d') ) === false) {
			return false;
			
		} else {
			return true;
			
		}
		
	}
	
	function delete($mtid) {

		if(!current_user_can('administrator')){
			wp_redirect($_SERVER['HTTP_REFERE'].'&errors=incorrect+permissions');
		} else {
			if($this->_db->delete('rwc_membership_types', array(
				'membership_type_id' 	=> intval($mtid)
			), array(
				'%d'
			)) === false) {
				wp_redirect($_SERVER['HTTP_REFERER'].'&errors=couldnt+remove');
			} else {
				wp_redirect($_SERVER['HTTP_REFERER'].'&removed');
			}
		}
	}

	function update($mtid, $data) {

		$current_user = wp_get_current_user();
		//update main
		
		if($this->_db->update('rwc_membership_types', array(
			'display_name'					=> $data['display_name'],
			'duration'							=> intval($data['duration']),
			'last_updated_time'		=> date('Y-m-d H:i:s'),
			'last_updated_user'		=> $current_user->user_firstname.' '. $current_user->user_lastname,
			'updated_amount'			=> intval($data['updated_amount']) + 1

		),array(
			'membership_type_id' 	=> intval($mtid)
		), array('%s', '%d', '%s', '%s', '%d'), array('%d')) === false) {
			return false;
			
		}
		
	}

	function create($data) {
		if(isset($data['display_name']) && isset($data['duration'])) {


			if($this->_db->insert('rwc_membership_types', array(
				'display_name'			=> $data['display_name'],
				'duration'					=> intval($data['duration'])
			), array(
				'%d','%d'
			)) === false){
				$this->errors = array('Failed to save');
				echo 'adsad';
	
			} else{
				echo 'asa';
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Ftypes%2Fview%2Fall');
				
			}

	}
}

}