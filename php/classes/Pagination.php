<?php
use Rampworld\Event\EventFetch as EventFetch;
require_once __dir__ . '/../../../../themes/rampworld/modules/vendor/autoload.php';
class Pagination {
    private $_db;
	public $breadcrumb = '';
	public $body = '';
	public $products,
		$total_pages;
	public $sessions = array();
	public $asi;

	public function __construct($sessions = null) {
		$this->sesssions = $sessions;
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function bookings($search_field, $search_value , $start, $limit, $page) {
		
		require 'Booking.php';
		$booking = new Booking();
		$start_no = ( $page - 1 ) * $limit;
		$booking->getAll( $search_field, $search_value , $start_no, $limit);

		if(count($booking->bookings) > 0){
			
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Booking Name</th><th>Booking Email</th><th>Session Date</th><th>Session Times</th><th>Purchased Date</th><th>Modify</th></tr></thead><tbody>';
			for($i = 0; $i < count($booking->bookings); $i++) {

				$this->body .= '<tr><td>'.$booking->bookings[$i]['id'].'</td><td>'.$booking->bookings[$i]['name'].'</td><td>' . $booking->bookings[$i]['email']  . '</td><td>'.$booking->bookings[$i]['session_date'].'</td><td>'.$booking->bookings[$i]['times'].'</td><td>'.$booking->bookings[$i]['purchased'].'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&bid='.$booking->bookings[$i]['id'].'" class="btn btn-default" style=" margin: 0px;">View</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';

			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $booking->count)? ($booking->count): intval($start_no + $limit)).' of '.$booking->count.' bookings</p><ul class="pagination">';
			$num_of_pages = ceil($booking->count/$limit);

			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.($page-1) .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' >'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' >'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.($page + 1) .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' aria-label="">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';

		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No bookings</span><small>Please refine your search</small></div></div>';
		}
		

	}

	public function entries($forename, $surname, $membership_id, $sessionTime, $end_dt, $start, $limit, $page) {

		require_once 'Entries.php';
		require_once 'Session.php';
		$entries = new Entries();
			

		$sessions = new Session();
		$sessions->getTodaysSessions();

		$start_no = ( $start - 1 ) * $limit;
		$entries->getAll($forename, $surname, $membership_id, $sessionTime, $end_dt, intval($start_no), intval($limit));
		if($sessionTime == null) {
			$session_time = new DateTime();
		} else {
			$session_time = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(explode('T', $sessionTime)[0]. ' '. explode('T', $sessionTime)[1])));
		}
		
		$can_delete = $session_time->getTimestamp() >= strtotime('now');
		$can_modify = $session_time->format('Y-m-d') == date('Y-m-d');

		$expertise = array(0 => 'Beginner', 1=> 'Novice', 2 => 'Experienced', 3 => 'Advanced' , 4 => 'Expert', 5 => 'Professional');

		if(count($entries->entries) > 0){
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th >#</th><th>Forename</th><th>Surname</th><th>Date</th><th>Time in</th><th>Session end</th><th>Age</th><th>View member</th><th>Change entry</th><th>Remove entry</th></tr></thead><tbody>';

			for($i = 0; $i < count($entries->entries); $i++) {


				$dob = DateTime::createFromFormat('Y-m-d', $entries->entries[$i]['dob']);
				$errors = DateTime::getLastErrors();
				if (!empty($errors['warning_count'])) {
					$dob = 'Error.';
				} else {
					$dob = $today->diff($dob);
					$dob = $dob->y;
				}
				$this->body .= '<tr '.(($entries->entries[$i]['paidMembership'] != "0")? 'class="info"': '').'><td>'.$entries->entries[$i]['member_id'].'</td><td>'. ucwords(strtolower($entries->entries[$i]['forename'])).'</td><td>' . ucwords(strtolower($entries->entries[$i]['surname']))  . '</td><td>'.$entries->entries[$i]['entry_date'].'</td><td>'.$entries->entries[$i]['entry_time'].'</td><td>'.$entries->entries[$i]['exit_time'].'</td><td>'.$dob.'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$entries->entries[$i]['member_id'].'" class="btn btn-default" style=" margin: 0px;">View Member</a></td>
				
				<td>'.(( $can_modify && count($sessions->sessionTimes) >= 1)? '<a data-member-id="'.$entries->entries[$i]['member_id'].'" data-forename="'.$entries->entries[$i]['forename'].'" data-surname="'.$entries->entries[$i]['surname'].'" data-age="'.$dob.'" data-has-membership="'.(($entries->entries[$i]['paidMembership'] != "0") ? $entries->entries[$i]['paidMembership']: 'None').'" data-expertise="'.$expertise[$entries->entries[$i]['expertise']].'" data-current-end="'.$entries->entries[$i]['exit_time'].'" data-session-id="'.$entries->entries[$i]['entry_id'].'" data-current-times="'.$entries->entries[$i]['entry_time'].','.$entries->entries[$i]['exit_time'].'"class="btn btn-default change-session-btn" style=" margin: 0px;">Change session</a>':'<a class="btn btn-default disabled" style=" margin: 0px;">Cannot change session</a>').'</td><td>'.(($can_delete)? '<a data-entry-id="'.$entries->entries[$i]['entry_id'].'" data-member-id="'.$entries->entries[$i]['member_id'].'"  data-member="'.ucwords(strtolower($entries->entries[$i]['forename'])).' '.ucwords(strtolower($entries->entries[$i]['surname'])).'" class="btn btn-danger remove-participant-btn" style=" margin: 0px;">Delete entry</a>' : '<a class="btn btn-danger disabled" style=" margin: 0px;">Unable to delete</a' ).'</td></tr>';
			}
			$this->body .= '</tbody></table></div>';
			$this->breadcrumb = '<div class="pagination-container"><p style="margin-bottom: 0;">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $entries->count)? ($entries->count): intval($start_no + $limit)).' of '.$entries->count.' entries</small><ul class="pagination">';
			$num_of_pages = ceil($entries->count/$limit);

			
			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($page != 1)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page - 1) . (($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"':''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt: '') : '').'" aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:''): '').'">'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt: ''): '').'">'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: ''). (($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:''): '').'">'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page + 1) . (($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"':'').  (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:'') : '').'" aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>'; 
			$this->breadcrumb .= '</ul></div>';


			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No entries</span><small>Please refine your search</small></div></div>';
		}
		

	}

	public function members($membership_id, $searchField1, $searchQuery1, $searchField2, $searchQuery2, $start, $limit, $page, $linkToMember = false) {
		
		require 'Member.php';
		$members = new Member();

		$start_no = ( $start - 1 ) * $limit;
		$members->getAll($membership_id, $searchField1, $searchQuery1, $searchField2, $searchQuery2, intval($start_no), intval($limit));

		if(count($members->members) > 0){
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Forename</th><th>Surname</th><th>Number</th><th>Age</th><th>View</th></tr></thead><tbody>';
			for($i = 0; $i < count($members->members); $i++) {

				$dob = DateTime::createFromFormat('Y-m-d', $members->members[$i]['dob']);
				$errors = DateTime::getLastErrors();
				if (!empty($errors['warning_count'])) {
					$dob = 'Error.';
				} else {
					$dob = $today->diff($dob);
					$dob = $dob->y;
				}
				$this->body .= '<tr><td>'.$members->members[$i]['member_id'].'</td><td>'. ucwords(strtolower($members->members[$i]['forename'])).'</td><td>' . ucwords(strtolower($members->members[$i]['surname']))  . '</td><td>'.$members->members[$i]['number'].'</td><td>'.$dob.'</td><td><a href="'.(($linkToMember == true) ?  host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$members->members[$i]['member_id'] : host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fdates&member='.$members->members[$i]['member_id']).'" class="btn btn-default" style=" margin: 0px;">Select</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $members->count)? ($members->count): intval($start_no + $limit)).' of '.$members->count.' members</p><ul class="pagination">';
			$num_of_pages = ceil($members->count/$limit);
			
			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1.'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No members</span><small>Please refine your search</small></div></div>';
		}
		

	}

	public function sessions($hid, $day, $start, $limit, $page) {
		require_once 'Session.php';
		$session = new Session();

		$start_no = ( $start - 1 ) * $limit;
		$session->getAll($hid, $day,intval($start_no), intval($limit));
		if(count($session->sessions) > 0){
			$days = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Name</th><th>Day</th><th>Start time</th><th>End time</th><th>View</th></tr></thead><tbody>';
			$sundays = array();
			for($i = 0; $i < count($session->sessions); $i++) {
				if($session->sessions[$i]['day'] == 0){
					$sundays[] = $session->sessions[$i];
				} else {
					$this->body .= '<tr><td>'.$session->sessions[$i]['holiday_session_id'].'</td><td>'. ucwords(strtolower($session->sessions[$i]['display_name'])).'</td><td>' . $days[$session->sessions[$i]['day']]  . '</td><td>'.$session->sessions[$i]['start_time'].'</td><td>'.$session->sessions[$i]['end_time'].'</td><td><a href="'. host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fsingle&'.((isset($session->sessions[$i]['holiday_id']))? 'hsid='.$session->sessions[$i]['holiday_session_id'] : 'sid='.$session->sessions[$i]['holiday_session_id']).'" class="btn btn-default" style=" margin: 0px;">View</a></td></tr>';
				}
		
				
			} 
			for($i = 0; $i < count($sundays); $i++) {
					$this->body .= '<tr><td>'.$sundays[$i]['holiday_session_id'].'</td><td>'. ucwords(strtolower($sundays[$i]['display_name'])).'</td><td>Sunday</td><td>'.$sundays[$i]['start_time'].'</td><td>'.$session->sessions[$i]['end_time'].'</td><td><a href="'. host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fsingle&'.((isset($session->sessions[$i]['holiday_session_id']))? 'hsid='.$session->sessions[$i]['holiday_session_id'] : 'sid='.$session->sessions[$i]['session_id']).'" class="btn btn-default" style=" margin: 0px;">View</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $session->count)? ($session->count): intval($start_no + $limit)).' of '.$session->count.' sessions</p><ul class="pagination">';

			$num_of_pages = ceil($session->count/$limit);

			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fs%2Fmember%2Fview%2Fall&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '').(($hid != null)? '&hid='.$hid.'': '').(($day != null)? '&=day'.$day: '') .'"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($hid != null)? '&hid='.$hid.'': '').(($day != null)? '&=day'.$day: '') .'"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($hid != null)? '&hid='.$hid.'': '').(($day != null)? '&=day'.$day: '') .'"': '') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($hid != null)? '&hid='.$hid.'': '').(($day != null)? '&=day'.$day: '') .'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').(($hid != null)? '&hid='.$hid.'': '').(($day != null)? '&=day'.$day: '') .'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No sessions</span><small>Please refine your search</small></div></div>';
		}
	}


	public function premises($date, $type, $start, $limit, $page) {
		require_once 'PremisesCheck.php';
		$premises = new PremisesCheck();

		$start_no = ( $start - 1 ) * $limit;
		$premises->getAll($date,intval($start_no), intval($limit));

		if(count($premises->checks) > 0){
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>Date</th><th>Opening checks</th><th>Closing checks</th><th>View opening</th><th>View closing</th></tr></thead><tbody>';
			$sundays = array();
			for($i = 0; $i < count($premises->checks); $i++) {
					$this->body .= '<tr><td>'.$premises->checks[$i]['date'].'</td><td>'. (isset($premises->checks[$i]['opening_check_id']) ? '<a class="btn btn-success disabled">Complete</a>': '<a class="btn btn-warning disabled">Not complete</a>') .'</td><td>' . (isset($premises->checks[$i]['closing_check_id']) ? '<a class="btn btn-success disabled">Complete</a>': '<a class="btn btn-warning disabled">Not complete</a>')  . '</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fopening&date='.$premises->checks[$i]['date'].'" class="btn btn-default">View</a></td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fclosing&date='.$premises->checks[$i]['date'].'" class="btn btn-default">View</a></td></tr>';
				
		
				
			} 
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $premises->count)? ($premises->count): intval($start_no + $limit)).' of '.$premises->count.' checks</p><ul class="pagination">';

			$num_of_pages = ceil($premises->count/$limit);

			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '').(($date != null)? '&hid='.$date.'': '').(($type != null)? '&type='.$type: '') .'"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($date != null)? '&hid='.$date.'': '').(($type != null)? '&type='.$type: '') .'"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($date != null)? '&hid='.$date.'': '').(($type != null)? '&type='.$type: '') .'"': '') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p='.($i) .(($limit != null)? '&limit='.$limit.'': '').(($date != null)? '&hid='.$date.'': '').(($type != null)? '&type='.$type: '') .'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').(($date != null)? '&hid='.$date.'': '').(($type != null)? '&type='.$type: '') .'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No premises checks</span><small>Please refine your search</small></div></div>';
		}
	}


	public function holidays($holiday_id, $start_dt, $end, $start, $limit, $page) {

		require 'Holiday.php';
		$holiday = new Holiday();			

		$start_no = ( $start - 1 ) * $limit;
		$holiday->getAll($holiday_id, $start_dt, $end, intval($start_no), intval($limit));

		if(count($holiday->holidays) > 0){
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th >#</th><th>Name</th><th>Start date</th><th>End date</th><th>View</th><th>Publish</th><th>Report</th></tr></thead><tbody>';
			

			
			for($i = 0; $i < count($holiday->holidays); $i++) {
 				$this->body .= '<tr '.(($today <= date('d-m-Y', strtotime($holiday->holidays[$i]['start_date'])) && $today >= date('d-m-Y', strtotime($holiday->holidays[$i]['end_date'])))? 'class="primary"': '').'><td>'.$holiday->holidays[$i]['holiday_id'].'</td><td>'. ucwords(strtolower($holiday->holidays[$i]['display_name'])).'</td><td>' . $holiday->holidays[$i]['start_date'] . '</td><td>'.$holiday->holidays[$i]['end_date'].'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fsingle&hid='.$holiday->holidays[$i]['holiday_id'].'" class="btn btn-default">View</a></td>';
				
				$this->body .= '<td><a href="" class="btn disabled btn-'.(($holiday->holidays[$i]['visible'] == 0)? 'danger': 'success').'">'.(($holiday->holidays[$i]['visible'] == 0)? 'Unpublished': 'Published').'</a></td>';
				 $this->body .= '<td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&report=holiday&hid='.$holiday->holidays[$i]['holiday_id'].'" class="btn btn-default">Generate Report</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';
			$this->breadcrumb = '<div class="pagination-container"><p style="margin-bottom: 0;">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $holiday->count)? ($holiday->count): intval($start_no + $limit)).' of '.$holiday->count.' entries</small><ul class="pagination">';
			$num_of_pages = ceil($holiday->count/$limit);

			
			$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page - 1) . (($limit != null)? '&limit='.$limit: '').(($holiday_id != null)? '&holiday_id='.$holiday_id.'"':''). (($start_dt!= null)? '&start_dt='.$start.'"': ''). (($end != null)? '&end_dt='.$end.'"': '') : '').'" aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($holiday_id != null)? '&holiday_id='.$holiday_id.'"': ''). (($start_dt!= null)? '&start_dt='.$start.'"': ''). (($end != null)? '&end_dt='.$end.'"': ''): '').'">'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($holiday_id != null)? '&holiday_id='.$holiday_id.'"': ''). (($start_dt!= null)? '&start_dt='.$start.'"': ''). (($end != null)? '&end_dt='.$end.'"': ''): '').'">'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($holiday_id != null)? '&holiday_id='.$holiday_id.'"': ''). (($start_dt!= null)? '&start_dt='.$startsessionTime.'"': '') : ''). (($end != null)? '&end_dt='.$end.'"': 'h').'">'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page + 1) . (($limit != null)? '&limit='.$limit: '').(($holiday_id != null)? '&holiday_id='.$holiday_id.'"':''). (($start_dt!= null)? '&start_dt='.$start.'"': ''). (($end != null)? '&end_dt='.$end.'"': '') : '').'" aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>'; 
			$this->breadcrumb .= '</ul></div>';


			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No holidays</span><small>Please refine your search</small></div></div>';
		}
		

	}

	public function paidMemberships($membership_id, $searchField1, $searchQuery1, $searchField2, $searchQuery2, $start_dt, $end_dt, $type, $start, $limit, $page, $linkToMember = false) {
		
		require_once 'PaidMembership.php';
		$paidMembership = new PaidMembership();

		$start_no = ( $start - 1 ) * $limit;
		$paidMembership->getAll($membership_id, $searchField1, $searchQuery1, $searchField2, $searchQuery2,$start_dt, $end_dt,$type, intval($start_no), intval($limit));

		if(count($paidMembership->passes) > 0){
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Member No.</th><th>Forename</th><th>Surname</th><th>Type</th><th>Start date</th><th>End date</th><th>View member</th><th>View more details</th></tr></thead><tbody>';
			for($i = 0; $i < count($paidMembership->passes); $i++) {

				$this->body .= '<tr><td>'.$paidMembership->passes[$i]['paid_membership_id'].'</td><td>'.$paidMembership->passes[$i]['member_id'].'</td><td>'. ucwords(strtolower($paidMembership->passes[$i]['forename'])).'</td><td>' . ucwords(strtolower($paidMembership->passes[$i]['surname']))  . '</td><td>'.$paidMembership->passes[$i]['type'].'</td><td>'.$paidMembership->passes[$i]['start_date'].'</td><td>'.$paidMembership->passes[$i]['end_date'].'</td><td><a class="btn btn-default" href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$paidMembership->passes[$i]['member_id'].'">View member</a><td><a class="btn btn-default"  href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fsingle&pmid='.$paidMembership->passes[$i]['paid_membership_id'].'">More details</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $paidMembership->count)? ($paidMembership->count): intval($start_no + $limit)).' of '.$paidMembership->count.' members</p><ul class="pagination">';
			$num_of_pages = ceil($paidMembership->count/$limit);
			if($num_of_pages > 1)
				$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1.'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').(($membership_id != null)? '&\	membership_id='.$membership_id.'': '').(($searchField1 != null)? '&'. $searchField1 .'='.str_replace(' ', '+', $searchQuery1).'': '').(($searchField2 != null)? '&'. $searchField2.'='.str_replace(' ', '+', $searchQuery2).'': '').'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No paid memberships</span><small>Please refine your search</small></div></div>';
		}
		

	}

	public function tandcs() {
		require_once 'Terms.php';
		$term = new Terms();

		$term->getAll();

		if(count($term->terms) > 0){

			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Version number</th><th>Publish date</th><th>Deactivation date</th><th>Created date</th><th>Report</th><th>View more details</th></tr></thead><tbody>';
			for($i = 0; $i < count($term->terms); $i++) {

				$this->body .= '<tr><td>'.$term->terms[$i]['tandc_id'].'</td><td>'. $term->terms[$i]['version_number'].'</td><td>' . $term->terms[$i]['publish_date']  . '</td><td>'.(($term->terms[$i]['deactivation_date'] == null )? 'Active' : $term->terms[$i]['deactivation_date']) .'</td><td>'.$term->terms[$i]['created_date'].'</td><td><a class="btn btn-default" href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=terms&amp;tandcid='.$term->terms[$i]['tandc_id'].'">Create</a></td><td><a class="btn btn-default" href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fsingle&amp;tcid='.$term->terms[$i]['tandc_id'].'">More details</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No terms and conditions</span><div class="alert alert-danger"><strong>Danger</strong>Please set this immediately</div>';
		}
	}

	/* Advance serch */
	public function advanceSearch($membership_id, $forename, $surname, $start_age, $end_age, $gender, $dob, $email, $number, $address_line_one, $address_line_two, $address_city, $address_county, $address_postcode, $registered_start_date, $registered_end_date, $report_start_date, $report_end_date, $age_groups, $disciplines, $expertise, $start, $limit, $page, $url) {
		
		require 'Member.php';
		$members = new Member();
		$raw_url = $url;
		$url = host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&form=advance-search'.$url;

		$start_no = ( $start - 1 ) * $limit;
		$members->advance($membership_id, $forename, $surname, $start_age, $end_age, $gender, $dob, $email, $number, $address_line_one, $address_line_two, $address_city, $address_county, $address_postcode, $registered_start_date, $registered_end_date, $report_start_date, $report_end_date, $age_groups, $disciplines, $expertise, intval($start_no), intval($limit), $raw_url);

		$this->asi = $members->asi;
		if(count($members->members) > 0){
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable "><thead><tr><th>#</th><th>Forename</th><th>Surname</th><th>Number</th><th>Age</th><th>View</th></tr></thead><tbody>';
			for($i = 0; $i < count($members->members); $i++) {

				$dob = DateTime::createFromFormat('Y-m-d', $members->members[$i]['dob']);
				$errors = DateTime::getLastErrors();
				if (!empty($errors['warning_count'])) {
					$dob = 'Error.';
				} else {
					$dob = $today->diff($dob);
					$dob = $dob->y;
				}
				$this->body .= '<tr><td>'.$members->members[$i]['member_id'].'</td><td>'. ucwords(strtolower($members->members[$i]['forename'])).'</td><td>' . ucwords(strtolower($members->members[$i]['surname']))  . '</td><td>'.$members->members[$i]['number'].'</td><td>'.$dob.'</td><td><a href="'. host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$members->members[$i]['member_id'].'" class="btn btn-default" style=" margin: 0px;">Select</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $members->count)? ($members->count): intval($start_no + $limit)).' of '.$members->count.' members</p><ul class="pagination">';
			$num_of_pages = ceil($members->count/$limit);
			if($num_of_pages > 1)
				$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.$url.'&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '').'"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.$url.'&p='.$i .(($limit != null)? '&limit='.$limit.'': '').'"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.$url.'&p='.$i .(($limit != null)? '&limit='.$limit.'': '').'"': '') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.$url.'&p='.$i .(($limit != null)? '&limit='.$limit.'': '').'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.$url.'&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No members</span><small>Please refine your search</small></div></div>';
		}
		

	}
	public function events($eid, $cid, $name, $start_dt, $end_dt, $start, $limit, $page) {

			
		$events = new EventFetch();

		$start_no = ( $start - 1 ) * $limit;
		$events->get($eid, $cid, $name, $start_dt, $end_dt, intval($start_no), intval($limit));
		$query = (($eid != null)? '&eid='.$eid.'': '').(($cid != null)? '&cid='.$cid.'': '').(($start_dt != null)? '&start_dt='.$start_dt.'': '').(($end_dt != null)? '&end_dt='.$end_dt.'': '');
		if(count($events->events) > 0){
		
			$today = new DateTime();
			$this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Name</th><th>Date</th><th>Start time</th><th>End time</th><th>Online</th><th>View more details</th></tr></thead><tbody>';
			for($i = 0; $i < count($events->events); $i++) {
				$this->body .= '<tr><td>'.$events->events[$i]['event_id'].'</td><td>'. $events->events[$i]['display_name'].'</td><td>' . $events->events[$i]['date'] . '</td><td>'.$events->events[$i]['start_time'].'</td><td>'.$events->events[$i]['end_time'].'</td><td>'.(($events->events[$i]['online_exclusive']) ? 'Yes': 'No').'</td><td><a class="btn btn-default"  href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fsingle&eid='.$events->events[$i]['event_id'].'">More details</a></td></tr>';
			}
			$this->body .= '</tbody></table></div>';


			$this->breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $events->count)? ($events->count): intval($start_no + $limit)).' of '.$events->count.' members</p><ul class="pagination">';
			$num_of_pages = ceil($events->count/$limit);
			if($num_of_pages > 1)
				$this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p='.($page - 1) .(($limit != null)? '&limit='.$limit.'': '') . $query . '"': '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').$query. '"': '') .'>'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '').$query. '"' :'') .'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					$this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit.'': '') . $query.'"': '').'>'.$i.'</a></li>';
				}
			}
			$this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p='.($page + 1) .(($limit != null)? '&limit='.$limit.'': '').$query.'"': '').' aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			$this->breadcrumb .= '</ul></div>';
		} else {
			$this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No events</span><small>Please refine your search</small></div></div>';
		}
		

	}
}