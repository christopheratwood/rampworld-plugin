<?php
date_default_timezone_set('Europe/London');
class Session {

	private $_db,
			$_sessions;
	private $_counts = array();
	public $days = array(1=>'Monday', 2=>'Tuesdays', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday',7=> 'Sunday');
	public $dates;
	public $session_times = array(0=> '' ,1=> '',2=> '',3=> '',4=> '',5=> '',6=> '');
	public $formatted_times = array(0=> '' ,1=> '',2=> '',3=> '',4=> '',5=> '',6=> '');
	public $error = '';
	public $sessions = array();
	public $sessionsCount = array();
	public $sessionTimes = array();
	public $count = null;
	public $closed_dates = array();


	public function __construct() {

		$this->_db =  new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}
	public function printSessions($mode, $hid=null) {
		
		return (!$this->getSessions($mode, $hid))? false: true;
	}

	public function sessionTable($mode, $id = null) {
		if($this->_setSessions($mode, $id)) {
			
			$this->session_times = array('Monday'=> '','Tuesday'=> '','Wednesday'=> '','Thursday'=> '','Friday'=> '','Saturday'=> '','Sunday'=> '');
			foreach ($this->_sessions as $session) {

			 	switch ($session['day']) {
			 		case 1:
			 			$day = 'Monday';
			 			break;
			 		case 2:
			 			$day = 'Tuesday';
			 			break;
			 		case 3:
			 			$day = 'Wednesday';
			 			break;
			 		case 4:
			 			$day = 'Thursday';
			 			break;
			 		case 5:
			 			$day = 'Friday';
			 			break;
			 		case 6:
			 			$day = 'Saturday';
			 			break;
			 		case 0:
			 			$day = 'Sunday';
			 			break;


			 	}
			 	if($mode == 'holiday') {
					$this->session_times[$day] .= '
					<tr id="session_'.$session['holiday_session_id'].'">
						<th>
							'.$day.'<br><small>'.$day.' Session Times.</small>
						</th>
						<td data-label="Display Name">
							<input type="text" class="rwc_new" placeholder="Session Name" title="Session Name" required id="session_'.$session['holiday_session_id'].'_session_name" value="'.$session['display_name'].'" data-original="'.$session['display_name'].'" data-type="name">
							<span class="message error '.$session['day'].'_session_'.$session['holiday_session_id'].'_session_name"></span>
						</td>
						<td data-label=" Session Start Time">
							<input type="text" class="rwc_new" placeholder="Start Time" required title="Session Start Time"  id="session_'.$session['holiday_session_id'].'_start_time" value="'.$session['start_time'].'" data-original="'.$session['start_time'].'" data-type="time">
							<span class="message error '.$session['day'].'_session_'.$session['holiday_session_id'].'_start_time"></span>
						<td data-label="Session End Time">

							<input type="text" class="rwc_new" placeholder="End Time" required title="Session End Time" id="session_'.$session['holiday_session_id'].'_end_time"  value="'.$session['end_time'].'" data-original="'.$session['end_time'].'" data-type="time">
							<span class="message error '.$session['day'].'_session_'.$session['holiday_session_id'].'_end_time"></span>
						</td>
						<td>
							<a href="#" id="remove_'.$session['holiday_session_id'].'" class="class="remove_holiday"" title="Remove session '.$session['holiday_session_id'].'">Remove</a></td>

					</tr>';
			 	} else {
			 		$this->session_times[$day] .= '
					<tr id="session_'.$session['session_id'].'">
						<th>
							'.$day.'<br><small>'.$day.' Session Times.</small>
						</th>
						<td data-label="Display Name">
							<input type="text" class="rwc_new" placeholder="Session Name" title="Session Name" required id="session_'.$session['session_id'].'_session_name" value="'.$session['display_name'].'" data-original="'.$session['display_name'].'" data-type="name">
							<span class="message error '.$session['day'].'_session_'.$session['session_id'].'_session_name"></span>
						</td>
						<td data-label=" Session Start Time">
							<input type="text" class="rwc_new" placeholder="Start Time" required title="Session Start Time"  id="session_'.$session['session_id'].'_start_time" value="'.$session['start_time'].'" data-original="'.$session['start_time'].'" data-type="time">
							<span class="message error '.$session['day'].'_session_'.$session['session_id'].'_start_time"></span>
						<td data-label="Session End Time">

							<input type="text" class="rwc_new" placeholder="End Time" required title="Session End Time" id="session_'.$session['session_id'].'_end_time"  value="'.$session['end_time'].'" data-original="'.$session['end_time'].'" data-type="time">
							<span class="message error '.$session['day'].'_session_'.$session['session_id'].'_end_time"></span>
						</td>
						<td>
							<a href="#" id="remove_'.$session['session_id'].'" class="remove" title="Remove session '.$session['session_id'].'">Remove</a></td>

					</tr>';
			 	}
		 		

			}
			return true;
		} else {
			return false;
		}

		
	}
		public function getDates() {
		return $this->dates;
	}
	public function printHolidaySessions($mode) {

	}
	public function isHoliday() {
		return $this->_db->get_var('Select EXISTS( SELECT 1 from `rwc_holidays` WHERE `start_date` <= CURDATE() AND `end_date` >= CURDATE())');
	}
	public function add($data) {

		if($this->_db->insert('rwc_sessions', $data )) {
			
			return true;
		} else {
			$this->error = $this->_db->last_error;
			return false;
		}
	}
	public function getAll($hid, $day, $start_no, $limit) {
		if($hid == null && $day == null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT session_id as holiday_session_id, null as holiday_id, start_time, end_time, day, display_name, restrictions, consecutive, beginner, private FROM rwc_sessions ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array( $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results('SELECT count(*) as count FROM rwc_sessions', ARRAY_A)[0]['count'];
		} elseif($hid == null && $day != null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT session_id as holiday_session_id, null as holiday_id, start_time, end_time, day, display_name, restrictions, consecutive, beginner, private FROM rwc_sessions WHERE day = %d ORDER BY day ASC, start_time DESC, end_time ASC LIMIT %d, %d', array( intval($day), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_sessions WHERE day = %d',array(intval($day))), ARRAY_A)[0]['count'];
		} elseif($hid != null && $day == null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_holidays_sessions WHERE holiday_id = %d ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array(intval($hid), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays_sessions WHERE holiday_id = %d ',array($hid)), ARRAY_A)[0]['count'];

		} elseif($hid != null && $day != null) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT * FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ORDER BY day ASC, start_time ASC, end_time ASC LIMIT %d, %d', array($hid, intval($day), $start_no, $limit)), ARRAY_A);
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d',array($hid, $day)), ARRAY_A)[0]['count'];
		}
	}


	public function get($id, $isHoliday) {
		if($isHoliday) {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT rwc_holidays_sessions.*, rwc_holidays.display_name as holiday_name, start_date, end_date FROM rwc_holidays_sessions LEFT JOIN rwc_holidays ON rwc_holidays_sessions.holiday_id = rwc_holidays.holiday_id WHERE holiday_session_id = %d ', array(intval($id))), ARRAY_A);
			if(count($this->sessions) != 0)
				$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members_entries WHERE entry_date BETWEEN %s AND %s AND exit_time = %s',array($this->sessions[0]['start_date'], $this->sessions[0]['end_date'], $this->sessions[0]['end_time'])), ARRAY_A)[0]['count'];
		} else {
			$this->sessions = $this->_db->get_results($this->_db->prepare('SELECT rwc_sessions.* FROM rwc_sessions WHERE session_id = %d ', array(intval($id))), ARRAY_A);
			if(count($this->sessions) != 0){
				$dates = $this->_db->get_results('SELECT start_date, end_date FROM rwc_holidays ', ARRAY_A);

				$binding = array($this->sessions[0]['end_time']);
				$query = '';
				foreach($dates as $date ) {
					$query .= ' AND entry_date NOT BETWEEN %s AND %s';
					array_push($binding, $date['start_date'], $date['end_date']);
				}
				$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_members_entries WHERE exit_time = %s '.$query,$binding), ARRAY_A)[0]['count'];
			}
		}
	}


	public function create($data) {
		if($data['hid'] == 'null') {

			$session = $this->_db->get_results($this->_db->prepare('SELECT session_id FROM rwc_sessions WHERE day = %d AND (%s BETWEEN start_time AND end_time || %s BETWEEN start_time AND end_time) LIMIT 1', array(intval($data['day']), $data['start_time'], $data['end_time'])), ARRAY_A);
			if(count($session) != 0) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview2Fsingle&error=overlap&sid='.$session[0]['session_id']);
			} else {
				//insert new session
				$array = array(
					'start_time'	=> $data['start_time'],
					'end_time'		=> $data['end_time'],
					'day'					=> intval($data['day']),
					'display_name'=> $data['display_name'],
					'consecutive' => ($data['consecutive'] == '0') ? NULL : 1,
					'beginner' 		=> ($data['beginner'] == '0') ? NULL : 1,
					'private' 		=> ($data['private'] == '0') ? NULL : 1
				);

				if($this->_db->query('rwc_sessions', $array) !== false) {
					wp_redirect(host . 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview2Fsingle&sid='.$this->_db->insert_id);
				} else {
					
					$this->error = 'Could not add. '.$this->_db->last_error;
				}
			}
		} else {
			$session = $this->_db->get_results($this->_db->prepare('SELECT holiday_session_id FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d AND (%s BETWEEN start_time AND end_time AND end_time <> %s) LIMIT 1', array(intval($data['hid']), intval($data['day']), $data['start_time'], $data['end_time'])), ARRAY_A);
			if(count($session) != 0) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fsingle&error=overlap&hsid='.$session[0]['holiday_session_id']);
			} else {
				//insert new session
				$array = array(
					'holiday_id'	=> intval($data['hid']),
					'start_time'	=> $data['start_time'],
					'end_time'		=> $data['end_time'],
					'day'					=> intval($data['day']),
					'display_name'=> $data['display_name'],
					'consecutive' => ($data['consecutive'] == '0') ? NULL : 1,
					'beginner' 		=> ($data['beginner'] == '0') ? NULL : 1,
					'private' 		=> ($data['private'] == '0') ? NULL : 1
				);
				if($this->_db->insert('rwc_holidays_sessions', $array) !== false) {
					wp_redirect(host . 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fsingle&hsid='.$this->_db->insert_id);
				} else {
					echo $this->_db->last_query;
					$this->error = 'Could not add. '.$this->_db->last_error;
				}
			}
		}
	}

	public function update($data, $id, $type) {
		$current_user = wp_get_current_user();
		if($type == 'holiday') {
			$session = $this->_db->get_results($this->_db->prepare('SELECT holiday_session_id FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d AND (%s BETWEEN start_time AND end_time AND end_time <> %s) LIMIT 1', array(intval($data['hid']), intval($data['day']), $data['start_time'], $data['end_time'])), ARRAY_A);
			if(count($session) != 0) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fsingle&error=overlap&hsid='.$session[0]['holiday_session_id']);
			} else {
				//insert new session
				$array = array(
					'start_time'					=> $data['start_time'],
					'end_time'						=> $data['end_time'],
					'day'									=> intval($data['day']),
					'display_name'				=> $data['display_name'],
					'consecutive' 				=> ($data['consecutive'] == '0') ? NULL : 1,
					'beginner' 						=> ($data['beginner'] == '0') ? NULL : 1,
					'private' 						=> ($data['private'] == '0') ? NULL : 1,
					'last_updated_time'		=> date('Y-m-d H:i:s'),
					'last_updated_user'		=> $current_user->user_firstname.' '. $current_user->user_lastname,
					'updated_amount'			=> intval($data['update_counter']) + 1
				);
				if($this->_db->update('rwc_holidays_sessions', $array, array('holiday_session_id' => intval($id))) === false) {

					$this->error = 'Could not add. '.$this->_db->last_error;
				}
			}
		} else {
			$session = $this->_db->get_results($this->_db->prepare('SELECT session_id FROM rwc_sessions WHERE day = %d AND (%s BETWEEN start_time AND end_time || %s BETWEEN start_time AND end_time) LIMIT 1', array(intval($data['day']), $data['start_time'], $data['end_time'])), ARRAY_A);
			if(count($session) != 0) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview2Fsingle&error=overlap&sid='.$session[0]['session_id']);
			} else {
				//insert new session
				$array = array(
					'start_time'	=> $data['start_time'],
					'end_time'		=> $data['end_time'],
					'day'					=> intval($data['day']),
					'display_name'=> $data['display_name'],
					'consecutive' => ($data['consecutive'] == '0') ? NULL : 1,
					'beginner' 		=> ($data['beginner'] == '0') ? NULL : 1,
					'private' 		=> ($data['private'] == '0') ? NULL : 1,
					'last_updated_time'		=> date('Y-m-d H:i:s'),
					'last_updated_user'		=> $current_user->user_firstname.' '. $current_user->user_lastname,
					'updated_amount'			=> intval($data['update_counter']) + 1
				);

				if($this->_db->update('rwc_sessions', $array, array('session_id' => intval($id))) === false) {
					$this->error = 'Could not update. '.$this->_db->last_error;
				}
			}
		}
	}

	public function delete($id, $type) {
		if(!current_user_can('administrator')){
			wp_redirect($_SERVER['HTTP_REFERER'].'&error=incorrect+permissions');
		} else {	
			if($type == 'holiday')
				$this->_db->delete('rwc_holiday_sessions', array('holiday_session_id' => $id));
			else
				$this->_db->delete('rwc_sessions', array('session_id' => $id));
			
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&success=deleted+session&id='.$id);
		}
	}

	public function getTodaysSessions() {

		$date = date('w');
		$holiday = $this->_db->get_results('SELECT holiday_id FROM rwc_holidays WHERE CURDATE() between start_date AND end_date AND visible = 1', ARRAY_A);
		if(!empty($holiday ))
			$this->sessionTimes =  $this->_db->get_results($this->_db->prepare('SELECT end_time, display_name from rwc_holidays_sessions WHERE holiday_id = %d AND day = %d AND end_time >= %s ORDER BY end_time ASC', array($holiday[0]['holiday_id'], $date, date('H:i'))), ARRAY_A);
		else
			$this->sessionTimes =  $this->_db->get_results($this->_db->prepare('SELECT end_time, display_name from rwc_sessions WHERE day = %d AND end_time >= %s ORDER BY end_time ASC', array( $date, date('H:i'))), ARRAY_A);
	}

	public function getTotal($start_dt, $end_dt, $forename, $surname, $membership_id, $start_no, $limit) {
		require_once 'Entries.php';
		$entries = new Entries();
		$entries->getAll($forename, $surname, $membership_id, $start_dt, $end_dt, $start_no, $limit);
		$members = $entries->entries;

		$data = array(
			'0' => 0,
			'1' => 0,
			'2' => 0,
			'3' => 0,
			'4' => 0,
			'5' => 0,
			'6' => 0
			
		);
		foreach($members as $member) {
			$data[date_format(date_create( $member['entry_date']), 'w')]++;
		}
		var_dump($data);

		$entries = $this->_db->get_results($this->_db->prepare('SELECT count(*) as total, DATE_FORMAT(exit_time, "%%H:%%i") as exit_time, DAYOFWEEK(entry_date) as day FROM `rwc_members_entries` WHERE TIMESTAMP(`rwc_members_entries`.`entry_date`, `rwc_members_entries`.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s) GROUP BY exit_time, DAYOFWEEK(entry_date) ORDER BY DAYOFWEEK(entry_date) ASC,exit_time asc LIMIT %d, %d',
		array(
			explode('T', $start_dt)[0],
			explode('T', $start_dt)[1],
			explode('T', $end_dt)[0],
			explode('T', $end_dt)[1],
			$start_no,
			$limit
		)), ARRAY_A);

		$holiday_sessions = $this->_db->get_results($this->_db->prepare('SELECT 
			`rwc_holidays_sessions`.end_time, 
			`rwc_holidays_sessions`.day 
			FROM 
				rwc_holidays_sessions 
				lEFT JOIN rwc_holidays on `rwc_holidays_sessions`.holiday_id = `rwc_holidays`.holiday_id 
			WHERE 
				`rwc_holidays`.start_date <= %s',
			array(
				explode('T', $start_dt)[0],
			)), ARRAY_A);

		$sessions = $this->_db->get_results('SELECT end_time, day FROM `rwc_sessions` 
											UNION SELECT end_time, day FROM `rwc_holidays_sessions` ORDER by day ', ARRAY_A);

		$seshData;
		$daynames = array('Sunday','Monday', 'Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday');

		for($i =0 ; $i < count($sessions); $i++) {
			if(!isset($seshData[$sessions[$i]['day']]))
				$seshData[$sessions[$i]['day'] ] = array('day'=> $daynames[$sessions[$i]['day']], 'sessions' => array());
			if(!isset($seshData[$sessions[$i]['day']]['sessions'][$sessions[$i]['end_time']]))
				$seshData[$sessions[$i]['day']]['sessions'][$sessions[$i]['end_time']] = 0;
		}
		
	

		for ($i=0; $i < count($entries) ; $i++)
			$seshData[$entries[$i]['day'] - 1]['sessions'][$entries[$i]['exit_time']] = $entries[$i]['total'];
		
	
		
		if(array_key_exists ( '00:00' ,  $seshData[5]['sessions'] )) {
			$seshData[5]['sessions']['24:00'] = $seshData[5]['sessions']['00:00'];
			unset($seshData[5]['sessions']['00:00']);
		}

		
		for($i = 0; $i < count($seshData) ; $i++) {
			foreach ($seshData[$i]['sessions'] as $key => $value)
				if($value == 0)
					unset($seshData[$i]['sessions'][$key]);

		}
		
		$members_per_day = array();

		for($i = 0; $i < count($members); $i++){
			if(!isset($members_per_day[$members[$i]['entry_date']])) {
				$members_per_day[$members[$i]['entry_date']] = array(
					"total" 	=> 1,
					"day"			=> date("w", strtotime($members[$i]['entry_date'])),
					"date" 		=> date("l jS F Y", strtotime($members[$i]['entry_date'])),
					"members" => array($members[$i])
					);
			} else {
				$members_per_day[$members[$i]['entry_date']]['members'][] = $members[$i];
				$members_per_day[$members[$i]['entry_date']]['total']++;
			}
		}
		$this->sessions = $seshData;
		$this->sessionsCount = $members_per_day;
		
		echo '<pre>';print_r($this->sessionsCount);echo '</pre>';
	}


	public function getTotalAdvance($query, $join, $bindings, $start, $end) {
		require_once 'Entries.php';
		$entries = new Entries();
		$entries->getAllAdvance($query, $join, $bindings, $start, $end);
		$members = $entries->entries;
		
		$query .= ' AND TIMESTAMP(ent.`entry_date`, ent.`exit_time`) BETWEEN %s AND %s';
		$bindings[] = $start;
		$bindings[] = $end;

		$entries = $this->_db->get_results($this->_db->prepare('SELECT count(*) as total, DATE_FORMAT(exit_time, "%%H:%%i") as exit_time, DAYOFWEEK(entry_date) as day FROM `rwc_members_entries` as ent LEFT JOIN rwc_members as mem ON ent.member_id = mem.member_id '.$query.' GROUP BY exit_time, DAYOFWEEK(entry_date) ORDER BY DAYOFWEEK(entry_date) ASC,exit_time desc', $bindings), ARRAY_A);

		$daynames = array('Sunday','Monday', 'Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday');
		
		$this->sessions = array(0 => array('sessions' => array(), 'day' => 'Sunday'),1 => array('sessions' => array(), 'day' => 'Monday'),2 => array('sessions' => array(), 'day' => 'Tuesday'),3 => array('sessions' => array(), 'day' => 'Wednesday'),4 => array('sessions' => array(), 'day' => 'Thursday'),5 => array('sessions' => array(), 'day' => 'Friday'),6 => array('sessions' => array(), 'day' => 'Saturday'));

		for ($i=0; $i < count($entries) ; $i++)
			$this->sessions[$entries[$i]['day'] - 1]['sessions'][$entries[$i]['exit_time']] = $entries[$i]['total'];
			
		for ($i=0; $i < count($this->sessions) ; $i++)
			ksort($this->sessions[$i]['sessions']);
		
		
		if(array_key_exists ( '00:00' ,  $this->sessions[5]['sessions'] )) {
			$this->sessions[5]['sessions']['24:00'] = $this->sessions[5]['sessions']['00:00'];
			unset($this->sessions[5]['sessions']['00:00']);
		}
	
		$this->sessions[7] = $this->sessions[0];
		unset($this->sessions[0]);
		


		for($i = 0; $i < count($members); $i++){
			if(!isset($this->sessionsCount[$members[$i]['entry_date']])) {
				$this->sessionsCount[$members[$i]['entry_date']] = array(
					"total" 	=> 1,
					"day"			=> date("w", strtotime($members[$i]['entry_date'])),
					"date" 		=> date("l jS F Y", strtotime($members[$i]['entry_date'])),
					"members" => array($members[$i])
					);
			} else {
				$this->sessionsCount[$members[$i]['entry_date']]['members'][] = $members[$i];
				$this->sessionsCount[$members[$i]['entry_date']]['total']++;
			}
		}
	}

	public function getTotalHoliday($start, $end) {
		require_once 'Entries.php';
		$entries = new Entries();
		$entries->getAllHoliday($start, $end);
		$members = $entries->entries;
		


		$entries = $this->_db->get_results($this->_db->prepare('SELECT count(*) as total, DATE_FORMAT(exit_time, "%%H:%%i") as exit_time, DAYOFWEEK(entry_date) as day FROM `rwc_members_entries` as ent LEFT JOIN rwc_members as mem ON ent.member_id = mem.member_id  WHERE date(ent.entry_date) BETWEEN %s AND %s GROUP BY exit_time, DAYOFWEEK(entry_date) ORDER BY DAYOFWEEK(entry_date)  DESC,exit_time desc', array($start, $end)), ARRAY_A);

		$daynames = array('Sunday','Monday', 'Tuesday','Wednesday', 'Thursday', 'Friday', 'Saturday');
		
		$this->sessions = array(0 => array('sessions' => array(), 'day' => 'Sunday'),1 => array('sessions' => array(), 'day' => 'Monday'),2 => array('sessions' => array(), 'day' => 'Tuesday'),3 => array('sessions' => array(), 'day' => 'Wednesday'),4 => array('sessions' => array(), 'day' => 'Thursday'),5 => array('sessions' => array(), 'day' => 'Friday'),6 => array('sessions' => array(), 'day' => 'Saturday'));

		for ($i=0; $i < count($entries) ; $i++)
			$this->sessions[$entries[$i]['day'] - 1]['sessions'][$entries[$i]['exit_time']] = $entries[$i]['total'];
			
		for ($i=0; $i < count($this->sessions) ; $i++)
			ksort($this->sessions[$i]['sessions']);
		
		
		if(array_key_exists ( '00:00' ,  $this->sessions[5]['sessions'] )) {
			$this->sessions[5]['sessions']['24:00'] = $this->sessions[5]['sessions']['00:00'];
			unset($this->sessions[5]['sessions']['00:00']);
		}
	
		$this->sessions[7] = $this->sessions[0];
		unset($this->sessions[0]);
		


		for($i = 0; $i < count($members); $i++){
			if(!isset($this->sessionsCount[$members[$i]['entry_date']])) {
				$this->sessionsCount[$members[$i]['entry_date']] = array(
					"total" 	=> 1,
					"day"			=> date("w", strtotime($members[$i]['entry_date'])),
					"date" 		=> date("l jS F Y", strtotime($members[$i]['entry_date'])),
					"members" => array($members[$i])
					);
			} else {
				$this->sessionsCount[$members[$i]['entry_date']]['members'][] = $members[$i];
				$this->sessionsCount[$members[$i]['entry_date']]['total']++;
			}
		}
	}

	private function _setSessions($id = null) {
		$this->session_times = array(0=> '' ,1=> '',2=> '',3=> '',4=> '',5=> '',6=> '');
		$this->_counts = array();
		$this->closed_dates = $this->_db->get_results('SELECT date_format(date, "%d/%m/%Y") as d FROM rwc_closed_dates WHERE date >= CURDATE() ORDER BY date ASC', ARRAY_A);

		if($id != null) {
			$this->dates = $this->_db->get_results($this->_db->prepare('SELECT holiday_id, display_name, DATE_FORMAT(start_date, "%%D \o\f %%M %%Y") as start_date, DATE_FORMAT(end_date, "%%D \o\f %%M %%Y") as end_date FROM `rwc_holidays` WHERE holiday_id = %d ORDER BY start_date LIMIT 1', array($id)), ARRAY_A);
			if(!empty($this->dates)){
				$this->_sessions = $this->_db->get_results($this->_db->prepare('SELECT * FROM `rwc_holidays_sessions` WHERE holiday_id = %d ORDER BY day ASC, start_time ASC', array($this->dates[0]['holiday_id'])), ARRAY_A);

			} else {
				return false;
			}
		}else {
				$this->_sessions  = $this->_db->get_results('SELECT * FROM rwc_sessions ORDER BY day ASC, start_time ASC', ARRAY_A);
		}
		
		

		if(!empty($this->_sessions)) {

				
			foreach ($this->_sessions as $key=>$subarr) {
				if($subarr['day'] == 0) {
					if (isset($this->_counts['Sunday'])) {
					    $this->_counts['Sunday']++;
					  } else {
					  	$this->_counts['Sunday'] = 1;
					  }
				} else {
				  // Add to the current group count if it exists
				  if (isset($this->_counts[$this->days[$subarr['day']]])) {
				    $this->_counts[$this->days[$subarr['day']]]++;
				  } else {
				  	$this->_counts[$this->days[$subarr['day']]] = 1;
				  }
				}
			  // Or the ternary one-liner version
			  // instead of the preceding if/else block
			  //$this->_counts[$this->days[$subarr['day']]] = isset($this->_counts[$subarr['day']]) ? $this->_counts[$subarr['day']]++ : 1;
			}
			
			foreach ($this->_sessions as $session) {

				$this->session_times[$session['day']][] = array('start_time' => $session['start_time'],'end_time' => $session['end_time'], 'display_name'=>$session['display_name']);
		 	}
		 	$tmp = $this->session_times[0];
		 	unset($this->session_times[0]);
		 	$this->session_times[7] = $tmp;
		 	return true;
		 }
	}
	private function getSessions($mode, $hid = null) {
		
		if($this->_setSessions($mode, $hid)) {
			$indexs_to_skip = array();
			for($i = count($this->session_times) ; $i > 0; $i--) {

				if(!in_array($i, $indexs_to_skip)) {

				
					$indexs_to_skip[] =$i;
					if(isset($this->session_times[$i - 1]) && ($this->session_times[$i] === $this->session_times[$i - 1])) {

						if(isset($this->session_times[$i - 2])  && ($this->session_times[$i-1] === $this->session_times[$i - 2])) {
							$indexs_to_skip[] = $i-1;
							$indexs_to_skip[] = $i-2;
							
							$index = 0;
							if(empty($this->formatted_times))
								$key_index = 0;
							else
								$key_index = key(array_slice( $this->formatted_times, -1, 1, TRUE )) + 1;
							

							foreach ($this->session_times[$i] as $key => $value) {
								if($index == $this->_counts[$this->days[$i]] -1) {
									if($index == 0) {
										$this->formatted_times[$key_index] = '<tr class="printmode lastrecord"><td>
												'.$this->days[$i-2].', '.$this->days[$i-1].', '.$this->days[$i].'<br><small>Session Times.</small>
											</td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</trS>';
									} else {
										$this->formatted_times[$key_index] .= '<tr class="printmode lastrecord"><td></td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									}
								} else {
									if($index == 0) {
										$this->formatted_times[$key_index] = '<tr><td>
												'.$this->days[$i-2].', '.$this->days[$i-1].', '.$this->days[$i].'<br><small>Session Times.</small>
											</td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									} else {
										$this->formatted_times[$key_index] .= '<tr><td></td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									}
								}
								$index++;
							}
						} else {

							$indexs_to_skip[] = $i-1;
							$index = 0;
							if(empty($this->formatted_times))
								$key_index = 0;
							else
								$key_index = key(array_slice( $this->formatted_times, -1, 1, TRUE )) + 1;
							

							foreach ($this->session_times[$i] as $key => $value) {

								if($index == $this->_counts[$this->days[$i]] -1) {
									if($index == 0) {
										$this->formatted_times[$key_index] = '<tr class="printmode lastrecord"><td>
												'.$this->days[$i-1].', '.$this->days[$i].'<br><small>Session Times.</small>
											</td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									} else {
										$this->formatted_times[$key_index] .= '<tr class="printmode lastrecord"><td></td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									}
								} else {
									if($index == 0) {
										$this->formatted_times[$key_index] = '<tr><td>
													'.$this->days[$i-1].', '.$this->days[$i].'<br><small>Session Times.</small>
											</td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									} else {
										$this->formatted_times[$key_index] .= '<tr><td></td>
											<td data-label="Display Name">
												<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

											</td>
											<td>
												<p class="medium">'.$value['display_name'].'</p>
											</td>
											</tr>';
									}
								}
								$index++;
							}
							
						}
							
					} else {
						$index = 0;
						if(empty($this->formatted_times))
							$key_index = 0;
						else
							$key_index = key(array_slice( $this->formatted_times, -1, 1, TRUE )) + 1;
						

						foreach ($this->session_times[$i] as $key => $value) {
							if($index == $this->_counts[$this->days[$i]] -1) {
								if($index == 0) {
									$this->formatted_times[$key_index] = '<tr class="printmode lastrecord"><td>
											'.$this->days[$i].'<br><small>Session Times.</small>
										</td>
										<td data-label="Display Name">
											<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

										</td>
										<td>
											<p class="medium">'.$value['display_name'].'</p>
										</td>
										</tr>';
								} else {
									$this->formatted_times[$key_index] .= '<tr class="printmode lastrecord"><td></td>
										<td data-label="Display Name">
											<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

										</td>
										<td>
											<p class="medium">'.$value['display_name'].'</p>
										</td>
										</tr>';
								}
							} else {
								if($index == 0) {
									$this->formatted_times[$key_index] = '<tr><td>
											'.$this->days[$i].'<br><small>Session Times.</small>
										</td>
										<td data-label="Display Name">
											<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

										</td>
										<td>
											<p class="medium">'.$value['display_name'].'</p>
										</td>
										</tr>';
								} else {
									$this->formatted_times[$key_index] .= '<tr><td></td>
										<td data-label="Display Name">
											<p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>

										</td>
										<td>
											<p class="medium">'.$value['display_name'].'</p>
										</td>
										</tr>';
								}
							}
							$index++;
						}
					}
				}
			}
		 	
			return true;
		} else {
			return false;
		}

		 

	}
	
}