<?php

/**
 * Undocumented class
 */
class Timetable
{

    private $_session;

    public $timetable = array();
    public $details = array();
    public $closed_dates = array();

    public function __construct()
    {
        require_once 'Session.php';

        //initalise new session object
        $this->_session = new Session();
    }

    /**
     * Undocumented function
     *
     * @param [int] $hid
     * @return void
     */
    public function export($hid = null)
    {
        $this->_session->printSessions($hid, $hid, 128);
        $this->timetable = $this->_session->formatted_times;
        $this->details = $this->_session->dates;
        $this->closed_dates = $this->_session->closed_dates;
    }
}
