<?php

class Validation
{
    public $errors = array();
    public $passed = true;
    public function __construct()
    {
    }
    public function validate($form, array $data)
    {
        switch ($form) {
            case 'member':
                //personal details
                if (isset($_POST['forename']) && strlen($_POST['forename']) == 0) {
                    $this->errors['forename'] = 'Forename is required.';
                    $this->passed = false;
                } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['forename'])) {
                    $this->errors['forename'] = 'Forename must only contain characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['forename']) < 2) {
                    $this->errors['forename'] = 'Forename must be more 2 characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['forename']) > 100) {
                    $this->errors['forename'] = 'Forename must be under 100 characters.';
                    $this->passed = false;
                }
                    //surname validation
                if (empty($_POST['surname'])) {
                    $this->errors['surname'] = 'Surname is required.';
                    $this->passed = false;
                } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['surname'])) {
                    $this->errors['surname'] = 'Surname must only contain characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['surname']) < 2) {
                    $this->errors['surname'] = 'Surname must be more 2 characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['surname']) > 100) {
                    $this->errors['surname'] = 'Surname must be under 100 characters.';
                    $this->passed = false;
                }

                //Gender
                if (empty($_POST['gender'] )) {
                    $this->errors['gender'] = 'Your gender is required.';
                    $this->passed = false;
                } elseif (in_array( $_POST['gender'], array('m','f', 'o') ) != true) {
                    $this->errors['gender'] = 'Please provide a valid gender.';
                    $this->passed = false;
                }
                $today = new DateTime(date('d-m-Y H:i:s'));
                $dob = DateTime::createFromFormat('Y-m-d', $_POST['dob']);
            
                $errors = DateTime::getLastErrors();
                if (!empty($errors['warning_count'])) {
                    $this->errors['dob'] = 'Please provide a valid Date of Birth.';
                    $this->passed = false;
                } else {
                    if (is_bool($dob)) {
                        $this->errors['dob'] = 'Please provide a valid Date of Birth.';
                        $this->passed = false;
                    } else {
                        $intval = $today->diff($dob);

                        if (intval($intval->y) < 2) {
                            $this->errors['dob'] = 'Your must be at least 2 years old.';
                            $this->passed = false;
                        } elseif (intval($intval->y) >100) {
                            $this->errors['dob'] = 'Your must be under 100 years old.';
                            $this->passed = false;
                        }
                    }
                }
                $_POST['age'] =intval($intval->y);
                

                //email address
                if (empty($_POST['email'])) {
                    $this->errors['email'] = 'Email address is required.';
                    $this->passed = false;
                } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $this->formErrors['email'] = 'Email address must be valid. We will use this to confirm your membership.';
                    $this->passed = false;
                } elseif (strlen($_POST['email']) > 255) {
                    $this->errors['email'] = 'Email Address must number 255 characters.';
                    $this->passed = false;
                }
                //telephone
                if (empty($_POST['number'])) {
                    $this->errors['number'] = 'Your mobile/ telephone number is required.';
                    $this->passed = false;
                } elseif (!is_numeric($_POST['number'])) {
                    $this->errors['number'] ='Your number must consist of only digits.';
                    $this->passed = false;
                } elseif (strlen($_POST['number']) < 7 || strlen($_POST['number']) > 16) {
                    $this->errors['number'] = 'Please enter a valid telephone number.';
                    $this->passed = false;
                }
                if (empty($_POST['address_line_one'])) {
                    $this->errors['address_line_one'] = 'Please enter the first line of your address';
                    $this->passed = false;
                } elseif (strlen($_POST['address_line_one']) > 50) {
                    $this->errors['address_line_one'] = 'First address line must be under 50 characters.';
                    $this->passed = false;
                }
                if (!empty($_POST['address_line_two']) && strlen($_POST['address_line_two']) > 50) {
                    $this->errors['address_line_two'] = 'Second address line must be under 50 characters.';
                    $this->passed = false;
                }
                if (!empty($_POST['address_city']) && strlen($_POST['address_city']) > 50) {
                    $this->errors['address_city'] = 'City line must be under 50 characters.';
                    $this->passed = false;
                }
                if (!empty($_SESSION['address_county']) && strlen($_POST['address_county']) > 50) {
                    $this->errors['address_county'] = 'County line must be under 50 characters.';
                    $this->passed = false;
                }
                if (empty($_POST['address_postcode'])) {
                    $this->errors['address_postcode'] = 'Please enter your postcode.';
                    $this->passed = false;
                } elseif (strlen($_POST['address_postcode']) < 5 || strlen($_POST['address_postcode']) > 10) {
                    $this->errors['address_postcode'] = 'Please enter a valid postcode.';
                    $this->passed = false;
                } else {
                    $_POST['address_postcode'] = strtoupper(wordwrap(str_replace(' ', '', $_POST['address_postcode']), strlen(str_replace(' ', '', $_POST['address_postcode']))-3, ' ', true));
                }
                if (intval($intval->y) < 16) {
                    if (empty($_POST['consent_name'])) {
                        $this->errors['consent_name'] = 'Please provide parent / guardian\'s name.';
                        $this->passed = false;
                    } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['consent_name'])) {
                        $this->errors['consent_name'] = 'Surname must only contain characters.';
                        $this->passed = false;
                    } elseif (strlen($_POST['consent_name']) > 100) {
                        $this->errors['consent_name'] = 'Surname must number 100 characters.';
                        $this->passed = false;
                    }
                    //parent number
                    if (empty($_POST['consent_number'])) {
                        $this->errors['consent_number'] = 'Please provide parent / guardian\'s first address line.';
                        $this->passed = false;
                    } elseif (!is_numeric($_POST['consent_number'])) {
                        $this->errors['consent_number'] ='Your number must consist of only digits.';
                        $this->passed = false;
                    } elseif (strlen($_POST['consent_number']) < 7 || strlen($_POST['consent_number']) > 16) {
                        $this->errors['consent_number'] = 'Please enter a valid telephone number.';
                        $this->passed = false;
                    }
                    
                    //parent address
                    if (empty($_POST['consent_address_one'])) {
                        $this->errors['consent_address_one'] = 'Please provide parent / guardian\'s name.';
                        $this->passed = false;
                    } elseif (strlen($_POST['consent_address_one']) > 50) {
                        $this->errors['consent_address_one'] = 'Address line must be under 50 characters long.';
                        $this->passed = false;
                    }

                    //parent postcode
                    if (empty($_POST['consent_postcode'])) {
                        $this->errors['consent_postcode'] = 'Please enter your postcode.';
                        $this->passed = false;
                    } elseif (strlen($_POST['consent_postcode']) < 5 || strlen($_POST['consent_postcode']) > 10) {
                        $this->errors['consent_postcode'] = 'Please enter a valid postcode.';
                        $this->passed = false;
                    } else {
                        $_POST['consent_postcode'] = strtoupper(wordwrap(str_replace(' ', '', $_POST['consent_postcode']), strlen(str_replace(' ', '', $_POST['consent_postcode']))-3, ' ', true));
                    }
                    //parental email address
                    if (empty($_POST['consent_email'])) {
                        $this->errors['consent_email'] = 'Email address is required.';
                        $this->passed = false;
                    } elseif (!filter_var($_POST['consent_email'], FILTER_VALIDATE_EMAIL)) {
                        $this->errors['consent_email'] = 'Email address must be valid. We will use this to confirm your membership.';
                        $this->passed = false;
                    } elseif (strlen($_POST['consent_email']) > 255) {
                        $this->errors['consent_email'] = 'Email Address must number 255 characters.';
                        $this->passed = false;
                    }
                }
                if (!isset($_POST['discipline'])) {
                    $this->errors['discipline'] = "Please pick your discipline(s).";
                    $this->passed = false;
                }
                if (!isset($_POST['expertise']) || ($_POST['expertise'] < 0 || $_POST['expertise'] > 5)) {
                    $this->errors['expertise'] = "Please click a valid option.";
                    $this->passed = false;
                }
                if (empty($_POST['emergency_name_one'])) {
                    $this->errors['emergency_name_one'] = "Please provide a emergency contact name.";
                    $this->passed = false;
                } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['emergency_name_one'])) {
                    $this->errors['emergency_name_one'] = "Emergency name must only contain characters.";
                    $this->passed = false;
                } elseif (strlen($_POST['emergency_name_one']) > 100) {
                    $this->errors['emergency_name_one'] = 'Name must number 100 characters.';
                    $this->passed = false;
                }
                //emergency number 1
                if (empty($_POST['emergency_number_one'])) {
                    $this->errors['emergency_number_one'] = 'A emergency number is required.';
                    $this->passed = false;
                } elseif (!is_numeric($_POST['emergency_number_one'])) {
                    $this->errors['emergency_number_one'] ='The emergency number must only consist of digits.';
                    $this->passed = false;
                } elseif (strlen($_POST['emergency_number_one']) < 7 || strlen($_POST['emergency_number_one']) > 16) {
                    $this->errors['emergency_number_one'] = 'Please enter a valid emergency number.';
                    $this->passed = false;
                }

                //emergency name 2
                if (!empty($_POST['emergency_name_two']) && !empty($_POST['emergency_number_two'])) {
                    if (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['emergency_name_two'])) {
                        $this->errors['emergency_name_two'] = "Emergency name must only contain characters. (a-z A-Z)";
                        $this->passed = false;
                    } elseif (strlen($_POST['emergency_name_two']) > 100) {
                        $this->errors['emergency_name_two'] = 'Name must number 100 characters.';
                        $this->passed = false;
                    }
                    if (!is_numeric($_POST['emergency_number_two'])) {
                        $this->errors['emergency_number_two'] ='The emergency number must only consist of digits.';
                        $this->passed = false;
                    } elseif (strlen($_POST['emergency_number_two']) < 7 || strlen($_POST['emergency_number_two']) > 16) {
                        $this->errors['emergency_number_two'] = 'Please enter a valid emergency number.';
                        $this->passed = false;
                    }
                } elseif (!empty($_POST['emergency_name_two']) && empty( $_POST['emergency_number_two'] )) {
                    $this->errors['emergency2_number'] = 'Please enter a valid emergency number.';
                    $this->passed = false;
                } elseif (empty($_POST['emergency_name_two']) && !empty( $_POST['emergency_number_two'] )) {
                    $this->errors['emergency_name_two'] = 'Please enter a valid emergency contact name.';
                    $this->passed = false;
                }
            
                break;
        
            case 'holiday':
                if (isset($_POST['display_name']) && strlen($_POST['display_name']) == 0) {
                    $this->errors['display_name'] = 'Display name is required.';
                    $this->passed = false;
                } elseif (strlen($_POST['display_name']) < 2) {
                    $this->errors['display_name'] = 'Display name must be more 2 characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['display_name']) > 100) {
                    $this->errors['display_name'] = 'Display name must be under 100 characters.';
                    $this->passed = false;
                }
                if ((isset($_POST['start_date']) && strlen($_POST['start_date']) == 0 ) || (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0 )) {
                    $this->errors['start_date'] = 'Start date and end date is required.';
                    $this->passed = false;
                } else {
                    $t = strtotime('now');
                    $s = strtotime($_POST['start_date']);
                    $e = strtotime($_POST['end_date']);
                    if ($s < $t || $e < $t) {
                        $this->errors['start_date'] = 'Holidays cannot be in the past.';
                        $this->passed = false;
                    } elseif ($s > $e) {
                        $this->errors['start_date'] = 'Start date must be before end date.';
                        $this->passed = false;
                    }
                }

                if (!isset($_POST['visible'])) {
                    $this->errors['visible'] = 'Publish status is required.';
                    $this->passed = false;
                }

                if (!isset($_POST['update_counter'])) {
                    if (!isset($_POST['inherit'])) {
                        $this->errors['inherit'] = 'Default times is required.';
                        $this->passed = false;
                    }
                }
                break;
            case 'session':
                if (isset($_POST['display_name']) && strlen($_POST['display_name']) == 0) {
                    $this->errors['display_name'] = 'Display name is required.';
                    $this->passed = false;
                } elseif (strlen($_POST['display_name']) < 2) {
                    $this->errors['display_name'] = 'Display name must be more 2 characters.';
                    $this->passed = false;
                } elseif (strlen($_POST['display_name']) > 50) {
                    $this->errors['display_name'] = 'Display name must be under 50 characters.';
                    $this->passed = false;
                }
                if (isset($_POST['day']) && $_POST['day'] == 'null') {
                    $this->errors['day'] = 'Day is required.';
                    $this->passed = false;
                }
                if (isset($_POST['start_time']) && strlen($_POST['start_time']) == 0) {
                    $this->errors['start_time'] = 'Start time is required.';
                    $this->passed = false;
                }
                if (isset($_POST['end_time']) && strlen($_POST['end_time']) == 0) {
                    $this->errors['end_time'] = 'End time is required.';
                    $this->passed = false;
                }
                if ((isset($_POST['start_time']) && strlen($_POST['start_time']) != 0) && (isset($_POST['end_time']) && strlen($_POST['end_time']) != 0)) {
                    $s = strtotime($_POST['start_time']);
                    $e = strtotime($_POST['end_time']);

                    if ($_POST['end_time'] != '00:00' && $s > $e) {
                            $this->errors['start_time'] = 'Start time must be before end time.';
                            $this->errors['end_time'] = 'End time is before start time.';
                            $this->passed = false;
                    } elseif ($s == $e) {
                        $this->errors['start_time'] = 'Start time cannot be the same as end time.';
                        $this->errors['end_time'] = 'End time cannot be the same as start time.';
                        $this->passed = false;
                    }
                }

                if (!isset($_POST['beginner'])) {
                    $this->errors['beginner'] = 'Beginner status is required.';
                    $this->passed = false;
                }
                if (!isset($_POST['consecutive'])) {
                    $this->errors['consecutive'] = 'Consecutive status is required.';
                    $this->passed = false;
                }
                if (!isset($_POST['private'])) {
                    $this->errors['private'] = 'Private status is required.';
                    $this->passed = false;
                }

                break;
            case 'note':
                if (empty($_POST['type'] )) {
                    $this->errors['type'] = 'The type is required.';
                    $this->passed = false;
                } elseif (in_array( $_POST['type'], array('1','2', '3', '4') ) != true) {
                    $this->errors['type'] = 'Please provide a valid type.';
                    $this->passed = false;
                }

                if (isset($_POST['start_date']) && strlen($_POST['start_date']) == 0) {
                    $this->errors['start_date'] = 'Start time is required.';
                    $this->passed = false;
                }
                if (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0) {
                    $this->errors['end_date'] = 'End time is required.';
                    $this->passed = false;
                }
                if ((isset($_POST['start_date']) && strlen($_POST['start_date']) != 0) && (isset($_POST['end_date']) && strlen($_POST['end_date']) != 0)) {
                    $s = strtotime($_POST['start_date']);
                    $e = strtotime($_POST['end_date']);

                    if ($_POST['end_date'] != '00:00' && $s > $e) {
                            $this->errors['start_date'] = 'Start time must be before end time.';
                            $this->errors['end_date'] = 'End time is before start time.';
                            $this->passed = false;
                    } elseif ($s == $e) {
                        $this->errors['start_date'] = 'Start time cannot be the same as end time.';
                        $this->errors['end_date'] = 'End time cannot be the same as start time.';
                        $this->passed = false;
                    }
                }

                if (!isset($_POST['comments'])) {
                    $this->errors['comments'] = 'Comment is required.';
                    $this->passed = false;
                }
                break;
            case 'membership':
                if (empty($_POST['type'] )) {
                    $this->errors['type'] = 'The type is required.';
                    $this->passed = false;
                }

                if (isset($_POST['start_date']) && strlen($_POST['start_date']) == 0) {
                    $this->errors['start_date'] = 'Start time is required.';
                    $this->passed = false;
                }
                if (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0) {
                    $this->errors['end_date'] = 'End time is required.';
                    $this->passed = false;
                }
                if ((isset($_POST['start_date']) && strlen($_POST['start_date']) != 0) && (isset($_POST['end_date']) && strlen($_POST['end_date']) != 0)) {
                    $s = strtotime($_POST['start_date']);
                    $e = strtotime($_POST['end_date']);

                    if ($_POST['end_date'] != '00:00' && $s > $e) {
                        $this->errors['start_date'] = 'Start time must be before end time.';
                        $this->errors['end_date'] = 'End time is before start time.';
                        $this->passed = false;
                    } elseif ($s == $e) {
                        $this->errors['start_date'] = 'Start time cannot be the same as end time.';
                        $this->errors['end_date'] = 'End time cannot be the same as start time.';
                        $this->passed = false;
                    }
                }

                break;
        }
    }
}
