<?php


function rwc_add_links() {
	global $wp_admin_bar;
	$args = ['id' 		=> 'membership',
			 'title' 	=> '<span class="ab-icon dashicons-admin-users"></span>RampWorldCardiff Membership',
			 'href' 	=> 'http://www.rampworldcardiff.co.uk/wp-admin/admin.php?page=membership_entry'
			];
	$wp_admin_bar->add_menu( $args);
	$args = ['id' 		=> 'facebook',
			 'title' 	=> '<span class="ab-icon dashicons-facebook-alt"></span>Facebook',
			 'href' 	=> 'https://www.facebook.com/RampWorldCardiff'
			];
	$wp_admin_bar->add_menu( $args);
	$args = ['id' 		=> 'instagram',
			 'title' 	=> 'Instagram',
			 'href' 	=> 'https://www.instagram.com/RampWorldCardiff'
			];
	$wp_admin_bar->add_menu( $args);
}
add_action( 'wp_before_admin_bar_render', 'rwc_add_links');
