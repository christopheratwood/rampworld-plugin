<?php
	use Dompdf\Dompdf;
	use Rampworld\Pagination\Create as Pagination;
	use Rampworld\Event\EventFetch as EventFetch;
	use Rampworld\Event\EventModify as EventModify;
	use Rampworld\Email\Send as Send;
	use Rampworld\Event\Category\CategoryFetch as CategoryFetch;
	use Rampworld\Validation\Check as Check;

	require_once __dir__ . '/../../themes/rampworld/modules/vendor/autoload.php';
/**
 *	Plugin Name: Rampworld cardiff
 * 	Plugin URI: http://www.rampworldcardiff.co.uk
 * 	Description: Allows you to sign member in with ease
 * 	Author: Christopher Atwood
 *  Version: 2.15
 *  License: GPLv2
 */
//Exit if accessed Directly
//Ajax Handler
remove_filter('the_content', 'wpautop');
date_default_timezone_set('Europe/London');
function rw_styles() {

	wp_enqueue_style( 'rwc-admin-membership', plugin_dir_url( __FILE__ ) . 'assets/dist/css/rwcui.min.css', null, null);

	wp_localize_script('rwc-admin-js', 'membership', [
		'security' => wp_create_nonce('membership')]);
}
add_action( 'admin_enqueue_scripts', 'rw_styles' );

//add short codes
require_once 'shortcodes/shortcodes.php';

function rampworld_cardiff() {
	
	add_menu_page(
			'Memberships',
			'RampWorld Cardiff',
			'level_2',
			'membership_entry',
			'rwc_entry',
			'dashicons-admin-users',
			2

		);
	/* top level navigation */

	add_submenu_page('membership_entry', 'Entries', 'Entries', 'level_2', __FILE__.'/entries/view/all', 'membership__entries__view__all');
	add_submenu_page('membership_entry', 'Members', 'Members', 'level_2', __FILE__.'/members/view/all', 'membership__members__view__all');
	add_submenu_page('membership_entry', 'Sessions', 'Sessions', 'manage_options', __FILE__.'/sessions/view/all', 'membership__sessions__view__all');
	add_submenu_page('membership_entry', 'Holidays', 'Holidays', 'level_2', __FILE__.'/holidays/view/all', 'membership__holidays__view__all');
	add_submenu_page('membership_entry', 'Booking', 'Bookings', 'level_2', __FILE__.'/bookings/view/all', 'membership__bookings__view__all');

	add_submenu_page('membership_entry', 'Premises Checks', 'Premises Checks', 'level_2', __FILE__.'/premises-checks/view/all', 'membership__premises__view__all');
	add_submenu_page('membership_entry', 'Settings', 'Settings', 'manage_options', __FILE__.'/settings', 'rwc_membership_settings');
	add_submenu_page('membership_entry', 'Reports', 'Reports', 'manage_options', __FILE__.'/reports', 'membership__report');
	add_submenu_page('membership_entry', 'Paid memberships', 'Paid memberships', 'level_2', __FILE__.'/paid-memberships/view/all', 'membership__paid_membership__view__all');
	add_submenu_page('membership_entry', 'Terms and Conditions', 'Terms and Conditions', 'manage_options', __FILE__.'/terms-and-conditions/view/all', 'membership__tandc__view__all');

	/* end top level navigation */
	/* Secondary level  pages */
	//booking
	add_submenu_page('null', 'Booking', 'Booking', 'manage_options', __FILE__.'/bookings/view/single', 'membership__bookings__view__item');
	add_submenu_page('null', 'Booking', 'Booking', 'manage_options', __FILE__.'/bookings/refund/participant', 'membership__bookings__refund__participant');
	add_submenu_page('null', 'Booking', 'Booking', 'manage_options', __FILE__.'/bookings/refund/booking', 'membership__bookings__refund__booking');
	add_submenu_page('null', 'Staff Notes', 'Staff Notes', 'level_2', __FILE__.'/staff_notes', 'rwc_membership_staff_notes');

	/* Member pages */
	add_submenu_page('null', 'Members', 'Members', 'level_2', __FILE__.'/members/view/', 'membership__members__view__item');
	add_submenu_page('null', 'Members', 'Members', 'manage_options', __FILE__.'/members/delete/', 'membership__members__delete');

	

	//Statistics report
	add_submenu_page('null', 'Statistics', 'Statistics', 'manage_options', __FILE__.'/reports/statistics', 'membership__report__statistics');

	//Holidays
	add_submenu_page('null', 'Holidays', 'Holidays', 'level_2', __FILE__.'/holidays/view/single', 'membership__holidays__view__item');
	add_submenu_page('null', 'Holidays', 'Holidays', 'level_2', __FILE__.'/holidays/create', 'membership__holidays__create');
	add_submenu_page('null', 'Holidays', 'Holidays', 'manage_options', __FILE__.'/holidays/delete', 'membership__holidays__delete');
	
	//Sessions
	add_submenu_page('null', 'Sessions', 'Sessions', 'level_2', __FILE__.'/sessions/create', 'membership__sessions__create');
	add_submenu_page('null', 'Sessions', 'Sessions', 'level_2', __FILE__.'/sessions/view/single', 'membership__sessions__view__item');
	add_submenu_page('null', 'Sessions', 'Sessions', 'manage_options', __FILE__.'/sessions/delete', 'membership__sessions__delete');

	add_submenu_page('null', 'Sessions', 'Sessions', 'manage_options', __FILE__.'/sessions/print', 'membership__sessions__print');
	
	/* report creation page */
	add_submenu_page('null', 'Create report', 'Create report', 'level_2', __FILE__.'/reports/create', 'membership__report__create');	

	/* Premises checks */
	add_submenu_page('null', 'Premises checks', 'Premises checks', 'level_2', __FILE__.'/premises-checks/view/opening', 'membership__premises__view__opening');
	add_submenu_page('null', 'Premises checks', 'Premises checks', 'level_2', __FILE__.'/premises-checks/view/closing', 'membership__premises__view__closing');	
	add_submenu_page('null', 'Premises checks', 'Premises checks', 'level_2', __FILE__.'/premises-checks/add/opening', 'membership__premises__create__opening');	
	add_submenu_page('null', 'Premises checks', 'Premises checks', 'level_2', __FILE__.'/premises-checks/add/closing', 'membership__premises__create__closing');	


	/* System notes - requires member*/
	add_submenu_page('null', 'System notes', 'System notes', 'level_2', __FILE__.'/members/notes/view/all', 'membership__members__notes__view__all');
	add_submenu_page('null', 'System notes', 'System notes', 'level_2', __FILE__.'/members/notes/add', 'membership__members__notes__create');
	add_submenu_page('null', 'System notes', 'System notes', 'level_2', __FILE__.'/members/notes/delete', 'membership__members__notes__delete');

	/*Paid memberships*/
	add_submenu_page('null', 'Paid memberships', 'Paid memberships', 'level_2', __FILE__.'/paid-memberships/add', 'membership__paid_membership__create');
	add_submenu_page('null', 'Paid memberships', 'Paid memberships', 'manage_options', __FILE__.'/paid-memberships/delete', 'membership__paid_membership__delete');
	add_submenu_page('null', 'Paid memberships', 'Paid memberships', 'level_2', __FILE__.'/paid-memberships/view/single', 'membership__paid_membership__view__item');
	add_submenu_page('null', 'Membership types', 'Membership types', 'level_2', __FILE__.'/paid-memberships/types/view/all', 'membership__paid_membership__types__view__item');
	add_submenu_page('null', 'Membership types', 'Membership types', 'level_2', __FILE__.'/paid-memberships/types/add', 'membership__paid_membership__types__add');
	add_submenu_page('null', 'Membership types', 'Membership types', 'manage_options', __FILE__.'/paid-memberships/types/delete', 'membership__paid_membership__types__delete');
	add_submenu_page('null', 'Terms and Conditions', 'Terms and Conditions', 'manage_options', __FILE__.'/terms-and-conditions/view/single', 'membership__tandc__view__item');
	add_submenu_page('null', 'Terms and Conditions', 'Terms and Conditions', 'manage_options', __FILE__.'/terms-and-conditions/add', 'membership__tandc__create');	
	add_submenu_page('null', 'Terms and Conditions', 'Terms and Conditions', 'manage_options', __FILE__.'/terms-and-conditions/delete', 'membership__tandc__delete');

	/* Events */
	add_submenu_page('membership_entry', 'Events', 'Events', 'level_2', __FILE__.'/events/view/all', 'membership__events__view__all');
	add_submenu_page('null', 'View Event', 'View Event', 'manage_options', __FILE__.'/events/view/single', 'membership__events__view__item');
	add_submenu_page('null', 'Create event', 'Membership types', 'manage_options', __FILE__.'/events/add', 'membership__events__create');
	add_submenu_page('null', 'Remove event', 'Remove event', 'manage_options', __FILE__.'/events/delete', 'membership__events__delete');

	/* Event categories */
	add_submenu_page('null', 'Event category', 'Event category', 'manage_options', __FILE__.'/events/category/view/all', 'membership__events__category__view');
	add_submenu_page('null', 'Create event category', 'Create event category', 'manage_options', __FILE__.'/events/category/create', 'membership__events__category__create');
	
	


	add_submenu_page('null', 'Advance search', 'Advance search', 'manage_options', __FILE__.'/members/advance', 'membership__members__advance');
	

	define('database_prefix', "rwc_");
	require_once 'php/classes/Statistics.php';
	require_once 'php/classes/PremisesCheck.php';
	require_once 'php/classes/Session.php';

	$stats  = new Statistics();
	$checks = new PremisesCheck();
	$session = new Session();
	define("isHoliday", $session->isHoliday());
	define("totalMembersOnPremises", $stats->getTotalMembersOnPremises());
	define("totalPaidSessionsToday", $stats->getTotalPaidSessionsToday());
	define("totalPaidPassesToday", $stats->getTotalPaidPassesToday());
	define("openingCheckCompleted", $checks->completedOpeningChecks());
	define("closingCheckCompleted", $checks->completedClosingChecks());
	define("openingCheckWarning", $checks->openingWarning());
	define("closingCheckWarning", $checks->closingWarning());

	$warningMessage = $checks->checkMessage();

	if( $warningMessage !== false ) {
		define("openingMessage__ID", $warningMessage['id']);
		define("openingMessage__message", $warningMessage['message']);
		define("openingMessage__member", $warningMessage['member']);
		define("openingMessage__date", $warningMessage['date']);
	}

	//define("host", "http://127.0.0.1/");
	define("host", "//www.rampworldcardiff.co.uk/");
	//var_dump(openingMessage__ID);
}

add_action('admin_menu','rampworld_cardiff');

function report() {

	if(isset($_GET['report'])) {
		if($_GET['report'] == 'member') {
			if(isset($_GET['member'])) {

				$reissue = (isset($_GET['reissue']) && $_GET['reissue'] == true)? true: false;

				require 'php/classes/Report.php';
				$report= new Report();
				$report->createMember($_GET['member'], $reissue);
			}else {
				wp_redirect($_SERVER['HTTP_REFERER']);
			}
		} else if($_GET['report'] == 'time-duration') {
		
			if(isset($_GET['asi'])) {

				require 'php/classes/Report.php';
				$report= new Report();

				$report->createDurationAdvance( $_GET['asi']);
			} else {
				if(isset($_GET['start_dt']) ) {

					require 'php/classes/Report.php';
					$report= new Report();

					$start = $_GET['start_dt'];
					
					$end = isset($_GET['end_dt'])? $_GET['end_dt'] : null;
					$forename = isset($_GET['forename'])? $_GET['forename'] : null;
					$surname = isset($_GET['surname'])? $_GET['surname'] : null;	
					$membership_id = isset($_GET['membership_id'])? $_GET['membership_id'] : null;
					$page = isset($_GET['p'])? (( $_GET['p'] - 1 ) * $_GET['limit']) : null;
					$limit = isset($_GET['limit'])? $_GET['limit'] : null;
					$reissue = (isset($_GET['reissue']) && $_GET['reissue'] == true)? true: false;

					$report->createDuration( $start, $end, $forename, $surname, $membership_id, $page, $limit);
				} else {
					wp_redirect($_SERVER['HTTP_REFERER']);
				}
			}
			
		} else if($_GET['report'] == 'statistics') {
			if(isset($_GET['asi'])) {

				require 'php/classes/Report.php';
				$report= new Report();

				$report->createStatsAdvance( $_GET['asi']);

			} else if(isset($_GET['start_dt']) ) {

				require 'php/classes/Report.php';
				$report= new Report();

				$start = $_GET['start_dt'];
				$end = isset($_GET['end_dt'])? $_GET['end_dt'] : null;
				$all = isset($_GET['all'])? $_GET['all'] : null;
				$reissue = (isset($_GET['reissue']) && $_GET['reissue'] == true)? true: false;


				
				$report->createStats( $start, $end, $all, $reissue);
			} else {
				wp_redirect($_SERVER['HTTP_REFERER']);
			}
		} else if($_GET['report'] == 'terms'){
			
				if(isset($_GET['tandcid'])) {
	
					require 'php/classes/Report.php';
					$report= new Report();
					$report->createTerms($_GET['tandcid']);
				} else {
					wp_redirect($_SERVER['HTTP_REFERER']);
				}
		} else if($_GET['report'] == 'holiday') {
			if(isset($_GET['start_dt']) && isset($_GET['end_dt'])) {
				require 'php/classes/Report.php';
				$report= new Report();

				$start = $_GET['start_dt'];
				
				$end = $_GET['end_dt'];
		

				$report->createHoliday( $start, $end);
			} else {
				//wp_redirect($_SERVER['HTTP_REFERER']);
			}
		
		} else if($_GET['report'] == 'email_member') {
			if(isset($_GET['member_id'])) {
				require_once 'php/classes/Member.php';
				$member = new Member();
				$member->sendEmail($_GET['member_id']);
				if(isset($_GET['membership'])) {
					wp_redirect('https://www.rampworldcardiff.co.uk/wp-admin/admin.php?page=membership_entry&member_id='. $_GET['member_id']);
				} else {
					wp_redirect($_SERVER['HTTP_REFERER']);
				}
			}
		} else {
			wp_redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
}
add_action('init', 'report');



/* start booking functions */



/**
 * View all bookings
 * 
 */
function membership__bookings__view__all( ) {

	Pagination::booking();
	$menu_name 	= 'booking';
	$sub_name 	= 'view';
	$page_name 	= 'View All Bookings';
	$form_name 	= 'booking';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	?>
	
	<div style="margin: 0 10px;">
		<h2>Bookings</h2>

		<?php echo Pagination::$body;?>

		<?php echo Pagination::$breadcrumb;?>

		
	</div>
	<?php
}


/**
 * View a booking
 * 
 */
function membership__bookings__view__item( ) {
	
	if(!isset($_GET['bid']))  {
		wp_redriect(host. 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall');
	} else {
		require_once 'php/classes/Booking.php';
		$booking = new Booking();
		$booking->get($_GET['bid']);

		$menu_name 	= 'booking';
		$sub_name 	= 'view';
		$page_name 	= 'View Booking';
		$form_name 	= 'booking';
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/booking.php';
	}
	?>
	<?php
}


/**
 * Refund a participant 
 */
function membership__bookings__refund__participant( ) {
	$booking_id 				= (isset($_GET['bid'])) ? intval($_GET['bid']): null;
	$booking_member_id 	= (isset($_GET['ppid'])) ? intval($_GET['ppid']): null;
	$booking_member_pid = (isset($_GET['ppid'])) ? intval($_GET['ppid']): null;
	$booking_member_mid = (isset($_GET['pmid'])) ? intval($_GET['pmid']): null;
	$type 							= ($booking_member_mid != null)? 'membership': 'session';
	$booking_member_id 	= ($type == 'membership') ? $booking_member_mid: $booking_member_pid;

	if($booking_id == null )  {
		wp_redirect(host. 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall');
	} else {
		
			if($booking_id != null && $booking_member_id != null ){
				require_once 'php/classes/Booking.php';
				$booking = new Booking();
				if($booking->refundParticipant($booking_member_id, $type)) {

					wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&refund_success=true&bid='.$booking_id.'');
					die();
				} else {
					wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&bid='.$booking_id.'&refund_error='. str_replace(' ','_' ,$booking->error_message));
					die();
				}
			}
	}
	?>
	<?php
}


/**
 * Refund a booking
 * 
 */
function membership__bookings__refund__booking( ) {

	if(!isset($_GET['bid']) )  {
		wp_redirect(host. 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall');
	} else {
		$booking_id 				= (isset($_GET['bid'])) ? intval($_GET['bid']): null;
		$booking_member_pid = (isset($_GET['ppid'])) ? intval($_GET['ppid']): null;
		$booking_member_mid = (isset($_GET['pmid'])) ? intval($_GET['pmid']): null;
		$type 							= ($booking_member_mid != null)? 'membership': 'session';
		$booking_member_id 	= ($type == 'membership') ? $booking_member_mid: $booking_member_pid;

		if($booking_id != null && $booking_member_id == null) {
			require_once 'php/classes/Booking.php';
			$booking = new Booking();
			if($booking->refund($booking_id)) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings/view/single&refund_success=true&bid='.$booking_id.'&');
				die();
			} else {
				
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings/view/single&bid='.$booking_id.'&refund_error='. str_replace(' ','_' ,$booking->error_message));
				die();
			}
		}
	}
	?>
	<?php
}

/* End of booking functions */

/* Start report functions */

/**
 * View all reports
 *
 * @return void
 */
function membership__report() {
	require 'php/classes/ReportLog.php';
	$reportLog = new ReportLog();
	$menu_name 	= 'report';
	$sub_menu 	= 'view';
	$page_name 	= 'Reports';
	$form_name 	= 'report';
	$report = '';

	$reportLog->getLatest(5);
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/views/report.php';
}

/**
 * Report select for statistics
 *
 * @return void
 */
function membership__report__statistics() {
	$menu_name 	= 'report';
	$report 		= 'member';
	$sub_name 	= 'view';
	$page_name 	= 'Statistical Report';
	$form_name 	= 'statistics';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	//require 'assets/templates/gui/reports/forms/report.php';
}

/* end report functions */

/* start holiday functions */

/**
 * View all holidays
 *
 * @return html form of holidays
 */
function membership__holidays__view__all() {

	require 'php/classes/Pagination.php';
	$pagination = new Pagination();

	$menu_name 	= 'holiday';
	$sub_menu 	= 'view';
	$page_name 	= 'Holiday report';
	$form_name 	= 'holidays';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';?>

	<?php if(isset($_GET['success'])):?>
		<div class="alert alert-success"><strong>Success!</strong> You have deleted holiday #<?php echo $_GET['hid'];?></div>
	<?php endif;?>
	<?php if(isset($_GET['error'])):?>
		<?php if($_GET['error'] == 'incorrect+permissions'):?>
			<div class="alert alert-error"><strong>Error!</strong> You do not the required permissions to remove this member. Action <strong>aborted</strong></div>
		<?php endif;?>
	<?php endif;?>
	<div style="margin: 0 10px;">
		<h2>Holidays</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}


/**
 * View a signle holiday
 * 
 * Requires hid param
 *
 * @return form
 */
function membership__holidays__view__item() {

	if(!isset($_GET['hid']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Holiday.php';
		require_once 'php/classes/Entries.php';

		$holiday = new Holiday();
		

		/* Check if form has been posted */
		if(isset($_POST['display_name'])) {
			
			require_once 'php/classes/Validation.php';
			$validation = new Validation();
			$validation->validate('holiday', $_POST);
			if($validation->passed) {
				$holiday->update($_GET['hid'], $_POST);
				$updated_successfull = true;
			} else {
				$updated_successfull = $validation->errors;
			}

		}

		$holiday->get($_GET['hid']);

		$menu_name 	= 'holiday';
		$sub_name 	= 'view';
		$page_name 	= 'View holiday';

		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/holiday.php';

	}
}


/**
 * Removes a holiday
 * 
 * Requires Adminstration permissions
 *
 * @return void
 */
function membership__holidays__delete() {
	if(!isset($_GET['hid']))  {
		//wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Holiday.php';


		$holiday = new Holiday();
		$holiday->delete($_GET['hid']);
	}
}


/**
 * Create a new holiday
 *
 * @return void
 */
function membership__holidays__create() {

	require_once 'php/classes/Holiday.php';

	$holiday = new Holiday();

	/* Check if form has been posted */
	if(isset($_POST['display_name'])) {
		
		require_once 'php/classes/Validation.php';
		$validation = new Validation();
		$validation->validate('holiday', $_POST);
		if($validation->passed) {
			$holiday->create($_POST);
			//$updated_successfull = true;
		} else {
			$updated_successfull = $validation->errors;
		}
	}

	$menu_name 	= 'holiday';
	$sub_menu 	= 'create';
	$page_name 	= 'Create holiday';

	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/holiday.php';


}

/* End holiday functions */
/* Start session functions */

/**
 * View all sessions normal and holiday
 *
 * @return form
 */
function membership__sessions__view__all() {
	if(!isset($_GET['hid'])) {
		$error = 'noHID';
	}
	require 'php/classes/Pagination.php';
	require 'php/classes/Holiday.php';
	$pagination = new Pagination();
	$holiday = new Holiday();
	$holiday->getList();

	$menu_name 	= 'sessions';
	$sub_menu 	= 'view';
	$page_name 	= 'Sessions';
	$form_name 	= 'sessions';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';?>

	<?php if(isset($_GET['success'])):?>
		<div class="alert alert-success"><strong>Success!</strong> You have deleted session #<?php echo $_GET['member'];?></div>
	<?php endif;?>
	<?php if(isset($_GET['error'])):?>
		<?php if($_GET['error'] == 'incorrect+permissions'):?>
			<div class="alert alert-error"><strong>Error!</strong> You do not the required permissions to remove this session. Action <strong>aborted</strong></div>
		<?php endif;?>
	<?php endif;?>
	<div style="margin: 0 10px;">
		<h2>Sessions</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}


/**
 * View a single session
 * 
 *  Requires hsid or sid param
 *
 * @return void
 */
function membership__sessions__view__item() {
	if(!isset($_GET['hsid']) && !isset($_GET['sid']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Session.php';

		$session = new Session();
		$id = (isset($_GET['hsid'])) ? $_GET['hsid']: $_GET['sid'];
		$type = (isset($_GET['hsid'])) ? 'holiday': 'session';

		/* Check if form has been posted */
		if(isset($_POST['display_name'])) {
			
			require_once 'php/classes/Validation.php';
			$validation = new Validation();
			$validation->validate('session', $_POST);
			if($validation->passed == true) {
				$session->update($_POST, $id, $type);
	
				if($session->error == '')
					$updated_successfull = true;
				else 
					$error = $session->error;
			} else {
				$updated_successfull = $validation->errors;
			}
		}
		
		$session->get($id, (isset($_GET['hsid']) ? true: false));

		$menu_name 	= 'sessions';
		$sub_name 	= 'view';
		$page_name 	= 'View session';

		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/session.php';

	}
}


/**
 * Create a new session for both normal and holiday sessions
 *
 * @return form
 */
function membership__sessions__create() {
	require_once 'php/classes/Holiday.php';
	require_once 'php/classes/Session.php';
	
	
	$holiday = new Holiday();
	$session = new Session();

	$holiday->getList();
	/* Check if form has been posted */
	if(isset($_POST['display_name'])) {
		
		require_once 'php/classes/Validation.php';
		$validation = new Validation();
		$validation->validate('session', $_POST);
		if($validation->passed == true) {
			$session->create($_POST);

			if($session->error == '')
				$updated_successfull = true;
			else 
				$error = $session->error;
		} else {
			$updated_successfull = $validation->errors;
		}
	}
	
	$menu_name 	= 'sessions';
	$sub_menu 	= 'create';
	$page_name 	= 'Create session';

	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/session.php';
}


/**
 * Deletes a session
 * 
 * requires: administration permissions
 *
 * @return form
 */
function membership__sessions__delete() {
	if(!isset($_GET['id']) && !isset($_GET['type']))  {
		wp_redirect($_SERVER['HTTP_REFERER'].'&error=incorrect+permissions');
	} else {
		require_once 'php/classes/Session.php';

		$session = new Session();
		$session->delete($_GET['id'], $_GET['type']);

	}
}


/**
 * print a timetable
 * 
 *
 * @return form
 */
function membership__sessions__print() {
	$menu_name 	= 'sessions';
	$sub_menu 	= 'print';
	$page_name 	= 'Print timetable';

	
	require 'php/classes/Timetable.php';
	$tt = new Timetable();

	$hid = (isset($_GET['hid']))? $_GET['hid'] : null;

	$tt->export($hid);
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/views/timetable.php';
}
/* End session functions */
/* Start Member functions */


/**
 * View all members
 *
 * @return void
 */
function membership__members__view__all() {
	require 'php/classes/Pagination.php';
	$pagination = new Pagination();
	if(isset($_GET['form']) && $_GET['form'] == 'advance-search') {
		require_once 'php/classes/AdvanceSearchLog.php';
		$asl = new AdvanceSearchLog();
		
	}
	

	$menu_name 	= 'members';
	$sub_name 	= 'view_all';
	$page_name 	= 'View Members';
	$form_name 	= 'members';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	?>
	<?php if(isset($_GET['success'])):?>
		<div class="alert alert-success"><strong>Success!</strong> You have deleted member #<?php echo $_GET['member'];?></div>
	<?php endif;?>
	<?php if(isset($_GET['error'])):?>
		<?php if($_GET['error'] == 'could not backup'):?>
			<div class="row"><div class="alert alert-danger"><strong>Error!</strong> A backup was not able to be created. Action <strong>aborted</strong></div></div>
		<?php elseif($_GET['error'] == 'incorrect member'):?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> Member could not be found. Action <strong>aborted</strong></div></div>
		<?php elseif($_GET['error'] == 'incorrect permissions'):?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> You do not the required permissions to remove this member. Action <strong>aborted</strong></div></div>
		<?php elseif($_GET['error'] == 'select options'):?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> You must select valid options from the advance search. Action  <strong>aborted</strong></div></div>
		<?php else:?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> You must select a member to add a membership.Action <strong>aborted</strong></div></div>
		<?php endif;?>
	<?php endif;?>
	<div style="margin: 0 10px;">
		<h2>Members</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}


/**
 * View a single member
 * 
 * Requires: member param
 *
 * @return void
 */
function membership__members__view__item() {
	if(!isset($_GET['member']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Member.php';
		require_once 'php/classes/Entries.php';
		require_once 'php/classes/Booking.php';
		require_once 'php/classes/Purchases.php';
		require_once 'php/classes/Terms.php';
		require_once 'php/classes/Note.php';
		require_once 'php/classes/PaidMembership.php';
		$member = new Member();
		$entries = new Entries();
		$bookings = new Booking();
		$purchases = new Purchases();
		$terms = new Terms();
		$note = new Note();
		$pm = new PaidMembership();

		/* Check if form has been posted */
		if(isset($_POST['forename'])) {

			implode(',',$_POST['discipline']);
			require_once 'php/classes/Validation.php';
			$validation = new Validation();
			$validation->validate('member', $_POST);
			if($validation->passed) {
				$member->update($_GET['member'], $_POST);
				$updated_successfull = true;
			} else {
				$updated_successfull = $validation->errors;
			}

		}
		$member->get($_GET['member']);
		if(count($member->members) != 1)
			wp_redirect($_SERVER['HTTP_REFERER']);

		$entries->getMember($_GET['member']);
		$bookings->getMember($_GET['member']);
		$terms->getByDate($member->members[0]['registered']);
		$note->getMember($_GET['member']);
		$pm->getMember($_GET['member']);

		$menu_name 	= 'members';
		$sub_name 	= 'view';
		$page_name 	= 'View member';

		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/member.php';

	}
}


/**
 * Delete a given member
 *
 * Requires: Administration permissions and member_id param
 * 
 * @return void
 */
function membership__members__delete() {
	if(!isset($_GET['member_id']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Member.php';

		$member = new Member();
		$member->delete($_GET['member_id']);

	}
}

function membership__members__advance() {

	$menu_name 	= 'members';
	$sub_name 	= 'view';
	$page_name 	= 'Advance search';
	$form_name 	= 'entries';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/advance_search.php';
}
/* End member functions */



/* start entry functions */

/**
 * 
 *
 * @return render a paginated list of entries
 */
function membership__entries__view__all() {

	require_once 'php/classes/Pagination.php';
	require_once 'php/classes/Entries.php';
	$pagination = new Pagination();
	$entries = new Entries();


	/* Get current number of attendees */
	//$sessions = $entries->getOverview();
	$sessionOverview = '';

	/* 	foreach($sessions as $session) {
		$sessionOverview .= '<span class="sessionChart" data-total="'.$session['total'].'" data-session-time="'.$session['end_time'].'" data-session-data="scooter:'.$session['Scooter'].';bmx:'.$session['BMX'].';inline:'.$session['Inline'].';skateboard:'.$session['Skateboard'].';spectator:'.$session['Spectator'].';mtb:'.$session['MTB'].';other:'.$session['Other'].'"></span>';
	} */

	$menu_name 	= 'entries';
	$sub_name 	= 'view';
	$page_name 	= 'View Entrires';
	$form_name 	= 'entries';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	require 'assets/templates/gui/views/entries.php';

}


/**
 * View a single member
 * 
 * Requires: member param
 *
 * @return void
 */
function membership__premises__view__opening() {
	if(!isset($_GET['date']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/PremisesCheck.php';

		$premises = new PremisesCheck();

		if(isset($_POST['jump_boxes_0'])) {
			$premises->addOpening($_POST, $_GET['date']);
		}
		$premises->get('opening', $_GET['date']);

		if(count($premises->checks) != 1)
			wp_redirect($_SERVER['HTTP_REFERER']);

		$menu_name 	= 'premises';
		$sub_menu 	= 'view-opening';
		$page_name 	= 'View opening checks';

		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/premises/opening.php';

	}
}


/**
 * View a closing check
 * 
 * Requires: date Y-m-d param
 *
 * @return void
 */
function membership__premises__view__closing() {
	if(!isset($_GET['date']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/PremisesCheck.php';

		$premises = new PremisesCheck();

		if(isset($_POST['stock_0'])) {
			$premises->addClosing($_POST, $_GET['date']);
		}
		$premises->get('closing', $_GET['date']);

		if(count($premises->checks) != 1)
			wp_redirect($_SERVER['HTTP_REFERER']);

		$menu_name 	= 'premises';
		$sub_menu 	= 'view-closing';
		$page_name 	= 'View closing checks';

		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/premises/closing.php';

	}
}



/* start holiday functions */

/**
 * View all holidays
 *
 * @return html form of holidays
 */
function membership__paid_membership__view__all() {
	
		require 'php/classes/Pagination.php';
		require_once 'php/classes/PaidMembership.php';
		$pagination = new Pagination();
		$pm = new PaidMembership();

		$pm->getTypes();
	
		$menu_name 	= 'memberships';
		$sub_menu 	= 'view';
		$page_name 	= 'Paid memberships';
		$form_name 	= 'memberships';
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/forms/search.php';?>
	
		<?php if(isset($_GET['success'])):?>
			<div class="alert alert-success"><strong>Success!</strong> You have deleted member #<?php echo $_GET['member'];?></div>
		<?php endif;?>
		<?php if(isset($_GET['error'])):?>
			<?php if($_GET['error'] == 'incorrect+permissions'):?>
				<div class="alert alert-error"><strong>Error!</strong> You do not the required permissions to remove this member. Action <strong>aborted</strong></div>
			<?php endif;?>
		<?php endif;?>
		<div style="margin: 0 10px;">
			<h2>Paid memberships</h2>
	
			<?php echo $pagination->body;?>
	
			<?php echo $pagination->breadcrumb;?>
	
			
		</div>
		<?php
}


	/**
 * View all holidays
 *
 * @return html form of holidays
 */

	
	
	/**
	 * View a signle holiday
	 * 
	 * Requires hid param
	 *
	 * @return form
	 */
	function membership__paid_membership__view__item() {
	
		if(!isset($_GET['pmid']))  {
			wp_redirect($_SERVER['HTTP_REFERER']);
		} else {
			require_once 'php/classes/PaidMembership.php';
			require_once 'php/classes/Entries.php';
	
			$pm = new PaidMembership();
			$entries = new Entries();
	
			/* Check if form has been posted */
			if(isset($_POST['start_date'])) {
				var_dump($_POST);
				require_once 'php/classes/Validation.php';
				$validation = new Validation();
				$validation->validate('membership', $_POST);
				if($validation->passed) {

					$pm->update($_GET['pmid'], $_POST);
					$updated_successfull = true;
				} else {
					$updated_successfull = $validation->errors;
				}
	
			}
	
			$pm->get($_GET['pmid']);
	
			$menu_name 	= 'memberships';
			$sub_name 	= 'view';
			$page_name 	= 'View membership';
	
			require 'assets/templates/gui/header.php';
			require 'assets/templates/gui/views/membership.php';
	
		}
	}
	
	
	/**
	 * Removes a holiday
	 * 
	 * Requires Adminstration permissions
	 *
	 * @return void
	 */
	function membership__paid_membership__delete() {
		if(!isset($_GET['pmid']))  {
			//wp_redirect($_SERVER['HTTP_REFERER']);
		} else {
			require_once 'php/classes/PaidMembership.php';
	
	
			$pm = new PaidMembership();
			$pm->delete($_GET['pmid']);
		}
	}
	
	
	/**
	 * Create a new holiday
	 *
	 * @return void
	 */
	function membership__paid_membership__create() {

		if(!isset($_GET['member']))
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&error=select+member+to+add+membership');
	
		require_once 'php/classes/PaidMembership.php';
		require_once 'php/classes/Member.php';
	
		$pm = new PaidMembership();
		$member = new Member();
	
		/* Check if form has been posted */
		if(isset($_POST['type'])) {

			$pm->create($_GET['member'], $_POST);
		}

		$member->get(intval($_GET['member']));
		$pm->getTypes();
		$menu_name 	= 'memberships';
		$sub_menu 	= 'add';
		$page_name 	= 'Create membership';
	
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/forms/membership.php';
	
	
	}


	function membership__paid_membership__types__view__item() {

		require 'php/classes/MembershipType.php';
		$mt = new MembershipType();
		
		if(isset($_POST['mtid'])) {
		
			if($mt->update($_POST['mtid'], $_POST) == true) {
				$updated_successfull = true;
			} else {
				$updated_successfull = array('failed to save');
			}
		

		}
		$mt->getAll();


		$menu_name 	= 'memberships';
		$sub_menu 	= 'types';
		$page_name 	= 'View notes';
		
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/membership_type.php';

	}
	
	

	function membership__paid_membership__types__add(){
		require 'php/classes/MembershipType.php';
		$mt = new MembershipType();
		
		if(isset($_POST['display_name'])) {
		
			if($mt->add($_POST['display_name'], $_POST['duration']) == true) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Ftypes%2Fview%2Fall');
			} else {
				$updated_successfull = array('failed to save');
			}
		

		}

		$menu_name 	= 'memberships';
		$sub_menu 	= '';
		$page_name 	= 'View notes';
		
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/forms/membership_type.php';

		
	}


	function membership__paid_membership__types__delete() {
		if(!isset($_GET['mtid']))  {
			wp_redirect($_SERVER['HTTP_REFERER']);
		} else {
			require_once 'php/classes/MembershipType.php';
	
	
			$mt = new MembershipType();
			$mt->delete($_GET['mtid']);
		}
	}
	/* End holiday functions */




/**
 * Undocumented function
 *
 * @return void
 */

function rwc_entry() {
	if(isset($_GET['member_id'])) {
		require_once 'php/classes/Member.php';
		$member = new Member();
		$results = $member->getMemberEntry($_GET['member_id'], 'entry');
	}
	$page_name = 'Membership Entry';
	$menu_name = "main";
	require_once 'assets/templates/gui/header.php';
	require_once 'assets/templates/gui/views/entry.php';?>

	
	<?php if(isset($_GET['member_id'])):?>
		<script type="text/javascript">$('html, body').animate({scrollTop: $("#results").offset().top}, 500);</script>
	<?php endif;
}

/* end entry functions */
/*
*/

/* Start premises functions */

/**
 * View all premises checks
 */
function membership__premises__view__all() {
	require 'php/classes/Pagination.php';
	$pagination = new Pagination();
	

	$menu_name 	= 'premises';
	$sub_menu 	= 'view';
	$page_name 	= 'View checks';
	$form_name 	= 'premises';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	?>
	
	<div style="margin: 0 10px;">
		<h2>Premises checks</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}



/**/


/**
 * View all members
 *
 * @return void
 */
function membership__members__notes__view__all() {
	if(!isset($_GET['member']))
		wp_redirect($_SERVER['HTTP_REFERER']);

	require 'php/classes/Note.php';
	require 'php/classes/Member.php';
	$note = new Note();
	
	if(isset($_POST['nid'])) {
		require 'php/classes/Validation.php';
		$validation = new Validation();

		$validation->validate('note', $_POST);
		if($validation->passed) {
			if($note->update($_POST['nid'], $_POST) == true) {
				$updated_successfull = true;
			} else {
				$updated_successfull = array('failed tosave');
			}
		} else {
			$updated_successfull = $validation->errors;
		}

	}


	$menu_name 	= 'notes';
	$sub_menu 	= 'view';
	$page_name 	= 'View notes';
	$note->getMember($_GET['member']);
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/views/note.php';

	?>
	
	<?php
}


/**
 * add all members
 *
 * @return void
 */
function membership__members__notes__create() {
	if(!isset($_GET['member']))
		wp_redirect($_SERVER['HTTP_REFERER']);

	require 'php/classes/Note.php';
	$note = new Note();
	if(isset($_POST['mid'])) {
		require 'php/classes/Validation.php';
		$validation = new Validation();

		$validation->validate('note', $_POST);
		if($validation->passed) {
			if($note->add($_POST['mid'], $_POST) == true) {
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fview%2Fall&success&member='.$_POST['mid']);	
			} else {
				$updated_successfull = array('failed to save');
			}
		} else {
			$updated_successfull = $validation->errors;
		}

	}


	$menu_name 	= 'notes';
	$sub_menu 	= 'create';
	$page_name 	= 'Add notes';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/note.php';

	?>
	
	<?php
}


/**
 * Removes a holiday
 * 
 * Requires Adminstration permissions
 *
 * @return void
 */
function membership__members__notes__delete() {
	if(!isset($_GET['nid']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		require_once 'php/classes/Note.php';


		$note = new Note();
		$note->delete($_GET['nid']);
		wp_redirect($_SERVER['HTTP_REFERER']);
	}
}



/* End member notes */

/* Start terms and conditions */


/**
 * Undocumented function
 *
 * @return void
 */
function membership__tandc__view__all() {
	require 'php/classes/Pagination.php';
	$pagination = new Pagination();
	

	$menu_name 	= 'tandc';
	$sub_menu 	= 'view';
	$page_name 	= 'View terms and conditions';
	$form_name 	= 'tandcs';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	?>
	<?php if(isset($_GET['success'])):?>
		<div class="alert alert-success"><strong>Success!</strong> You have deleted member #<?php echo $_GET['member'];?></div>
	<?php endif;?>
	<?php if(isset($_GET['error'])):?>
		<?php if($_GET['error'] == 'could+not+backup'):?>
			<div class="row"><div class="alert alert-danger"><strong>Error!</strong> A backup was not able to be created. Action <strong>aborted</strong></div></div>
		<?php elseif($_GET['error'] == 'incorrect+member'):?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> Member could not be found. Action <strong>aborted</strong></div></div>
		<?php elseif($_GET['error'] == 'incorrect+permissions'):?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> You do not the required permissions to remove this member. Action <strong>aborted</strong></div></div>
		<?php else:?>
		<div class="row"><div class="alert alert-danger"><strong>Error!</strong> You must select a member to add a membership.Action <strong>aborted</strong></div></div>
		<?php endif;?>
	<?php endif;?>
	<div style="margin: 0 10px;">
		<h2>Terms and conditions</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}


/**
 * Undocumented function
 *
 * @return void
 */
function membership__tandc__view__item() {

	if(!isset($_GET['tcid']))
		wp_redirect($_SERVER['HTTP_REFERER']);

	require 'php/classes/Terms.php';
	$terms = new Terms();
	
	if(isset($_POST['publish_date'])) {
		
		$isValid = $terms->check(array(
			'publish_date' => date('Y-m-d H:i', strtotime($_POST['publish_date'])),
			'content' => preg_replace('~>\s+<~', '><', $_POST['content']),
			'version_number' => $_POST['version_number']
		));
	
		if($isValid === true) {
			if($terms->update(preg_replace('~>\s+<~', '><', $_POST['content']), date('Y-m-d H:i', strtotime($_POST['publish_date'])), $_POST['version_number'], 0) === true ){
				wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fsingle&tcid='.$terms->insert_id.'&success=created');
			} else {
				$updated_successfull = array('Failed to create new terms and conditions. Please contact administration.');
			}
		} else {
			$updated_successfull = $terms->errors;
		}

	}
	$terms->get($_GET['tcid']);


	$menu_name 	= 'tandc';
	$sub_menu 	= '';
	$page_name 	= 'View terms';
	
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/views/terms.php';

}


/**
 * Undocumented function
 *
 * @return void
 */
function membership__tandc__create() {
	
}


/**
 * Undocumented function
 *
 * @return void
 */
function membership__tandc__delete() {
	
}

/* Events  */


function membership__events__view__all() {

	require 'php/classes/Pagination.php';
	$pagination = new Pagination();

	
	$menu_name 	= 'events';
	$sub_menu 	= 'view';
	$page_name 	= 'View events';
	$form_name 	= 'events';
	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/search.php';
	?>
	<?php if(isset($_GET['success'])):?>
		<div class="alert alert-success"><strong>Success!</strong> You have deleted event #<?php echo $_GET['eid'];?></div>
	<?php endif;?>
	<div style="margin: 0 10px;">
		<h2>Events</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>

		
	</div>
	<?php
}

/**
 * Removes a holiday
 * 
 * Requires Adminstration permissions
 *
 * @return void
 */
function membership__events__delete() {
	if(!isset($_GET['eid']))  {
		wp_redirect($_SERVER['HTTP_REFERER']);
	} else {
		$event = new EventModify();
		$event->delete( $_GET['eid'] );
	}
}





function membership__events__create() {

	$event = new EventModify();
	$validation = new Check();

	/* Check if form has been posted */
	if(isset($_POST['display_name'])) {
		$validation::validate('event', $_POST);
		if($validation::$passed) {
			$id = $event->create($_POST);
			wp_redirect(host. 'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fsingle&eid='.$id);
			$updated_successfull = true;
		} else {
			$updated_successfull = $validation::$errors;
		}
	}

	$menu_name 	= 'events';
	$sub_menu 	= 'add';
	$page_name 	= 'Create event';

	require 'assets/templates/gui/header.php';
	require 'assets/templates/gui/forms/event.php';

}

function membership__events__view__item() {
	
	$update = new EventModify();
	$event = new EventFetch();

	/* Check if form has been posted */
	if(isset($_GET['eid'])) {
		$eid = intval( $_GET['eid'] );
		
	
		if( isset( $_POST['display_name'] ) ) {
			$validation = new Check();
			$validation::validate('event', $_POST);
			if($validation::$passed) {
				$update->update(intval($eid), $_POST);
				$updated_successfull = true;
			} else {
				$updated_successfull = $validation::$errors;
			}
		}

		$event->getEvent( intval( $eid ) );
		$menu_name 	= 'events';
		$sub_menu 	= 'view';
		$page_name 	= 'View event';
	
		require 'assets/templates/gui/header.php';
		require 'assets/templates/gui/views/event.php';

		
	}


}
require 'php/cleandash.php';
require 'php/ajaxrequest.php';