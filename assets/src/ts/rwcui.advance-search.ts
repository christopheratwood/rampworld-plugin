declare var define: any;
declare var $: any;
declare var moment: any;

define(['datepicker'], function () {
	const roots: any = {
		live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
		dev: '//127.0.0.1/wp-admin/admin.php'
	}
	const init = function () {

		$('.date').datetimepicker({
			format: 'YYYY-MM-DD'
		});

		$('#advance-search-btn').off().on('click', function (e: any) {
			e.preventDefault();
			let membership_id: string = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val().replace(/, /g, '|').replace(/,/g, '|') : null);

			const forename: string = (($('#forename').val() !== undefined && $('#forename').val().length > 0) ? $('#forename').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const surname: string = (($('#surname').val() !== undefined && $('#surname').val().length > 0) ? $('#surname').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const start_age: string = (($('#start_age').val() !== undefined && $('#start_age').val().length > 0) ? $('#start_age').val() : null);
			const end_age: string = (($('#end_age').val() !== undefined && $('#end_age').val().length > 0) ? $('#end_age').val() : null);
			const gender: string = (($('#genders:checked').val() !== undefined && $('#genders:checked').val().length > 0) ? $('#genders:checked').map(function () { return this.value; }).get().join('|') : null);
			const dob: string = (($('#dob').val() !== undefined && $('#dob').val().length > 0) ? $('#dob').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const email: string = (($('#email').val() !== undefined && $('#email').val().length > 0) ? $('#email').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const number: string = (($('#form_number').val() !== undefined && $('#form_number').val().length > 0) ? $('#form_number').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const address_line_one: string = (($('#address_line_one').val() !== undefined && $('#address_line_one').val().length > 0) ? $('#address_line_one').val().replace(/, /, '|').replace(/,/g, '|') : null)
			const address_line_two: string = (($('#address_line_two').val() !== undefined && $('#address_line_two').val().length > 0) ? $('#address_line_two').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const address_county: string = (($('#address_county').val() !== undefined && $('#address_county').val().length > 0) ? $('#address_county').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const address_city: string = (($('#address_city').val() !== undefined && $('#address_city').val().length > 0) ? $('#address_city').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const address_postcode: string = (($('#address_postcode').val() !== undefined && $('#address_postcode').val().length > 0) ? $('#address_postcode').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const registered_start_date = (($('#start_date').val() !== undefined && $('#start_date').val().length > 0) ? $('#start_date').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const registered_end_date: string = (($('#end_date').val() !== undefined && $('#end_date').val().length > 0) ? $('#end_date').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const report_start: string = (($('#report_start').val() !== undefined && $('#report_start').val().length > 0) ? $('#report_start').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const report_end: string = (($('#report_end').val() !== undefined && $('#report_end').val().length > 0) ? $('#report_end').val().replace(/, /, '|').replace(/,/g, '|') : null);
			const age_groups: string = (($('#age_groups:checked').val() !== undefined && $('#age_groups:checked').val().length > 0) ? $('#age_groups:checked').map(function () { return this.value; }).get().join('|') : null);
			const disciplines: string = (($('#disciplines:checked').val() !== undefined && $('#disciplines:checked').val().length > 0) ? $('#disciplines:checked').map(function () { return this.value; }).get().join('|') : null);
			const expertise: string = (($('#expertise:checked').val() !== undefined && $('#expertise:checked').val().length > 0) ? $('#expertise:checked').map(function () { return this.value; }).get().join('|') : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&form=advance-search${((membership_id != null) ? '&membership_id=' + membership_id : '')}
			${((forename != null) ? '&forename=' + forename : '')}
			${((surname != null) ? '&surname=' + surname : '')}
			${((start_age != null) ? '&start_age=' + start_age : '')}
			${((end_age != null) ? '&end_age=' + end_age : '')}
			${((gender != null) ? '&gender=' + gender : '')}
			${((dob != null) ? '&dob=' + dob : '')}
			${((email != null) ? '&email=' + email : '')}
			${((number != null) ? '&number=' + number : '')}
			${((address_line_one != null) ? '&address_line_one=' + address_line_one : '')}
			${((address_line_two != null) ? '&address_line_two=' + address_line_two : '')}
			${((address_city != null) ? '&address_city=' + address_city : '')}
			${((address_county != null) ? '&address_county=' + address_county : '')}
			${((address_postcode != null) ? '&address_postcode=' + address_postcode : '')}
			${((registered_start_date != null) ? '&registered_start_date=' + registered_start_date : '')}
			${((registered_end_date != null) ? '&registered_end_date=' + registered_end_date : '')}
			${((report_start != null) ? '&report_start=' + report_start : '')}
			${((report_end != null) ? '&report_end=' + report_end : '')}
			${((age_groups != null) ? '&age_groups=' + age_groups : '')}
			${((disciplines != null) ? '&disciplines=' + disciplines : '')}
			${((expertise != null) ? '&expertise=' + expertise : '')}`;

		});
		$(document).on('change', '#check-all--expertise', function (this: any) {
			$('.expertise-item').prop("checked", this.checked);
		});

		$(document).on('change', '#check-all--gender', function (this: any) {
			$('.gender-item').prop("checked", this.checked);
		});
		$(document).on('change', '#check-all--discipline', function (this: any) {
			$('.discipline-item').prop("checked", this.checked);
		});
		$(document).on('change', '#check-all--age', function (this: any) {
			$('.age-item').prop("checked", this.checked);
		});
	};
	return {
		init: init
	}
});