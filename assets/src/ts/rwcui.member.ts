declare var $: any;
declare var define: any;

declare var ajaxurl: string;

define(['rwcui.loader', 'rwcui.entry'], function (loader: any, entry: any) {

	let processing: boolean = false;
	const init = function (): void {
		
		/* Start On enter event handlers  */
		$('#membership-number-input').on('keydown', function (e: any) {
			let keycode: number = e.keyCode || e.which;
			if (keycode == 13) {
				e.preventDefault();
				processMember();
			}
		});

		$('#custom-input-1').on('keydown', function (e: any) {

			let keycode: number = e.keyCode || e.which;
			if (keycode == 13) {

				e.preventDefault();
				processMemberCustom();
			}
		});

		$('#custom-input-2').on('keydown', function (e: any) {

			let keycode: number = e.keyCode || e.which;
			if (keycode == 13) {

				e.preventDefault();
				processMemberCustom();
			}
		});
		/* End On enter event handlers  */

		/* Start On click event handlers  */
		$('#search-member-number').on('click touchstart', function (e: any) {
			
			e.preventDefault();
			processMember();
		});
		
		$('#search-custom').on('click touchstart', function (e: any) {

			e.preventDefault();
			processMemberCustom();
		});

		$('#reset-entry').on('click', function (e: any) {

			entry.reset();
		});
		/* End On click event handlers  */

	};
	const processMember = function (): void {
		let memId: any = $('#membership-number-input');

		let start_time: number;

		if (memId.val().length < 1) {
			$(memId).parent().addClass('has-feedback has-error');
		} else {
			$.ajax({
				url: ajaxurl,
				type: 'post',
				data: {
					action: 'get_member_by_id',
					memberID: memId.val()
				},
				beforeSend: function () {
					start_time = performance.now();
					loader.create('search-member-number');
					$('#members').html('<tr><td colspan="8"><p class="lead" style="text-align: center">Searching&hellip;</p></td></tr>');

				},
				success: function (response) {
					//set load time

					$('#query-time').text(Math.floor(performance.now() - start_time) + 'ms');

					loader.remove('search-member-number');

					render(response);
					$('#entry-table tbody').off().on('click', 'tr', function (e) {
						e.preventDefault();
						if( $( this ).data( 'booking-id' ) == null ) {
							if ($(this).attr('data-staff-verification') == '1') {
								var id = $(this).attr('data-member');
								entry.verifyMemberGUI(id, $(this).children('td:nth-child(4)').text(), null);
							} else {
								entry.processEntry(
									$(this).children('td').first().text(),
									$(this).children('td:nth-child(2)').text(),
									$(this).children('td:nth-child(3)').text(),
									$(this).children('td:nth-child(4)').text(),
									($(this).children('td:nth-child(8)').text() != '') ? $(this).children('td:nth-child(8)').text() : false,
									$(this).data('is-banned'),
									$(this).data('has-membership'),
									$(this).children('td:nth-child(5)').text(),
									null,
									$(this).data('already-on-premises'),
									$(this).data('current-session'),
									null)
							}
						} else {
							entry.processBooking( $( this ).data( 'booking-id' ) );
						}
					});
				},
				error: function (error) {
					$('#query-time').text(Math.floor(performance.now() - start_time) + 'ms');

					loader.remove('search-member-number');
					var start_time = performance.now();
					clearTable();
					$('#entry-table tbody').append('<tr><td colspan="8"><p class="lead" style="text-align: center">No results</p></td></tr>');

					$('html, body').animate({
						scrollTop: $("#entry-table tbody").offset().top
					}, 500);
					$('#render-time').text(Math.floor(performance.now() - start_time) + "ms");
				}
			})
		}
	};
	const processMemberCustom = function () {

		let input_1: any = $('#custom-input-1').val();
		let field_1: any = $('#custom-field-1').val();
		let input_2: any = $('#custom-input-2').val();
		let field_2: any = $('#custom-field-2').val();

		if (field_1 == 'date of birth') {
			let day: number = parseInt($('#day').val());
			let month: number = parseInt($('#month').val());
			let year: any = $('#year').val();
			let currentYear: any = (new Date()).getFullYear();

			let formatted: any = {
				day: (day < 10) ? '0' + day : day,
				month: (month < 10) ? '0' + month : month
			};

			if (year.length == 2) {
				if (year <= parseInt(currentYear.toString().substr(-2))) {
					input_1 = `20${year}-${formatted.month}-${formatted.day}`;
				} else {
					input_1 = `19${year}-${formatted.month}-${formatted.day}`;
				}
			} else {
				input_1 = `${year}-${formatted.month}-${formatted.day}`;
			}
		}
		if (field_2 == 'date of birth') {
			let day: number = parseInt($('#day').val());
			let month: number = parseInt($('#month').val());
			let year: any = $('#year').val();
			let currentYear: any = (new Date()).getFullYear();

			let formatted: any = {
				day: (day < 10) ? '0' + day : day,
				month: (month < 10) ? '0' + month : month
			};
			if (year.length == 2) {
				if (year <= parseInt(currentYear.toString().substr(-2))) {
					input_2 = `20${year}-${formatted.month}-${formatted.day}`;
				} else {
					input_2 = `19${year}-${formatted.month}-${formatted.day}`;
				}
			} else {
				input_2 = `${year}-${formatted.month}-${formatted.day}`;
			}

		}
		if (input_1 === undefined && input_2 === undefined) {
			$('#custom-input-1').parent().parent().addClass('has-feedback has-error');
		} else if (input_1.length < 1 && input_2.length < 1) {
			$('#custom-input-1').parent().parent().addClass('has-feedback has-error');

		} else {
			$('#custom-input-1').parent().parent().removeClass('has-error').addClass('has-feedback has-success');
			var start_time: number = 0;
			var data: any;

			data = {
				action: 'get_member_by_custom',
				field_one_opt: (input_1.length != 0) ? field_1 : null,
				field_one_input: (input_1.length != 0) ? input_1 : null,
				field_two_opt: (input_2.length != 0) ? field_2 : null,
				field_two_input: (input_2.length != 0) ? input_2 : null
			};
			$.ajax({
				url: ajaxurl,
				type: 'post',
				data: data,
				beforeSend: function () {
					start_time = performance.now();
					loader.create('search-custom');
					$('#members').html('<tr><td colspan="8"><p class="lead" style="text-align: center">Searching&hellip;</p></td></tr>');

				},
				success: function (response) {
					$('#query-time').text(Math.floor(performance.now() - start_time) + 'ms');
					loader.remove('search-custom');
					render(response);

					$('#entry-table tbody').off().on('click', 'tr', function (e) {
						e.preventDefault();
						processing = true;
						if( $( this ).data( 'booking-id' ) == null ) {
							if ($(this).data('staff-verification') == '1') {
								var id = $(this).attr('data-member');
								entry.verifyMemberGUI(id, $(this).children('td:nth-child(4)').text(), $(this).data('ppid'));
							} else {
								entry.processEntry(
									$(this).children('td').first().text(),
									$(this).children('td:nth-child(2)').text(),
									$(this).children('td:nth-child(3)').text(),
									$(this).children('td:nth-child(4)').text(),
									($(this).children('td:nth-child(8)').text() != '') ? $(this).children('td:nth-child(8)').text() : false,
									$(this).data('is-banned'),
									$(this).data('has-membership'),
									$(this).children('td:nth-child(5)').text(),
									null,
									$(this).data('already-on-premises'),
									$(this).data('current-session'),
									$(this).data('ppid'), );
							}
						} else {
							entry.processBooking( $( this ).data( 'booking-id' ));
						}
					});
				},
				error: function (error: any) {
					$('#query-time').text(Math.floor(performance.now() - start_time) + 'ms');

					loader.remove('search-custom');
					start_time = performance.now();
					clearTable();
					$('#entry-table tbody').append(`
						<tr>
							<td colspan="8"><p class="lead" style="text-align: center">No results</p></td>
						</tr>`);

					$('html, body').animate({
						scrollTop: $("#entry-table tbody").offset().top
					}, 500);
					$('#render-time').text(Math.floor(performance.now() - start_time) + "ms");

				}
			});

		}
	};
	const render = function (result: any) {
		let start_time: number = performance.now();
		const members: any = JSON.parse(result);

		$('#number-of-members').html(members.length);

		$('#entry-table tbody').focus();

		clearTable();
		if (members.length == 0) {
			$('#entry-table tbody').append('<tr><td colspan="8"><p class="lead" style="text-align: center;">No results</p></td></tr>');

			$('html, body').animate({
				scrollTop: $("#entry-table tbody#members").offset().top
			}, 500)
		} else {
			if(members[0].booking_id != null) {
				$('#entry-table thead').html('<tr><th>#</th><th>Booking name</th><th>Booking number</th><th>Booking email</th><th>Total participants</th></tr>');
				for (var i = 0; i < members.length; i++) {
					/* class */


					$('#entry-table tbody#members').append(`
						<tr class="status-booking" data-booking-id="${members[i].booking_id}">
							<td>${members[i].booking_id}</td>
							<td>${members[i].name}</td>
							<td>${members[i].number}</td>
							<td>${members[i].email}</td>
							<td>${parseInt(members[i].countSession) + parseInt(members[i].countPaid)}</td>
						
						</tr>`);

					if (members.length == 1) {
						entry.processBooking( members[0].has_booking);
					}

				}
			} else {

				for (var i = 0; i < members.length; i++) {
					/* class */
					/* staff verification */
					let sv: boolean = (members[i].staff_verification == '0') ? true : false;

					let class_name: string = "";
					if (members[i].is_banned)
						class_name = "banned"
					else if (members[i].alreadyOnPremises)
						class_name = "on-premises"
					else if (members[i].staff_verification == '0')
						class_name = "new-member"
					else if (members[i].has_membership )
						class_name = "membership"
					else if (members[i].has_booking != null )
						class_name = "booking"
					else if (members[i].note_comment != null)
						class_name = "notes"
					else
						class_name = "success"

					/* prepare session */
					let booking: any = (members[i].has_booking != null) ? members[i].has_booking : false;
					
					/* Has membership */
					let hm: any = (members[i].has_membership != null) ? members[i].has_membership : "None";
					/*is banned*/
					let ib: any = (members[i].is_banned != null) ? members[i].is_banned : false;
					/*Current session*/
					let cs: any = (members[i].currentSession != null) ? members[i].currentSession : false;

					let notes: string = '';
					let types: any = { 1: 'Ban', 2: 'Temporary Ban', 3: 'Anti-social', 4: 'Other' };

				
					$('#entry-table tbody#members').append(`
						<tr class="status-${class_name}" data-already-on-premises="${members[i].alreadyOnPremises}" data-member="${members[i].member_id}" ${((sv != false) ? 'data-staff-verification="1" data-title="Staff verfication is required" data-toggle="tooltip"' : '')} data-is-banned="${ib}" ${((booking != false) ? 'data-booking-id="' + booking + '"' : 'data-booking-id="null"')} ${((hm != false) ? 'data-has-membership="' + hm + '"' : 'data-ppid="0"')} data-current-session="${cs}">
							<td>${members[i].member_id}</td>
							<td>${members[i].forename}</td>
							<td>${members[i].surname}</td>
							<td>${calcuateAge(members[i].dob)}</td>
							<td>${expertise(members[i].expertise)}</td>
							<td>${members[i].discipline}</td>
							<td>${(members[i].medical_notes != null) ? members[i].medical_notes : ''}</td>
							<td>${(members[i].note_comment != null) ? members[i].note_comment : ''}</td>
						</tr>`);


				}

				if (members.length == 1) {
					if( members[0].has_booking == null) {

					
						if (members[0].staff_verification == '0') {
		
							entry.verifyMemberGUI(
								members[0].member_id,
								calcuateAge(members[0].dob),
								expertise(members[0].expertise),
								(members[0].has_membership != null) ? members[0].has_membership : "None",
								null);
						} else {
							entry.processEntry(
								members[0].member_id,
								members[0].forename,
								members[0].surname,
								calcuateAge(members[0].dob),
								(members[0].note_comment != null )? members[0].note_comment : false,
								members[0].is_banned != null,
								(members[0].has_membership != null) ? members[0].has_membership : "None",
								expertise(members[0].expertise),
								null,
								members[0].alreadyOnPremises,
								members[0].currentSession,
								null);
		
						}
					} else {
						entry.processBooking( members[0].has_booking );
					}
				} else {
					$('html, body').animate({
						scrollTop: $("#entry-table tbody#members").offset().top - 100
					}, 500)
				}
			}
		}
		$('#render-time').text(Math.floor(performance.now() - start_time) + "ms");
	};
	const expertise = function (id: string): string {
		let expertise: string = '';

		switch (id) {
			case '0':
				expertise = 'Beginner';
				break;
			case '1':
				expertise = 'Novice';
				break;
			case '2':
				expertise = 'Experienced';
				break;
			case '3':
				expertise = 'Advanced';
				break;
			case '4':
				expertise = 'Expert';
				break;
			case '5':
				expertise = 'Professional';
				break;
			default:
				expertise = 'Beginner';
				break
		}
		return expertise
	};

	const calcuateAge = function (dob: any): number {

		let ageDifMs: number = (Date.now() - new Date(dob).getTime());
		let ageDate: Date = new Date(ageDifMs);

		return Math.abs(ageDate.getUTCFullYear() - 1970)
	};
	const clearTable = function (): void {
		const total_rows: number = parseInt($('#entry-table tbody tr').length);
		const interval: number = 100 / total_rows;

		$.each($('#entry-table > tbody > tr'), function (i, e) {
			$(this)
				.find('td')
				.wrapInner('<div style="display: block;" />')
				.parent()
				.find('td > div')
				.slideUp(interval, function () {
					$(this).parent().parent().remove();

				});

		});
	}
	return {
		init: init,
		calcuateAge: calcuateAge,
		expertise: expertise
	}
});