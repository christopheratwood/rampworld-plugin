declare var $: any;
declare var define: any;

define(['jquery'], function ($: any) {
	const create = function (element: string) {
		$('#' + element).addClass('bg-success');
		$('#' + element).html(`
			<div class="cs-loader">
				<div class="cs-loader-inner">
					<label>	●</label><label>	●</label><label>	●</label>
				</div>
			</div>`);
		fixedCreate();
	};
	const remove = function (element: string) {
		$('#' + element).removeClass('bg-success');
		$('#' + element).html('<i class="glyphicon glyphicon-arrow-right"></i>');
		fixedDelete();
	};
	const fixedCreate = function (message: string = "Loading") {
		$('body').append(`
			<div id="loading_fixed_notice" class="active"><p>${message}</p></div>`);
	};
	const fixedDelete = function () {
		$('#loading_fixed_notice').removeClass('active');
		setTimeout(function () {
			$('#loading_fixed_notice').remove();
		}, 100);
	}
	const toast = function (message, duration = 500) {
		if ($('#loading_fixed_notice').length) {

			$('#loading_fixed_notice > p').text(message);
			setTimeout(function () {
				$('#loading_fixed_notice').removeClass('active');
				setTimeout(function () {
					$('#loading_fixed_notice').remove();
				}, 300);
			}, duration);

		} else {
			$('body').append(`
			<div id="loading_fixed_notice" class="active"><p>${message}</p></div>`);
			setTimeout(function () {
				$('#loading_fixed_notice > p').text(message);
				setTimeout(function () {
					$('#loading_fixed_notice').removeClass('active');
					setTimeout(function () {
						$('#loading_fixed_notice').remove();
					}, 300);
				 }, duration);
			}, 300);
		}
	}
	return {
		create: create,
		remove: remove,
		fixedCreate: fixedCreate,
		fixedDelete: fixedDelete,
		toast: toast
	}
});