declare var $: any;
declare var define: any;

define(['datepicker', 'moment'], function (date: any, moment: any) {
	const roots: any = {
		live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
		dev: '//127.0.0.1/wp-admin/admin.php'
	}
	const init = function (): void {


		if ($('#table-dates-panel').length) {

			if ($('#start_dt').length) {
				$('#start_dt').datetimepicker({
					format: 'YYYY-MM-DD HH:mm'
				});
			}
			if ($('#end_dt').length) {
				$('#end_dt').datetimepicker({
					format: 'YYYY-MM-DD HH:mm'
				});
			}
			if ($('#startDateGui').length) {
				$('#startDateGui').datetimepicker({
					defaultDate: moment().subtract(7, 'd').format('YYYY-M-DD'),
				});
			}
			if ($('#endDateGui').length) {
				$('#endDateGui').datetimepicker({
					defaultDate: moment().format('YYYY-M-DD'),
				});
			}
			$('#holiday_start_dt').datetimepicker({
				format: 'YYYY-MM-DD'
			});
		}
		if ($('.time').length) {
			$('.time').datetimepicker({
				format: 'HH:mm:ss'
			});
			$('.date').datetimepicker({
				format: 'YYYY-MM-DD'
			});
			$('.datetime').datetimepicker({
				format: 'YYYY-MM-DD HH:mm:ss'
			});
		}

		$(document).on('click', '.panel-heading', function (this: any, e: any) {
			var self = $(this);
			toggle(self);
		});


		//proudct filter
		$('#filter_btn_bookings').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			let field: string = (($('#searchField').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchField').val() : null);
			let query: string = (($('#searchQuery').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchQuery').val() : null);

			if (field == 'booking date') {
				if (query.indexOf('/') != -1) {
					let s: any = query.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query.indexOf('-') != -1) {
					let s: any = query.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query.indexOf('.') != -1) {
					let s: any = query.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			const limit = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((field != null) ? '&search_field=' + field.replace(/ /g, "+") : '')}
				${((query != null) ? '&search_query=' + query.replace(/ /g, '+') : '')}`;
		});
		$('#reset_btn_bookings').on('click', function (this: any, e: any) {
			e.preventDefault();

			$("#limit:checked").removeAttr("checked");
			$('#searchQuery').val('');

			$('.input-group-btn .dropdown-toggle').html('Booking Email <span class="caret"></span>');
		});

		$('#filter_btn_entries').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			const forename: string = (($('#forename').val() !== undefined && $('#forename').val().length > 0) ? $('#forename').val() : null);
			const surname: string = (($('#surname').val() !== undefined && $('#surname').val().length > 0) ? $('#surname').val() : null);
			const membership_id: string = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val() : null);
			let start_dt: any = (($('#start_dt').val() !== undefined && $('#start_dt').val().length > 0) ? $('#start_dt').val() : null);
			let end_dt: any = (($('#end_dt').val() !== undefined && $('#end_dt').val().length > 0) ? $('#end_dt').val() : null);

			if (start_dt != null) {
				start_dt = start_dt.split(' ');

				start_dt = start_dt[0] + 'T' + start_dt[1];
			}
			if (end_dt != null) {
				end_dt = end_dt.split(' ');

				end_dt = end_dt[0] + 'T' + end_dt[1];
			}
			const limit: number = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : 999);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((forename != null) ? '&forename=' + forename.replace(/ /g, '+') : '')}
				${((surname != null) ? '&surname=' + surname.replace(/ /g, '+') : '')}
				${((membership_id != null) ? '&membership_id=' + membership_id.replace(/ /g, '+') : '')}
				${((start_dt != null) ? '&start_dt=' + start_dt : '')}
				${((end_dt != null) ? '&end_dt=' + end_dt : '')}`;
		});
		$('#reset_btn_entries').on('click', function (this: any, e: any) {
			e.preventDefault();

			$('#forname').val('');
			$('#surname').val('');
			$('#membership_id').val('');
			$('#session').val('');
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p=1&limit=999`;

		});

		$('#filter_btn_report_member').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			let field1: any = (($('#searchField').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchField').val() : null);
			let query1: any = (($('#searchQuery').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchQuery').val() : null);
			let field2: any = (($('#searchField2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchField2').val() : null);
			let query2: any = (($('#searchQuery2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchQuery2').val() : null);
			let membership_id: string = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val() : null);


			if (field1 == 'date of birth') {
				if (query1.indexOf('/') != -1) {
					let s: any = query1.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('-') != -1) {
					let s: any = query1.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('.') != -1) {
					let s: any = query1.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			if (field2 == 'date of birth') {
				if (query2.indexOf('/') != -1) {
					let s: any = query2.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('-') != -1) {
					let s: any = query2.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('.') != -1) {
					let s: any = query2.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			const limit = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fmember&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((membership_id != null) ? '&membership_id=' + membership_id : '')}
				${((field1 != null) ? '&searchField1=' + field1.replace(/ /g, '+') : '')}
				${((query1 != null) ? '&searchQuery1=' + query1.replace(/ /g, '+') : '')}
				${((field2 != null) ? '&searchField2=' + field2.replace(/ /g, '+') : '')}
				${((query2 != null) ? '&searchQuery2=' + query2.replace(/ /g, '+') : '')}`;
		});

		$('#filter_btn_member').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			let field1: any = (($('#searchField').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchField').val() : null);
			let query1: any = (($('#searchQuery').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchQuery').val() : null);
			let field2: any = (($('#searchField2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchField2').val() : null);
			let query2: any = (($('#searchQuery2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchQuery2').val() : null);
			let membership_id: string = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val() : null);


			if (field1 == 'date of birth') {
				if (query1.indexOf('/') != -1) {
					let s: any = query1.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('-') != -1) {
					let s: any = query1.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('.') != -1) {
					let s: any = query1.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			if (field2 == 'date of birth') {
				if (query2.indexOf('/') != -1) {
					let s: any = query2.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('-') != -1) {
					let s: any = query2.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('.') != -1) {
					let s: any = query2.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			const limit = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((membership_id != null) ? '&membership_id=' + membership_id : '')}
				${((field1 != null) ? '&searchField1=' + field1.replace(/ /g, '+') : '')}
				${((query1 != null) ? '&searchQuery1=' + query1.replace(/ /g, '+') : '')}
				${((field2 != null) ? '&searchField2=' + field2.replace(/ /g, '+') : '')}
				${((query2 != null) ? '&searchQuery2=' + query2.replace(/ /g, '+') : '')}`;
		});
		$('#reset_btn_member').on('click', function (this: any, e: any) {
			e.preventDefault();

			$('#searchQuery').val('');
			$('#searchQuery2').val('');
			$('#membership_id').val('');
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&p=1&limit=16`;

		});

		$('#filter_btn_session').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			const hid: any = (($('#hid').val() !== undefined && $('#hid').val().length > 0) ? $('#hid').val() : null);
			const day: any = (($('#day').val() !== undefined && $('#day').val().length > 0 && $('#day').val() != null) ? $('#day').val() : null);
			const limit: any = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((hid != null && hid != 'null') ? '&hid=' + hid : '')}
				${((day != null && day != 'null') ? '&day=' + day : '')}`;
		});
		$('#reset_btn_session').on('click', function (this: any, e: any) {
			$('#hid').val("null");
			$('#day').val("null");
		});

		$('#filter_btn_holidays').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			let start: string = (($('#startDate').val() !== undefined && $('#startDate').val().length > 0) ? $('#startDate').val() : null);
			let end: string = (($('#endDate').val() !== undefined && $('#endDate').val().length > 0) ? $('#endDate').val() : null);
			let holiday_id: string = (($('#holiday_id').val() !== undefined && $('#holiday_id').val().length > 0) ? $('#holiday_id').val() : null);
			let limit: any = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			let start_dt: any;
			let end_dt: any;

			if (start != null) {
				start_dt = start.split(' ');
				start = start_dt[0];
			}
			if (end != null) {
				end_dt = end.split(' ');
				end = end_dt[0];
			}
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((holiday_id != null) ? '&hid=' + holiday_id : '')}
				${((start != null) ? '&start_dt=' + start + ((end != null) ? '&end_dt=' + end : '') : '')}`;
		});
		$('#reset_btn_holidays').on('click', function (this: any, e: any) {
			e.preventDefault();

			$('#holiday_id').val('');
			$("#startDate").val('');
			$('#endDate').val('');
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall&p=1`;

		});

		$('#filter_btn_premises').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			var date = (($('#start_date').val() !== undefined && $('#start_date').val().length > 0) ? $('#start_date').val() : null);


			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall&p=1
				${((date != null) ? '&date=' + date : '')}`;
		});
		$('#reset_btn_premises').on('click', function (this: any, e: any) {
			e.preventDefault();
			$('#start_date').val('');
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall`;
		});
		/* reports*/
		$('#report_create_member').on('click', function (this: any, e: any) {
			e.preventDefault();
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&report=member
				&member=${$("#member_id").val()}
				&start_date=${$('#startDate').val()}
				&end_date=${$('#endDate').val()}`;
		});
		/* reports*/
		$('#report_create_stats').on('click', function (this: any, e: any) {
			e.preventDefault();
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&report=statistics
				&start_dt=${$('#startDate').val()}
				&end_dt=${$('#endDate').val()}
				${($('#all').is(':checked') ? '&all=true' : '&all=false')}`;
		});

		$('#filter_btn_memberships').off().on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			let field1: any = (($('#searchField').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchField').val() : null);
			let query1: any = (($('#searchQuery').val() !== undefined && $('#searchQuery').val().length > 0) ? $('#searchQuery').val() : null);
			let field2: any = (($('#searchField2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchField2').val() : null);
			let query2: any = (($('#searchQuery2').val() !== undefined && $('#searchQuery2').val().length > 0) ? $('#searchQuery2').val() : null);
			let membership_id: any = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val() : null);
			let start_dt: any = (($('#start_date').val() !== undefined && $('#start_date').val().length > 0) ? $('#start_date').val() : null);
			let end_dt: any = (($('#end_date').val() !== undefined && $('#end_date').val().length > 0) ? $('#end_date').val() : null);
			let type: any = (($('#type').val() !== undefined && $('#type').val().length > 0 && $('#type').val() != 'select membership type') ? $('#type').val() : null);


			if (field1 == 'date of birth') {
				var s: any;
				if (query1.indexOf('/') != -1) {
					s = query1.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('-') != -1) {
					s = query1.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query1.indexOf('.') != -1) {
					s = query1.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query1 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			if (field2 == 'date of birth') {
				let s: any;
				if (query2.indexOf('/') != -1) {
					s = query2.split('/');
					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];
					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('-') != -1) {
					s = query2.split('-');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				} else if (query2.indexOf('.') != -1) {
					s = query2.split('.');

					if (s[0].length != 2 && s[0] < 10)
						s[0] = '0' + s[0];
					if (s[1].length != 2 && s[1] < 10)
						s[1] = '0' + s[1];
					if (s[2].length == 2)
						s[2] = '20' + s[2];

					query2 = s[2] + '-' + s[1] + '-' + s[0];
				}
			}
			const limit: string = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);


			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((membership_id != null) ? '&membership_id=' + membership_id : '')}
				${((field1 != null) ? '&searchField1=' + field1.replace(/ /g, '+') : '')}
				${((query1 != null) ? '&searchQuery1=' + query1.replace(/ /g, '+') : '')}
				${((field2 != null) ? '&searchField2=' + field2.replace(/ /g, '+') : '')}
				${((query2 != null) ? '&searchQuery2=' + query2.replace(/ /g, '+') : '')}
				${((start_dt != null) ? '&start_dt=' + start_dt : '')}
				${((end_dt != null) ? '&end_dt=' + end_dt : '')}
				${((type != null) ? '&type=' + type : '')}`;
		});

		//proudct filter
		$('#filter_btn_events').on('click', function (this: any, e: any) {
			e.preventDefault();

			//search
			const id: string = (($('#eid').val() !== undefined && $('#eid').val().length > 0) ? $('#eid').val() : null);
			const name: string = (($('#name').val() !== undefined && $('#name').val().length > 0) ? $('#name').val() : null);
			const start: string = (($('#start_dt').val() !== undefined && $('#start_dt').val().length > 0) ? $('#start_dt').val() : null);
			const end: string = (($('#end_dt').val() !== undefined && $('#end_dt').val().length > 0) ? $('#end_dt').val() : null);
			const limit = (($('#limit:checked').val() !== undefined) ? $('#limit:checked').val() : null);

			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p=1
				${((limit != null) ? '&limit=' + limit : '')}
				${((id != null) ? '&eid=' + id : '')}
				${((name != null) ? '&name=' + name : '')}
				${((start != null) ? '&start_dt=' + start : '')}
				${((end != null) ? '&end_dt=' + end : '')}`;
		});


		$('#reset_btn_events').on('click', function (this: any, e: any) {
			e.preventDefault();
			$('#eid').val('');
			$('#name').val('');
			$('#start_dt').val('');
			$('#end_dt').val('');
			$("#limit:checked").removeAttr("checked");
			window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&p=1`;
			
		});

	};
	const hide = function (self: any): void {
		if (!self.hasClass('panel-collapsed')) {
			self.closest('.panel-body').slideUp();
			self.addClass('panel-collapsed');
			self.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	};
	const show = function (self: any) {
		self.closest('.panel-body').slideDown();
		self.removeClass('panel-collapsed');
		self.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	};
	const toggle = function (self: any) {

		if (!self.hasClass('panel-collapsed')) {
			$('#' + self.data('child')).slideUp();
			self.addClass('panel-collapsed');
			self.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$('#' + self.data('child')).slideDown();
			self.removeClass('panel-collapsed');
			self.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	};
	return { init: init }
});