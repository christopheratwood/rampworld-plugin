declare var define: any;
declare var $: any

interface colours {
	okay: string,
	warning: string,
	bad: string
}

define(['percentage'], function () {
	const colours: colours = {
		okay: '#5cb85c',
		warning: '#f0ad4e',
		bad: '#d9534f'
	}
	let config: any = {
		size: 100
	}
	let counter: any = {
		percentage: $('#entry-percentage').val(),
		amount: $('#entry-amount').val(),
		passes: $('#paid-pass').val(),
		sessions: $('#paid-sesh').val()
	}
	const init = function () {
		createEntry();
		createPaid();

	}

	const createEntry = function () {
		counter.percentage = $('#entry-percentage').val();
		counter.amount = $('#entry-amount').val();

		let progressBarOptions: any = {}
		if (counter.amount < 40) {
			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: counter.percentage,
				fill: {
					color: colours.okay
				}
			}
		} else if (counter.amount < 90) {
			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: counter.percentage,
				fill: {
					color: colours.warning
				}
			}
		} else {
			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: counter.percentage,
				fill: {
					color: colours.bad
				}
			}
		}
		$('#entryCounter').circleProgress(progressBarOptions).on('circle-animation-progress', function (this: any, event: any, progress: any, stepValue) {
			$(this).find('#number').text(counter.amount);
			$(this).find('#percentage').text((stepValue.toFixed(2) * 100).toFixed(0) + "%");
		});
	}
	const createPaid = function () {
		counter.passes = $('#paid-pass').val();
		counter.sessions = $('#paid-sesh').val();
		let total_p: number = (parseInt(counter.passes) + parseInt(counter.sessions)) / 220;
		let total: number = parseInt(counter.passes) + parseInt(counter.sessions);
		let progressBarOptions: any = {};

		if (total < 40) {

			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: total_p,
				fill: {
					color: colours.okay
				}
			}
		} else if (total < 90) {
			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: total_p,
				fill: {
					color: colours.warning
				}
			}
		} else {
			progressBarOptions = {
				startAngle: -1.55,
				size: config.size,
				value: total_p,
				fill: {
					color: colours.bad
				}
			}
		}

		$('#paidCounter').circleProgress(progressBarOptions).on('circle-animation-progress', function (this: any, event: any, progress: any, stepValue: any) {
			$(this).find('#passes_txt').html(`<span>Pass</span>${counter.passes}`);
			$(this).find('#sessions_txt').html(`<span>Book</span>${counter.sessions}`);
		});
	}
	return {
		init: init,
		createEntry: createEntry,
		createPaid: createPaid
	}

});
