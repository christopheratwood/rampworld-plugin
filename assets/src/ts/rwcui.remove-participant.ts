declare var $: any;
declare var define: any;
declare var ajaxurl: string;

define(['rwcui.prompt', 'rwcui.loader'], function (prompts:any, loader: any) {

	let entry_id: any = null;
	let object: any = null;
	const init = function () {
		$(function () {
			$('.remove-participant-btn').on('click', function (this: any, e: any) {
				let self: any = $(this);

				let table: string = `<div>
				<div class="right">
					<h1 id="seshTitle">Are you sure!</h1>
				</div>
				<div class="centerContent">
					<div class="error-icon"><i class="glyphicon glyphicon-warning-sign"></i></div>
					<table class="table" style="font-size:14px;">
						<tbody>
						<tr><th>Member number</th><td>${self.data('member-id')}</td></tr>
							<tr><th>Members name</th><td>${self.data('member')}</td></tr>
						</tbody>
					</table>
					<p>You are attempting to remove <strong>${self.data('member')}</strong>, are you sure?</p>
				</div>
			</div>
				`;
				let prompt = {
					day: {
						html: table,
						buttons: [
							{
								title: 'Cancel',
								value: -1,
								classes: ['entry_large', "cancel"]
							},
							{
								title: 'Remove',
								value: 1,
								classes: ['entry_large', "cancel"]
							}],
						focus: 1,
						submit: function (e: any, v: any, m: any, f: any) {
							e.preventDefault();
							if (v == -1) {
								$.prompt.close();

							} else {
								remove(self);
								$.prompt.close();
							}
						}
					}
				}

				prompts.danger(prompt)

				entry_id = $(this).data('entry-id');
				$('body').append(`<div class="modal fade bd-example-modal-lg" id="remove-participant-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg">\
					<div class="modal-content">
						<div class="modal-header">
							<h1>Are you sure?</h1>
						</div>
						<div class="modal-body">
							<p class="lead">Are you sure you would like to remove ${object.data('member')}?</p>
						<div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
							<button type="button" class="btn btn-danger" id="remove-participant-confirm-btn">Yes</button>
						</div>
					</div>
				</div>
			</div>`);
				$('#remove-participant-modal').modal('toggle');
				$('body').on('click', '#remove-participant-confirm-btn', function (e) {
					//remove(self);
					$('#remove-participant-modal').modal('toggle');

					$('#remove-participant-modal').on('hidden.bs.modal', function (e) {
						$('#remove-participant-modal').remove();
						$('.fade in').remove(); 
					})

				});
			});
		});
	};
	const remove = function (self: any): void {
		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'remove_entry',
				id: entry_id
			},
			beforeSend: function () {
				loader.fixedCreate('Removing participant');
			},
			success: function (response) {

				loader.fixedDelete();
				if (!response) {
					loader.fixedCreate('Could not remove participant');
				} else {
					$('#entry-amount').val(parseInt($('#entry-amount').val()) - 1);
					$('#entry-percentage').val(parseInt($('#entry-amount').val()) / 200).triggerHandler('change');
				}
				self.parent().parent().remove()
				loader.fixedCreate('Participant was removed');
				setTimeout(function () {
					loader.fixedDelete();
				}, 1000);
			},
			error: function (e) {
				loader.fixedCreate('Could not remove participant');
				setTimeout(function () {
					loader.fixedDelete();
				}, 1000);
			}
		});
	};
	return {
		init: init
	}
});