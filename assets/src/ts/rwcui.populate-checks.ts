declare var $: any;
declare var define: any;
declare var ajaxurl: string;

define(['jquery', 'rwcui.prompt'], function ($: any, prompt:any ) {
	const init = function() {
		if($('.top').data('message-id') != null) {
			prompt.openingMessage($('.top').data('message'), $('.top').data('message-member'), $('.top').data('message-date'), $('.top').data('message-id'));
		}
	}
	const opening = function () {
		$('#editOpening').on('click', function (this: any, e: any) {
			if ($(this).text() == 'Save changes') {
				$('#editForm').submit();
			} else {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-primary');
				$(this).text('Save changes');
				$('[readonly]').removeAttr('readonly');
				$('[disabled]').removeAttr('disabled');
				$(this).before('<a class="btn mg-10 btn-warning btn-ms-block" id="cancelEdit">Cancel</a><a class="btn btn-default" id="populateAllOpening">Populate all</a>');
				
			}

		});
		$('body').on('click', '.populate-section', function (this: any, e: any) {
			e.preventDefault();
			populateOpening($(this).data('section'));
		});
		$('body').on('click', '#populateAllOpening', function (this: any, e: any) {
			e.preventDefault();
			populateOpening();
		});
		$('body').on('click', '#cancelEdit', function (this: any, e: any) {

			$('#editOpening').removeClass('btn-primary');
			$('#editOpening').addClass('btn-warning');
			$('#editOpening').text('Change details');


			$('input.form-control ').attr('readonly', 'readonly');
			$('input[type="radio"].form-control ').attr('disabled', 'disabled');
			$('input[type="checkbox"].form-control ').attr('disabled', 'disabled');
			$('textarea.form-control ').attr('readonly', 'readonly');
			$(this).remove();
			$('#populateAllOpening').remove();
		});
	};
	const closing = function () {
		$('#editClosing').on('click', function (this: any, e: any) {
			if ($(this).text() == 'Save changes') {
				$('#editForm').submit();
			} else {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-primary');
				$(this).text('Save changes');
				$('[readonly]').removeAttr('readonly');
				$('[disabled]').removeAttr('disabled');
				console.log($(this).data('can-populate-all'));
				if($(this).data('can-populate-all') == true) {
					$(this).before('<a class="btn mg-10 btn-warning btn-ms-block" id="cancelEdit">Cancel</a><a class="btn btn-default" id="populateAllClosing">Populate all</a>');
				} else {
					$(this).before('<a class="btn mg-10 btn-warning btn-ms-block" id="cancelEdit">Cancel</a>');
				}
			}

		});
		$('body').on('click', '.populate-section', function (this: any, e: any) {
			e.preventDefault();
			populateClosing($(this).data('section'));
		});
		$('body').on('click', '#populateAllClosing', function (this: any, e: any) {
			e.preventDefault();
			populateClosing();
		});
		$('body').on('click', '#cancelEdit', function (this: any, e: any) {

			$('#editClosing').removeClass('btn-primary');
			$('#editClosing').addClass('btn-warning');
			$('#editClosing').text('Change details');


			$('input.form-control ').attr('readonly', 'readonly');
			$('input[type="radio"].form-control ').attr('disabled', 'disabled');
			$('input[type="checkbox"].form-control ').attr('disabled', 'disabled');
			$('textarea.form-control ').attr('readonly', 'readonly');
			$(this).remove();
			$('#populateAllClosing').remove();
		});
	};
	const populateOpening = function (section: string = "all"): void {
		const sections: any = ["bowl", "jumpboxes", "foamresi", "street", "spine", "cctv", "reception", "spectators", "toilets", "thursday", 'fireCheck'];

		switch (section) {
			case "all":
				for (let i: number = 0; i < sections.length; i++)
					populateOpening(sections[i]);
				break;
			case "bowl":
				$('[name="bowl_0"][value="1"]')[0].checked = !0;
				$('[name="bowl_1"][value="1"]')[0].checked = !0;
				$('[name="bowl_2"][value="1"]')[0].checked = !0;
				$('[name="bowl_3"][value="0"]')[0].checked = !0;
				$('[name="bowl_4"][value="1"]')[0].checked = !0;
				break;
			case "jumpboxes":
				$('[name="jump_boxes_0"][value="1"]')[0].checked = !0;
				$('[name="jump_boxes_1"][value="1"]')[0].checked = !0;
				$('[name="jump_boxes_2"][value="1"]')[0].checked = !0;
				$('[name="jump_boxes_3"][value="0"]')[0].checked = !0;
				$('[name="jump_boxes_4"][value="1"]')[0].checked = !0;
				break;
			case "foamresi":
				$('[name="foam_resi_0"][value="1"]')[0].checked = !0;
				$('[name="foam_resi_1"][value="1"]')[0].checked = !0;
				$('[name="foam_resi_2"][value="1"]')[0].checked = !0;
				$('[name="foam_resi_3"][value="0"]')[0].checked = !0;
				$('[name="foam_resi_4"][value="1"]')[0].checked = !0;
				break;
			case "street":
				$('[name="street_area_0"][value="1"]')[0].checked = !0;
				$('[name="street_area_1"][value="1"]')[0].checked = !0;
				$('[name="street_area_2"][value="1"]')[0].checked = !0;
				$('[name="street_area_3"][value="0"]')[0].checked = !0;
				$('[name="street_area_4"][value="1"]')[0].checked = !0;
				break;
			case "spine":
				$('[name="spine_mini_pump_track_0"][value="1"]')[0].checked = !0;
				$('[name="spine_mini_pump_track_1"][value="1"]')[0].checked = !0;
				$('[name="spine_mini_pump_track_2"][value="1"]')[0].checked = !0;
				$('[name="spine_mini_pump_track_3"][value="0"]')[0].checked = !0;
				$('[name="spine_mini_pump_track_4"][value="1"]')[0].checked = !0;
				break;
			case "cctv":
				$('[name="cctv_0"][value="1"]')[0].checked = !0;
				$('[name="cctv_1"][value="1"]')[0].checked = !0;
				break;
			case "reception":
				$('[name="reception_0"][value="1"]')[0].checked = !0;
				$('[name="reception_1"][value="1"]')[0].checked = !0;
				$('[name="reception_2"][value="1"]')[0].checked = !0;
				$('[name="reception_3"][value="1"]')[0].checked = !0;
				$('[name="reception_4"][value="1"]')[0].checked = !0;
				$('[name="reception_5"][value="1"]')[0].checked = !0;
				$('[name="reception_6"][value="1"]')[0].checked = !0;
				$('[name="reception_7"][value="1"]')[0].checked = !0;
				$('[name="reception_8"][value="1"]')[0].checked = !0;
				$('[name="reception_9"][value="1"]')[0].checked = !0;
				$('[name="reception_10"][value="1"]')[0].checked = !0;
				$('[name="reception_11"][value="1"]')[0].checked = !0;
				break;
			case "spectators":
				$('[name="spectators_0"][value="1"]')[0].checked = !0;
				$('[name="spectators_1"][value="1"]')[0].checked = !0;
				$('[name="spectators_2"][value="1"]')[0].checked = !0;
				$('[name="spectators_3"][value="1"]')[0].checked = !0;
				$('[name="spectators_4"][value="1"]')[0].checked = !0;
				$('[name="spectators_5"][value="1"]')[0].checked = !0;
				$('[name="spectators_6"][value="1"]')[0].checked = !0;
				break;
			case "toilets":
				$('[name="toilets_0"][value="1"]')[0].checked = !0;
				$('[name="toilets_1"][value="1"]')[0].checked = !0;
				$('[name="toilets_2"][value="1"]')[0].checked = !0;
				$('[name="toilets_3"][value="1"]')[0].checked = !0;
				$('[name="toilets_4"][value="1"]')[0].checked = !0;
				$('[name="toilets_5"][value="1"]')[0].checked = !0;
				$('[name="toilets_6"][value="1"]')[0].checked = !0;
				break;
			case "fireCheck":
				$('[name="fireChecks_0"][value="n"]')[0].checked = !0;
				$('[name="fireChecks_1"][value="n"]')[0].checked = !0;
				$('[name="fireChecks_2"][value="n"]')[0].checked = !0;
				$('[name="fireChecks_3"][value="n"]')[0].checked = !0;
				break;
			case "thursday":
				if (new Date().getDay() == 4) {
					$('[name="thursday_0"][value="1"]')[0].checked = !0;
					$('[name="thursday_1"][value="1"]')[0].checked = !0;
					$('[name="thursday_2"][value="1"]')[0].checked = !0;
					$('[name="thursday_3"][value="1"]')[0].checked = !0;
					$('[name="thursday_4"][value="1"]')[0].checked = !0;
					$('[name="thursday_5"][value="1"]')[0].checked = !0;
					$('[name="thursday_6"][value="1"]')[0].checked = !0;
					$('[name="thursday_7"][value="1"]')[0].checked = !0
				}
				break
		}
	};
	const populateClosing = function (section: string = "all") {
		const sections: any = ["stock", "cleaning", "park", "till", "power", "final"];
		switch (section) {
			case "all":
				for (let i: number = 0; i < sections.length; i++)
					populateClosing(sections[i]);
				break;
			case "stock":
				$('[name="stock_0"][value="1"]')[0].checked = !0;
				$('[name="stock_1"][value="1"]')[0].checked = !0;
				$('[name="stock_2"][value="1"]')[0].checked = !0;
				break;
			case "cleaning":
				$('[name="cleaning_0"][value="1"]')[0].checked = !0;
				$('[name="cleaning_1"][value="1"]')[0].checked = !0;
				$('[name="cleaning_2"][value="1"]')[0].checked = !0;
				$('[name="cleaning_3"][value="1"]')[0].checked = !0;
				$('[name="cleaning_4"][value="1"]')[0].checked = !0;
				$('[name="cleaning_5"][value="1"]')[0].checked = !0;
				$('[name="cleaning_6"][value="1"]')[0].checked = !0;
				$('[name="cleaning_7"][value="1"]')[0].checked = !0;
				$('[name="cleaning_8"][value="1"]')[0].checked = !0;
				$('[name="cleaning_9"][value="1"]')[0].checked = !0;
				$('[name="cleaning_10"][value="1"]')[0].checked = !0;
				$('[name="cleaning_11"][value="1"]')[0].checked = !0;
				break;
			case "park":
				$('[name="park_check_0"][value="1"]')[0].checked = !0;
				$('[name="park_check_1"][value="1"]')[0].checked = !0;
				$('[name="park_check_2"][value="1"]')[0].checked = !0;
				$('[name="park_check_3"][value="1"]')[0].checked = !0;
				break;
			case "till":
				$('[name="till_0"][value="1"]')[0].checked = !0;
				$('[name="till_1"][value="1"]')[0].checked = !0;
				break;
			case "power":
				$('[name="power_off_0"][value="1"]')[0].checked = !0;
				$('[name="power_off_1"][value="1"]')[0].checked = !0;
				$('[name="power_off_2"][value="1"]')[0].checked = !0;
				$('[name="power_off_3"][value="1"]')[0].checked = !0;
				$('[name="power_off_4"][value="1"]')[0].checked = !0;
				$('[name="power_off_5"][value="1"]')[0].checked = !0;
				$('[name="power_off_6"][value="1"]')[0].checked = !0;
				break;
			case "final":
				$('[name="final_0"][value="1"]')[0].checked = !0;
				$('[name="final_1"][value="1"]')[0].checked = !0;
				$('[name="final_2"][value="1"]')[0].checked = !0;
				$('[name="final_3"][value="1"]')[0].checked = !0;
				break
		}
	};
	return {
		init: init,
		opening: opening,
		closing: closing,
		populateOpening: populateOpening,
		populateClosing: populateClosing
	}
});