declare var $: any;
declare var define: any;

define(['jquery'], function ($: any) {
	const add = function (id: string, text: string, classes: string = '') {
		$(id).removeClass('hidden');
		$(id).text(text).addClass(classes);
	};
	const remove = function (id: string) {
		$(id).removeClass('visible');
		$(id).addClass('hidden');
		$(id).text('');

	};
	return {
		add: add,
		remove: remove
	}
});