declare var define: any;
declare var $: any;
declare var moment: any;
declare var window: Window;

define(['jquery', 'chart'], function () {
	const roots: any = {
		live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
		dev: '//127.0.0.1/wp-admin/admin.php'
	}
	const colours: any = {
		spectator: "#772E7B",
		scooter: "#21a1e1",
		bmx: "#CDEAC0",
		mtb: "#FF934F",
		skateboard: "#F85AE3",
		inline: "#bbb",
		other: "#d9534f"

	}

	const init = function () {
		$(function () {
			$.each($('.sessionChart'), function (this: any, i: number, v: any) {

				$(this).draw(setData($(this).data('session-data')), $(this).data('session-time'), $(this).data('total'));
			});
		});
	}

	const setData = function (data: any): any {

		let items: any = [];
		if (data.length > 0) {
			data = data.split(';');
			data.forEach(element => {
				items.push({
					title: element.split(":")[0],
					value: element.split(":")[1],
					color: colours[element.split(":")[0]]
				});
			});
		}
		console.log(items);
		return items;
	};

	return {
		init: init
	}

});