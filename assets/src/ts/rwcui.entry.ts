declare var $: any;
declare var define: any;

declare var ajaxurl: string;

var today: any = new Date();
declare var $: any;

define(['rwcui.entry-counter', 'rwcui.prompt', 'rwcui.loader', 'rwcui.error', 'rwcui.member'], function (entryCounter: any, prompts: any, loader: any, error: any, memberClass: any) {
	let prompt: any;
	const roots: any = {
		live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
		dev: '//127.0.0.1/wp-admin/admin.php'
	}
	let sessiontime: string;
	let processing: boolean = false;
	let config: any = {
		lasttime: null,
		onlyPrivate: null,
		hasMembershipBtn: false,
		entry_stats: {
			active: 0,
			reserved: 0,
			percentage: 0
		},
		date: {
			today: new Date(),
			thisMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1).toLocaleDateString(),
			thisSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 7).toLocaleDateString(),
			nextMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (8 - today.getDay())).toLocaleDateString(),
			nextSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (14 - today.getDay())).toLocaleDateString(),
			twoWeekMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (15 - today.getDay())).toLocaleDateString(),
			twoWeekSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (21 - today.getDay())).toLocaleDateString(),
		},
		isFull: false,
		maxAmount: 220,
		premises: {
			onPremises: $('#entry-amount').val(),
			sessions: $('#paid-sesh').val(),
			passes: $('#paid-pass').val()

		},
		buttons: [],
		sessions: [],
		sessoion_atm: false,
		default_html: `
		<div>
			<div class="right">
				<h1 id="seshTitle">{{actiontitle}}</h1>
			</div>
			<div class="centerContent">
				<span class="name">{{name}}</span>
				<div class="age-notification">
					<span class="age">{{age}}</span>
					<span class="message">years of age</span>
				</div>
				{{memberinfo}}
				{{notices}}
			</div>
		</div>`,
		default_booking: `
		<div>
			<div class="right">
				<h1 id="seshTitle">{{actiontitle}}</h1>
			</div>
			<div class="centerContent">
				<span class="name">{{name}}</span>
				<div class="age-notification">
					<span class="age">{{booking_number}}</span>
					<span class="message">booking No.</span>
				</div>
				<p class="lead">Booking information</p>
				{{booking_info}}
				<p class="lead">Session information</p>
				{{session_info}}
				<p class="lead">Participants</p>
				<small>Click to process member</small>
				{{member_info}}
			</div>
		</div>`,
		default_simple_html: `
		<div>
			<div class="right">
				<h1 id="seshTitle">{{actiontitle}}</h1>
			</div>
			<div class="centerContent">
				{{message}}
			</div>
		</div>`,
		default_membership_html: `
		<div>
			<div class="right">
				<h1 id="seshTitle">{{actiontitle}}</h1>
			</div>
			<div class="centerContent">
				<span class="name">{{name}}</span>
				<div class="age-notification">
					<span class="age">{{age}}</span>
					<span class="message">years of age</span>
				</div>
				<p class="lead">Dates</p>
				<p class="lead">{{startdate}} - {{enddate}}</p>
				<div class="alert alert-warning">Please explain to the customer that the pass ends on the {{enddate}}</div>
				{{notices}}
			</div>
		</div>`
	}
	let booking: any = {
		details: {
			id: null
		},
		total: null
	}

	const init = function (): void {
		let total: number = parseInt($('#entry-amount').val()) + parseInt($('#paid-pass').val()) + parseInt($('#paid-sesh').val());

		if (total >= config.maxAmount) {
			config.isFull = true;
			prompts.full();
		} else if (total == config.maxAmount - 20)
			prompts.almostFull(config.maxAmount - total);
		else if (total == config.maxAmount - 10)
			prompts.almostFull(config.maxAmount - total);
		else if (total == config.maxAmount - 5)
			prompts.almostFull(config.maxAmount - total);

		setSessions();

		//change session event handler
		$('.change-session-btn').off().on('click', function (this: any, e: any) {
			e.preventDefault();

			changeSession(
				$(this).data('member-id'),
				$(this).data('forename'),
				$(this).data('surname'),
				$(this).data('expertise'),
				$(this).data('age'),
				$(this).data('has-membership'),
				$(this).data('session-id'),
				$(this).data('current-times'),
				false);
		});

		$( "body" ).on( 'click', '#booking-members tbody tr', function( this:any, e:any ) {
			if ( $( this ).data( 'verification-required' ) != '0' ) {
				verifyBookingMember($( this ).data( 'member-id' ), $( this ).data( 'session-id' ), $( this ).data( 'booking-end-time' ), $( this ).data( 'session-type' ) );
			} else {
				switch( $( this ).data( 'session-type' ) ){
					case 'pre-session':
						
						setBookingEntry('session', $( this ).data( 'member-id' ), $( this ).data( 'session-id' ), $( this ).data( 'booking-end-time' ), false, false );
						break;
					case 'weekpass':
						setBookingEntry('pass', $( this ).data( 'member-id' ), $( this ).data( 'session-id' ), $( this ).data( 'booking-end-time' ), false, false);
						break;
				}
			}
		});
	}
	const processBooking = function(bookingID: number) {
		booking.details.id = bookingID;

		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'get_full_booking_details',
				bookingID: booking.details.id,
			},
			success: function (response: any) {
				if (response !== false) {
					let booking_response: any = JSON.parse(response);
					booking.total = booking_response.members.length;
					const details = `
					<table class="table">
						<tr>
							<th>Phone number</th>
							<th>Email</th>
						</tr>
						<tr>
							<td>${booking_response.details.number}</td>
							<td>${booking_response.details.email}</td>
						</tr>
					</table>`;
					const session_details = `
					<table class="table">
					<tr>
						<th>Start time</th>
						<th>End time</th>
						<th>Session band</th>
					</tr>
					<tr>
						<td>${booking_response.details.session_start_time}</td>
						<td>${booking_response.details.session_end_time}</td>
						<td>unknown</td>
					</tr>
				</table>`;
					let members: string = `
						<div class="table-responsive" id="booking-members" style="max-height: 200px">
							<table class="table">
								<thead>
									<th>#</th><th>Name</th><th>Age</th><th>Expertise</th><th>Process</th>
								</thead>
								<tbody>`;

					for(const member of booking_response.members) {
						members += `
							<tr data-session-type="${( ( member.pre_session_id != null ) ? 'pre-session' : 'weekpass' )}"
									data-session-id="${ ((member.pre_session_id != null) ? member.pre_session_id : member.paid_pass_id)}"
									data-booking-end-time="${member.session_end_time}" data-verification-required="${((member.staff_verification == 0) ? 1: 0)}"
									data-member-id="${member.member_id}">
								<td>${member.member_id}</td>	
								<td>${member.forename} ${member.surname}</td>
								<td>${member.dob}</td>
								<td>${member.expertise}</td>
								<td><a class="btn btn-default disabled" >Process</a></td>
							</tr>`;
					}
					members += '</tbody></table></div>'
					prompt = {
						day: {
							html: config.default_booking.replace('{{actiontitle}}', 'Select action')
								.replace('{{name}}', booking_response.details.name)
								.replace('{{booking_number}}', bookingID)
								.replace('{{booking_info}}', details)
								.replace('{{session_info}}', session_details)
								.replace('{{member_info}}', members),
							buttons: [{
								title: "Close",
								value: false,
								classes: ['tworow', 'cancel']
							}],
							focus: 1,
							submit: function ( e: any, v: any, m: any, f: any ) {

							}
						}

					}

					prompts.booking( prompt );
				}
			}
		});
	}


	const verifyBookingMember = function( memberID: number, ppid: number, end_time: string, type:string ) {

		$.ajax({

			url: ajaxurl,
			type: 'post',
			data: {
				mid: memberID,
				action: 'verify_member_json'
			},
			success: function (response: any) {
				if (response !== false) {
					let details: any = JSON.parse(response);
					const member = details[0];
					let confirm_details: string = `
						<table class="table" style="font-size:14px;">
							<tr>
								<th>#</th><td>${memberID}</td>
							</tr>
							<tr>
								<th>Phone numner</th><td>${member.number}</td>
							</tr>
							<tr>
								<th>Email</th><td>${member.email}</td>
							</tr>
							<tr>
								<th>Gender</th><td>${member.gender}</td>
							</tr>
							<tr>
								<th>Date of birth</th><td>${member.dob}</td>
							</tr>
							<tr>
								<th>Address line one</th><td>${member.address_line_one}</td>
							</tr>
							<tr>
							<th>Postcode</th><td>${member.address_postcode}</td>
							</tr>
							<tr>
								<th>Medical notes</th><td colspan="3">${((member.medical_notes == null) ? '' : member.medical_notes)}</td>
							</tr>
						</table>`;

					let notices = `
							<div class="alert alert-warning">Please ensure these details are correct be proceeding.</div>`;

					prompt = {
						day: {
							html: config.default_html.replace('{{actiontitle}}', 'Select action')
								.replace('{{age}}', member.age)
								.replace('{{name}}', `${member.forename}  ${member.surname}`)
								.replace('{{memberinfo}}', confirm_details)
								.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership	=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
							buttons: [{
								title: "Cancel",
								value: false,
								classes: ['tworow', 'cancel']
							}, {
								title: "Verify",
								value: true,
								class: ['tworow']
							}],
							focus: 1,
							submit: function (e: any, v: any, m: any, f: any) {
								e.preventDefault();
								if (v == false) {
									$.prompt.close();
									processing = false
								} else {
									if( type == 'pre-session' ) {
										setBookingEntry('session', memberID, ppid, end_time, true, false );
									} else {
										setBookingEntry('pass', memberID, ppid, null, true, false );
									}
								}
							}
						}
					}
				
					prompts.state(prompt);
				}
			}
		});
		
	
	
	};

	const processEntry = function (memberID: number, forename: string, surname: string, age: number, notes: any = false, is_banned: any = false, has_membership: string = "None", expertise: any = null, redirect: any = null, already: any = null, currentSession: number, pp: any = null) {

		if (Object.keys(config.sessions).length === 0) {
			prompts.warning(`
			<h3 style="text-align:center">No available sessions</h3>
			<div class="well">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>`);

		} else {

			if (has_membership == null)
				has_membership = "None";

			if (is_banned != false) {
				let table: string = `
				<table class="table" style="font-size:14px;">
					<tbody>
						<tr><th>Member number</th><td>${memberID}</td></tr>
						<tr><th>Expertise</th><td> ${expertise}</td>
						</tr><tr><th>Membership</th><td>${has_membership}</td></tr>
					</tbody>
				</table>`;

				let notices: string = `<div class="alert alert-danger" style="max-height: 120px; overflow-y:auto"><strong>Danger!</strong> This member is banned from the premises. You are required to refuse entry.</div>`;

				prompt = {
					day: {
						html: config.default_html.replace('{{actiontitle}}', 'This member is banned')
							.replace('{{age}}', age).replace('{{name}}', `${forename} ${surname}`)
							.replace('{{memberinfo}}', table)
							.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
						buttons: [{
							title: 'Cancel',
							value: -1,
							classes: ['entry_large', "cancel"]
						}],
						focus: 1,
						submit: function (e: any, v: any, m: any, f: any) {
							e.preventDefault();
							if (v == -1) {
								$.prompt.close();
								processing = false

							}
						}
					}
				}

				prompts.danger(prompt)
			} else {

				if (isNaN(memberID)) {
					prompts.simple(config.default_simple_html.replace('{{message}}', '<p class="lead">Member ID must be Integer.</p>').replace('{{actiontitle}}', 'Choose action'));
				} else {
					if (already != null && already != false) {
						changeSession(memberID, forename, surname, expertise, age, has_membership, currentSession, already, notes);
					
					} else {

						if (has_membership == "None" && pp == null && config.hasMembershipBtn == false) {
							config.buttons.push({
								title: 'Membership',
								value: 'add_membership',
								classes: ['membership']
							});
							config.hasMembershipBtn = true;
						}
						if (pp != null) {
							$.ajax({
								url: ajaxurl,
								type: 'post',
								data: {
									action: 'get_booking_details',
									memberID: memberID,
									ppid: pp
								},
								success: function (response: any) {
									if (response !== false) {
										let details: any = JSON.parse(response);
										let table: string = `
										<table class="table" style="font-size:14px;">
											<tbody>
												<tr><th>Member number</th><td>${memberID}</td></tr>
												<tr><th>Expertise</th><td>${expertise}</td></tr>
												<tr><th>Membership</th><td>${has_membership}</td></tr>
											</tbody>
										</table>`;

										let notices: string = (notes !== false) ? `
										<div class="alert alert-warning">
											<p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p>
											<p>${notes}</p>
										</div>` : '';

										prompt = {
											day: {
												html: config.default_html.replace('{{actiontitle}}', 'Select action')
													.replace('{{age}}', age)
													.replace('{{name}}', `${forename} ${surname}`)
													.replace('{{memberinfo}}', table)
													.replace('{{notices}}', `
													<p class="lead">Booking details</p>
													<table class="table" style="font-size:14px;">
														<thead>
															<tr><th>Start time</th><th>End time</th></tr>
														</thead>
														<tbody>
															<tr><td>${details.session_start_time}</td><td>${details.session_end_time}</td></tr>
														</tbody>
													</table>`),
												buttons: [{
													title: "Cancel",
													value: false,
													classes: ['tworow', 'cancel']
												}, {
													title: "Next",
													value: true,
													class: ['tworow']
												}],
												focus: 1,
												submit: function (e: any, v: any, m: any, f: any) {
													e.preventDefault();
													if (v == false) {
														$.prompt.close();
														processing = false
													} else {
														sessiontime = v;
														setBookingEntry('pass', memberID, pp, details.session_end_time);

													}
												}
											}

										}

										prompts.state(prompt)
									} else {
										let details: any = JSON.parse(response);
										let table: string = `
											<div class="alert alert-danger">
												Booking details coild not be rendered. Please verify with customer. <strong>You must contact administration to manual change this booking status</strong>
											</div>
											<table class="table" style="font-size:14px;">
												<tbody>
													<tr><th>Member number</th><td>${memberID}</td></tr>
													<tr><th>Expertise</th><td>${expertise}</td></tr>
													<tr><th>Membership</th><td>${has_membership}</td></tr>
												</tbody>
											</table>`;
										let notices: string = (notes !== false) ? `
											<div class="alert alert-warning">
												<p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p>
												<p>${notes}</p>
											</div>` : '';

										prompt = {
											day: {
												html: config.default_html.replace('{{actiontitle}}', 'Select action')
													.replace('{{age}}', age)
													.replace('{{name}}', `${forename}  ${surname}`)
													.replace('{{memberinfo}}', table)
													.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
												buttons: config.buttons,
												focus: 1,
												submit: function (e: any, v: any, m: any, f: any) {
													e.preventDefault();
													if (v == false) {
														$.prompt.close();
														processing = false
													} else {
														sessiontime = v;
														setEntry(memberID, sessiontime);
													}
												}
											}
										}
										prompts.state(prompt);
									}
								}
							});

						} else {
							if (has_membership != "None" && config.onlyPrivate == false) {

								let table: string = `
									<table class="table" style="font-size:14px;">
										<tbody>
											<tr><th>Member number</th><td>${memberID}</td></tr>
											<tr><th>Expertise</th><td>${expertise}</td></tr>
											<tr><th>Membership</th><td>${has_membership}</td></tr>
										</tbody>
									</table>`;
								let notices: string = (notes !== false) ? `
											<div class="alert alert-warning">
												<p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p>
												<p>${notes}</p>
											</div>` : '';
								prompt = {
									day: {
										html: config.default_html.replace('{{actiontitle}}', 'Select action')
											.replace('{{age}}', age)
											.replace('{{name}}', `${forename}  ${surname}`)
											.replace('{{memberinfo}}', table)
											.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
										buttons: [{
											title: "Cancel",
											value: false,
											classes: ['tworow', 'cancel']
										}, {
											title: "Next",
											value: true,
											class: ['tworow']
										}],
										focus: 1,
										submit: function (e: any, v: any, m: any, f: any) {
											e.preventDefault();
											if (v == false) {
												$.prompt.close();
												processing = false
											} else {

												setPassEntry(memberID, config.lasttime);
											}
										}
									}
								}

								prompts.state(prompt);

							} else {
								let table: string = `
									<table class="table" style="font-size:14px;">
										<tbody>
											<tr><th>Member number</th><td>${memberID}</td></tr>
											<tr><th>Expertise</th><td>${expertise}</td></tr>
											<tr><th>Membership</th><td>${has_membership}</td></tr>
										</tbody>
									</table>`;
								let notices: string = (notes !== false) ? `
											<div class="alert alert-warning">
												<p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p>
												<p>${notes}</p>
											</div>` : '';
								prompt = {
									day: {
										html: config.default_html.replace('{{actiontitle}}', 'Select action')
											.replace('{{age}}', age)
											.replace('{{name}}', `${forename}  ${surname}`)
											.replace('{{memberinfo}}', table)
											.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
										buttons: config.buttons,
										focus: 1,
										submit: function (e: any, v: any, m: any, f: any) {
											e.preventDefault();
											if (v == false) {
												$.prompt.close();
												processing = false
											} else if (v == 'add_membership') {
												$.prompt.goToState('membership')
											} else {
												sessiontime = v;
												setEntry(memberID, sessiontime);

											}
										}
									},
									membership: {
										html: config.default_membership_html.replace('{{actiontitle}}', 'Select action')
											.replace('{{age}}', age)
											.replace('{{name}}', `${forename} ${surname}`)
											.replace('{{notices}}', notices).replace('{{startdate}}', config.date.thisMonday)
											.replace(/{{enddate}}/g, config.date.thisSunday),
										buttons: [{
											title: 'Cancel',
											value: 0,
											classes: 'tworow'
										}, {
											title: 'Back',
											value: -1,
											classes: ['tworow', 'cancel']
										}, {
											title: 'Continue',
											value: 2,
											classes: 'threerow'
										}],
										focus: 1,
										submit: function (e: any, v: any, m: any, f: any) {
											e.preventDefault();
											if (v == 0) {
												$.prompt.close()
											} else if (v == -1) {
												$.prompt.goToState('day')
											} else {
												setWeekPass(memberID, config.lasttime, config.date.thisMonday);

											}
										}
									}
								}

								prompts.state(prompt);

								let sessiontime = ''
							}
						}
					}
				}
			}
		}
	};
	const verifyMemberGUI = function (mid: any, age: any, expertise: string, has_membership: string = "None", ppid: any = null): void {
		if (Object.keys(config.sessions).length === 0) {
			prompts.warning(`
				<h3 style="text-align:center">No available sessions</h3>
				<div class="well">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>`);

		} else {

			let data: any = {
				"mid": mid,
				"action": 'verify_member_json'
			}
			if (config.hasMembershipBtn == false) {
				config.buttons.push({
					title: 'Membership',
					value: 'add_membership',
					classes: ['membership']
				});
				config.hasMembershipBtn = true;
			}

			$.ajax({
				url: ajaxurl,
				type: 'post',
				data: data,
				beforeSend: function () {
					loader.fixedCreate();
				},
				success: function (response: any) {
					loader.fixedDelete();
					let member: any = JSON.parse(response);
					member = member[0];

					let confirm_details: string = `
					<table class="table" style="font-size:14px;">
					<tr>
						<th>#</th><td>${mid}</td>
					</tr>
					<tr>
						<th>Phone numner</th><td>${member.number}</td>
					</tr>
					<tr>
						<th>Email</th><td>${member.email}</td>
					</tr>
					<tr>
						<th>Gender</th><td>${member.gender}</td>
					</tr>
					<tr>
						<th>Date of birth</th><td>${member.dob}</td>
					</tr>
					<tr>
						<th>Address line one</th><td>${member.address_line_one}</td>
					</tr>
					<tr>
					<th>Postcode</th><td>${member.address_postcode}</td>
					</tr>
					<tr>
						<th>Medical notes</th><td colspan="3">${((member.medical_notes == null) ? '' : member.medical_notes)}</td>
					</tr>
				</table>`;
	
					let notices = `
						<div class="alert alert-warning">Please ensure these details are correct be proceeding.</div>`;
					let table = `
						<table class="table" style="font-size:14px;">
							<tbody>
								<tr><th>Member number</th><td>${mid}</td></tr>
								<tr><th>Expertise</th><td>${expertise}</td></tr>
								<tr><th>Membership</th><td>${has_membership}</td></tr>
							</tbody>
						</table>`;



					if (ppid == null) {

						if (has_membership != "None") {
							prompt = {
								verify: {
									html: config.default_html.replace('{{name}}', `${member.forename} ${member.surname}`)
										.replace('{{age}}', age)
										.replace('{{actiontitle}}', 'Are these details correct?')
										.replace('{{memberinfo}}', confirm_details)
										.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}" class="btn btn-block btn-primary"> View member</a>`),
									buttons: [
										{
											title: 'Cancel',
											value: -1,
											classes: ['entry_large', "cancel"]
										},
										{
											title: 'Verify',
											value: 1,
											classes: ['entry_large']
										}
									],
									focus: 1,
									submit: function (e: any, v: any, m: any, f: any) {
										e.preventDefault();
										if (v == -1) {
											$.prompt.close();
											processing = false
										} else if (v == 'view') {
											window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}`;
										} else {
											$.prompt.goToState('day');

										}
									}
								},
								day: {
									html: config.default_html.replace('{{actiontitle}}', 'Select action')
										.replace('{{age}}', age)
										.replace('{{name}}', `${member.forename}  ${member.surname}`)
										.replace('{{memberinfo}}', table)
										.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${member.member_id}" class="btn btn-block btn-primary">View member</a>`),
									buttons: [{
										title: "Cancel",
										value: false,
										classes: ['tworow', 'cancel']
									}, {
										title: "Next",
										value: true,
										class: ['tworow']
									}],
									focus: 1,
									submit: function (e: any, v: any, m: any, f: any) {
										e.preventDefault();
										if (v == false) {
											$.prompt.close();
											processing = false
										} else {

											setPassEntry(member.member_id, config.lasttime, true);
										}
									}
								}
							}
						} else {
							prompt = {
								verify: {
									html: config.default_html.replace('{{name}}', `${member.forename} ${member.surname}`)
										.replace('{{age}}', age)
										.replace('{{actiontitle}}', 'Are these details correct?')
										.replace('{{memberinfo}}', confirm_details)
										.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}" class="btn btn-block btn-primary"> View member</a>`),
									buttons: [
										{
											title: 'Cancel',
											value: -1,
											classes: ['entry_large', "cancel"]
										},
										{
											title: 'Verify',
											value: 1,
											classes: ['entry_large']
										}
									],
									focus: 1,
									submit: function (e: any, v: any, m: any, f: any) {
										e.preventDefault();
										if (v == -1) {
											$.prompt.close();
											processing = false
										} else if (v == 'view') {
											window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}`;
										} else {
											$.prompt.goToState('entry');

										}
									}
								},
								entry: {
									html: config.default_html.replace('{{actiontitle}}', 'Select action')
										.replace('{{age}}', age)
										.replace('{{name}}', `${member.forename} ${member.surname}`)
										.replace('{{memberinfo}}', table)
										.replace('{{notices}}', notices)
										.replace('{{memberid}}', ''),
									buttons: config.buttons,
									focus: 1,
									submit: function (e: any, v: any, m: any, f: any) {
										e.preventDefault();
										if (v == false) {
											$.prompt.goToState('verify');
										} else if (v == 'add_membership') {
											$.prompt.goToState('membership');
										} else {
											sessiontime = v;
											setEntry(member.member_id, sessiontime, true);
										}
									}
								},
								membership: {
									html: config.default_membership_html.replace('{{actiontitle}}', 'Select action')
										.replace('{{age}}', age)
										.replace('{{name}}', `${member.forename} ${member.surname}`)
										.replace('{{notices}}', notices)
										.replace('{{startdate}}', config.date.thisMonday)
										.replace(/{{enddate}}/g, config.date.thisSunday),
									buttons: [{
										title: 'Back',
										value: -1,
										classes: ['tworow', 'cancel']
									}, {
										title: 'Continue',
										value: 2,
										classes: 'threerow'
									}],
									focus: 1,
									submit: function (e: any, v: any, m: any, f: any) {
										e.preventDefault();
										if (v == -1) {
											$.prompt.goToState('entry')
										} else {
											setWeekPass(mid, config.lasttime, config.date.thisMonday, true)
										}
									}
								}
							}
						}
						prompts.state(prompt);
					} else {

						$.ajax({
							url: ajaxurl,
							type: 'post',
							data: {
								action: 'get_booking_details',
								memberID: member.member_id,
								ppid: ppid
							},
							beforeSend: function () {
								loader.fixedCreate();
							},
							success: function (response) {
								loader.fixedDelete();
								let details: any = JSON.parse(response);

								if (details == false) {

									prompt = {
										verify: {
											html: config.default_html.replace('{{name}}', `${member.forename} ${member.surname}`)
												.replace('{{age}}', age)
												.replace('{{actiontitle}}', 'Are these details correct?')
												.replace('{{memberinfo}}', confirm_details)
												.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}" class="btn btn-block btn-primary"> View member</a>`),
											buttons: [
												{
													title: 'Cancel',
													value: -1,
													classes: ['entry_large', "cancel"]
												},
												{
													title: 'Verify',
													value: 1,
													classes: ['entry_large']
												}
											],
											focus: 1,
											submit: function (e: any, v: any, m: any, f: any) {
												e.preventDefault();
												if (v == -1) {
													$.prompt.close();
													processing = false
												} else if (v == 'view') {
													window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}`;
												} else {
													$.prompt.goToState('day');

												}
											}
										},
										day: {
											html: config.default_html.replace('{{actiontitle}}', 'Select action')
												.replace('{{age}}', age)
												.replace('{{name}}', `${member.forename} ${member.surname}`)
												.replace('{{memberinfo}}', table)
												.replace('{{notices}}', notices)
												.replace('{{memberid}}', ''),
											buttons: config.buttons,
											focus: 1,
											submit: function (e: any, v: any, m: any, f: any) {
												e.preventDefault();
												if (v == false) {
													$.prompt.close();
													processing = false
												} else {
													sessiontime = v;
													setBookingEntry(member.member_id, sessiontime, ppid, true);
												}
											}
										}

									}
									prompts.state(prompt)

								} else {

									prompt = {
										verify: {
											html: config.default_html.replace('{{name}}', `${member.forename} ${member.surname}`)
												.replace('{{age}}', age)
												.replace('{{actiontitle}}', 'Are these details correct?')
												.replace('{{memberinfo}}', confirm_details)
												.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}" class="btn btn-block btn-primary"> View member</a>`),
											buttons: [
												{
													title: 'Cancel',
													value: -1,
													classes: ['entry_large', "cancel"]
												},
												{
													title: 'Verify',
													value: 1,
													classes: ['entry_large']
												}
											],
											focus: 1,
											submit: function (e: any, v: any, m: any, f: any) {
												e.preventDefault();
												if (v == -1) {
													$.prompt.close();
													processing = false
												} else if (v == 'view') {
													window.location.href = `${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${mid}`;
												} else {
													$.prompt.goToState('day');

												}
											}
										},
										day: {
											html: config.default_html.replace('{{name}}', `${member.forename} ${member.surname}`)
												.replace('{{age}}', age)
												.replace('{{actiontitle}}', 'Are these details correct?')
												.replace('{{memberinfo}}', table)
												.replace('{{notices}}', `
													<p class="lead">Booking details</p>
													<table class="table" style="font-size:14px;">
														<thead>
															<tr><th>Start time</th><th>End time</th></tr>
														</thead>
														<tbody>
															<tr><td>${details.session_start_time}</td><td>${details.session_end_time}</td></tr>
														</tbody>
													</table>`)
												.replace('{{memberid}}', member.member_id),

											buttons: [{
												title: "Cancel",
												value: false,
												classes: ['tworow', 'cancel']
											}, {
												title: "Next",
												value: true,
												class: ['tworow']
											}],
											focus: 1,
											submit: function (e: any, v: any, m: any, f: any) {
												e.preventDefault();
												if (v == false) {
													$.prompt.close();
													processing = false
												} else {

													setBookingEntry(member.member_id, ppid, details.session_end_time, true);
												}
											}
										}
									}
									prompts.state(prompt);
								}
							}
						});
						prompts.state(prompt);
					}
				}
			});
		}
	};
	const changeSession = function (memberID: number, forename: string, surname: any, expertise: string, age, has_membership: any, session_id: number, times: string, notes: any = false) {
		//updateSessions();
		if (Object.keys(config.sessions).length === 0) {
			prompts.warning(`
				<h3 style="text-align:center">No available sessions</h3>
				<div class="well">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>`);

		} else {

			let table: string = `
				<table class="table" style="font-size:14px;">
					<tbody>
					<tr><th>Member number</th><td>${memberID}</td></tr>
						<tr><th>Expertise</th><td>${expertise}</td></tr>
						<tr><th>Membership</th><td>${has_membership}</td></tr>
					</tbody>
				</table>
				<div class="alert alert-info">
					This member is already on the premises. Member is due to entered @ <strong>${times.split(',')[0]}</strong> and due to leave @ <strong>${times.split(',')[1]}</strong>
				</div>`;
			let notices: string = (notes !== false) ? `
				<div class="alert alert-warning">
					<p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p>
					<p>${notes}</p>
				</div>` : '';

			prompt = {
				day: {
					html: config.default_html.replace('{{actiontitle}}', 'Change session time')
						.replace('{{age}}', age)
						.replace('{{name}}', `${forename} ${surname}`)
						.replace('{{memberinfo}}', table)
						.replace('{{notices}}', `${notices} <a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=${memberID}&amp;membership=1" class="btn btn-block btn-default" style="margin-bottom:20px">Reissue email</a><a href="${roots.live}?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=${memberID}" class="btn btn-block btn-primary">View member</a>`),
					buttons: config.buttons,
					focus: 1,
					submit: function (e: any, v: any, m: any, f: any) {
						e.preventDefault();
						if (v == false) {
							$.prompt.close();
							processing = false
						} else {
							sessiontime = v;
							updateSession(session_id, sessiontime);
						}
					}
				}
			}
			prompts.state(prompt)

		}
	};
	const setEntry = function (memberID: any, exitTime: any, verify: boolean = false) {
		//updateSessions();
		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'set_member_entry',
				memberID: memberID,
				timeOut: exitTime,
				verify: verify
			},
			beforeSend: function (res: any) {
				loader.fixedCreate()
			},
			success: function (response: any) {
				loader.fixedDelete();

				if (response == 'passed') {
					if (memberID != 2) {
						config.premises.onPremises++;
						$('#entry-amount').val(parseInt(config.premises.onPremises));
						$('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
						entryCounter.createEntry();


					}

					$.prompt.close();
					processing = false;
					reset();

				} else {

					prompts.warning(`<p class="lead">${response}</p>`);
					processing = false
				}
			},
			error: function (error) {
				prompts.warning(`<h3 style="text-align:center">Error</h3><p class="lead">${error}</p>`);
			}
		});
	};

	const setWeekPass = function (memberID: any, exitTime: any, week: any, verify: boolean = false, close:boolean = true) {
		//updateSessions();
		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'set_new_weekpass',
				memberID: memberID,
				week: week,
				time: exitTime,
				verify: verify
			},
			beforeSend: function (res) {
				loader.fixedCreate()
			},
			success: function (response) {

				loader.fixedDelete();
				if (response == 'passed') {
					if (memberID != 2) {
						config.premises.onPremises++;
						$('#entry-amount').val(parseInt(config.premises.onPremises));
						$('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
						entryCounter.createEntry();
					}
					if( close ) {
						$.prompt.close();
						reset();
						processing = false;
					} else {
						$( `a[data-member-id="${memberID}"]` ).text( ' Processed ' ).removeClass( 'btn-default' ).addClass( 'btn-success' );
						return true;
					}

				} else {
					prompts.warning(`
						<h3 style="text-align:center">Error</h3>
						<div class="well">Error: ${response}</div>`);
					processing = false
				}
			},
			error: function (error) {
				prompts.warning(`
					<h3 style="text-align:center">Error</h3>
					<div class="well">Error: ${error}</div>`);
			}
		})
	};

	const setBookingEntry = function( type:string, memberID: any, pp: any, sessiontime: any, verify: boolean = false, close: boolean = true ) {
		switch (type) {
			case 'session':
				$.ajax({
					url: ajaxurl,
					type: 'post',
					data: {
						action: 'set_member_entry_pp',
						memberID: memberID,
						ppid: pp,
						timeOut: sessiontime,
						verify: verify
					},
					beforeSend: function (res: any) {
						loader.fixedCreate()
					},
					success: function (response: any) {
						loader.fixedDelete();
						if (response == 'passed') {
							config.premises.onPremises++;
							config.premises.sessions--;
		
							$('#entry-amount').val(parseInt(config.premises.onPremises));
							$('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
							entryCounter.createEntry();
		
							$('#paid-sesh').val(config.premises.sessions);
							$('#paid-pass').val(config.premises.passes).triggerHandler('change');
							entryCounter.createPaid();
							if( close === true ) {
								$.prompt.close();
								reset();
								processing = false;
							} else {
								$.prompt.close();
								booking.total--;
								
								if( booking.total > 0 ){
									processBooking( booking.details.id );
								} else {
									reset();
									processing = false;
								}
							}
		
						} else {
							$.prompt.close();
						
							return true;
						}
					},
					error: function (error) {
						loader.fixedDelete();
		
						prompts.simple(config.default_simple_html.replace('{{message}}', `<p class="lead">${error}</p>`))
							.replace('{{actiontitle}}', 'Choose action');
					}
				});
				break;
			case 'pass':
				$.ajax({
					url: ajaxurl,
					type: 'post',
					data: {
						action: 'set_member_entry_booking_pass',
						passid: pp,
						memberID: memberID,
						timeOut: config.lasttime,
						verify: verify
					},
					beforeSend: function (res: any) {
						loader.fixedCreate()
					},
					success: function (response: any) {
						loader.fixedDelete();
						if (response == 'passed') {
							config.premises.onPremises++;
							config.premises.sessions--;
		
							$('#entry-amount').val(parseInt(config.premises.onPremises));
							$('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
							entryCounter.createEntry();
		
							$('#paid-sesh').val(config.premises.sessions);
							$('#paid-pass').val(config.premises.passes).triggerHandler('change');
							entryCounter.createPaid();
							if( close === true ) {
								$.prompt.close();
								reset();
								processing = false;
							} else {
								$.prompt.close();
								booking.total--;

								if( booking.total > 0 ){
									processBooking( booking.details.id );
								} else {

									reset();
									processing = false;
								}
							}
		
						} else {
							prompts.simple(config.default_simple_html.replace('{{message}}', `<p class="lead">${response}</p>`))
								.replace('{{actiontitle}}', 'Choose action');
		
							processing = false
						}
					},
					error: function (error) {
						loader.fixedDelete();
		
						prompts.simple(config.default_simple_html.replace('{{message}}', `<p class="lead">${error}</p>`))
							.replace('{{actiontitle}}', 'Choose action');
					}
				});
		}
		

	};

	const setPassEntry = function (memberID: any, sessiontime: any, verify: boolean = false) {
		updateSessions();
		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'set_member_entry',
				memberID: memberID,
				timeOut: sessiontime,
				verify: verify
			},
			beforeSend: function (res) {
				loader.fixedCreate()
			},
			success: function (response) {
				loader.fixedDelete();
				if (response == 'passed') {
					config.premises.onPremises++;
					config.premises.passes--;
					$('#entry-amount').val(parseInt(config.premises.onPremises));
					$('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
					entryCounter.createEntry();

					$('#paid-sesh').val(config.premises.sessions);
					$('#paid-pass').val(config.premises.passes).triggerHandler('change');
					entryCounter.createPaid();

					$.prompt.close();
					processing = false;
					reset();

				} else {

					prompts.warning(`<p class="lead">An error occured.  Please check the member has been added into the system <a href="${roots.live}?page=rampworld-membership/membership.php/entries%2Fview%2Fall&membership_id=${memberID}" target="_blank">here</a>.</p><div class="well">${error}</div>`);
					processing = false
				}
			},
			error: function (error) {
				prompts.warning(`<p class="lead">An error occured.  Please check the member has been added into the system <a href="${roots.live}?page=rampworld-membership/membership.php/entries%2Fview%2Fall&membership_id=${memberID}" target="_blank">here</a>.</p><div class="well">${error}</div>`);
			}
		})
	};
	const setSessions = function (): void {
		 $.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'get_todays_sessions'
			},
			beforeSend: function () {
				loader.fixedCreate();
			},
			success: function (response) {
				loader.fixedDelete();
				if (response != "") {
					config.sessions = JSON.parse(response);

					config.buttons.push({
						title: 'Cancel',
						value: false,
						classes: ['entry_large', "cancel"]
					});
					for (let i = 0; i < config.sessions.length; i++) {
						if (i % 3 != 0) {

							config.buttons.push({
								title: ((config.sessions[i].display_name.toLowerCase().includes('private')) ? config.sessions[i].end_time + " " + config.sessions[i].display_name.replace('Only', '') : config.sessions[i].end_time),
								value: config.sessions[i].end_time,
								classes: ['threerow', 'noBorder']
							})
						} else {
							config.buttons.push({
								title: ((config.sessions[i].display_name.toLowerCase().includes('private')) ? config.sessions[i].end_time + " " + config.sessions[i].display_name.replace('Only', '') : config.sessions[i].end_time),
								value: config.sessions[i].end_time,
								classes: 'threerow'
							})
						}
						if (!config.sessions[i].display_name.toLowerCase().includes('private') && (config.sessions[i].end_time > config.lasttime || config.lasttime == null))
							config.lasttime = config.sessions[i].end_time

						if ((config.onlyPrivate == true || config.onlyPrivate == null) && config.sessions[i].display_name.toLowerCase().includes('private')) {
							config.onlyPrivate = true;
						} else {
							config.onlyPrivate = false;
						}

					}
					config.sessoion_atm = true;

				} else {
					config.sessoion_atm = false;
					prompts.warning(`
						<h3 style="text-align: center;">Could not load session times</h3>
						<div class="well">Please contact administration to notify them of the issue.</div>`);

				}
			},
			error: function (e: any) {
				prompts.warning(`
					<h3 style="text-align: center;">Could not load session times</h3>
					<div class="well">Please contact administration to notify them of the issue.</div>`);

			}
		});

	};

	const updateSession = function (session_id: number, sessiontime: any) {
		//updateSessions();
		$.ajax({
			url: ajaxurl,
			type: 'post',
			data: {
				action: 'update_member_entry',
				session_id: session_id,
				exit_time: sessiontime
			},
			beforeSend: function (res) {
				loader.fixedCreate()
			},
			success: function (response) {
				loader.fixedDelete();
				if (response == 'passed') {

					$.prompt.close();
					reset();
					processing = false;

				} else {
					prompts.warning(`
						<h3>A database error occured.</h3>
						<div class="well"><strong>Error log</strong><br>${response}</div>`);
					processing = false;
				}
			},
			error: function (error) {
				prompts.warning(`
				<h3>A database error occured.</h3>
				<div class="well"><strong>Error log</strong><br>${error}</div>`);
				processing = false;
			}
		});
		
	};

	/**
	 * call every 10 minutes to update sessions or when processing new member
	 */
	const updateSessions = function (): any {
		let current_date = new Date();
		let temp_buttons = [];
		let updated: boolean= false;

		/*if (Object.keys(config.sessions).length > 0) {
			if (config.sessions[0].end_time < current_date.getHours() + ':' + current_date.getMinutes()) {
				$.each(config.sessions, function (i, v) {
					if (v.end_time < current_date.getHours() + ':' + current_date.getMinutes()) {
						delete config.sessions[i];
						updated = true;
					}


					console.log(i, v);
				});

				if (updated) {
					
					loader.toast('Removing expired sessions');

					
					$.each(config.sessions, function (i, v) {
						temp_buttons.push({
							title: ((v.display_name.toLowerCase().includes('private')) ? v.end_time + " " + v.display_name.replace('Only', '') : v.end_time),
							value: v.end_time,
							classes: 'threerow'
						})


						if ((config.onlyPrivate == true || config.onlyPrivate == null) && v.display_name.toLowerCase().includes('private')) {
							config.onlyPrivate = true;
						} else {
							config.onlyPrivate = false;
						}
					});
					config.buttons = temp_buttons;

				} else {
					loader.toast('Sessions up-to-date.');

				}
			}
		}*/
	};

	const reset = function () {

		$('#membership-number-input').val('');
		if ($('#custom-input-1').length == 0) {
			$('#day').remove();
			$('#month').remove();
			$('#year').after('<input type="text" class="form-control input-lg border-bottom-6" id="custom-input-1" placeholder="Search for Forename" style="width:100%" value="">').remove();
		}
		if ($('#custom-input-2').length == 0) {
			$('#day').remove();
			$('#month').remove();
			$('#year').after('<input type="text" class="form-control input-lg border-bottom-6" id="custom-input-2" placeholder="Search for Surname" style="width:100%" value="">').remove();
		}
		$('#custom-input-1').val('');
		$('#custom-input-1').attr('type', 'text');
		$('#custom-input-1').attr('placeholder', 'Please enter Forename');
		$('#custom-field-1').val('forename');
		$('#custom-input-2').val('');
		$('#custom-input-2').attr('type', 'text');
		$('#custom-input-2').attr('placeholder', 'Please enter Surname');
		$('#custom-field-2').val('surname');
		$('#number-of-members').text('-');
		$('#query-time').text('-');
		$('#render-time').text('-');
		$('#members').html(`
		<tr>
			<td colspan="8"><p class="lead" style="text-align: center;">${$('#entry-amount').val()} members on premises</p></td>
		</tr>`);

		$('.input-group:first-child .input-group-btn .dropdown-toggle').html('Forename <span class="caret"></span>');
		$('.input-group:last-child .input-group-btn .dropdown-toggle').html('Surname <span class="caret"></span>');

		$('.has-feedback').removeClass('has-feedback');
		$('.has-error').removeClass('has-error');
		$('.has-success').removeClass('has-success');
		$('#entry-table thead').html('<tr><th>#</th><th>Forename</th><th>Surname</th><th>Age</th><th>Expertise</th><th>Disipline</th><th>Medical notes</th><th>System notes</th></tr>');
	}
	return {
		init: init,
		processBooking: processBooking,
		processEntry: processEntry,
		verifyMemberGUI: verifyMemberGUI,
		reset: reset
	}
});