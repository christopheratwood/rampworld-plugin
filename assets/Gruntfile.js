module.exports = function (grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            styles: {

                files: ['src/sass/**/*.sass', 'src/sass/bootstrap/**/*.scss', 'src/sass/bootstrap/bootstrap/**/*.scss', 'src/ts/**/*.ts'],
                tasks: ['sass', 'autoprefixer', 'cssmin', 'ts', 'replace']
            },
            options: {
                nospawn: false
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compact',
                    sourcemap: 'none'
                },

                files: {
                    'dist/css/rwcui.css': 'src/sass/main.sass'
                }
            }
        },

        autoprefixer: {
            dist: {
                files: {
                    'dist/css/rwcui.css': 'dist/css/rwcui.css'
                }
            }
        },
        ts: {
            default: {
                src: "src/ts/**/*.ts",
                outDir: "dist/js",
                options: {
                    rootDir: "src/ts",
                    target: "es5",
                    noImplicitUseStrict: false,
                    sourceMap: false,
                    declaration: false,
                    module: "amd",
                    comment: false,
                    pretty: false
                }

            }
        },
        replace: {
            dist: {
                options: {
                    patterns: [{
                        match: /\\t|\\n/g,
                        replacement: '',
                    }],
                    usePrefix: false
                },
                files: [{
                    expand: true,
                    flatten: true,
                    src: ['dist/js/*.js'],
                    dest: 'dist/js'
                }]
            }
        },
        uglify: {
            jslive: {
                options: {
                    mangle: true,
                    unused: false
                },

                files: [{
                    expand: true,
                    src: 'dist/js/*.js',
                    dest: 'dist/js/',
                    cwd: '.'

                }]
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/css/rwcui.min.css': ['dist/css/rwcui.css']
                }
            }
        },
        uglify: {
            all: {
                files: [{
                    src: 'dist/js/*.js',
                    dest: 'dist/js/',
                    flatten: true,
                    expand: true,
                }]
            }
        }

    });



    // put said tasks into default grunt task
    grunt.registerTask('default', ['sass', 'autoprefixer', 'cssmin', 'ts', 'replace', 'uglify', 'watch']);

};