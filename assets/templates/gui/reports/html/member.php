<?php
$gender = array('m' => 'Male', 'f' => 'Female', 'o' => 'Other');
$note = array('1' => 'Ban notice', '2' => 'General notice');
$template = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
  <style>
    /* Default style definitions */



    @page { 
      margin: 0.25in;
    }

    /* General
    -----------------------------------------------------------------------*/
    body { 
      background-color: transparent;
      color: black;
      font-family: "verdana", "sans-serif";
      margin: 0px;
      padding-top: 0px;
      font-size: 1em;
    }

    @media print { 
      p { margin: 2px; }
    }

    h1 {
      font-size: 1.1em;
      font-style: italic;
    }

    h2 {
      font-size: 1.05em;
    }

    img { 
      border: none;
    }

    pre {
      font-family: "verdana", "sans-serif";
      font-size: 0.7em;
    }

    ul {
      list-style-type: circle;
      list-style-position: inside;
      margin: 0px;
      padding: 3px;
    }

    li.alpha {
      list-style-type: lower-alpha;
      margin-left: 15px;
    }

    p {
      font-size: 0.8em;
    }

    a:link,
    a:visited {
      /* font-weight: bold;  */
      text-decoration: none;
      color: black;
    }

    a:hover {
      text-decoration: underline;
    }

    #body { 
      padding-bottom: 2em;
      padding-top: 5px;
    }

    #body pre {
    }

    .center {
      text-align: center;
    }

    .right {
      text-align: right;
    }

    #money {
      text-align: right;
      padding-right: 20px;
    }

    /* Footer
    -----------------------------------------------------------------------*/
    #footer {
      color: black;
    }

    #copyright { 
      padding: 5px;
      font-size: 0.6em;
      background-color: white;
    }

    #footer_spacer_row {
      width: 100%;
    }

    #footer_spacer_row td {
      padding: 0px;
      border-bottom: 1px solid #000033;
      background-color: #F7CF07;
      height: 2px;
      font-size: 2px;
      line-height: 2px;
    }

    #logos {
      padding: 5px;
      float: right;
    }

    /* Section Header
    -----------------------------------------------------------------------*/
    #section_header {
      text-align: center;
    }

    #job_header { 
      text-align: left;
      background-color: white;
      margin-left: 5px;
      padding: 5px;
      border: 1px dashed black;
    }

    #job_info {
      font-weight: bold;
    }

    .header_details td {
      font-size: 0.6em;
    }

    .header_label {
      padding-left: 20px;
    }

    .header_field {
      padding-left: 5px;
      font-weight: bold;
    }

    /* Content
    -----------------------------------------------------------------------*/
    #content {
      padding: 0.2em 1% 0.2em 1%;
      min-height: 15em;
    }

    .page_buttons {
      text-align: center;
      margin: 3px;
      font-size: 0.7em;
      white-space: nowrap;
      font-weight: bold;
      width: 74%;
    }

    .link_bar {
      font-size: 0.7em;
      text-align: center;
      margin: auto;
    /*  white-space: nowrap; */
    }

    .link_bar a {
      white-space: nowrap;
      font-weight: bold;
    }

    .page_menu li {
      margin: 5px;
      font-size: 0.8em;
    }

    /* Detail
    -----------------------------------------------------------------------*/
    .detail_table {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      padding: 3px;
      margin: 15px;
    }

    .detail_head td {
      background-color: #ddd;
      color: black;
      font-weight: bold;
      padding: 3px;
      font-size: 0.75em;
      text-align: center;
    }

    .detail_label { 
      padding: 3px;
      font-size: 0.75em;  
      width: 16%;
      border-top: 1px solid #fff;
      border-bottom: 1px solid #fff;
      background-color: #ddd;
    }

    .detail_field { 
      width: 33%;
      font-size: 0.8em;
      color: ;
      text-align: center;
      padding: 3px;  
    }

    .detail_sub_table {
      font-size: 1em;
    }

    .detail_spacer_row td {
      border-top: 1px solid white;
      border-bottom: 1px solid white;
      background-color: #999;
      font-size: 2px;
      line-height: 2px;
    }

    #narrow {
      width: 50%;
    }

    .operation {
      width: 1%;
    }

    .summary_spacer_row {
      font-size: 0.1em;
    }

    .bar { 
      border-top: 1px solid black;
    }

    /* Forms
    -----------------------------------------------------------------------*/
    .form {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      margin-top: 10px;
    }

    .form td {
      padding: 3px;
    }

    .form th, .form_head td { 
      background-color: #ddd;
      border-bottom: 1px solid black;
      color: black;
      padding: 3px;
      text-align: center;
      font-size: 0.65em;
      font-weight: bold;
    }

    .form_head a:link,
    .form_head a:visited {
      color: black;
    }

    .form_head a:hover {
    }

    .sub_form_head td {
      border: none;
      font-size: 0.9em;
      white-space: nowrap;
    }

    .form input {
      color: black;
      background-color: white;
      border: 1px solid black;
      padding: 1px 2px 1px 2px;
      text-decoration: none;
      font-size: 1em;
    }

    .form textarea {
      color: black;
      background-color: white;
      border: 1px solid black;
      font-size: 1em;
    }

    .form select {
      color: black;
      background-color: white;
      font-size: 1em;
    }

    .button, a.button { 
      color: black;
      background-color: white;
      border: 1px solid black;
      font-weight: normal;
      white-space: nowrap;
      text-decoration: none;
    }

    a.button { 
      display: inline-block;
      text-align: center;
      padding: 2px;
    }

    a.button:hover {
      text-decoration: none;
      color: black;
    }

    .form_field {
      color: black;
      background-color: white;
      font-size: 0.7em;
    }

    .form_label {
      color: black;
      background-color: #ddd;
      font-size: 0.7em;
      padding: 3px;
    }

    /*
    .form_foot {
      background-color: #E5D9C3;
      font-size: 0.6em;
    }
    */

    .form_foot td { 
      background-color: #ddd;
      border-bottom: 1px solid black;
      color: black;
      padding: 3px;
      text-align: center;
      font-size: 0.65em;
      font-weight: bold;
    }

    .form_foot a:link,
    .form_foot a:visited {
      color: black;
    }

    .form_foot a:hover {
      color: black;
    }

    .no_border_input input {
      border: none;
    }

    .no_wrap {
      white-space: nowrap;
    }

    tr.row_form td { 
      white-space: nowrap;
    }

    /* Wizards
    -----------------------------------------------------------------------*/
    .wizard {
      font-size: 0.8em;
      border-top: 1px solid black;
    }

    #no_border {
      border: none;
    }

    .wizard p {
      text-indent: 2%;
    }

    .wizard td { 
      padding: 3px;
    /*  padding-left: 3px;
      padding-right: 3px;
      padding-bottom: 3px;*/
    }

    .wizard input {
      color: black;
      background-color: white;
      border: 1px solid black;
      padding: 1px 2px 1px 2px;
      text-decoration: none;
    }

    .wizard textarea {
      color: black;
      background-color: white;
      border: 1px solid black;
    }

    .wizard select {
      color: black;
      background-color: white;
      border: 1px solid black;
    }

    .wizard_head {
      color: black;
      font-weight: bold;
    }

    .wizard_buttons {
      border-top: 1px solid black;
      padding-top: 3px;
    }

    .wizard_buttons a {
      background-color: white;
      border: 1px solid black;
      padding: 2px 3px 2px 3px;
    }

    /* List
    -----------------------------------------------------------------------*/
    .list_table,
    .notif_list_table {
      color: black;
      padding-bottom: 4px;
      background-color: white;
    }

    .list_table td,
    .notif_list_table td { 
      padding: 3px 5px 3px 5px;
    }

    .list_table input {
      color: black;
      background-color: white;
      border: 1px solid black;
      padding: 1px 2px 1px 2px;
      text-decoration: none;
    }

    .list_head,
    .notif_list_head {
      font-weight: bold;
      background-color: #ddd;
      font-size: 0.65em;
    }

    .list_head td,
    .notif_list_head td {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      color: black;
      text-align: center;
      white-space: nowrap;
    }

    .list_head a:link,
    .list_head a:visited,
    .notif_list_head a:link,
    .notif_list_head a:visited {
      color: black;
    }

    .list_head a:hover,
    .notif_list_head a:hover {
    }

    .list_foot {
      font-weight: bold;
      background-color: #ddd;
      font-size: 0.65em;
    }

    .list_foot td {
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      color: black;
      text-align: right;
      white-space: nowrap;
    }

    .sub_list_head td {
      border: none;
      font-size: 0.7em;
    }

    .odd_row td {
    /*  background-color: #EDF2F7;
      border-top: 2px solid #FFFFff;*/
      background-color: transparent;
      border-bottom: 0.9px solid #ddd; /* 0.9 so table borders take precedence */
    }

    .even_row td {
    /*  background-color: #F8EEE4;
      border-top: 3px solid #FFFFff;*/
      background-color: #f6f6f6;
      border-bottom: 0.9px solid #ddd;
    }

    .spacer_row td { 
      line-height: 2px;
      font-size: 2px;
    }

    .phone_table td {
      border: none;
      font-size: 0.8em;
    }

    div.notif_list_text { 
      margin-bottom: 1px;
      font-size: 1.1em;
    }

    .notif_list_row td.notif_list_job { 
      text-align: center;
      font-weight: bold;
      font-size: 0.65em;
    }

    .notif_list_row td.notif_list_dismiss table td {
      text-align: center;
      font-size: 1em;
      border: none;
      padding: 0px 2px 0px 2px;
    }

    .notif_list_row td { 
      padding: 5px 5px 7px 5px;
      border-bottom: 1px dotted #ddd;
      background-color: white;
      font-size: 0.6em;
    }

    .notif_list_row:hover td {
      background-color: #ddd;
    }

    /* Page
    -----------------------------------------------------------------------*/
    .page {
      border: none;
      padding: 0in;
      margin-right: 0.1in;
      margin-left: 0.1in;
      /*margin: 0.33in 0.33in 0.4in 0.33in; */
      background-color: transparent;
    }

    .page table.header h1{ 
      font-size: 12pt;
    }

    .page>h2,
    .page>p { 
      margin-top: 2pt;
      margin-bottom: 2pt;  
    }

    .page h2 { 
      page-break-after: avoid;
    }

    .money_table {
      border-collapse: collapse;
      font-size: 6pt;
    }

    /* Tree
    -----------------------------------------------------------------------*/
    .tree_div { 
      display: none;
      background-color: #ddd;
      border: 1px solid #333;
    }

    .tree_div .tree_step_bottom_border { 
      border-bottom: 1px dashed #8B9DBE;
    }

    .tree_div .button, .tree_row_table .button,
    .tree_div .no_button {
      width: 110px;
      font-size: 0.7em;
      padding: 3px;
      text-align: center;
    }

    /*
    .tree_div .button a, .tree_row_table .button a {
      text-decoration: none;
      color: #114C8D;
    }
    */

    .tree_row_desc { 
      font-weight: bold;
      font-size: 0.7em;
      text-indent: -10px;  
    }

    .tree_row_info {
      font-size: 0.7em;
      width: 200px;
    }

    .tree_div_head a,
    .tree_row_desc a { 
      color: #000033;
      text-decoration: none;
    }

    .tree_div_head { 
      font-weight: bold;
      font-size: 0.7em;
    }

    /* Summaries
    -----------------------------------------------------------------------*/
    .summary {
      border: 1px solid black;
      background-color: white;
      padding: 1%;
      font-size: 0.8em;
    }

    .summary h1 {
      color: black;
      font-style: normal;
    }

    /* Sales-agreement specific
    -----------------------------------------------------------------------*/
    table.sa_signature_box { 
      margin: 2em auto 2em auto;
    }

    table.sa_signature_box tr td { 
      padding-top: 1.25em;
      vertical-align: top;
      white-space: nowrap;
    }

    .special_conditions { 
      font-style: italic;
      margin-left: 2em; 
      white-space: pre;
    }

    .sa_head * { 
      font-size: 7pt;
    }

    /* Change order specific
    -----------------------------------------------------------------------*/
    table.change_order_items { 
      font-size: 8pt;
      width: 100%;
      border-collapse: collapse;
      margin-top: 2em;
      margin-bottom: 2em;
    }

    table.change_order_items>tbody { 
      border: 1px solid black;
    }

    table.change_order_items>tbody>tr>th { 
      border-bottom: 1px solid black;
    }

    table.change_order_items>tbody>tr>td { 
      border-right: 1px solid black;
      padding: 0.5em;
    }

    td.change_order_total_col { 
      padding-right: 4pt;
      text-align: right;
    }

    td.change_order_unit_col { 
      padding-left: 2pt;
      text-align: left;
    }

  </style>
  
</head>

<body>

  <div id="body">

    <div id="section_header">
      <img width="204px" src="/Applications/XAMPP/xamppfiles/htdocs/wp-content/plugins/membership/assets/branding/logo/footer_logo_dark.jpeg">
    </div>

    <div id="content">

      <div class="page" style="font-size: 7pt">
        <table style="width: 100%;" class="header">
          <tr>
            <td>
              
              <h1 style="text-align: left">Member Report</h1>
            </td>
            <td>
              <h1 style="text-align: right">'. date('d-m-Y').'</h1>
            </td>
          </tr>
        </table>

        <table style="width: 100%; font-size: 8pt;">
          <tr><td><h2>Personal Details</h2></td><td><h2>Contact Details</h2></td</tr>
          <tr>
            <td>Member number: <strong>'. $member->members[0]['member_id'].'</strong></td>
            <td>Registration date: <strong>'. $member->members[0]['registered'].'</strong></td>
          </tr>

          <tr>
            <td>Name: <strong>'. ucwords(strtolower($member->members[0]['forename'])). ' '.ucwords(strtolower($member->members[0]['surname'])).'</strong></td>
            <td>Phone number: <strong>'.$member->members[0]['number'].'</strong></td>
          </tr>

          <tr>
            <td>Gender: <strong>'.$gender[$member->members[0]['gender']].'</strong></td>
            <td>Email: <strong>'.$member->members[0]['email'].'</strong></td>
          </tr>
          <tr>
            <td>Date of birth: <strong>'.$member->members[0]['dob'].'</strong></td>
            <td>Address: <strong>'.$member->members[0]['address_line_one'].', '.(($member->members[0]['address_line_two'] != '') ? $member->members[0]['address_line_two']. ', ' : '' ).''.(($member->members[0]['address_city'] != '') ? $member->members[0]['address_city']. ', ' : '' ).''.(($member->members[0]['address_county'] != '') ? $member->members[0]['address_county']. ', ' : '' ).''. $member->members[0]['address_postcode'].'</strong></td>
          </tr>
          <tr>
            <td colspan="2">Medical Notes:<strong>'.$member->members[0]['medical_notes'].'</strong></td>
          </tr>

          <tr><td><h2>Emergency Contact Details</h2></td><td><h2>Consent Details (if applicable)</h2></td</tr>
          <tr><td>Emergnecy Name: <strong>'.ucwords(strtolower($member->emergency[0]['name'])).'</strong></td><td>'.((isset($member->consent[0]))?'Name: <strong>'. $member->consent[0]['name'] . '</strong>': '').' </strong></td></tr>
          <tr><td>Emergnecy Number: <strong>'.$member->emergency[0]['emergency_number'].'</strong></td><td>'.((isset($member->consent[0]))? 'Number: <strong>'. $member->consent[0]['number'] . '</strong>': '').' </strong></td></tr>
          <tr><td>Emergnecy Name: <strong>'.((isset($member->emergency[1]))? $member->emergency[1]['name']:'').'</strong></td><td>'.((isset($member->consent[0]))? 'Email:  <strong>' . strtolower($member->consent[0]['email'] . '</strong>'): '').' </strong></td></tr>
          <tr><td>Emergnecy Number: <strong>'.((isset($member->emergency[1]))? $member->emergency[1]['emergency_number']:'').'</strong></td><td>'.((isset($member->consent[0]))? 'Address: <strong>'. $member->consent[0]['address_line_one'].','.$member->consent[0]['address_postcode'] . '</strong>': '').'</td></tr>
        </table>
        <table class="change_order_items">

          <tr>
            <td colspan="2">
              <h2>System notes</h2>
            </td>
          </tr>

          <tbody>';
for($i = 0; $i < count($notes->notes); $i++){
  $class=  (($i % 2)? 'even_row': 'odd_row');
  $template .= '<tr class="'.$class.'">
                <td style="text-align: center"><strong>#</strong>: '.$notes->notes[$i]['note_id'].'</td>
                <td rowspan="4" style="text-align: center; width: 60%" >'.$notes->notes[$i]['comments'].'</td>
              </tr>
              <tr class="'.$class.'">
                <td style="text-align: center"><strong>Start date</strong>: '.$notes->notes[$i]['start_date'].'</td>
              </tr>
              <tr class="'.$class.'">
                <td style="text-align: center"><strong>End date</strong>: '.$notes->notes[$i]['end_date'].'</td>
              </tr>
              <tr class="'.$class.'">
                <td style="text-align: center"><strong>Note type</strong>: '.$note[$notes->notes[$i]['type']].'</td>
              </tr>
             ';
}
$template .='</tbody>


          <tr>
            <td  style="text-align: right;"><strong>Total notes:</strong></td>
            <td class="change_order_total_col"><strong>'.count($notes->notes).'</strong></td>
          </tr>
        </table>

        <table class="change_order_items">

          <tr>
            <td colspan="6">
              <h2>Memberships <span style="font-weight: normal;">( Between '.$startDate.' and '.$endDate.')</span></h2>
            </td>
          </tr>

          <tbody>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Purchased Date</th>
              <th>Start Date</th>
              <th>Expiry Date</th>
              <th>Terminated</th>
            </tr>';
for($i = 0; $i < count($memberships->memberships); $i++){
  $template .= '
              <tr class="'.(($i % 2)? 'even_row': 'odd_row').'">
              <td style="text-align: center">'.$memberships->memberships[$i]['paid_membership_id'].'</td>
              <td style="text-align: center">'.$memberships->memberships[$i]['display_name'].'</td>
              <td style="text-align: center">'.$memberships->memberships[$i]['date'].'</td>
              <td style="text-align: center;">'.$memberships->memberships[$i]['start_date'].'</td>
              <td style="text-align: center;">'.$memberships->memberships[$i]['end_date'].'</td>
              <td style="text-align: center;">'.(($memberships->memberships[$i]['premature'] != '')? $memberships->memberships[$i]['premature'] : '').'</td>
            </tr>';
}
$template .='</tbody>




          <tr>
            <td colspan="5" style="text-align: right;"><strong>Total Paid Memberships:</strong></td>
            <td class="change_order_total_col"><strong>'.count($memberships->memberships).'</strong></td>
          </tr>
        </table>
        
        <table class="change_order_items">

          <tr>
            <td colspan="5">
              <h2>Online Booking <span style="font-weight: normal;">( Between '.$startDate.' and '.$endDate.')</span></h2>
            </td>
          </tr>

          <tbody>
            <tr>
              <th>#</th>
              <th>Transaction #</th>
              <th>Session Date</th>
              <th>Session Start</th>
              <th>Session End</th>
            </tr>';
for($i = 0; $i < count($session->sessions); $i++){
  $template .= '
              <tr class="'.(($i % 2)? 'even_row': 'odd_row').'">
              <td style="text-align: center">'.$session->sessions[$i]['prepaid_id'].'</td>
              <td style="text-align: center">'.$session->sessions[$i]['transaction_id'].'</td>
              <td style="text-align: center">'.$session->sessions[$i]['session_date'].'</td>
              <td style="text-align: center">'.$session->sessions[$i]['session_start_time'].'</td>
              <td style="text-align: center">'.$session->sessions[$i]['session_end_time'].'</td>
              </tr>';
}
$template .='</tbody>


          <tr>
            <td colspan="4" style="text-align: right;"><strong>Total Paid Sessions:</strong></td>
            <td class="change_order_total_col"><strong>'.count($session->sessions).'</strong></td>
          </tr>
        </table>
        <table class="change_order_items">

          <tr>
            <td colspan="5">
              <h2>Sessions <span style="font-weight: normal;">( Between '.$startDate.' and '.$endDate.')</span></h2>
            </td>
          </tr>

          <tbody>
            <tr>
              <th>#</th>
              <th>Entry Date</th>
              <th>Entry Time</th>
              <th>Exit Time</th>
              <th>Premature</th>
            </tr>';
for($i = 0; $i < count($entries->entries); $i++){
  $template .= '
              <tr class="'.(($i % 2)? 'even_row': 'odd_row').'">
              <td style="text-align: center">'.$entries->entries[$i]['entry_id'].'</td>
              <td style="text-align: center">'.$entries->entries[$i]['entry_date'].'</td>
              <td style="text-align: center">'.$entries->entries[$i]['entry_time'].'</td>
              <td style="text-align: center">'.$entries->entries[$i]['exit_time'].'</td>
              <td style="text-align: center;">'.(($entries->entries[$i]['premature_exit'] != '')? $entries->entries[$i]['premature_exit'] : '').'</td>
              </tr>';
}
$template .='</tbody>


          <tr>
            <td colspan="4" style="text-align: right;"><strong>Total Sessions:</strong></td>
            <td class="change_order_total_col"><strong>'.count($entries->entries).'</strong></td>
          </tr>
        </table>
        <p style="page-break-before: always;"></p>
        <table class="change_order_items">

          <tr>
            <td colspan="4">
              <h2>Original Terms and Conditions</h2>
            </td>
          </tr>

          <tbody>
            <tr>
              <th>#</th>
              <th>Published Date</th>
              <th>Active</th>
              <th>Version</th>
            </tr
            <tr>
              <td>'.$terms->terms[0]['tandc_id'].'</td>
              <td>'.$terms->terms[0]['publish_date'].'</td>
              <td>'.(($terms->terms[0]['deactivation_date'] != null && $terms->terms[0]['deactivation_date'] <= date('Y-m-d H:i:s'))? 'Deactived on:<strong>' . $terms->terms[0]['deactivation_date'].'</strong>' : 'Active' ).'</td>
               <td>'.$terms->terms[0]['version_number'].'</td>
            </tr>
            <tr>
              <th colspan="4" style="text-align: center">Content</th>
            </tr>
            <tr>
                 <td colspan="4" style="font-size: 6pt">'.$terms->terms[0]['content'].'</td>
            </tr>
          </tbody>
        </table>';
        
if($terms->terms[0]['deactivation_date'] != null && $terms->terms[0]['deactivation_date'] <= date('Y-m-d H:i:s')) {
   $template .= '<p style="page-break-before: always;"></p>
           <table class="change_order_items">

          <tr>
            <td colspan="4">
              <h2>Current Terms and Conditions</h2>
            </td>
          </tr>

          <tbody>
            <tr>
              <th>#</th>
              <th>Published Date</th>
              <th>Active</th>
              <th>Version Number</th>
            </tr
            <tr>
              <td>'.$terms->currentTerms[0]['tandc_id'].'</td>
              <td>'.$terms->currentTerms[0]['publish_date'].'</td>
              <td>Active</td>
              <td>'.$terms->currentTerms[0]['version_number'].'</td>
            </tr>
            <tr>
              <th colspan="4">Content</th>
            </tr>
            <tr>
              <td colspan="4" style="font-size: 6pt">'.$terms->currentTerms[0]['content'].'</td>
            </tr></tbody></table>';
}
$template .='
        
      </div>

    </div>
  </div>';