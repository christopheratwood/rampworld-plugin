<div class="panel panel-default" id="report-dates-panel">
		<div class="panel-heading">
				<div class="panel-heading">
						<h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
						<small>Membership Number: <?php echo $_GET['member']?></small>
						
				</div>

		</div>
		<div class="panel-default ">
				<div class="panel-body">
						<form class="form-inline" name="input" method="post" action="">
								<div class="row">
										<div class=" col-md-4 col-sm-6 col-xs-12 col-ms-12">
												<div class="panel panel-collapse panel-default">
														<div class="panel-heading" data-child="search_filter" >

																<h4 class="panel-title">
																		Start date
																		<span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
																				<i class="glyphicon glyphicon-chevron-down"></i>
																		</span>
																</h4>
														</div>
														<div class="panel-body" id="search_filter">
																<div class="input-group" style="width: 100%;">
																	<div class='input-group date' id='startDateGui' style="width:100%">
																		<input type='text' class="form-control" id="startDate" />
																		<span class="input-group-addon">
																				<span class="glyphicon glyphicon-calendar"></span>
																		</span>
																	</div>
															</div>
														</div><!-- /input-group -->
												</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12 col-ms-12">
												<div class="panel panel-collapse panel-default">
														<div class="panel-heading" data-child="search_filter" >
																<h4 class="panel-title">
																		End Date
																		<span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
																				<i class="glyphicon glyphicon-chevron-down"></i>
																		</span>
																</h4>
														</div>
														<div class="panel-body" id="search_filter">
																<div class="input-group" style="width: 100%;">
																		<div class="input-group" style="width: 100%;">
																	<div class="input-group date" id="endDateGui" style="width:100%">
																		<input type="text" class="form-control" id="endDate" />
																		<span class="input-group-addon">
																				<span class="glyphicon glyphicon-calendar"></span>
																		</span>
																	</div></div>
															</div>
														</div><!-- /input-group -->
												</div>
										</div>
										<div class="col-md-4 col-sm-4 col-sm-offset-8 col-md-offset-0  col-xs-12 " >
												<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fmember"  class="btn btn-default btn-block">Back</a>
												<button type="submit" class="btn btn-primary btn-block" id="report_create_member" style="margin-left: 0px;">Create</button>
										</div>
								</div>
								<input type="hidden" name="member_id" id="member_id" value="<?php echo $_GET['member'];?>">
						</form>
				</div>
		</div>
		
</div>