<div class="content">
	

	<div class="row">
		<h1>Add</h1>
		<form class="form-horizontal" id="editForm" action="#" method="post">
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<input type="submit" value="Save" class="btn btn-warning mg-10 btn-ms-block">
				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="holiday-dates">
			<?php if(isset($updated_successfull) && !is_array($updated_successfull)):?>
				<div class="alert alert-success"><strong>Success!</strong> Member details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Holiday details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['error'])):?>
				<?php if($_GET['error'] == 'couldnt+save'):?>
					<div class="alert alert-danger"><strong>Error!</strong> New holiday sessions could not save.</div>
				<?php elseif($_GET['error'] == 'incorrect+format'):?>
						<div class="alert alert-danger"><strong>Error!</strong> Holiday information is in incorrect format. Please contact administrator .	</div>
				<?php endif?>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">

				<div class="form-group">
					
					<label for="name" class="col-xs-3 col-ms-12 control-label form-label required">Display name:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="name" name="display_name" class="form-control " value="<?php echo isset($_POST['display_name'])? $_POST['display_name']:'';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="start_date" name="start_date"  value="<?php echo isset($_POST['start_date'])? $_POST['start_date']:'';?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="end_date" name="end_date"  value="<?php echo isset($_POST['end_date'])? $_POST['end_date']:'';?>" />
					</div>
				</div>
				<div class="form-group">
					
					<label for="visible" class="col-xs-3 col-ms-12 control-label form-label required">Publish:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="visible" name="visible" class="form-control" value="0" <?php echo (isset($_POST['visible']) && $_POST['visible'] == '0')? 'checked':'';?>> No
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="visible" name="visible" class="form-control" value="1" <?php echo (isset($_POST['visible']) && $_POST['visible'] == '1')? 'checked':'';?>> Yes
						</label>
					</div>
				</div>
				<div class="form-group">
					
					<label for="inherit" class="col-xs-3 col-ms-12 control-label form-label required">Inherit default times:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="inherit" name="inherit" class="form-control" value="0" <?php echo (isset($_POST['inherit']) && $_POST['inherit'] == '0')? 'checked': (!isset($_POST['inherit']))? 'checked':'';?>> No
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="inherit" name="inherit" class="form-control" value="1" <?php echo (isset($_POST['inherit']) && $_POST['inherit'] == '1')? 'checked':'';?>> Yes
						</label>
					</div>
			</div>
		</form>
</div>
