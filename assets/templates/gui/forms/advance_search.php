<div class="panel panel-default" id="advance-search-panel">
	<div class="panel-heading">
		<div class="panel-heading">
				<h4 class="panel-title" style="">Advance search</h4>
				<small>You can provide multiple values for each attribute by added a (,) comma between each value</small>
		</div>

	</div>
	<div class="panel-default ">
			<div class="panel-body">
					<form class="form" name="input">
							<div class="row">
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ms-12">
										<div class="row" style="margin-top:5px">
											<div class="col-md-4 col-sm-6">
												<label for="forename" class="">Member No.(s)</label>
											</div>
											<div class="col-md-8 col-sm-6">
												<input type="text" class="form-control" name="membership_id" id="membership_id" placeholder="Enter member number(s)" style="width:100%" value="<?php echo (isset($_GET['membership_id']) != null) ? str_replace('|', ', ', $_GET['membership_id']): '';?>">
														</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="forename" class="">Forename(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="forename" id="forename" placeholder="Enter forename(s)" style="width:100%" value="<?php echo (isset($_GET['forename']) != null) ? str_replace('|', ', ', $_GET['forename']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="surname" class="">Surname(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="surname" id="surname" placeholder="Enter surname(s)" style="width:100%" value="<?php echo (isset($_GET['surname']) != null) ? str_replace('|', ', ', $_GET['surname']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="surname" class="">Age range</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<div class='input-group' style="width:100%">
													<div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">between</button></div>
													<input type='text' class="form-control " name="start_age" id='start_age' value="<?php echo (isset($_GET['start_age']) != null) ? $_GET['start_age']: '';?>"/>
													<div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">and</button></div>
													<input type='text' class="form-control" name="end_age" id='end_age' value="<?php echo (isset($_GET['end_age']) != null) ? $_GET['end_age']: '';?>"/>
											</div>
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="genders" class="">Genders(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<div class="checkbox">
													<label>
															<input type="checkbox" id="check-all--gender"> All
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="genders" name="genders[]" value="m" class="gender-item" <?php echo ((isset($_GET['genders'])) ? ((preg_match('/m/', $_GET['genders']))? 'checked': ''): '');?>>  Male
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="genders" name="genders[]" value="f" class="gender-item" <?php echo ((isset($_GET['genders'])) ? ((preg_match('/f/', $_GET['genders']))? 'checked': ''): '');?>> Female
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="genders" name="genders[]" value="n" class="gender-item" <?php echo ((isset($_GET['genders'])) ? ((preg_match('/o/', $_GET['genders']))? 'checked': ''): '');?>> Other
													</label>
											</div>
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="dob" class="">Date of Birth(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="dob" id="dob" placeholder="Enter DOB(s)" style="width:100%" value="<?php echo (isset($_GET['dob']) != null) ? str_replace('|', ', ', $_GET['dob']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="dob" class="">Email(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="email" id="email" placeholder="Enter email(s)" style="width:100%" value="<?php echo (isset($_GET['email']) != null) ? str_replace('|', ', ', $_GET['email']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="dob" class="">Phone number(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="number" id="form_number" placeholder="Enter number(s)" style="width:100%" value="<?php echo (isset($_GET['number']) != null) ? str_replace('|', ', ', $_GET['number']): '';?>">
									</div>
							</div>
							
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ms-12">
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_line_one" class="">Address line one(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="address_line_one" id="address_line_one" placeholder="Enter address line one(s)" style="width:100%" value="<?php echo (isset($_GET['address_line_one']) != null) ? str_replace('|', ', ', $_GET['address_line_one']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_line_two" class="">Address line two(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="address_line_two" id="address_line_two" placeholder="Enter address line two(s)" style="width:100%" value="<?php echo (isset($_GET['address_line_two']) != null) ? str_replace('|', ', ', $_GET['address_line_two']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_city" class="">Address city(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="address_city" id="address_city" placeholder="Enter address city(s)" style="width:100%" value="<?php echo (isset($_GET['address_city']) != null) ? str_replace('|', ', ', $_GET['address_county']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_county" class="">Address county(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="address_county" id="address_county" placeholder="Enter county(s)" style="width:100%" value="<?php echo (isset($_GET['address_county']) != null) ? str_replace('|', ', ', $_GET['address_county']): '';?>">
									</div>
							</div>

							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_postcode" class="">Address postcode(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<input type="text" class="form-control" name="address_postcode" id="address_postcode" placeholder="Enter postcodes(s)" style="width:100%" value="<?php echo (isset($_GET['address_postcode']) != null) ? str_replace('|', ', ', $_GET['address_postcode']): '';?>">
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_postcode" class="">Registered(s)</label>
									</div>
									<div class="col-md-8 col-sm-6 ">
													<button type="button" class="btn btn-default disabled" style="height: 34px; width: 100%;">between</button>
													<input type='text' class="form-control date" name="start_date" id='start_date' value="<?php echo (isset($_GET['registered_start_date']) != null) ? $_GET['registered_start_date']: '';?>"/>
													<button type="button" class="btn btn-default disabled" style="height: 34px; width: 100%;">and</button><input type='text' class="form-control date" name="end_date" id='end_date' value="<?php echo (isset($_GET['registered_end_date']) != null) ? $_GET['registered_end_date']: '';?>"/>
											</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="address_postcode" class="">Report range(s)</label>
									</div>
									<div class="col-md-8 col-sm-6 ">
													<button type="button" class="btn btn-default disabled" style="height: 34px; width: 100%;">between</button>
													<input type='text' class="form-control date" name="report_start" id='report_start' value="<?php echo (isset($_GET['report_start']) != null) ? $_GET['report_start']: '';?>"/>
													<button type="button" class="btn btn-default disabled" style="height: 34px; width: 100%;">and</button><input type='text' class="form-control date" name="report_end" id='report_end' value="<?php echo (isset($_GET['report_end']) != null) ? $_GET['report_end']: '';?>"/>
									</div>
							</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ms-12">
							
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="dob" class="">Age Groups (Years)</label>
									</div>
									<div class="col-md-8 col-sm-6">
									<div class="checkbox">
													<label>
															<input type="checkbox" id="check-all--age"> All
													</label>
											</div>
									<div class="checkbox">
											<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="7-" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/7-/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> Under 7
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="7-10" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/7-10/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 7 - 10
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="11-13" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/11-13/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 11 - 13
													</label>
													</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="14-17" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/14-17/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 14 - 17
													</label>
											</div>

											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="18-21" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/18-21/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 18 - 21
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="22-30" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/22-30/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 22 - 30
													</label>
											</div>

											<div class="checkbox">
													<label>
															<input type="checkbox" id="age_groups" name="age_groups[]" value="30>" <?php echo ((isset($_GET['age_groups'])) ? ((preg_match('/30>/', $_GET['age_groups']))? 'checked': ''): '');?> class="age-item"> 30+
													</label>
											</div>
									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="dob" class="">Disipline(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<div class="checkbox">
													<label>
															<input type="checkbox" id="check-all--discipline"> All
													</label>
											</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="BMX" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/BMX/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> BMX
															</label>
													</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="SMX" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/SMX/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> SMX
															</label>
													</div>
													<div class="checkbox">
																	<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="MTB" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/MTB/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> MTB
															</label>
													</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="dicsciiplines" name="disciplines[]" value="skateboard" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/skateboard/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> Skateboard
															</label>
															</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="inline" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/inline/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> Inline
															</label>
															</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="other" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/other/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> Other
															</label>
															</div>
													<div class="checkbox">
															<label>
																	<input type="checkbox" id="disciplines" name="disciplines[]" value="spectator" <?php echo ((isset($_GET['disciplines'])) ? ((preg_match('/spectator/', $_GET['disciplines']))? 'checked': ''): '');?> class="discipline-item"> Spectator
															</label>
													</div>

									</div>
							</div>
							<div class="row" style="margin-top:5px">
									<div class="col-md-4 col-sm-6">
											<label for="expertise" class="">Expertise(s)</label>
									</div>
									<div class="col-md-8 col-sm-6">
											<div class="checkbox">
													<label>
															<input type="checkbox" id="check-all--expertise"> All
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="0" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/0/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Beginner
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="1" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/1/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Novice
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="2" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/2/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Experienced
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="3" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/3/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Advanced
													</label>
													</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="4" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/4/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Expert
													</label>
											</div>
											<div class="checkbox">
													<label>
															<input type="checkbox" id="expertise" name="expertise[]" value="5" <?php echo ((isset($_GET['expertise'])) ? ((preg_match('/5/', $_GET['expertise']))? 'checked': ''): '');?> class="expertise-item"> Professional
													</label>
											</div>
									</div>
							</div>
					</div>
					<div class=" col-md-offset-9 col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-top:20px">
						<button type="submit" class="btn btn-primary btn-block" id="advance-search-btn">Set filter(s)</button>
					</div>
				</div>
			</form>
		</div>
	</div>
    
</div>