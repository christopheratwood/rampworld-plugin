<div class="content">
	
	<div class="row">
		<h1>Add</h1>
		<form class="form-horizontal" id="editForm" action="#" method="post">
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<input type="submit" value="Save" class="btn btn-warning mg-10 btn-ms-block">
				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="session-times">
			<?php if(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Session details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($error)):?>
					<div class="alert alert-danger"><strong>Error!</strong><?php echo $error;?></div>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">

				<div class="form-group <?php echo ((isset($validation->errors['display_name'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Display name: </label>
						<p class="text-right"><em>Please ensure correct grammar is used. This is displayed to the public.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<input type="text" id="name" name="display_name" class="form-control " value="<?php echo isset($_POST['display_name'])? $_POST['display_name']:'';?>">
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['hid'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Session type: </label>
						<p class="text-right"><em>Please select whether its a regular session or a holiday session.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<select class="form-control" id="hid" name="hid">
							<option value="null" <?php echo (isset($_POST['hid']) && $_POST['hid'] === null)? 'selected':'';?>>Normal sessions</option>
							<?php foreach($holiday->holidays as $holiday){
									echo '<option value="'.$holiday['holiday_id'].'" '.(((isset($_POST['hid']) && $_POST['hid'] == $holiday['holiday_id']) || isset($_GET['hid']) && !isset($_POST['hid']) && $_GET['hid'] === $holiday['holiday_id'])? 'selected':'').'>'.$holiday['display_name'].'</option>';
							}?>
						</select>
					</div>
				</div>
			
				<div class="form-group <?php echo ((isset($validation->errors['day'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="day" class="col-xs-12 control-label form-label required">Day: </label>
						<p class="text-right"><em>The day of session</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<select class="form-control" id="day" name="day">
							<option value="null" <?php echo ((isset($_POST['day']) && $_POST['day'] === 'null') || !isset($_POST['day']))? 'selected':'';?>>Select day</option>
							<option value="1" <?php echo (isset($_POST['day']) && $_POST['day'] === '1')? 'selected':'';?>>Monday</option>
							<option value="2" <?php echo (isset($_POST['day']) && $_POST['day'] === '2')? 'selected':'';?>>Tuesday</option>
							<option value="3" <?php echo (isset($_POST['day']) && $_POST['day'] === '3')? 'selected':'';?>>Wednesday</option>
							<option value="4" <?php echo (isset($_POST['day']) && $_POST['day'] === '4')? 'selected':'';?>>Thursday</option>
							<option value="5" <?php echo (isset($_POST['day']) && $_POST['day'] === '5')? 'selected':'';?>>Friday</option>
							<option value="6" <?php echo (isset($_POST['day']) && $_POST['day'] === '6')? 'selected':'';?>>Saturday</option>
							<option value="0" <?php echo (isset($_POST['day']) && $_POST['day'] === '0')? 'selected':'';?>>Sunday</option>
						</select>
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['start_time'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="start_time" class="col-xs-12 control-label form-label required">Start time: </label>
						<p class="text-right"><em>The start time of a the session.</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="start_time" name="start_time"  value="<?php echo isset($_POST['start_time'])? $_POST['start_time']:'';?>" />
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['end_time'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="end_time" class="col-xs-12 control-label form-label required">End time: </label>
						<p class="text-right"><em>The end time of a the session</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="end_time" name="end_time"  value="<?php echo isset($_POST['end_time'])? $_POST['end_time']:'';?>" />
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['beginner'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="beginner" class="col-xs-12 control-label form-label required">Beginner: </label>
						<p class="text-right"><em>Is this session only for beginners?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="beginner" name="beginner" class="form-control" value="1" <?php echo (isset($_POST['beginner']) && $_POST['beginner'] == '1')? 'checked':'';?>> Yes
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="beginner" name="beginner" class="form-control" value="0" <?php echo (isset($_POST['beginner']))? 'checked':'';?>> No
						</label>
						
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['consecutive'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="consecutive" class="col-xs-12 control-label form-label required">Consecutive: </label>
						<p class="text-right"><em>Can this session be used in alongside other sessions?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="consecutive" name="consecutive" class="form-control" value="1" <?php echo (isset($_POST['consecutive']) && $_POST['consecutive'] == '1')? 'checked':'';?>> Yes
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="consecutive" name="consecutive" class="form-control" value="0" <?php echo (!isset($_POST['consecutive']))? 'checked':'';?>> No
						</label>
						
					</div>
				</div>
				<div class="form-group <?php echo ((isset($validation->errors['private'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
					<div class="col-xs-3 col-ms-12">
						<label for="private" class="col-xs-12 control-label form-label required">Private </label>
						<p class="text-right"><em>Is this session a private hire?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="private" name="private" class="form-control" value="1" <?php echo (isset($_POST['private']) && $_POST['private'] == '1')? 'checked':'';?>> Yes
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="private" name="private" class="form-control" value="0" <?php echo (!isset($_POST['private']))? 'checked':'';?>> No
						</label>
						
					</div>
				</div>
			</div>
		</form>
</div>
