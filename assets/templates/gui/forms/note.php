<div class="content">

	<div class="row">
		<h1>Add</h1>
		<form class="form-horizontal" id="editForm" action="#" method="post">
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<input type="submit" value="Save" class="btn btn-warning mg-10 btn-ms-block">
				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="holiday-dates">
			<?php if(isset($updated_successfull) && $updated_successfull === true ):?>
				<div class="alert alert-success"><strong>Success!</strong> Note details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Note details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['error'])):?>
				<?php if($_GET['error'] == 'incorrect+member'):?>
					<div class="alert alert-danger"><strong>Error!</strong> Member did not exists.</div>
				<?php elseif($_GET['error'] == 'couldnt+save'):?>
					<div class="alert alert-danger"><strong>Error!</strong> Note could not save.</div>
				<?php elseif($_GET['error'] == 'incorrect+format'):?>
						<div class="alert alert-danger"><strong>Error!</strong> Note information is in incorrect format. Please contact administrator .	</div>
				<?php endif?>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">
			
			<div class="form-group <?php echo isset($_POST['start_date']) ? isset($validation->errors['type']) || !isset($_POST['type'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="type" class="col-xs-3 col-ms-12 control-label form-label required ">Type:</label>
				<div class="col-xs-6 col-ms-12">
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="type" name="type" class="form-control" value="1"  <?php echo ((isset($_POST['type']) && $_POST['type'] == '1')? 'checked':'');?>> Ban
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="type" name="type" class="form-control" value="2"  <?php echo ((isset($_POST['type']) && $_POST['type'] == '2')? 'checked':'');?>> Temporary ban
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="type" name="type" class="form-control" value="3"  <?php echo ((isset($_POST['type']) && $_POST['type'] == '3')? 'checked':'');?>> Anti-social behaviour
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="type" name="type" class="form-control" value="4"  <?php echo ((isset($_POST['type']) && $_POST['type'] == '4')? 'checked':'');?>> Other
					</label>
				</div>
			</div>
			<div class="form-group <?php echo isset($_POST['start_date']) ? isset($validation->errors['start_date'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
				<div class="col-xs-6 col-ms-12">
					<input type="text" class="form-control date" id="start_date"  name="start_date" value="<?php echo isset($_POST['start_date']) && $_POST['start_date'] != ''? DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime($_POST['start_date'])) )->format('Y-m-d'):'';?>">
	
				</div>
			</div>
			<div class="form-group <?php echo isset($_POST['end_date']) ? isset($validation->errors['end_date'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="end_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
				<div class="col-xs-6 col-ms-12">
					<input type="text" class="form-control date" id="end_date"  name="end_date" value="<?php echo isset($_POST['end_date']) && $_POST['end_date'] != ''? DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime($_POST['end_date'])) )->format('Y-m-d'): '';?>">
	
				</div>
			</div>
			<div class="form-group <?php echo isset($_POST['comments']) ? isset($validation->errors['comments'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="comments" class="col-xs-3 col-ms-12 control-label form-label">Comments:</label>
				<div class="col-xs-6 col-ms-12">
					<textarea class="form-control" id="comments"  name="comments" rows="3"><?php echo isset($_POST['comments']) ?$_POST['comments'] : '' ;?></textarea>
	
				</div>
			</div>

			<input type="hidden" name="mid" value="<?php echo $_GET['member'];?>">
			<hr>
			</form>
		</form>
</div>
