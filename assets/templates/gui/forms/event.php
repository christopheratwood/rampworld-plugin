<div class="content">

<div class="row">
  <h1>Add</h1>
  <form class="form-horizontal" id="editForm" action="#" method="post">
  <div class="well well-sm">
    <div class="row">
      <div class="col-md-9 col-xs-8 col-ms-12">
      <p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
      <input type="submit" value="Save" class="btn btn-warning mg-10 btn-ms-block">
      </div>
  
    </div>
  </div>

  <div class="col-md-8 col-sm-8" id="session-times">
    <?php if(isset($updated_successfull)):?>
      <div class="alert alert-danger"><strong>Error!</strong> Event details did not pass validation.<br>
        <?php foreach($validation::$errors as $error){
          echo '<p>'. $error.'</p>';
        }?>
      </div>
    <?php endif?>

    <form class="form-horizontal" id="editForm" action="#" method="post">

      <div class="form-group <?php echo ((isset($validation::$errors['display_name'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
      <div class="col-xs-4 col-ms-12">
          <label for="name" class="col-xs-12 control-label form-label required">Display name: </label>
          <p class="text-right"><em>Please ensure correct grammar is used. This is displayed to the public.</em></p>
        </div>

        <div class="col-xs-6 col-ms-12">
          <input type="text" id="name" name="display_name" class="form-control " value="<?php echo isset($_POST['display_name'])? $_POST['display_name']:'';?>">
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['date'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="date" class="col-xs-12 control-label form-label required">Event date: </label>
          <p class="text-right"><em>The end time of the event</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <input type='text' class="form-control date" id="date" name="date"  value="<?php echo isset($_POST['date'])? $_POST['date']:'';?>" />
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['start_time'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="start_time" class="col-xs-12 control-label form-label required">Start time: </label>
          <p class="text-right"><em>The start time of the event.</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <input type='text' class="form-control time" id="start_time" name="start_time"  value="<?php echo isset($_POST['start_time'])? $_POST['start_time']:'';?>" />
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['end_time'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="end_time" class="col-xs-12 control-label form-label required">End time: </label>
          <p class="text-right"><em>The end time of the event</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <input type='text' class="form-control time" id="end_time" name="end_time"  value="<?php echo isset($_POST['end_time'])? $_POST['end_time']:'';?>" />
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['price'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="name" class="col-xs-12 control-label form-label required">Event Price:</label>
          <p class="text-right"><em>Please enter the event price.  Please do not use currency symbol.</em></p>
        </div>

        <div class="col-xs-6 col-ms-12">
          <div class="input-group">
            <div class="input-group-addon">£</div>
            <input type="number" class="form-control" id="price" name="price" placeholder="00.00" value="<?php echo isset($_POST['price'])? $_POST['price']:'';?>">
          </div>
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['description'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="name" class="col-xs-12 control-label form-label required">Event description:</label>
          <p class="text-right"><em>Please enter the event description.  Please contain all relevant information that customers may way to know.</em></p>
        </div>

        <div class="col-xs-6 col-ms-12">
          <textarea class="form-control" name="description" id="description"><?php echo isset($_POST['description'])? $_POST['description']:'';?></textarea>
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['beginner'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="beginner" class="col-xs-12 control-label form-label required">Beginner: </label>
          <p class="text-right"><em>Is this session only for beginners?</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <label class="radio-inline radio-ms-block">
            <input type="radio" id="beginner" name="beginner" class="form-control" value="1" <?php echo (isset($_POST['beginner']) && $_POST['beginner'] == '1')? 'checked':'';?>> Yes
          </label>
          <label class="radio-inline radio-ms-block">
            <input type="radio" id="beginner" name="beginner" class="form-control" value="0" <?php echo (isset($_POST['beginner']))? 'checked':'';?>> No
          </label>
          
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['online'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="online" class="col-xs-12 control-label form-label required">Online exclusive: </label>
          <p class="text-right"><em>Is this event online exclusive?</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <label class="radio-inline radio-ms-block">
            <input type="radio" id="online" name="online" class="form-control" value="1" <?php echo (isset($_POST['beginner']) && $_POST['beginner'] == '1')? 'checked':'';?>> Yes
          </label>
          <label class="radio-inline radio-ms-block">
            <input type="radio" id="online" name="online" class="form-control" value="0" <?php echo (isset($_POST['beginner']))? 'checked':'';?>> No
          </label>
          
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['pass'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="pass" class="col-xs-12 control-label form-label required">Pass exclusion: </label>
          <p class="text-right"><em>Does pass holders attend this event for free?</em></p>
        </div>
        <div class="col-xs-6 col-ms-12">
          <label class="radio-inline radio-ms-block">
            <input type="radio" id="pass" name="pass" class="form-control" value="1" checked> No
          </label>         
        </div>
      </div>
      <div class="form-group <?php echo ((isset($validation::$errors['discipline'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label for="name" class="col-xs-12 control-label form-label required">Event  discipline(s):</label>
          <p class="text-right"><em>Please select all the disciplines that is allowed to attent this event.</em></p>
        </div>

        <div class="col-xs-6 col-ms-12">
          <div class="col-xs-6 col-ms-6">
            <label class="checkbox">
              <input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="BMX" <?php echo ((isset($_POST['discipline']) && in_array('BMX', $_POST['discipline']))? 'checked':'');?>> BMX
            </label>
            <label class="checkbox">
            <input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="SMX" <?php echo ((isset($_POST['discipline']) && in_array('SMX', $_POST['discipline']))? 'checked':'');?>> SMX
            </label>
            <label class="checkbox">
              <input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="MTB" <?php echo ((isset($_POST['discipline']) && in_array('MTB', $_POST['discipline']))? 'checked':'');?>> MTB
            </label>
          </div>
          <div class="col-xs-6 col-ms-6">

            <label class="checkbox">
              <input type="checkbox" id="disicpline" class="form-control" name="discipline[]" value="skateboard" <?php echo ((isset($_POST['discipline']) && in_array('skateboard', $_POST['discipline']))? 'checked':'');?>> Skateboard
            </label>
            <label class="checkbox">
                <input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="inline" <?php echo ((isset($_POST['discipline']) && in_array('inline', $_POST['discipline']))? 'checked':'');?>> Inline
            </label>
            <label class="checkbox">
              <input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="spectator" <?php echo ((isset($_POST['discipline']) && in_array('spectator', $_POST['discipline']))? 'checked':'');?>> Spectator
            </label>
          </div>
        </div>
      </div>
     
      <div class="form-group <?php echo ((isset($validation::$errors['image_portrait']) || isset($validation::$errors['image_landscape']) ) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
        <div class="col-xs-4 col-ms-12">
          <label class="col-xs-12 control-label form-label required">Event images:</label>
          <p class="text-right"><em></em></p>
        </div>

        <div class="col-xs-6 col-ms-12">
          <div class="form-group <?php echo ((isset($validation::$errors['image_portrait'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
            <label for="image_portrait" class="form-label required">Image to display on event overview page:</label>
            <input type="text" class="form-control" name="image_portrait" id="image_portrait" placeholder="Please enter full URL." value="<?php echo isset($_POST['image_portrait'])? $_POST['image_portrait']:'';?>">
          </div>
          <div class="form-group <?php echo ((isset($validation::$errors['image_landscape'])) ? 'has-feedback has-error': ((isset($validation::$errors))? 'has-feedback has-success' : ''));?>">
            <label for="image_landscape" class="form-label required">Image to display on event booking:</label>
            <input type="text" class="form-control" name="image_landscape" id="image_landscape" placeholder="Please enter full URL." value="<?php echo isset($_POST['image_landscape'])? $_POST['image_landscape']:'';?>">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
