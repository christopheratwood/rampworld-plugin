<?php
use Rampworld\Pagination\Create as Pagination;
require_once __dir__ . '/../../../../../../themes/rampworld/modules/vendor/autoload.php';

switch($form_name){ 
    
    case 'booking':

        /* search options */
        $searchField = (isset($_GET['search_field'])) ? $_GET['search_field']: null; 
        $searchQuery = (isset($_GET['search_query'])) ? $_GET['search_query']: null; 

        /* indexing options */
        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;
				
        Pagination::booking($searchField, $searchQuery, intval($page), intval($limit), intval($page));

        $filtertext = '';
        if($searchField != null) {
            if($searchField == 'booking date') {
                $s = explode('-', $searchQuery);
                $searchQuery = $s[2] . '/' . $s[1] . '/' . $s[0];
            }
            $filtertext = $searchField .' = '. $searchQuery;
        } ?>
        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filtertext) > 0)? '<small>('. ucwords($filtertext).')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class=" col-xs-9 col-ms-8">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="search_filter" >

                                        <h4 class="panel-title">
                                            Search
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="search_filter">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;"  data-element="searchField"><?php echo ($searchField != null) ? ucwords($searchField): 'Booking Reference';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Booking Reference</a></li>
                                                    <li><a href="#">Booking Email</a></li>
                                                    <li><a href="#">Booking Name</a></li>
                                                    <li><a href="#">Booking Date</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Member Name</a></li>
                                                    <li><a href="#">Member Name</a></li>
                                                </ul>
                                                <input type="hidden" name="searchField" id="searchField" value="<?php echo ($searchField != null)? $searchField :'booking reference'?>">
                                            </div><!-- /btn-group -->
                                            <input type="text" class="form-control" name="searchQuery" id="searchQuery" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery != null) ? $searchQuery: '';?>">
                                             </div><!-- /input-group -->
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_entires">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_bookings" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
            
        </div>
    <?php break; ?>
    <?php case 'entries':
        //get todays sessions
       
        $forename = (isset($_GET['forename'])) ? $_GET['forename']: null; 
        $surname = (isset($_GET['surname'])) ? $_GET['surname']: null; 
        $membership_id = (isset($_GET['membership_id'])) ? $_GET['membership_id']: null; 
        $start_dt = (isset($_GET['start_dt'])) ? str_replace('%3A', ':',$_GET['start_dt']): null; 
        $end_dt = (isset($_GET['end_dt'])) ? str_replace('%3A', ':',$_GET['end_dt']): null; 

        $limit = (isset($_GET['limit']))? $_GET['limit']: 999;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text = (($forename != null)? ', Forename = '.ucwords($forename) :'') . (($surname != null) ? ', Surname = '.$surname : '') . (($membership_id != null) ? ', Member ID = '.$membership_id : '') ;


        $pagination->entries($forename, $surname, $membership_id, $start_dt, $end_dt, intval($page), intval($limit), intval($page));?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading open" data-child="main">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. substr($filter_text, 2).')</small>' : '';?>
                    <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#search-panel" href="#main">
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </span>
                </div>

            </div>
            <div class="panel-default ">
                <div style="margin: 10px">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="forename_filter" >
                                        <h4 class="panel-title">
                                            Search by forename and surname
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="forename_filter">

                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Forename</button></div>
                                            <input type="text" class="form-control" name="forename" id="forename" placeholder="Search for ..." style="width:100%" value="<?php echo ($forename != null) ? $forename: '';?>">
                                             </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;" >Surname</button></div>
                                            <input type="text" class="form-control" name="surname" id="surname" placeholder="Search for ..." style="width:100%" value="<?php echo ($surname != null) ? $surname: '';?>">
                                                </div><!-- /input-group -->
                                    </div><!-- /input-group -->
                                </div>
                            </div>

          
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="table-dates-panel" >

                                        <h4 class="panel-title">
                                            Select by times
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="table-dates-panel">
                                        <div class='input-group date' style="width:100%">
                                             <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Start</button></div>
                                            <input type='text' class="form-control" name="start_dt" id='start_dt' value="<?php echo ($start_dt != null) ? str_replace('T', ' ',   str_replace('-', '/', $start_dt)): '';?>"/>
                                        </div>
                                        <div class='input-group date' style="width:100%">
                                             <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">End   </button></div>
                                            <input type='text' class="form-control" name="end_dt" id='end_dt' value="<?php echo ($end_dt != null) ? str_replace('T', ' ', str_replace('-', '/', $end_dt)): '';?>"/>
                                        </div>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="clearfix visible-sm-block"></div>
                            
                            
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-ms-12">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="member_filter" >
                                        <h4 class=" muted panel-title">
                                            Search by member No.
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="member_filter">
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;"><i class="glyphicon glyphicon-user " style="font-size:16px;line-height: 19px; "></i></button></div>
                                            <input type="number" class="form-control" name="membership_id" id="membership_id" placeholder="Search for ..." style="width:100%; height: 34px;" value="<?php echo ($membership_id != null) ? $membership_id: '';?>">
                                                </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-8" >
                                 <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="limit_filter" >
                                        <h4 class=" muted panel-title">
                                            Limit
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="limit_filter">
                                        <label class="radio-inline"><input type="radio" id="limit" name="limit" value="16" <?php echo ($limit == 16)? 'checked':'';?>> 16</label>
                                        <label class="radio-inline"><input type="radio" id="limit" name="limit" value="32" <?php echo ($limit == '32')? 'checked':'';?>> 32</label>
                                        <label class="radio-inline"><input type="radio" id="limit" name="limit" value="64" <?php echo ($limit == '64')? 'checked':'';?>> 64</label>
                                        <label class="radio-inline"><input type="radio" id="limit" name="limit" value="128" <?php echo ($limit == '128')? 'checked':'';?>> 128</label>
                                        <label class="radio-inline"><input type="radio" id="limit" name="limit" value="999" <?php echo ($limit == '999')? 'checked':'';?>> All</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_entries">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_entries" style="margin-left: 0px;">Filter</button>
                                <?php if( current_user_can('administrator')):?>
                                    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&report=time-duration&amp;p=<?php echo $page;?><?php echo (($limit != null) ? '&amp;limit=' . $limit : '');?><?php echo (($forename != null) ? '&amp;forename=' . str_replace(' ', '+', $forename) : '');?><?php echo (($surname != null) ? '&surname=' . str_replace(' ', '+', $surname) : '');?><?php echo (($membership_id != null) ? '&membership_id=' . $membership_id: '');?><?php echo (($start_dt != null) ? '&amp;start_dt=' . $start_dt :'');?><?php echo (( $end_dt != null) ? '&end_dt='.$end_dt:'');?>" type="submit" class="btn btn-success btn-block" id="filter_btn_entries" style="margin-left: 0px;">Generate report</a>
                                <?php endif;?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break; ?>
    <?php case 'report-member':


        $membership_id = (isset($_GET['membership_id'])) ? $_GET['membership_id']: null; 
        $searchField = (isset($_GET['searchField1'])) ? $_GET['searchField1']: null; 
        $searchQuery = (isset($_GET['searchQuery1'])) ? $_GET['searchQuery1']: null; 
        $searchField2 = (isset($_GET['searchField2'])) ? $_GET['searchField2']: null; 
        $searchQuery2 = (isset($_GET['searchQuery2'])) ? $_GET['searchQuery2']: null; 

        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text = (($membership_id != null)? ', Member ID = '.$membership_id :'') . (($searchField != null) ? ', '.$searchField.' = '.$searchQuery : '')  .(($searchField2 != null) ? ', '.$searchField2.' = '.$searchQuery2 : '')  ;


        $pagination->members($membership_id, $searchField, $searchQuery, $searchField2, $searchQuery2, intval($page), intval($limit), intval($page));?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. substr($filter_text, 2).')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="forename_filter" >

                                        <h4 class="panel-title">
                                            Search by membership number
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="forename_filter">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;"  data-element="searchField"><?php echo ($searchField != null) ? ucwords($searchField): 'Forename';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Forename</a></li>
                                                    <li><a href="#">Surname</a></li>
                                                    <li><a href="#">Date of birth</a></li>
                                                    <li><a href="#">Phone number</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Address line 1</a></li>
                                                    <li><a href="#">Postcode</a></li>
                                                </ul>
                                                <input type="hidden" name="searchField" id="searchField" value="<?php echo ($searchField != null)? $searchField :'forename'?>">
                                            </div><!-- /btn-group -->
                                            <input type="text" class="form-control" name="searchQuery" id="searchQuery" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery != null) ? $searchQuery: '';?>">
                                        </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;" data-element="searchField2"><?php echo ($searchField2 != null) ? ucwords($searchField2): 'Surname';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Forename</a></li>
                                                    <li><a href="#">Surname</a></li>
                                                    <li><a href="#">Date of birth</a></li>
                                                    <li><a href="#">Phone number</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Address line 1</a></li>
                                                    <li><a href="#">Postcode</a></li>
                                                </ul>
                                                <input type="hidden" name="searchField2" id="searchField2" value="<?php echo ($searchField2 != null)? $searchField2 :'surname'?>">
                                            </div><!-- /btn-group -->
                                            <input type="text" class="form-control" name="searchQuery2" id="searchQuery2" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery2 != null) ? $searchQuery2: '';?>">
                                        </div><!-- /input-group -->
                                    </div><!-- /input-group -->
                                </div>
                            </div>

                            
                            
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="limit_filter" >
                                        <h4 class=" muted panel-title">
                                            Search by member No.
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="limit_filter">
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;"><i class="glyphicon glyphicon-user " style="font-size:16px;line-height: 19px; "></i></button></div>
                                            <input type="number" class="form-control" name="membership_id" id="membership_id" placeholder="Search for ..." style="width:100%; height: 34px;" value="<?php echo ($membership_id != null) ? $membership_id: '';?>">
                                                </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_report_member">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_report_member" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break; ?>
    <?php case 'holidays':

        $holiday_id = (isset($_GET['hid'])) ? $_GET['hid']: null; 
        $start = (isset($_GET['start_dt'])) ? $_GET['start_dt']: null; 
        $end = (isset($_GET['end_dt'])) ? $_GET['end_dt']: null; 

        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text = (($holiday_id != null)? ', Holiday ID = '.$holiday_id :'') . (($start != null) ? ', Start date = '.$start: '')  .(($end != null) ? ', End date =  '.$end : '')  ;

        $pagination->holidays($holiday_id, $start, $end, intval($page), intval($limit), intval($page));?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. substr($filter_text, 2).')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-ms-12">
                                <div class="panel panel-collapse panel-default" id="holiday-dates">
                                    <div class="panel-heading" data-child="holiday_filter" >

                                        <h4 class="panel-title">
                                            Search by Holiday number
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="holiday_filter">

                                        <input type="number" class="form-control" name="holiday_id" id="holiday_id" placeholder="Search for ..." style="width:100%; height: 34px;" value="<?php echo (($holiday_id != null) ? $holiday_id: '');?>">
                                    </div><!-- /input-group -->
                                
                                </div><!-- /input-group -->
                            </div>
                            

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="forename_filter" >

                                        <h4 class="panel-title">
                                            Search by stat date
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="forename_filter">
                                        <div class="input-group" style="width: 100%;"
                                            <div class='input-group date' id='startDateGui' style="width:100%">
                                                <input type='text' class="form-control date" id="startDate" value="<?php echo ($start != null)? $start: '';?>" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        </div>
                                    </div>
                                
                                </div><!-- /input-group -->
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="end_filter" >

                                        <h4 class="panel-title">
                                            Search by end date
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="end_filter">
                                        <div class="input-group" style="width: 100%;"
                                            <div class='input-group' id='endDateGui' style="width:100%">
                                                <input type='text' class="form-control date" id="endDate" value="<?php echo ($end != null)? $end: '';?>" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    
                                    </div><!-- /input-group -->
                                </div>


                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-12 col-md-offset-9 col-sm-offset-10 float-right" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_holidays">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_holidays" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break; ?>
    <?php case 'members':
        $advance_search = (isset($_GET['form']) && $_GET['form'] == 'advance-search')? true: false;
        if($advance_search) {
            $membership_id =  (isset($_GET['membership_id'])) ? explode('|', $_GET['membership_id']): array(); 
            $forename =  (isset($_GET['forename'])) ? explode('|', $_GET['forename']): array(); 
            $surname =  (isset($_GET['surname'])) ? explode('|', $_GET['surname']): array(); 
            $start_age =  (isset($_GET['start_age'])) ? $_GET['start_age']: ''; 
            $end_age = (isset($_GET['end_age'])) ? $_GET['end_age']: $start_age; 
            $gender = (isset($_GET['gender'])) ? explode('|', $_GET['gender']): array(); 
            $dob = (isset($_GET['dob'])) ? explode('|', $_GET['dob']): array(); 
            $email = (isset($_GET['email'])) ? explode('|', $_GET['email']): array(); 
            $number = (isset($_GET['number'])) ? explode('|', $_GET['number']): array(); 
            $address_line_one = (isset($_GET['address_line_one'])) ? explode('|', $_GET['address_line_one']): array();
            $address_line_two = (isset($_GET['address_line_two'])) ? explode('|', $_GET['address_line_two']): array();
            $address_city = (isset($_GET['address_city'])) ? explode('|', $_GET['address_city']): array();
            $address_county = (isset($_GET['address_county'])) ? explode('|', $_GET['address_county']): array();
            $address_postcode = (isset($_GET['address_postcode'])) ? explode('|', $_GET['address_postcode']): array();
            $registered_start_date = (isset($_GET['registered_start_date'])) ? $_GET['registered_start_date']: '';
            $registered_end_date = (isset($_GET['registered_end_date'])) ? $_GET['registered_end_date']: $registered_start_date;
            $report_start_date = (isset($_GET['report_start'])) ? $_GET['report_start']: '';
            $report_end_date = (isset($_GET['report_end'])) ? $_GET['report_end']: $report_start_date;
            $age_groups = (isset($_GET['age_groups'])) ? explode('|', $_GET['age_groups']): array();
            $discipline = (isset($_GET['disciplines'])) ? explode('|', $_GET['disciplines']): array(); 
            $expertise = (isset($_GET['expertise'])) ? explode('|', $_GET['expertise']): array();

            $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
            $page = (isset($_GET['p']))? $_GET['p']: 1;
  
            $edit_url = host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fadvance'.
                    '&membership_id='.(!empty($membership_id)? implode('|', $membership_id) :'').
                    '&forename='.(!empty($forename)? implode('|', $forename) :'').
                    '&surname='.(!empty($surname)? implode('|', $surname) :'').
                    '&start_age='.(($start_age != '')? $start_age :'').
                    '&end_age='.(($end_age != '')? $end_age :'').
                    '&gender='.(!empty($gender)? implode('|', $gender) :'').
                    '&dob='.(!empty($dob)? implode('|', $dob) :'').
                    '&email='.(!empty($email)? implode('|', $email) :'').
                    '&number='.(!empty($number)? implode('|', $number) :'').
                    '&address_line_one='.(!empty($address_line_one)? implode('|', $address_line_one) :'').
                    '&address_line_two='.(!empty($address_line_two)? implode('|', $address_line_two) :'').
                    '&address_city='.(!empty($address_city)? implode('|', $address_city) :'').
                    '&address_county='.(!empty($address_county)? implode('|', $address_county) :'').
                    '&address_postcode='.(!empty($address_postcode != '')? implode('|', $address_postcode) :'').
                    '&registered_start_date='.(($registered_start_date != '')? $registered_start_date :'').
                    '&registered_end_date='.(($registered_end_date != '')? $registered_end_date :'').
                    '&report_start='.(($report_start_date != '')? $report_start_date :'').
                    '&report_end='.(($report_end_date != '')? $report_end_date :'').
                    '&age_groups='.(!empty($age_groups)? implode('|', $age_groups) :'').
                    '&disciplines='.(!empty($discipline)? implode('|', $discipline) :'').
                    '&expertise='.(!empty($expertise)? implode('|', $expertise) :'');
            $search_url = 
                    (!empty($membership_id)? '&membership_id='. implode('|', $membership_id) :'').
                    (!empty($forename)? '&forename=' . implode('|', $forename) :'').
                    (!empty($surname)? '&surname=' . implode('|', $surname) :'').
                    (($start_age != '')? '&start_age='. $start_age :'').
                    (($end_age != '')? '&end_age=' . $end_age :'').
                    (!empty($gender)? '&gender=' . implode('|', $gender) :'').
                    (!empty($dob)? '&dob=' . implode('|', $dob) :'').
                    (!empty($email)? '&email=' . implode('|', $email) :'').
                    (!empty($number)? '&number=' . implode('|', $number) :'').
                    (!empty($address_line_one)? '&address_line_one=' . implode('|', $address_line_one) :'').
                    (!empty($address_line_two)? '&address_line_two=' . implode('|', $address_line_two) :'').
                    (!empty($address_city)? '&address_city='. implode('|', $address_city) :'').
                    (!empty($address_county)? '&address_county='. implode('|', $address_county) :'').
                    (!empty($address_postcode)? '&address_postcode='. implode('|', $address_postcode) :'').
                    (($registered_start_date != '')? '&registered_start_date='. $registered_start_date :'').
                    (($registered_end_date != '')? '&registered_end_date='. $registered_end_date :'').
                    (($report_start_date != '')? '&report_start='. $report_start_date :'').
                    (($report_end_date != '')? '&report_end='. $report_end_date :'').
                    (!empty($age_groups)? '&age_groups='. implode('|', $age_groups) :'').
                    (!empty($discipline)? '&disciplines='. implode('|', $discipline) :'').
                    (!empty($expertise)? '&expertise='. implode('|', $expertise) :'');
                if($search_url == '')
                    wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&error=select+options'); 
            $pagination->advanceSearch($membership_id, $forename, $surname,$start_age, $end_age,$gender, $dob, $number, $email, $address_line_one, $address_line_two, $address_city, $address_county, $address_postcode, $registered_start_date, $registered_end_date, $report_start_date, $report_end_date, $age_groups, $discipline, $expertise, intval($page), intval($limit), intval($page), $search_url);
            $asl->getAll(5);
            ?>
            <div class="panel-heading open" data-child="main">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    
                    <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#search-panel" href="#main">
                        <i class="glyphicon glyphicon-chevron-down"></i>
                    </span>
                </div>

            </div>
           <div class="panel panel-default" id="search-panel">
                <div class="panel-heading open" data-child="main">

                    <h4 class="panel-title">Advance Search</h4>
                   <small>To edit filters please use navigation below.</small>
  
                </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 col-ms-12">
                                <p class="lead">Simple filters</p>
                                <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall" class="btn btn-default btn-block">Simple filters</a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 col-ms-12">
                                <p class="lead">Filters</p>
                                <a href="<?php echo $edit_url;?>" class="btn btn-primary btn-block">Adjust filters</a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 col-ms-12">
                                <p class="lead">Reports</p>
     
                                    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=statistics&amp;asi=<?php echo $pagination->asi;?>" class="col-xs-6 btn btn-default btn-block">Statistics</a>
                                    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=time-duration&amp;asi=<?php echo $pagination->asi;?>" class="col-xs-6 btn btn-default btn-block <?php echo !isset($_GET['report_start']) && !isset($_GET['report_end'])? 'disabled">You must select start and end date for the duration report': '">Entries';?></a>
                                
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col-ms-12">
                                <p class="lead">Previous advance reports</p>
                                <?php if(count($asl->logs) != 0):?>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead><tr><th>#</th><th>Created by</th><th>Created at</th><th>View</th></tr></thead>
                                            <tbody>
                                                <?php foreach($asl->logs as $log):?>
                                                    <tr><td><?php echo $log['advance_search_id'];?></td><td><?php echo $log['created_user'];?></td><td><?php echo $log['created_time'];?></td><td><a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fadvance<?php echo $log['uri'];?>" class="btn btn-sm btn-default">Use</a></td></tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php else:?>
                                    <small>No advance reports created</small>
                                <?php endif;?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>

       <?php } else {

            $membership_id = (isset($_GET['membership_id'])) ? $_GET['membership_id']: null; 
            $searchField = (isset($_GET['searchField1'])) ? $_GET['searchField1']: null; 
            $searchQuery = (isset($_GET['searchQuery1'])) ? $_GET['searchQuery1']: null; 
            $searchField2 = (isset($_GET['searchField2'])) ? $_GET['searchField2']: null; 
            $searchQuery2 = (isset($_GET['searchQuery2'])) ? $_GET['searchQuery2']: null; 

            $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
            $page = (isset($_GET['p']))? $_GET['p']: 1;

            $filter_text = (($membership_id != null)? ', Member ID = '.$membership_id :'') . (($searchField != null) ? ', '.$searchField.' = '.$searchQuery : '')  .(($searchField2 != null) ? ', '.$searchField2.' = '.$searchQuery2 : '')  ;

        
            $pagination->members($membership_id, $searchField, $searchQuery, $searchField2, $searchQuery2, intval($page), intval($limit), intval($page),true );?>
    
            <div class="panel panel-default" id="search-panel">
                <div class="panel-heading">
                    <div class="panel-heading">
                        <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                        <?php echo (strlen($filter_text) > 0)? '<small>('. substr($filter_text, 2).')</small>' : '';?>
                    </div>

                </div>
                <div class="panel-default ">
                    <div class="panel-body">
                        <form class="form-inline" name="input" method="post" action="">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                                    <div class="panel panel-collapse panel-default">
                                        <div class="panel-heading" data-child="forename_filter" >

                                            <h4 class="panel-title">
                                                Search by membership number
                                                <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                    <i class="glyphicon glyphicon-chevron-down"></i>
                                                </span>
                                            </h4>
                                        </div>
                                        <div class="panel-body" id="forename_filter">
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group-btn" style="width: 1%;">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;"  data-element="searchField"><?php echo ($searchField != null) ? ucwords($searchField): 'Forename';?> <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Forename</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Date of birth</a></li>
                                                        <li><a href="#">Phone number</a></li>
                                                        <li role="separator" class="divider"></li>
                                                        <li><a href="#">Address line 1</a></li>
                                                        <li><a href="#">Postcode</a></li>
                                                    </ul>
                                                    <input type="hidden" name="searchField" id="searchField" value="<?php echo ($searchField != null)? $searchField :'forename'?>">
                                                </div><!-- /btn-group -->
                                                <input type="text" class="form-control" name="searchQuery" id="searchQuery" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery != null) ? $searchQuery: '';?>">
                                            </div><!-- /input-group -->
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group-btn" style="width: 1%;">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;" data-element="searchField2"><?php echo ($searchField2 != null) ? ucwords($searchField2): 'Surname';?> <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Forename</a></li>
                                                        <li><a href="#">Surname</a></li>
                                                        <li><a href="#">Date of birth</a></li>
                                                        <li><a href="#">Phone number</a></li>
                                                        <li role="separator" class="divider"></li>
                                                        <li><a href="#">Address line 1</a></li>
                                                        <li><a href="#">Postcode</a></li>
                                                    </ul>
                                                    <input type="hidden" name="searchField2" id="searchField2" value="<?php echo ($searchField2 != null)? $searchField2 :'surname'?>">
                                                </div><!-- /btn-group -->
                                                <input type="text" class="form-control" name="searchQuery2" id="searchQuery2" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery2 != null) ? $searchQuery2: '';?>">
                                            </div><!-- /input-group -->
                                        </div><!-- /input-group -->
                                    </div>
                                </div>

                                
                                
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                    <div class="panel panel-default">
            
                                        <div class="panel-heading" data-child="limit_filter" >
                                            <h4 class=" muted panel-title">
                                                Search by member No.
                                                <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                    <i class="glyphicon glyphicon-chevron-down"></i>
                                                </span>
                                            </h4>
                                        </div>
                                    
                                    
                                        <div class="panel-body" id="limit_filter">
                                            <div class="input-group" style="width: 100%; margin-top: 10px;">
                                                <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;"><i class="glyphicon glyphicon-user " style="font-size:16px;line-height: 19px; "></i></button></div>
                                                <input type="number" class="form-control" name="membership_id" id="membership_id" placeholder="Search for ..." style="width:100%; height: 34px;" value="<?php echo ($membership_id != null) ? $membership_id: '';?>">
                                                    </div><!-- /input-group -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                    <button type="submit" class="btn btn-default btn-block" id="reset_btn_member">Reset</button>
                                    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fadvance" class="btn btn-warning btn-block">Advance search</a>
                                    <button type="submit" class="btn btn-primary btn-block" id="filter_btn_member" style="margin-left: 0px;">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        <?php }?>
    <?php break;?>
    <?php case 'sessions':

    	$days = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
        $holiday_id = (isset($_GET['hid'])) ? $_GET['hid']: null; 
        $day = (isset($_GET['day'])) ? $_GET['day']: null; 

        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text =  (($day != null) ? "Day = ".$days[$day]: '') ;


        $pagination->sessions($holiday_id, $day, intval($page), intval($limit), intval($page) );?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. $filter_text.')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="limit_filter" >
                                        <h4 class=" muted panel-title">
                                            Search by Holiday
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="limit_filter">
                                        <select class="form-control" id="hid">
                                            <option value="null" <?php echo ($holiday_id === null)? 'selected':'';?>>Normal sessions</option>
                                            <?php foreach($holiday->holidays as $holiday){
                                                echo '<option value="'.$holiday['holiday_id'].'" '.(($holiday_id == $holiday['holiday_id'])? 'selected':'').'>'.$holiday['display_name'].'</option>';
                                            }?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="limit_filter" >
                                        <h4 class=" muted panel-title">
                                            Search by day
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="limit_filter">
                                        <select class="form-control" id="day">
                                            <option value="null" <?php echo ($day === null)? 'selected':'';?>>Select day</option>
                                            <option value="1" <?php echo ($day == '1')? 'selected':'';?>>Monday</option>
                                            <option value="2" <?php echo ($day == '2')? 'selected':'';?>>Tuesday</option>
                                            <option value="3" <?php echo ($day == '3')? 'selected':'';?>>Wednesday</option>
                                            <option value="4" <?php echo ($day == '4')? 'selected':'';?>>Thursday</option>
                                            <option value="5" <?php echo ($day == '5')? 'selected':'';?>>Friday</option>
                                            <option value="6" <?php echo ($day == '6')? 'selected':'';?>>Saturday</option>
                                            <option value="0" <?php echo ($day === '0')? 'selected':'';?>>Sunday</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_session">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_session" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break;?>
    <?php case 'statistics':?>

        <div class="panel panel-default" id="report-dates-panel">
            <div class="panel-heading">
                    <div class="panel-heading">
                        <h4 class="panel-title" style=" display: inline-block">Time duration report</h4>
                            
                    </div>
            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="search_filter" >
                                        <h4 class="panel-title">
                                                Start date
                                                <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                                </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="search_filter">
                                        <div class="input-group" style="width: 100%;">
                                            <div class='input-group datetime' id='startDateGui' style="width:100%">
                                                <input type='text' class="form-control" id="startDate" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="search_filter" >
                                        <h4 class="panel-title">
                                                End Date
                                                <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                                </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="search_filter">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group datetime" id="endDateGui" style="width:100%">
                                                    <input type="text" class="form-control" id="endDate" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>    
                                            </div>
                                        </div>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="all_filter" >
                                        <h4 class="panel-title">
                                                All
                                                <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                                </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="all_filter">
                                        <label class="radio-inline"><input type="radio" id="all" name="all" value="true"> All</label>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6   col-ms-12 " >

                                <button type="submit" class="btn btn-primary btn-block" id="report_create_stats" style="margin-left: 0px;">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <?php break;?>
    <?php case 'premises':

        $date = (isset($_GET['date'])) ? $_GET['date']: null; 
        $type = (isset($_GET['type'])) ? $_GET['type']: null; 

        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text =  (($date != null) ? "Date = ".$date.'.': ''). (($type != null) ? "Type = ".$type.'.': '') ; ;


        $pagination->premises($date, $type, intval($page), intval($limit), intval($page) );?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. $filter_text.')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="holiday-dates" >
                                        <h4 class=" muted panel-title">
                                            Search by date
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="holiday-dates">
                                        <div class='input-group date' style="width:100%">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Date</button></div>
                                            <input type='text' class="form-control" name="start_date" id='start_date' value="<?php echo ($date != null) ? $date: '';?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_premises">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_premises" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break;?>
    <?php case 'memberships':
        $membership_id = (isset($_GET['membership_id'])) ? $_GET['membership_id']: null; 
        $searchField = (isset($_GET['searchField1'])) ? $_GET['searchField1']: null; 
        $searchQuery = (isset($_GET['searchQuery1'])) ? $_GET['searchQuery1']: null; 
        $searchField2 = (isset($_GET['searchField2'])) ? $_GET['searchField2']: null; 
        $searchQuery2 = (isset($_GET['searchQuery2'])) ? $_GET['searchQuery2']: null; 
        $start_dt = (isset($_GET['start_dt']))? $_GET['start_dt']: null;
        $end_dt = (isset($_GET['end_dt']))? $_GET['end_dt']: null;
        $type = (isset($_GET['type']))? $_GET['type']: null;

        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;

        $filter_text = (($membership_id != null)? ', Member ID = '.$membership_id :'') . (($searchField != null) ? ', '.$searchField.' = '.$searchQuery : '')  .(($searchField2 != null) ? ', '.$searchField2.' = '.$searchQuery2 : '')  ;


        $pagination->paidMemberships($membership_id, $searchField, $searchQuery, $searchField2, $searchQuery2, $start_dt, $end_dt, $type, intval($page), intval($limit), intval($page),true );?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                    <?php echo (strlen($filter_text) > 0)? '<small>('. substr($filter_text, 2).')</small>' : '';?>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-ms-12">
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="forename_filter" >

                                        <h4 class="panel-title">
                                            Search by membership information
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="forename_filter">
                                    <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Member no.</button></div>
                                            <input type="number" class="form-control" name="membership_id" id="membership_id" placeholder="Search for ..." style="width:100%; height: 34px;" value="<?php echo ($membership_id != null) ? $membership_id: '';?>">
                                        </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;"  data-element="searchField"><?php echo ($searchField != null) ? ucwords($searchField): 'Forename';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Forename</a></li>
                                                    <li><a href="#">Surname</a></li>
                                                    <li><a href="#">Date of birth</a></li>
                                                    <li><a href="#">Phone number</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Address line 1</a></li>
                                                    <li><a href="#">Postcode</a></li>
                                                </ul>
                                                <input type="hidden" name="searchField" id="searchField" value="<?php echo ($searchField != null)? $searchField :'forename'?>">
                                            </div><!-- /btn-group -->
                                            <input type="text" class="form-control" name="searchQuery" id="searchQuery" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery != null) ? $searchQuery: '';?>">
                                        </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 1%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;" data-element="searchField2"><?php echo ($searchField2 != null) ? ucwords($searchField2): 'Surname';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Forename</a></li>
                                                    <li><a href="#">Surname</a></li>
                                                    <li><a href="#">Date of birth</a></li>
                                                    <li><a href="#">Phone number</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Address line 1</a></li>
                                                    <li><a href="#">Postcode</a></li>
                                                </ul>
                                                <input type="hidden" name="searchField2" id="searchField2" value="<?php echo ($searchField2 != null)? $searchField2 :'surname'?>">
                                            </div><!-- /btn-group -->
                                            <input type="text" class="form-control" name="searchQuery2" id="searchQuery2" placeholder="Search for ..." style="width:100%" value="<?php echo ($searchQuery2 != null) ? $searchQuery2: '';?>">
                                        </div><!-- /input-group -->
                                       
                                    </div><!-- /input-group -->
                                    
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-ms-12" >
                                <div class="panel panel-collapse panel-default">
                                    <div class="panel-heading" data-child="holiday-dates" >

                                        <h4 class="panel-title">
                                            Select by times
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                    <div class="panel-body" id="holiday-dates">
                                        <div class='input-group date' style="width:100%">
                                                <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Start</button></div>
                                            <input type='text' class="form-control" name="start_date" id='start_date' value="<?php echo ($start_dt != null) ? $start_dt: '';?>"/>
                                        </div>
                                        <div class='input-group date' style="width:100%">
                                                <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">End   </button></div>
                                            <input type='text' class="form-control" name="end_date" id='end_date' value="<?php echo ($end_dt != null) ? ucwords($end_dt): '';?>"/>
            
                                        </div>

                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            
                            
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">

                                    <div class="panel-heading" data-child="limit_filter" >
                                        <h4 class=" muted panel-title">
                                            Search by membership type
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="limit_filter">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-btn" style="width: 100%;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px; width: 100%;" data-element="type"><?php echo ($type != null) ? ucwords($type): 'Select membership type';?> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                <li><a href="#">Select membership type</a></li>
                                                    <?php foreach($pm->types as $t):?>
                                                        <li><a href="#"><?php echo $t['display_name'];?></a></li>
                                                    <?php endforeach;?>
                                                </ul>
                                                <input type="hidden" name="type" id="type" value="<?php echo ($type != null)? $type :''?>">
                                            </div><!-- /btn-group -->
                                        </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_member">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_memberships" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
    <?php break;?>
    <?php case 'tandcs':
  
        $pagination->tandcs();?>
    <?php break;?>
    <?php case 'events':
        $eid = (isset($_GET['eid'])) ? $_GET['eid']: null; 
        $cid = (isset($_GET['cid'])) ? $_GET['cid']: null; 
        $name = (isset($_GET['name'])) ? $_GET['name']: null; 
        $start_dt = (isset($_GET['start_dt'])) ? $_GET['start_dt']: null; 
        $end_dt = (isset($_GET['end_dt'])) ? $_GET['end_dt']: null; 
        
        $limit = (isset($_GET['limit']))? $_GET['limit']: 16;
        $page = (isset($_GET['p']))? $_GET['p']: 1;



        $pagination->events($eid, $cid, $name, $start_dt, $end_dt, intval($page), intval($limit), intval($page) );?>

        <div class="panel panel-default" id="search-panel">
            <div class="panel-heading">
                <div class="panel-heading">
                    <h4 class="panel-title" style="width: 60px; display: inline-block">Filters</h4>
                </div>

            </div>
            <div class="panel-default ">
                <div class="panel-body">
                    <form class="form-inline" name="input" method="post" action="">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="event_info" >
                                        <h4 class=" muted panel-title">
                                            Search by event info
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="event_info">
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">ID</button></div>
                                            <input type="number" class="form-control" name="eid" id="eid" placeholder="Search by event number..." style="width:100%; height: 34px;" value="<?php echo ($eid != null) ? $eid: '';?>">
                                        </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Name</button></div>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Search by event name..." style="width:100%; height: 34px;" value="<?php echo ($name != null) ? $name: '';?>">
                                        </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-ms-8">
                                <div class="panel panel-default">
        
                                    <div class="panel-heading" data-child="dates" >
                                        <h4 class=" muted panel-title">
                                            Search by start and end date
                                            <span class="pull-right panel-collapse-clickable" data-toggle="collapse" data-parent="#accordion" href="#filterPanel">
                                                <i class="glyphicon glyphicon-chevron-down"></i>
                                            </span>
                                        </h4>
                                    </div>
                                
                                
                                    <div class="panel-body" id="dates">
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">Start</button></div>
                                            <input type="text" class="form-control datetime" name="start_dt" id="start_dt" style="width:100%; height: 34px;" value="<?php echo ($start_dt != null) ? $start_dt: '';?>">
                                        </div><!-- /input-group -->
                                        <div class="input-group" style="width: 100%; margin-top: 10px;">
                                            <div class="input-group-btn" style="width: 1%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">End</button></div>
                                            <input type="test" class="form-control datetime" name="end_dt" id="end_dt" style="width:100%; height: 34px;" value="<?php echo ($end_dt != null) ? $end_dt: '';?>">
                                        </div><!-- /input-group -->
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-2 col-ms-4" >
                                <button type="submit" class="btn btn-default btn-block" id="reset_btn_events">Reset</button>
                                <button type="submit" class="btn btn-primary btn-block" id="filter_btn_events" style="margin-left: 0px;">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
        
    
    
    <?php break;
}