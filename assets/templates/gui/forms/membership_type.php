<div class="content">

	<div class="row">
		<h1>Add</h1>
		<form class="form-horizontal" id="editForm" action="#" method="post">
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<input type="submit" value="Save" class="btn btn-warning mg-10 btn-ms-block">
				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="holiday-dates">
			<?php if(isset($updated_successfull) && !is_array($updated_successfull)):?>
				<div class="alert alert-success"><strong>Success!</strong> Note details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Note details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['error'])):?>
				<?php if($_GET['error'] == 'incorrect+member'):?>
					<div class="alert alert-danger"><strong>Error!</strong> Member did not exists.</div>
				<?php elseif($_GET['error'] == 'couldnt+save'):?>
					<div class="alert alert-danger"><strong>Error!</strong> Note could not save.</div>
				<?php elseif($_GET['error'] == 'incorrect+format'):?>
						<div class="alert alert-danger"><strong>Error!</strong> Note information is in incorrect format. Please contact administrator .	</div>
				<?php endif?>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">
			
			<div class="form-group <?php echo isset($_POST['display_name']) ? isset($validation->errors['display_name']) || !isset($_POST['display_name'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="type" class="col-xs-3 col-ms-12 control-label form-label required ">Display name:</label>
				<div class="col-xs-6 col-ms-12">
				<input type="text" class="form-control" id="display_name"  name="display_name" value="<?php echo isset($_POST['display_name']) ? $_POST['display_name']: '';?>">
				</div>
			</div>
			<div class="form-group <?php echo isset($_POST['duration']) ? isset($validation->errors['duration'])? 'has-feedback has-error': 'has-feedback has-success': '';?>">
				
				<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
				<div class="col-xs-6 col-ms-12">
					<div class='input-group' style="100px">
							<input type='text' class="form-control" name="duration" id='duration'  style="width: 100px" value="<?php echo (isset($_POST['duration']))? $_POST['duration'] : '';?>"/>
							<div class="input-group-btn" style="width: 0%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">weeks</button></div>
					</div>
	
				</div>
			</div>
			
		</form>
</div>
