<div class="content">

	<div class="row">
		<h1><?php echo ucwords(strtolower($pm->details[0]['forename'])) .' '. ucwords(strtolower($pm->details[0]['surname']));?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=pass&amp;pass=<?php echo $pm->details[0]['paid_membership_id'];?>" class="btn btn-default mg-10 btn-ms-block disabled">Generate report</a>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&amp;member=<?php echo $pm->details[0]['member_id'];?>" class="btn mg-10 btn-ms-block btn-default">View member</a>
				<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>
				</div>
				<?php if(current_user_can('administrator') && $pm->details[0]['premature'] == null):?>
				<div class="col-md-3 col-xs-4 col-ms-12 float-right">
					<a class="btn btn-danger mg-10 btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fdelete&amp;pmid=<?php echo $pm->details[0]['paid_membership_id'];?>">Permanently remove pass</a>
				</div>
				<?php endif;?>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8">
			<?php if(isset($updated_successfull) && $updated_successfull === true):?>
				<div class="alert alert-success"><strong>Success!</strong> Membership details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Membership details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['delete']) && $_GET['delete'] == 'success'):?>
				<div class="alert alert-success"><strong>Success!</strong> Membership is disabled.</div>
			<?php elseif(isset($_GET['delete'])):?>
				<div class="alert alert-danger"><strong>Error!</strong> Membership could not be disabled.</div>
			<?php endif?>
			<?php if($pm->details[0]['premature'] != null):?>
					<div class="alert alert-warning"><strong>Cancelled</strong> This membership was cancelled on <?php echo $pm->details[0]['premature'];?>.</div>
			<?php elseif(date('Y-m-d', strtotime($pm->details[0]['end_date'])) < date('Y-m-d')):?>
				<div class="alert alert-warning"><strong>Expired</strong> This membership expired on <?php echo $pm->details[0]['end_date'];?>.</div>
			<?php endif;?>
			
			<form class="form-horizontal" id="editForm" action="#" method="post">
				<h2>Personal Information</h2>
				<div class="form-group" id="holiday-dates">
					
					<label for="forename" class="col-xs-3 col-ms-12 control-label form-label required">Type:</label>
					<div class="col-xs-6 col-ms-12">

						<select disabled name="type">
							<?php foreach($pm->types as $t):?>
									<option value="<?php echo $t['membership_type_id'];?>" <?php echo ($t['membership_type_id'] == $pm->details[0]['membership_type'])? 'selected':'';?>><?php echo $t['display_name'];?></option>
							<?php endforeach;?>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="start_date" name="start_date" class="form-control" readonly value="<?php echo ucwords(strtolower($pm->details[0]['start_date']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="end_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="end_date" name="end_date" class="form-control" readonly value="<?php echo ucwords(strtolower($pm->details[0]['end_date']));?>">
						<input type="hidden" name="updated_amount" class="form-control" disabled value="<?php echo $pm->details[0]['updated_amount'];?>">
					</div>
				</div>

			</form>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Entries (<?php echo count($pm->entries);?>)</p>
				</div>
				<div class="content">
					<?php if(count($pm->entries) > 0):?>
						<div class="table-responsive">	
							<table class="table table-condensed">
								<thead>
									<tr><th>#</th><th>Date</th><th>Entry time</th><th>Session end</th></tr>
								<tbody>
									<?php for($i = 0; $i < count($pm->entries); $i++):?>
										<?php if($i > 10):?>
											<tr class="hidden"><td><?php echo $pm->entries[$i]['entry_id'];?></td><td><?php echo $pm->entries[$i]['entry_date'];?></td><td><?php echo $pm->entries[$i]['entry_time'];?></td><td><?php echo $pm->entries[$i]['exit_time'];?></td></tr>
										<?php else:?>
											<tr><td><?php echo $pm->entries[$i]['entry_id'];?></td><td><?php echo $pm->entries[$i]['entry_date'];?></td><td><?php echo $pm->entries[$i]['entry_time'];?></td><td><?php echo $pm->entries[$i]['exit_time'];?></td></tr>
										<?php endif;?>
									<?php endfor;?>
								</tbody>
							</table>
						</div>
						<?php if(count($pm->entries) > 10):?>
							<a id="removeHidden" class="btn btn-default btn-sm pull-right">View more (<?php echo count($pm->entries) - 10;?>)</a>
						<?php endif;?>
					<?php else:?>
					<small>No entries</small>
					<?php endif;?>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Account information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
								<tr><th>Purchased on</th><td><?php echo $pm->details[0]['date'];?></td></tr>	
								<tr><th>Staff @ purchase</th><td><?php echo $pm->details[0]['staff_init'];?></td></tr>
								<tr><th>Expired at</th>
								<?php
								if(date('Y-m-d', strtotime($pm->details[0]['end_date'])) < date('Y-m-d'))
									echo '<td>'.$pm->details[0]['end_date'].'</td></tr>';	
								elseif($pm->details[0]['premature'] != null)
									echo '<td>'.$pm->details[0]['premature'].'</td></tr>';	
								else
									echo '<td>Active</td></tr>';	
								?>
								
								<tr><th>Account last updated at</th><td><?php echo (($pm->details[0]['last_updated_time'] != '')? $pm->details[0]['last_updated_time']: 'Not updated');?></td></tr>
								<tr><th>Account last updated by</th><td><?php echo (($pm->details[0]['last_updated_user'] != '')? ucwords(strtolower($pm->details[0]['last_updated_user'])): 'Not updated');?></td></tr>
								<tr><th>No. of updates</th><td><?php echo ($pm->details[0]['updated_amount'] == '') ? 'Not updated': $pm->details[0]['updated_amount'];?></td></tr>

							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	
</div>