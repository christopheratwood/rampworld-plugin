<div class="content">

	<div class="row">
		<h1><?php echo $event->details[0]['display_name'];?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
					<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p> 
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall" class="btn btn-default mg-10">Back</a>
					<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>
				</div>
				<?php if(current_user_can('administrator')):?>
					<div class="col-md-3 col-xs-4 col-ms-12 float-right">
						<a class="btn btn-danger mg-10 btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fdelete&amp;eid=<?php echo $eid;?>">Permanently delete event</a>
					</div>
				<?php endif;?>
			</div>
		</div>
		<div class="col-md-8 col-sm-8" id="session-times">
			<form class="form-horizontal" id="editForm" action="#" method="post">
				<?php if(isset($updated_successfull) && $updated_successfull !== true):?>
					<div class="alert alert-danger"><strong>Error!</strong> Event details did not pass validation.<br>
						<?php foreach($validation::$errors as $error){
							echo '<p>'. $error.'</p>';
						}?>
					</div>
				<?php endif?>
				<?php if(isset($updated_successfull) && $updated_successfull === true):?>
					<div class="alert alert-success"><strong>Success!</strong> Event details did has been updated.<br>
					</div>
				<?php endif;?>
				<div class="form-group ">
					<div class="col-xs-4 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Event URL: </label>
						<p class="text-right"><em>Please use this URL to share the event.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<a href="<?php echo host;?>events/<?php echo $eid;?>/<?php echo str_replace(' ', '-', $event->details[0]['display_name']);?>" class="btn btn-link" target="_blank"><?php echo host;?>events/<?php echo $eid;?>/<?php echo str_replace(' ', '-', $event->details[0]['display_name']);?></a>
					</div>
				</div>		
				<div class="form-group ">
					<div class="col-xs-4 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Display name: </label>
						<p class="text-right"><em>Please ensure correct grammar is used. This is displayed to the public.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<input type="text" id="name" name="display_name" class=" form-control " readonly value="<?php echo isset($event->details[0]['display_name'])? $event->details[0]['display_name']:'';?>">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="date" class="col-xs-12 control-label form-label required">Event date: </label>
						<p class="text-right"><em>The end time of the event</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class=" form-control date" id="date" name="date" readonly  value="<?php echo isset($event->details[0]['date'])? $event->details[0]['date']:'';?>" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="start_time" class="col-xs-12 control-label form-label required">Start time: </label>
						<p class="text-right"><em>The start time of the event.</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class=" form-control time" id="start_time" name="start_time"  readonly value="<?php echo isset($event->details[0]['start_time'])? $event->details[0]['start_time']:'';?>" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="end_time" class="col-xs-12 control-label form-label required">End time: </label>
						<p class="text-right"><em>The end time of the event</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class=" form-control time" id="end_time" name="end_time"  readonly value="<?php echo isset($event->details[0]['end_time'])? $event->details[0]['end_time']:'';?>" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Event Price:</label>
						<p class="text-right"><em>Please enter the event price.  Please do not use currency symbol.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<div class="input-group">
							<div class="input-group-addon">£</div>
							<input type="number" class=" form-control" id="price" name="price" placeholder="00.00" readonly value="<?php echo isset($event->details[0]['cost'])? $event->details[0]['cost']:'';?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Event description:</label>
						<p class="text-right"><em>Please enter the event description.  Please contain all relevant information that customers may way to know.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<textarea class=" form-control" readonly name="description" id="description"><?php echo isset($event->details[0]['description'])? $event->details[0]['description']:'';?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="beginner" class="col-xs-12 control-label form-label required">Beginner: </label>
						<p class="text-right"><em>Is this session only for beginners?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="beginner" name="beginner"  readonly class=" form-control" value="1" <?php echo (isset($event->details[0]['beginner']) && $event->details[0]['beginner'] == '1')? 'checked':'';?>> Yes
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="beginner" name="beginner"  readonly class=" form-control" value="0" <?php echo (isset($event->details[0]['beginner']))? 'checked':'';?>> No
						</label>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="online" class="col-xs-12 control-label form-label required">Online exclusive: </label>
						<p class="text-right"><em>Is this event online exclusive?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="online" name="online"  readonly class=" form-control" value="1" <?php echo (isset($event->details[0]['online_exclusive']) && $event->details[0]['online_exclusive'] == '1')? 'checked':'';?>> Yes
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="online" name="online"  readonly class=" form-control" value="0" <?php echo (isset($event->details[0]['online']))? 'checked':'';?>> No
						</label>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="pass" class="col-xs-12 control-label form-label required">Pass exclusion: </label>
						<p class="text-right"><em>Does pass holders attend this event for free?</em></p>
					</div>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="pass" name="pass" class="form-control" value="1" checked> No
						</label>						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label for="name" class="col-xs-12 control-label form-label required">Event  discipline(s):</label>
						<p class="text-right"><em>Please select all the disciplines that is allowed to attent this event.</em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<div class="col-xs-6 col-ms-6">
							<label class="checkbox">
								<input type="checkbox" id="discipline"  readonly class=" form-control" name="discipline[]" value="BMX" <?php echo ((substr($event->details[0]['discipline_exclusive'], 1,1) == true)? 'checked':'');?>> BMX
							</label>
							<label class="checkbox">
							<input type="checkbox" id="discipline"  readonly class=" form-control" name="discipline[]" value="SMX" <?php echo (substr($event->details[0]['discipline_exclusive'], 0,1)? 'checked':'');?>> SMX
							</label>
							<label class="checkbox">
								<input type="checkbox" id="discipline"  readonly class=" form-control" name="discipline[]" value="MTB" <?php echo (substr($event->details[0]['discipline_exclusive'], 2,1)? 'checked':'');?>> MTB
							</label>
						</div>
						<div class="col-xs-6 col-ms-6">

							<label class="checkbox">
								<input type="checkbox" id="disicpline"  readonly class=" form-control" name="discipline[]" value="skateboard" <?php echo ((substr($event->details[0]['discipline_exclusive'], 3,1))? 'checked':'');?>> Skateboard
							</label>
							<label class="checkbox">
									<input type="checkbox" id="discipline"  readonly class=" form-control" name="discipline[]" value="inline" <?php echo ((substr($event->details[0]['discipline_exclusive'], 4,1))? 'checked':'');?>> Inline
							</label>
							<label class="checkbox">
								<input type="checkbox" id="discipline"  readonly class=" form-control" name="discipline[]" value="spectator" <?php echo ((substr($event->details[0]['discipline_exclusive'], 5,1))? 'checked':'');?>> Spectator
							</label>
						</div>
					</div>
				</div>
			
				<div class="form-group">
					<div class="col-xs-4 col-ms-12">
						<label class="col-xs-12 control-label form-label required">Event images:</label>
						<p class="text-right"><em></em></p>
					</div>

					<div class="col-xs-6 col-ms-12">
						<div class="form-group">
							<label for="image_portrait" class="form-label required">Image to display on event overview page:</label>
							<input type="text" class=" form-control" readonly name="image_portrait" id="image_portrait" placeholder="Please enter full URL." value="<?php echo isset($event->details[0]['image_portrait'])? $event->details[0]['image_portrait']:'';?>">
						</div>
						<div class="form-group ">
							<label for="image_landscape" class="form-label required">Image to display on event booking:</label>
							<input type="text" class=" form-control" readonly name="image_landscape" id="image_landscape" placeholder="Please enter full URL." value="<?php echo isset($event->details[0]['image_landscape'])? $event->details[0]['image_landscape']:'';?>">
							<input type="hidden" name="update_counter" value="<?php echo $event->details[0]['updated_amount'];?>">
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Event information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
									<tr><th>Creation date</th><td><?php echo $event->details[0]['created'];?></td></tr>
									<tr><th>Number of bookings</th><td><?php echo ($event->statistics['booked'] == 0)? 'No bookings': $event->statistics['booked'];?></td></tr>
									<tr><th>Number of attendees</th><td><?php echo ($event->statistics['attended'] == 0)? 'No attendees': $event->statistics['attended'];?></td></tr>
									<tr><th>Event last updated at</th><td><?php echo ($event->details[0]['last_updated_time'] == null)? 'Not updated': $event->details[0]['last_updated_time'];?></td></tr>
									<tr><th>Event last updated by</th><td><?php echo ($event->details[0]['last_updated_user'] == null)? 'Not updated': $event->details[0]['last_updated_user'];?></td></tr>
									<tr><th>No. of updates</th><td><?php echo ($event->details[0]['updated_amount'] == null)? 'Not updated': $event->details[0]['updated_amount'];?></td></tr>
							

							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	</div>
</div>
