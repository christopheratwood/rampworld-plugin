<?php $days = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');?>
<div class="content">

	<div class="row">
		<h1>V<?php echo $terms->terms[0]['version_number'];?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a class="btn btn-default" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=terms&amp;tandcid=<?php echo $terms->terms[0]['tandc_id'];?>">Generate report</a>
				<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>
				</div>
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="session-times">
			<?php if(isset($updated_successfull) && $updated_successfull === true):?>
				<div class="alert alert-success"><strong>Success!</strong> Terms and conditions has been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Terms and conditions did not pass validation.<br>
					<?php foreach($updated_successfull as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['success']) && $_GET['success'] == 'created'):?>
				<div class="alert alert-success"><strong>Success!</strong> New terms and conditions has been created.</div>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">
				<div class="form-group">
							
					<label for="type" class="col-xs-3 col-ms-12 control-label form-label required">Content:</label>
					<div class="col-xs-9 col-ms-12">
						<textarea name="content" id="content" class="tinymce " style="height: 300px" contenteditable="false"><?php echo $terms->terms[0]['content'];?></textarea>
					</div>
				</div>
				<div class="form-group">
							
					<label for="type" class="col-xs-3 col-ms-12 control-label form-label required">Publish date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" class="form-control datetime" readonly name="publish_date" value="<?php echo $terms->terms[0]['publish_date'];?>">
					</div>
				</div>
				<div class="form-group">
							
					<label for="type" class="col-xs-3 col-ms-12 control-label form-label required">Version number:</label>
					<div class="col-xs-6 col-ms-12">
						<div class='input-group' style="100px">
							<div class="input-group-btn" style="width: 0%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">V</button></div>
								<input type='text' class="form-control" readonly name="version_number" id='version_number'  style="width: 100px" value="<?php echo ($terms->terms[0]['version_number'] != null) ? $terms->terms[0]['version_number']: '';?>"/>
								
						</div>
					</div>
				</div>
				<div class="form-group">
				
					<label for="type" class="col-xs-3 col-ms-12 control-label form-label">Notify members:</label>
					<div class="col-xs-6 col-ms-12">
						<p>Currently not available. Please notify member via social media and at reception.  You must allow members to have a copy of the terms and conditions. Ideal, members should be given a notice period.</p>
					</div>
				</div>
					
				
			</form>
		</div>
		<div class="col-md-4 col-sm-4">
		</div>
	</div>
</div>
<script src="<?php echo host;?>wp-content/plugins/rampworld-membership/assets/dist/thirdparty/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

			tinymce.init({
					menubar:false,
				statusbar: false,
				toolbar: "undo redo | indent outdent",
				
				selector:'textarea',
				indentation: '15px',
				content_css : '<?php echo host;?>wp-content/themes/birdfield/css/rwc_membership.css',
				
				style_formats_merge: true
			});

</script>
