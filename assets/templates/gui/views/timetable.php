<div class="content">
	<div class="row">
		<h1>Print timetable</h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
					<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
					<a class="btn btn-success mg-10 btn-ms-block" id="print-timetable">Print</a>
				</div>
			</div>
		</div>
			<?php if (!$tt->timetable) :?>
                <h1>No Sessions Availabel</h1>
            <?php else :?>
            <div class="print-container" id='printTimes'>
                <div class="print-header">
                    <img src="<?php echo host;?>wp-content/uploads/2016/06/footer_logo.png">
                </div>
                <div class="print-title">
                    <?php if (!empty($tt->details)) :?>
                        <p>
                            <?php echo $tt->details[0]['display_name'];?>
                        </p>
                        <span>
                            <?php echo $tt->details[0]['start_date'];?> until <?php echo $tt->details[0]['end_date'];?>
                        </span>
                    <?php else :?>
                        <p>Cardiff School Term Time Hours</p>
                        <span> </span>
                    <?php endif;?>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Session Time</th>
                            <th>Information</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (array_reverse($tt->timetable) as $session) {
                            echo $session;
}?>
                    </tbody>
                </table>
                <div class="footer print-title">
                    <p>www.rampworldcardiff.co.uk</p>
                </div>

                <?php if(count($tt->closed_dates) != 0){
                    echo '<div class="alert alert-danger" style="margin-top: 20px"><p class="lead">RampWorld Cardiff is closed on the following dates: <strong>';
                    for($i = 0; $i< count($tt->closed_dates); $i++){
                        if($i > 0) {
                            if($i == count($tt->closed_dates) - 1 ) {
                                echo ', and '.$tt->closed_dates[$i]['d'];
                            } else {
                                echo ', '.$tt->closed_dates[$i]['d'];
                            }
                        } else {
                            echo $tt->closed_dates[$i]['d'];
                        }
                    }
                    echo '</strong></p></div>';
                }?>
            </div>
            <?php endif; ?>
            <script src="<?php echo host;?>wp-content/plugins/rampworld-membership/assets/dist/thirdparty/js/html2canvas.min.js" type="text/javascript"></script>
            <script>
                html2canvas(document.getElementById("printTimes"), {
                    onrendered: function (a) {
                        var b = new Date,
                            c = b.getMonth() + 1,
                            d = b.getDate(),
                            e = (d < 10 ? "0" : "") + d + "_" + (c < 10 ? "0" : "") + c + "/" + b.getFullYear(),
                            f = document.getElementById("print-timetable");
                        f.href = a.toDataURL("image/png").replace("image/jpeg", "image/octet-stream"), f.download = "sessionsTimes_" +
                            e + ".jpg", console.log("")
                    }
                });
            </script>
        </div>


</div>