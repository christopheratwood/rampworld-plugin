
<div class="content system-dates">
	

	<div class="row">
		<h1><?php echo ucwords(strtolower($note->members[0]['forename'])) .' '. ucwords(strtolower($note->members[0]['surname']));?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fadd&amp;member=<?php echo $_GET['member'];?>" class="btn btn-default mg-10 btn-ms-block">Add note</a>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&amp;member=<?php echo $_GET['member'];?>" class="btn mg-10 btn-ms-block btn-default">View member</a>
				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8">
		<?php if(isset($_GET['errors'])):?>

				<?php if($_GET['errors'] == 'incorrect permissions'):?>
					<div class="alert alert-danger"><strong>Failed!</strong> You do not have the correct permissions.</div>
				<?php elseif($_GET['errors'] == 'couldnt remove'):?>
					<div class="alert alert-danger"><strong>Failed!</strong> Note was not removed.</div>
				<?php endif;?>
			<?php endif;?>
			<?php if(isset($_GET['success'])):?>
				<div class="alert alert-success"><strong>Success!</strong> New note has been added.</div>
			<?php elseif(isset($_GET['removed'])):?>
				<div class="alert alert-success"><strong>Success!</strong> note has been removed.</div>
			<?php elseif(isset($updated_successfull) && $updated_successfull == true):?>
				<div class="alert alert-success"><strong>Success!</strong> notes details have been changed.</div>
			<?php elseif(isset($updated_successfull) && $updated_successfull == false):?>
				<div class="alert alert-warning"><strong>Success!</strong> note has been deleted.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> notes details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			
				<h2>System notes</h2>	
				<?php if(count($note->notes) == 0):?>
					<p >No notes for the member. <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fadd&amp;member=<?php echo $_GET['member'];?>" class="btn btn-link">Add note here</a></p>
				<?php else:?>
					<?php foreach($note->notes as $n):?>
						<form class="form-horizontal" id="editForm_<?php echo $n['note_id'];?>" action="#" method="post">
						
						<div class="form-group">
							
							<label for="type" class="col-xs-3 col-ms-12 control-label form-label required">Type:</label>
							<div class="col-xs-6 col-ms-12">
								<label class="radio-inline radio-ms-block">
									<input type="radio" id="type" name="type" class="form-control" value="1" disabled <?php echo (($n['type'] == '1')? 'checked':'');?>> Ban
								</label>
								<label class="radio-inline radio-ms-block">
									<input type="radio" id="type" name="type" class="form-control" value="2" disabled <?php echo (($n['type'] == '2')? 'checked':'');?>> Temporary ban
								</label>
								<label class="radio-inline radio-ms-block">
									<input type="radio" id="type" name="type" class="form-control" value="3" disabled <?php echo (($n['type'] == '3')? 'checked':'');?>> Anti-social behaviour
								</label>
								<label class="radio-inline radio-ms-block">
									<input type="radio" id="type" name="type" class="form-control" value="4" disabled <?php echo (($n['type'] == '4')? 'checked':'');?>> Other
								</label>
							</div>
						</div>
						<div class="form-group">
							
							<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
							<div class="col-xs-6 col-ms-12">
								<input type="text" class="form-control date" id="start_date" readonly name="start_date" value="<?php echo DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime($n['start_date'])) )->format('Y-m-d');?>">
				
							</div>
						</div>
						<div class="form-group">
							
							<label for="end_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
							<div class="col-xs-6 col-ms-12">
								<input type="text" class="form-control date" id="<?php echo $n['note_id'];?>end_date" readonly name="end_date" value="<?php echo DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime($n['end_date'])) )->format('Y-m-d');?>">
				
							</div>
						</div>
						<div class="form-group">
							
							<label for="comments" class="col-xs-3 col-ms-12 control-label form-label">Comments:</label>
							<div class="col-xs-6 col-ms-12">
								<textarea class="form-control" id="comments" readonly name="comments" rows="3"><?php echo $n['comments'];?></textarea>
				
							</div>
							</div>
							<div class="form-group" >
								
								<label class="col-xs-3 col-ms-12 control-label form-label">Actions:</label>
								<div class="col-xs-6 col-ms-12">
								<a class="btn btn-warning btn-sm mg-10 btn-ms-block editNote" data-note-id="<?php echo $n['note_id'];?>">Change</a>
								<?php if(current_user_can('administrator')):?>
									<a class="btn btn-sm mg-10 btn-danger btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fdelete&amp;nid=<?php echo $n['note_id'];?>" disabled>Remove</a>
								<?php endif;?>
								</div>
							</div>
							<div class="form-group" >
								
								<label class="col-xs-3 col-ms-12 control-label form-label">Metadata:</label>
								<div class="col-xs-6 col-ms-12">
									<table class="table">
										<tbody>
											<tr><th>Created at</th><td><?php echo ($n['created'] != "")?$n['created']: 'Date not set';?></td></tr>
											<tr><th>Last updated at</th><td><?php echo ($n['last_updated_time'] != "")?$n['last_updated_time']: 'Not updated';?></td></tr>
											<tr><th>Last updated by</th><td><?php echo ($n['last_updated_by'] != "")?$n['last_updated_by']: 'Not updated';?></td></tr>
											<tr><th>Nuber of updates</th><td><?php echo ($n['updated_amount']) != ""?$n['updated_amount']: 'Not updated';?></td></tr>
										</tbody>
									</table>
					
								</div>
							</div>

							<input type="hidden" name="nid" value="<?php echo $n['note_id'];?>">
							<input type="hidden" name="updated_amount" value="<?php echo $n['updated_amount'];?>">
							<hr>
						</form>
					<?php endforeach;?>
				<?php endif;?>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Member details</p>
				</div>
				<div class="content">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							</thead>
							<tbody
								<tr><th>Name </th><td><?php echo ucwords(strtolower($note->members[0]['forename']));?> <?php echo ucwords(strtolower($note->members[0]['surname']));?></td></tr>
								<tr><th>Date of brith</th><td><?php echo $note->members[0]['dob']?></td></tr>
								<tr><th>Email</th><td><?php echo strtolower($note->members[0]['email']);?></td></tr>
								<tr><th>Phone number</th><td><?php echo $note->members[0]['number'];?></td></tr>
						</tbody>
						</table>
						<div class="col-ms-12 text-right">
							<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&amp;member=<?php echo $_GET['member'];?>" class="btn btn-sm btn-default">View member</a>
						</div>

				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Details</p>
				</div>
				<div class="content">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							</thead>
							<tbody
								<tr><th>No. of system notes </th><td><?php echo count($note->notes);?> note(s)</td></tr>
						</tbody>
						</table>
				</div>
			</section>
			
	</div>
	
</div>