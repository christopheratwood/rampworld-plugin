<div class="content">
	<div class="row">
		<h1><?php echo (($booking->details['name'] == null)? $booking->details['member_name']: $booking->details['name']);?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
					<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p> 
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall" class="btn btn-default mg-10">Back</a>
				</div>
				<div class="col-md-3 col-xs-4 col-ms-12 float-right">
					<?php
						if($booking->isUsed == true) {
							echo '<a class="btn btn-danger mg-10 btn-ms-block disabled">Cannot refund sessions.</a>';
						} else {
							if(!$booking->isMembership){
								if(date('Y-m-d') > $booking->details['session_date'] && $booking->details['refunded'] == null ) {
									echo '<a class="btn btn-danger mg-10 btn-ms-block disabled">Cannot Refund sessiosn</a>';
								} else if($booking->details['refunded'] != null) {
									echo '<a class="btn btn-danger mg-10 btn-ms-block disabled">Refunded £'.$booking->details['refunded_total'].' on: '.$booking->details['refunded'].'</a>';
								} else {
									echo '<a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Frefund%2Fbooking&bid='.$booking->details['id'].'" class="btn btn-danger mg-10 btn-ms-block" id="refund_btn">Refund</a>';
								}
							} else {
								if(date('Y-m-d') > $booking->details['end_date'] && $booking->details['refunded'] == null ) {
									echo '<a class="btn btn-danger mg-10 btn-ms-block disabled">Cannot Refund sessions</a>';
								} else if($booking->details['refunded'] != null) {
									echo '<a class="btn btn-danger mg-10 btn-ms-block disabled">Refunded £'.$booking->details['refunded_total'].' on: '.$booking->details['refunded'].'</a>';
								} else {
									echo '<a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Frefund%2Fbooking&bid='.$booking->details['id'].'" class="btn btn-danger mg-10 btn-ms-block" id="refund_btn">Refund</a>';
								}
							}
						}?>
					</div>
			</div>
		</div>

		<div class="col-md-8 col-sm-8">
			<?php if(isset($_GET['refund_error'])):?>
				<div class="alert alert-danger visible block">
					<span>Refund was not successfull. <?php echo str_replace('_', ' ', $_GET['refund_error']);?></span>
				</div>
			<?php endif;?>
			<?php if(isset($_GET['refund_success'])):?>
				<div class="alert alert-success visible block">
					<span>Refund was successfull.</span>
				</div>
			<?php endif;?>	

			<h1>Booking Charges</h1>
			<div class="table-responsive">   

				<table class="table table-striped table-hover" id="booking_charges">
					<thead>
						<tr>
							<th>Booking Session Cost</th>
							<th>Booking Fee</th>
							<th>Booking Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
						echo '<tr><td>£'.$booking->details['session_cost'].'</td><td>£'.$booking->details['booking_cost'].'</td><td>£'.number_format($booking->details['session_cost'] + $booking->details['booking_cost'], 2).'</td></tr>';
						?>
					</tbody>
				</table>
			</div>
			<h1>Participants Details</h1>
			<?php if(!$booking->isMembership):?>
			<div class="table-responsive">   
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Member ID</th>
							<th>Member Name</th>
							<th>View member</th>
							<th>Session Cost</th>
							<th>Used</th>
							<th>Change User</th>
							<th>Refund</th>
						</tr>
					</thead>
					<tbody>
						<?php for($i = 0; $i < count($booking->participants); $i++) {

							echo '<tr>
							<td>'.$booking->participants[$i]['prepaid_id'].'</td>
							<td>'.$booking->participants[$i]['member_id'].'</td>
							<td>'.$booking->participants[$i]['name'].'</td>
							<td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$booking->participants[$i]['member_id'].'" class="btn btn-default">View</a></td>
							<td>£'.$booking->participants[$i]['cost'].'</td>
							<td>'.(($booking->participants[$i]['used'] == null )? '<a class="btn btn-default disabled".>Not been Used</a>' : ' <a class="btn btn-warning disabled".>'.$booking->participants[$i]['used'].'</a>').'</td>';
							

							if($booking->participants[$i]['used_on'] != null) {
								echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td>
								<td><a class="btn btn-danger disabled">Used on: '.$booking->participants[$i]['used_on'].'</a></td>';
							} else {
								if($booking->participants[$i]['refunded'] != null) {
									echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td><td><a class="btn btn-danger disabled">Refunded on: '.$booking->participants[$i]['refunded'].'</a></td></tr>';
								} else if(date('Y-m-d') > $booking->details['session_date']) {
									echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td><td><a  class="btn btn-danger disabled">Cannot Refund session ITP</a></td></tr>';
								} else {
									echo '<td><a id="change_booking_user" data-ppid="'.$booking->participants[$i]['prepaid_id'].' "data-prev="'.$booking->participants[$i]['prepaid_id'].'" class="btn btn-default">Change Member</a></td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Frefund%2Fparticipant&bid='.$booking->details['id'].'&ppid='.$booking->participants[$i]['prepaid_id'].'&action=refund" class="btn btn-danger" id="refund_btn">Refund</a></td></tr>';
								}
							}
						}?>
						
					</tbody>
				</table>
			</div>
			<?php else:?>
				<div class="table-responsive">   
					<table class="table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Member ID</th>
								<th>Member Name</th>
								<th>Session Cost</th>
								<th>Used</th>
								<th>Refunded</th>
								<th>Change User</th>
								<th>Refund</th>
							</tr>
						</thead>
						<tbody>
							<?php for($i = 0; $i < count($booking->participants); $i++) {
								echo '<tr>
								<td>'.$booking->participants[$i]['paid_membership_id'].'</td>
								<td>'.$booking->participants[$i]['member_id'].'</td>
								<td>'.$booking->participants[$i]['name'].'</td>
								<td>£'.$booking->participants[$i]['cost'].'</td>
								<td>'.(($booking->participants[$i]['used'] == null )? '<a class="btn btn-default disabled".>Not been Used</a>' : ' <a class="btn btn-warning disabled".>'.$booking->participants[$i]['used'].'</a>').'</td>
								<td>'.(($booking->participants[$i]['refunded'] == null )? '<a class="btn btn-default disabled".>Not been Refunded</a>' : ' <a class="btn btn-warning disabled".>'.$booking->participants[$i]['refunded'].'</a>').'</td>';
								

								if($booking->participants[$i]['used_on'] != null) {
									echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td>
									<td><a class="btn btn-danger disabled">Used on: '.$booking->participants[$i]['used_on'].'</a></td>';
								} else {
									if($booking->participants[$i]['refunded'] != null) {
										echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td><td><a class="btn btn-danger disabled">Refunded on: '.$booking->participants[$i]['refunded'].'</a></td></tr>';
									} else if(date('Y-m-d') > $booking->details['end_date']) {
										echo '<td><a class="btn btn-default disabled">Cannot be changed</a></td><td><a  class="btn btn-danger disabled" data-toggle="popover" data-text="as">Cannot Refund session </a></td></tr>';
									} else {
										echo '<td><a id="change_booking_user" data-pmid="'.$booking->participants[$i]['paid_membership_id'].' "data-prev="'.$booking->participants[$i]['member_id'].'" class="btn btn-default">Change Member</a></td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Frefund%2Fparticipant&mode=view&bid='.$booking->details['id'].'&pmid='.$booking->participants[$i]['paid_membership_id'].'" class="btn btn-danger" id="refund_btn">Refund</a></td></tr>';
									}
									
									
									
								}
							}?>
							
						</tbody>
					</table>
				</div>
			<?php endif;?>

	
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Booking information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
									<tr><th>Purchased date</th><td><?php echo $booking->details['purchased'];?></td></tr>
									<tr><th>Customer</th><td><?php echo (($booking->details['name'] == null)? $booking->details['member_name']: $booking->details['name']);?></td></tr>
									<tr><th>Session date</th><td><?php echo $booking->details['session_date'];?></td></tr>
									<tr><th>Session times</th><td><?php echo $booking->details['times'];?></td></tr>
									<tr><th>Application No.</th><td><?php echo $booking->details['app_version'];?></td></tr>
							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	</div>
</div>
