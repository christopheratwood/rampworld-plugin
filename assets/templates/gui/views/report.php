<?php
$latestReports = null;
	if(count($reportLog->history) >0) {
		foreach($reportLog->history as $log) {
			$latestReports .= '<tr><td>'.ucwords($log['staff']).'</td><td>'.$log['display_name'].'</td><td>'.$log['date_generated'].'</td><td><a href="http://'.$_SERVER['HTTP_HOST']. $log['report_uri'].'" class="btn btn-sm btn-default">Download</a></td></tr>';
		}
	}
?>
<div class="content">
	

	<div class="row">
		<h1>Reports</h1>
			
		<div class="col-md-8 col-sm-8">

			<div class="row">
						
				<div class="report-item col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fadvance">
						<span class="report-icon"><i class="glyphicon glyphicon-user"></i></span>
						<span class="report-name">Member</span>
					</a>
				</div>
				<div class="report-item  col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall">
						<span class="report-icon"><i class="glyphicon glyphicon-time"></i></span>
						<span class="report-name">Time duration</span>
					</a>
				</div>
				<div class="report-item col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fstatistics">
						<span class="report-icon"><i class="glyphicon glyphicon-stats"></i></span>
						<span class="report-name">Statistics</span>
					</a>
				</div>
				<div class="report-item col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall">
						<span class="report-icon"><i class="glyphicon glyphicon-pencil"></i></span>
						<span class="report-name">T&amp;Cs</span>
					</a>
				</div>
				<!--<div class="report-item col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php">
						<span class="report-icon"><i class="glyphicon glyphicon-list-alt"></i></span>
						<span class="report-name">Memberships</span>
					</a>
				</div>-->
				<div class="report-item col-lg-2 col-md-4 col-xs-6 col-ms-12">
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall">
						<span class="report-icon"><i class="glyphicon glyphicon-calendar"></i></span>
						<span class="report-name">Holidays</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12">
			<section class="related-content">
				<div class="title">
					<p class="lead">Recent reports</p>
				</div>
				<div class="content">
					<?php if($latestReports != null):?>
					<div class="table-responsive">
						<table class="table table-hover table-condensed table-stripped">
							<thead>
								<tr><th>Creator</th><th>Type</th><th>Time</th><th>Re-download</th></tr>
							</thead>
							<tbody>
								<?php echo $latestReports;?>
							</tbody>
						</table>
					</div>
						<?php else:?>
							<small>No recent reports</small>
						<?php endif;?>
				</div>
			</section>
		</div>		
	</div>
</div>