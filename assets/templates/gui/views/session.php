<?php $days = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');?>
<div class="content">

	<div class="row">
		<h1><?php echo ucwords(strtolower($session->sessions[0]['display_name']));?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>
				</div>
				<?php if(current_user_can('administrator')):?>
				<div class="col-md-3 col-xs-4 col-ms-12 float-right">
					<a class="btn btn-danger mg-10 btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fdelete&amp;id=<?php echo $id;?>&amp;type=<?php echo $type;?>">Permanently delete session</a>
				</div>
				<?php endif;?>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="session-times">
			<?php if(isset($updated_successfull) && $updated_successfull == true):?>
				<div class="alert alert-success"><strong>Success!</strong> Session details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Session details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['success']) && $_GET['success'] == 'created'):?>
				<div class="alert alert-success"><strong>Success!</strong> New session has been created.</div>
			<?php endif?>
			<?php if(isset($_GET['error']) && $_GET['error'] == 'overlap'):?>
				<div class="alert alert-danger"><strong>Could not create session!</strong> The session dates overlapped with this session.</div>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">

			<div class="form-group <?php echo ((isset($validation->errors['display_name'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
			<div class="col-xs-3 col-ms-12">
					<label for="name" class="col-xs-12 control-label form-label required">Display name: </label>
					<p class="text-right"><em>Please ensure correct grammar is used. This is displayed to the public.</em></p>
				</div>

				<div class="col-xs-6 col-ms-12">
					<input type="text" id="name" name="display_name" class="form-control " readonly value="<?php echo isset($session->sessions[0]['display_name'])? $session->sessions[0]['display_name']:'';?>">
				</div>
			</div>
		
			<div class="form-group <?php echo ((isset($validation->errors['day'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="day" class="col-xs-12 control-label form-label required">Day: </label>
					<p class="text-right"><em>The day of session</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<select class="form-control" id="day" name="day" readonly>
						<option value="null" <?php echo ((isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === 'null') || !isset($session->sessions[0]['day']))? 'selected':'';?>>Select day</option>
						<option value="1" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '1')? 'selected':'';?>>Monday</option>
						<option value="2" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '2')? 'selected':'';?>>Tuesday</option>
						<option value="3" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '3')? 'selected':'';?>>Wednesday</option>
						<option value="4" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '4')? 'selected':'';?>>Thursday</option>
						<option value="5" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '5')? 'selected':'';?>>Friday</option>
						<option value="6" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '6')? 'selected':'';?>>Saturday</option>
						<option value="0" <?php echo (isset($session->sessions[0]['day']) && $session->sessions[0]['day'] === '0')? 'selected':'';?>>Sunday</option>
					</select>
				</div>
			</div>
			<div class="form-group <?php echo ((isset($validation->errors['start_time'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="start_time" class="col-xs-12 control-label form-label required">Start time: </label>
					<p class="text-right"><em>The start time of a the session.</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<input type='text' class="form-control" id="start_time" name="start_time" readonly value="<?php echo isset($session->sessions[0]['start_time'])? $session->sessions[0]['start_time']:'';?>" />
				</div>
			</div>
			<div class="form-group <?php echo ((isset($validation->errors['end_time'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="end_time" class="col-xs-12 control-label form-label required">End time: </label>
					<p class="text-right"><em>The end time of a the session</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<input type='text' class="form-control" id="end_time" name="end_time" readonly value="<?php echo isset($session->sessions[0]['end_time'])? $session->sessions[0]['end_time']:'';?>" />
				</div>
			</div>
			<div class="form-group <?php echo ((isset($validation->errors['beginner'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="beginner" class="col-xs-12 control-label form-label required">Beginner: </label>
					<p class="text-right"><em>Is this session only for beginners?</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="beginner" name="beginner" class="form-control" value="1" disabled <?php echo (isset($session->sessions[0]['beginner']) && $session->sessions[0]['beginner'] == '1')? 'checked':'';?>> Yes
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="beginner" name="beginner" class="form-control" value="0" disabled <?php echo (!isset($session->sessions[0]['beginner']))? 'checked':'';?>> No
					</label>
					
				</div>
			</div>
			<div class="form-group <?php echo ((isset($validation->errors['consecutive'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="consecutive" class="col-xs-12 control-label form-label required">Consecutive: </label>
					<p class="text-right"><em>Can this session be used in alongside other sessions?</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="consecutive" name="consecutive" class="form-control" value="1" disabled <?php echo (isset($session->sessions[0]['consecutive']) && $session->sessions[0]['consecutive'] == '1')? 'checked':'';?>> Yes
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="consecutive" name="consecutive" class="form-control" value="0" disabled <?php echo (!isset($session->sessions[0]['consecutive']))? 'checked':'';?>> No
					</label>
					
				</div>
			</div>
			<div class="form-group <?php echo ((isset($validation->errors['private'])) ? 'has-feedback has-error': ((isset($validation->errors))? 'has-feedback has-success' : ''));?>">
				<div class="col-xs-3 col-ms-12">
					<label for="private" class="col-xs-12 control-label form-label required">Private </label>
					<p class="text-right"><em>Is this session a private hire?</em></p>
				</div>
				<div class="col-xs-6 col-ms-12">
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="private" name="private" class="form-control" value="1" disabled <?php echo (isset($session->sessions[0]['private']) && $session->sessions[0]['private'] == '1')? 'checked':'';?>> Yes
					</label>
					<label class="radio-inline radio-ms-block">
						<input type="radio" id="private" name="private" class="form-control" value="0" disabled <?php echo (!isset($session->sessions[0]['private']))? 'checked':'';?>> No
					</label>
					<input type="hidden" name="update_counter" value="<?php echo $session->sessions[0]['updated_amount'];?>">
				</div>
			</div>
			</form>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Session information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
									<?php if(isset($_GET['hsid'])):?>
										<tr><th>Holiday</th><td><a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fsingle&amp;hid=<?php echo $session->sessions[0]['holiday_id'];?>"><strong>#<?php echo $session->sessions[0]['holiday_id'];?></strong> <?php echo $session->sessions[0]['holiday_name'];?></a></td></tr>
										<tr><th>Operation dates</th><td><?php echo $session->sessions[0]['start_date'];?> - <?php echo $session->sessions[0]['end_date'];?></td></tr>
									<?php endif;?>
									<tr><th>Creation date</th><td><?php echo $session->sessions[0]['created'];?></td></tr>
									<tr><th>Number of entries</th><td><?php echo $session->count;?></td></tr>
									<tr><th>Holiday last updated at</th><td><?php echo (!isset($session->sessions[0]['last_updated_time']))? 'Not updated': $session->sessions[0]['last_updated_time'];?></td></tr>
									<tr><th>Holiday last updated by</th><td><?php echo (!isset($session->sessions[0]['last_updated_user']))? 'Not updated': $session->sessions[0]['last_updated_user'];?></td></tr>
									<tr><th>No. of updates</th><td><?php echo ($session->sessions[0]['updated_amount'] == 0)? 'Not updated': $session->sessions[0]['updated_amount'];?></td></tr>
							

							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	
</div>
