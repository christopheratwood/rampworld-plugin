<div class="content" id="opening-checks">

<div class="row">
	<h1><?php echo ($premises->checks[0]['date']  != null)? $premises->checks[0]['date']: date('l \t\h\e jS \o\f F Y') ;?></h1>
	<div class="well well-sm">
		<div class="row">
			<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=premises&amp;premises=<?php echo $premises->checks[0]['premises_id'];?>" class="btn btn-default mg-10 btn-ms-block">Generate report</a>
				
				
				
				<?php if(current_user_can('administrator')):?>

					<a class="btn btn-warning mg-10 btn-ms-block" id="editClosing" data-can-populate-all="true">Change details</a>
				<?php elseif(date('Y-m-d') == date('Y-m-d', strtotime($_GET['date']))):?>
					<a class="btn btn-warning mg-10 btn-ms-block" id="editClosing" data-can-populate-all="true">Change details</a>
				<?php endif;?>
			</div>
		</div>
	</div>

	<div class="col-md-8 col-sm-8" id="premises-dates">
		<?php if(isset($_GET['success'])):?>
			<div class="alert alert-success"><strong>Success!</strong> New checks has been created.</div>
		<?php endif?>
		<?php if(isset($_GET['error']) && $_GET['error'] == 'save'):?>
			<div class="alert alert-danger"><strong>Could not create premises!</strong> The entry could not be saved.</div>
		<?php endif?>
		<form class="form-horizontal" id="editForm" action="#" method="post">
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Stock <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="stock" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Fridge:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="stock_0" value="1" <?php echo ( (isset($_POST['stock_0']) && $_POST['stock_0'] == "1") || !empty($premises->checks[0]['stock']) && substr($premises->checks[0]['stock'], 0, 1) == 1) ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="stock_0" value="0" <?php echo ( (isset($_POST['stock_0']) && $_POST['stock_0']=== "0") || !empty($premises->checks[0]['stock']) && substr($premises->checks[0]['stock'], 0, 1)=== "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="stock_0" value="n" <?php echo ( (isset($_POST['stock_0']) && $_POST['stock_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Pot noodles:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_1" value="1" <?php echo ( (isset($_POST['stock_1']) && $_POST['stock_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_1" value="0" <?php echo ( (isset($_POST['stock_1']) && $_POST['stock_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 1, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_1" value="n" <?php echo ( (isset($_POST['stock_1']) && $_POST['stock_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Crisps and sweets:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_2" value="1" <?php echo ( (isset($_POST['stock_2']) && $_POST['stock_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 2, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_2" value="0" <?php echo ( (isset($_POST['stock_2']) && $_POST['stock_2']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 2, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="stock_2" value="n" <?php echo ( (isset($_POST['stock_2']) && $_POST['stock_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['stock'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
				<fieldset class="col-md-6 col-sm-12">
					<legend>Till <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="till" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Cash up:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_0" value="1" <?php echo ( (isset($_POST['till_0']) && $_POST['till_0'] == "1") || !empty($premises->checks[0]['till']) && substr($premises->checks[0]['till'], 0, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_0" value="0" <?php echo ( (isset($_POST['till_0']) && $_POST['till_0'] === "0") || !empty($premises->checks[0]['till']) && substr($premises->checks[0]['till'], 0, 1) === "0") ? "checked": ""?>> No	</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_0" value="n" <?php echo ( (isset($_POST['till_0']) && $_POST['till_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['till'], 0, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Complete float sheet:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_1" value="1" <?php echo ( (isset($_POST['till_1']) && $_POST['till_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['till'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_1" value="0" <?php echo ( (isset($_POST['till_1']) && $_POST['till_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['till'], 1, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="till_1" value="n" <?php echo ( (isset($_POST['till_1']) && $_POST['till_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['till'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-12 col-sm-12">
					<legend>Cleaning <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="cleaning" disabled>Populate</a></legend>
				
					<div class="col-md-6">
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Coffee machine steam and hot water nozzle(s):</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_0" value="1" <?php echo ( (isset($_POST['cleaning_0']) && $_POST['cleaning_0'] == "1") || !empty($premises->checks[0]['cleaning']) && substr($premises->checks[0]['cleaning'], 0, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_0" value="0" <?php echo ( (isset($_POST['cleaning_0']) && $_POST['cleaning_0'] === "0") || !empty($premises->checks[0]['cleaning']) && substr($premises->checks[0]['cleaning'], 0, 1)=== "0") ? "checked": ""?>> No	</label>
								<label class="radio-inline">	<input type="radio" class="form-control"disabled name="cleaning_0" value="n" <?php echo ( (isset($_POST['cleaning_0']) && $_POST['cleaning_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 0, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Empty and clean knockout tray:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_1" value="1" <?php echo ( (isset($_POST['cleaning_1']) && $_POST['cleaning_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_1" value="0" <?php echo ( (isset($_POST['cleaning_1']) && $_POST['cleaning_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 1, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_1" value="n" <?php echo ( (isset($_POST['cleaning_1']) && $_POST['cleaning_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Wash w/ soapy water and disinfect all surfaces:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_2" value="1" <?php echo ( (isset($_POST['cleaning_2']) && $_POST['cleaning_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 2, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_2" value="0" <?php echo ( (isset($_POST['cleaning_2']) && $_POST['cleaning_2']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 2, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_2" value="n" <?php echo ( (isset($_POST['cleaning_2']) && $_POST['cleaning_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Empty, clean and disinfect slush puppies tray(s):</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_3" value="1" <?php echo ( (isset($_POST['cleaning_3']) && $_POST['cleaning_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 3, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_3" value="0" <?php echo ( (isset($_POST['cleaning_3']) && $_POST['cleaning_3']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 3, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_3" value="n" <?php echo ( (isset($_POST['cleaning_3']) && $_POST['cleaning_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Disinfect slush puppies tray(s):</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_4" value="1" <?php echo ( (isset($_POST['cleaning_4']) && $_POST['cleaning_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 4, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_4" value="0" <?php echo ( (isset($_POST['cleaning_4']) && $_POST['cleaning_4']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 4, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_4" value="n" <?php echo ( (isset($_POST['cleaning_4']) && $_POST['cleaning_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Wash w/ soapy water and disinfect all surfaces around slush puppy Machine:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_5" value="1" <?php echo ( (isset($_POST['cleaning_5']) && $_POST['cleaning_5'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 5, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_5" value="0" <?php echo ( (isset($_POST['cleaning_5']) && $_POST['cleaning_5']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 5, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_5" value="n" <?php echo ( (isset($_POST['cleaning_5']) && $_POST['cleaning_5'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 5, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Wash w/ soapy water and disinfect microwave:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_6" value="1" <?php echo ( (isset($_POST['cleaning_6']) && $_POST['cleaning_6'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 6, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_6" value="0" <?php echo ( (isset($_POST['cleaning_6']) && $_POST['cleaning_6']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 6, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_6" value="n" <?php echo ( (isset($_POST['cleaning_6']) && $_POST['cleaning_6'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 6, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Sink emptied and clean:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_7" value="1" <?php echo ( (isset($_POST['cleaning_7']) && $_POST['cleaning_7'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 7, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_7" value="0" <?php echo ( (isset($_POST['cleaning_7']) && $_POST['cleaning_7']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 7, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_7" value="n" <?php echo ( (isset($_POST['cleaning_7']) && $_POST['cleaning_7'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 7, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Load dishwasher w/ pizza cutters, chopping board and knives</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_8" value="1" <?php echo ( (isset($_POST['cleaning_8']) && $_POST['cleaning_8'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 8, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_8" value="0" <?php echo ( (isset($_POST['cleaning_8']) && $_POST['cleaning_8']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 8, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_8" value="n" <?php echo ( (isset($_POST['cleaning_8']) && $_POST['cleaning_8'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 8, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Switch dishwasher on w/ normal wash:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_9" value="1" <?php echo ( (isset($_POST['cleaning_9']) && $_POST['cleaning_9'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 9, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_9" value="0" <?php echo ( (isset($_POST['cleaning_9']) && $_POST['cleaning_9']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 9, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_9" value="n" <?php echo ( (isset($_POST['cleaning_9']) && $_POST['cleaning_9'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 9, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Wash fridge door and inside fridge:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_10" value="1" <?php echo ( (isset($_POST['cleaning_10']) && $_POST['cleaning_10'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 10, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_10" value="0" <?php echo ( (isset($_POST['cleaning_10']) && $_POST['cleaning_10']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 10, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_10" value="n" <?php echo ( (isset($_POST['cleaning_10']) && $_POST['cleaning_10'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 10, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Is Reception Clean?:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_11" value="1" <?php echo ( (isset($_POST['cleaning_11']) && $_POST['cleaning_11'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 11, 1) == 1) ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_11" value="0" <?php echo ( (isset($_POST['cleaning_11']) && $_POST['cleaning_11']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 11, 1)=== "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="cleaning_11" value="n" <?php echo ( (isset($_POST['cleaning_11']) && $_POST['cleaning_11'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cleaning'], 11, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Park <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="park" disabled>Populate</a></legend>
			
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Bins emptied:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="park_check_0" value="1" <?php echo ( (isset($_POST['park_check_0']) && $_POST['park_check_0'] == "1") || !empty($premises->checks[0]['park_check']) && substr($premises->checks[0]['park_check'], 0, 1) == 1) ? "checked": ""?>> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="park_check_0" value="0" <?php echo ( (isset($_POST['park_check_0']) && $_POST['park_check_0']=== "0") || !empty($premises->checks[0]['park_check']) && substr($premises->checks[0]['park_check'], 0, 1)=== "0") ? "checked": ""?>> No	
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="park_check_0" value="n" <?php echo ( (isset($_POST['park_check_0']) && $_POST['park_check_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 0, 1) == "n") ? "checked": ""?>> N/A
								</label>
								
							</div>
						</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Check toilets for cleanliness, tunning taps and toilets rolls:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_1" value="1" <?php echo ( (isset($_POST['park_check_1']) && $_POST['park_check_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_1" value="0" <?php echo ( (isset($_POST['park_check_1']) && $_POST['park_check_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 1, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_1" value="n" <?php echo ( (isset($_POST['park_check_1']) && $_POST['park_check_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Spectator's area clean inc. no food on floors:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_2" value="1" <?php echo ( (isset($_POST['park_check_2']) && $_POST['park_check_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 2, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_2" value="0" <?php echo ( (isset($_POST['park_check_2']) && $_POST['park_check_2']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 2, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_2" value="n" <?php echo ( (isset($_POST['park_check_2']) && $_POST['park_check_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Parent's room light and heater(Winter Only) off:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_3" value="1" <?php echo ( (isset($_POST['park_check_3']) && $_POST['park_check_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 3, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_3" value="0" <?php echo ( (isset($_POST['park_check_3']) && $_POST['park_check_3']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 3, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="park_check_3" value="n" <?php echo ( (isset($_POST['park_check_3']) && $_POST['park_check_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['park_check'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					</fieldset>
			
				<fieldset class="col-md-6 col-sm-12">
					<legend>Power off <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="power" disabled>Populate</a></legend>
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Tablet:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="power_off_0" value="1" <?php echo ( (isset($_POST['power_off_0']) && $_POST['power_off_0'] == "1") || !empty($premises->checks[0]['power_off']) && substr($premises->checks[0]['power_off'], 0, 1) == 1) ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="power_off_0" value="0" <?php echo ( (isset($_POST['power_off_0']) && $_POST['power_off_0']=== "0") || !empty($premises->checks[0]['power_off']) && substr($premises->checks[0]['power_off'], 0, 1)=== "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="power_off_0" value="n" <?php echo ( (isset($_POST['power_off_0']) && $_POST['power_off_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">PA amplifier:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_1" value="1" <?php echo ( (isset($_POST['power_off_1']) && $_POST['power_off_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_1" value="0" <?php echo ( (isset($_POST['power_off_1']) && $_POST['power_off_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 1, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_1" value="n" <?php echo ( (isset($_POST['power_off_1']) && $_POST['power_off_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">PA mixer:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_2" value="1" <?php echo ( (isset($_POST['power_off_2']) && $_POST['power_off_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 2, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_2" value="0" <?php echo ( (isset($_POST['power_off_2']) && $_POST['power_off_2']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 2, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_2" value="n" <?php echo ( (isset($_POST['power_off_2']) && $_POST['power_off_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">BMX display light:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_3" value="1" <?php echo ( (isset($_POST['power_off_3']) && $_POST['power_off_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 3, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_3" value="0" <?php echo ( (isset($_POST['power_off_3']) && $_POST['power_off_3']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 3, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_3" value="n" <?php echo ( (isset($_POST['power_off_3']) && $_POST['power_off_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Vending machine:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_4" value="1" <?php echo ( (isset($_POST['power_off_4']) && $_POST['power_off_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 4, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_4" value="0" <?php echo ( (isset($_POST['power_off_4']) && $_POST['power_off_4']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 4, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_4" value="n" <?php echo ( (isset($_POST['power_off_4']) && $_POST['power_off_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Slush puppy machine:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_5" value="1" <?php echo ( (isset($_POST['power_off_5']) && $_POST['power_off_5'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 5, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_5" value="0" <?php echo ( (isset($_POST['power_off_5']) && $_POST['power_off_5']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 5, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_5" value="n" <?php echo ( (isset($_POST['power_off_5']) && $_POST['power_off_5'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 5, 1) == "n") ? "checked": ""?>> N/A</label>
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Coffee machine:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_6" value="1" <?php echo ( (isset($_POST['power_off_6']) && $_POST['power_off_6'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 6, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_6" value="0" <?php echo ( (isset($_POST['power_off_6']) && $_POST['power_off_6']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 6, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="power_off_6" value="n" <?php echo ( (isset($_POST['power_off_6']) && $_POST['power_off_6'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['power_off'], 6, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Final check <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="final" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Walk park (check litter, screws) and check premises is empty:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="final_0" value="1" <?php echo ( (isset($_POST['final_0']) && $_POST['final_0'] == "1") || !empty($premises->checks[0]['final']) && substr($premises->checks[0]['final'], 0, 1) == 1) ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="final_0" value="0" <?php echo ( (isset($_POST['final_0']) && $_POST['final_0']=== "0") || !empty($premises->checks[0]['final']) && substr($premises->checks[0]['final'], 0, 1)=== "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="final_0" value="n" <?php echo ( (isset($_POST['final_0']) && $_POST['final_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['final'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Turn CCTV off:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_1" value="1" <?php echo ( (isset($_POST['final_1']) && $_POST['final_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['final'], 1, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_1" value="0" <?php echo ( (isset($_POST['final_1']) && $_POST['final_1']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['final'], 1, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_1" value="n" <?php echo ( (isset($_POST['final_1']) && $_POST['final_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['final'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Turn RampWorld Cardiff logo off:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_2" value="1" <?php echo ( (isset($_POST['final_2']) && $_POST['final_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['final'], 2, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_2" value="0" <?php echo ( (isset($_POST['final_2']) && $_POST['final_2']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['final'], 2, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_2" value="n" <?php echo ( (isset($_POST['final_2']) && $_POST['final_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['final'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Entrance shutter closed:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_3" value="1" <?php echo ( (isset($_POST['final_3']) && $_POST['final_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['final'], 3, 1) == 1) ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_3" value="0" <?php echo ( (isset($_POST['final_3']) && $_POST['final_3']=== "0") || !empty($premises->checks) && substr($premises->checks[0]['final'], 3, 1)=== "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="final_3" value="n" <?php echo ( (isset($_POST['final_3']) && $_POST['final_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['final'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
				
				<fieldset class="col-md-12 col-sm-12">
					<legend>Comments</legend>
					<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Any comments:</label>
					<div class="col-xs-7 col-ms-6">
						<textarea name="comments" class=" form-control"><?php echo (isset($_POST['comments'])) ? $_POST['comments']: ((isset($premises->checks[0]['comments'])? $premises->checks[0]['comments']: ''));?></textarea>
					</div>
				</fieldset>

			</div>
		</form>
	</div>
	<div class="col-md-4 col-sm-4">
		<section class="related-content">
			<div class="title">
				<p class="lead">Holiday information</p>
			</div>
			<div class="content">
				<div class="table-responsive">	
					<table class="table table-condensed">
						<tbody>
								<tr><th>Creation date</th><td><?php echo ($premises->checks[0]['datetime']==null)?'Not created':$premises->checks[0]['datetime'] ;?></td></tr>
								<tr><th>Last updated at</th><td><?php echo ($premises->checks[0]['updated_amount'] == null)? 'Not updated': $premises->checks[0]['last_updated_time'];?></td></tr>
								<tr><th>Last updated by</th><td><?php echo ($premises->checks[0]['last_updated_user'] == null)? 'Not updated': $premises->checks[0]['last_updated_user'];?></td></tr>
								<tr><th>No. of updates</th><td><?php echo ($premises->checks[0]['updated_amount'] == null)? 'Not updated': $premises->checks[0]['updated_amount'];?></td></tr>
						

						</tbody>
					</table>
				</div>
			</div>
		</section>
</div>

</div>
