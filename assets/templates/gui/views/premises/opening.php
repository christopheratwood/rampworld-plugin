<div class="content" id="opening-checks">
<div class="row">
	<h1><?php echo $premises->checks[0]['date'];?></h1>
	<div class="well well-sm">
		<div class="row">
			<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=premises&amp;premises=<?php echo $premises->checks[0]['premises_id'];?>" class="btn btn-default mg-10 btn-ms-block">Generate report</a>
				
				<?php if(current_user_can('administrator')):?>
					<a class="btn btn-warning mg-10 btn-ms-block" id="editOpening">Change details</a>
				<?php elseif(date('Y-m-d') == date('Y-m-d', strtotime($_GET['date']))):?>
					<a class="btn btn-warning mg-10 btn-ms-block" id="editOpening">Change details</a>
				<?php endif;?>
			</div>
		</div>
	</div>

	<div class="col-md-8 col-sm-8" id="premises-dates">
		<?php if(isset($_GET['success'])):?>
			<div class="alert alert-success"><strong>Success!</strong> New checks has been created.</div>
		<?php endif?>
		<?php if(isset($_GET['error']) && $_GET['error'] == 'save'):?>
			<div class="alert alert-danger"><strong>Could not create premises!</strong> The opening checks could not be saved.</div>
		<?php endif?>
		<form class="form-horizontal" id="editForm" action="#" method="post">
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Bowl <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="bowl" disabled>Populate</a></legend>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dry:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="bowl_0" value="1" <?php echo ( (isset($_POST['bowl_0']) && $_POST['bowl_0'] == "1") || !empty($premises->checks[0]['bowl']) && substr($premises->checks[0]['bowl'], 1, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="bowl_0" value="0" <?php echo ( (isset($_POST['bowl_0']) && $_POST['bowl_0'] === "0") || !empty($premises->checks[0]['bowl']) && substr($premises->checks[0]['bowl'], 0, 1) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="bowl_0" value="n" <?php echo ( (isset($_POST['bowl_0']) && $_POST['bowl_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_1" value="1" <?php echo ( (isset($_POST['bowl_1']) && $_POST['bowl_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_1" value="0" <?php echo ( (isset($_POST['bowl_1']) && $_POST['bowl_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_1" value="n" <?php echo ( (isset($_POST['bowl_1']) && $_POST['bowl_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Screw check:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_2" value="1" <?php echo ( (isset($_POST['bowl_2']) && $_POST['bowl_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_2" value="0" <?php echo ( (isset($_POST['bowl_2']) && $_POST['bowl_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_2" value="n" <?php echo ( (isset($_POST['bowl_2']) && $_POST['bowl_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Damage:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_3" value="1" <?php echo ( (isset($_POST['bowl_3']) && $_POST['bowl_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_3" value="0" <?php echo ( (isset($_POST['bowl_3']) && $_POST['bowl_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_3" value="n" <?php echo ( (isset($_POST['bowl_3']) && $_POST['bowl_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rubish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_4" value="1" <?php echo ( (isset($_POST['bowl_4']) && $_POST['bowl_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_4" value="0" <?php echo ( (isset($_POST['bowl_4']) && $_POST['bowl_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="bowl_4" value="n" <?php echo ( (isset($_POST['bowl_4']) && $_POST['bowl_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['bowl'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
				<fieldset class="col-md-6 col-sm-12">
					<legend>Jump boxes <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="jumpboxes" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dry:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="jump_boxes_0" value="1" <?php echo ( (isset($_POST['jump_boxes_0']) && $_POST['jump_boxes_0'] == "1") || !empty($premises->checks[0]['jump_boxes']) && substr($premises->checks[0]['jump_boxes'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="jump_boxes_0" value="0" <?php echo ( (isset($_POST['jump_boxes_0']) && $_POST['jump_boxes_0'] === "0") || !empty($premises->checks[0]['jump_boxes']) && substr($premises->checks[0]['jump_boxes'], 0, 1) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="jump_boxes_0" value="n" <?php echo ( (isset($_POST['jump_boxes_0']) && $_POST['jump_boxes_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_1" value="1" <?php echo ( (isset($_POST['jump_boxes_1']) && $_POST['jump_boxes_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_1" value="0" <?php echo ( (isset($_POST['jump_boxes_1']) && $_POST['jump_boxes_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_1" value="n" <?php echo ( (isset($_POST['jump_boxes_1']) && $_POST['jump_boxes_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Screw check:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_2" value="1" <?php echo ( (isset($_POST['jump_boxes_2']) && $_POST['jump_boxes_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_2" value="0" <?php echo ( (isset($_POST['jump_boxes_2']) && $_POST['jump_boxes_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_2" value="n" <?php echo ( (isset($_POST['jump_boxes_2']) && $_POST['jump_boxes_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Damage:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_3" value="1" <?php echo ( (isset($_POST['jump_boxes_3']) && $_POST['jump_boxes_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_3" value="0" <?php echo ( (isset($_POST['jump_boxes_3']) && $_POST['jump_boxes_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_3" value="n" <?php echo ( (isset($_POST['jump_boxes_3']) && $_POST['jump_boxes_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rubish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_4" value="1" <?php echo ( (isset($_POST['jump_boxes_4']) && $_POST['jump_boxes_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_4" value="0" <?php echo ( (isset($_POST['jump_boxes_4']) && $_POST['jump_boxes_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="jump_boxes_4" value="n" <?php echo ( (isset($_POST['jump_boxes_4']) && $_POST['jump_boxes_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['jump_boxes'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Perfomance area <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="foamresi" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dry:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="foam_resi_0" value="1" <?php echo ( (isset($_POST['foam_resi_0']) && $_POST['foam_resi_0'] == "1") || !empty($premises->checks[0]['foam_resi']) && substr($premises->checks[0]['foam_resi'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="foam_resi_0" value="0" <?php echo ( (isset($_POST['foam_resi_0']) && $_POST['foam_resi_0'] === "0") || !empty($premises->checks[0]['foam_resi']) && substr($premises->checks[0]['foam_resi'], 0, 1) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="foam_resi_0" value="n" <?php echo ( (isset($_POST['foam_resi_0']) && $_POST['foam_resi_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_1" value="1" <?php echo ( (isset($_POST['foam_resi_1']) && $_POST['foam_resi_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_1" value="0" <?php echo ( (isset($_POST['foam_resi_1']) && $_POST['foam_resi_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_1" value="n" <?php echo ( (isset($_POST['foam_resi_1']) && $_POST['foam_resi_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Screw check:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_2" value="1" <?php echo ( (isset($_POST['foam_resi_2']) && $_POST['foam_resi_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_2" value="0" <?php echo ( (isset($_POST['foam_resi_2']) && $_POST['foam_resi_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_2" value="n" <?php echo ( (isset($_POST['foam_resi_2']) && $_POST['foam_resi_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Damage:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_3" value="1" <?php echo ( (isset($_POST['foam_resi_3']) && $_POST['foam_resi_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_3" value="0" <?php echo ( (isset($_POST['foam_resi_3']) && $_POST['foam_resi_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_3" value="n" <?php echo ( (isset($_POST['foam_resi_3']) && $_POST['foam_resi_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rubish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_4" value="1" <?php echo ( (isset($_POST['foam_resi_4']) && $_POST['foam_resi_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_4" value="0" <?php echo ( (isset($_POST['foam_resi_4']) && $_POST['foam_resi_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="foam_resi_4" value="n" <?php echo ( (isset($_POST['foam_resi_4']) && $_POST['foam_resi_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['foam_resi'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
				<fieldset class="col-md-6 col-sm-12">
					<legend>Street area <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="street" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dry:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="street_area_0" value="1" <?php echo ( (isset($_POST['street_area_0']) && $_POST['street_area_0'] == "1") || !empty($premises->checks[0]['street_area']) && substr($premises->checks[0]['street_area'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="street_area_0" value="0" <?php echo ( (isset($_POST['street_area_0']) && $_POST['street_area_0'] === "0") || !empty($premises->checks[0]['street_area']) && substr($premises->checks[0]['street_area'], 0, 1) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="street_area_0" value="n" <?php echo ( (isset($_POST['street_area_0']) && $_POST['street_area_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_1" value="1" <?php echo ( (isset($_POST['street_area_1']) && $_POST['street_area_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_1" value="0" <?php echo ( (isset($_POST['street_area_1']) && $_POST['street_area_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_1" value="n" <?php echo ( (isset($_POST['street_area_1']) && $_POST['street_area_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Screw check:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_2" value="1" <?php echo ( (isset($_POST['street_area_2']) && $_POST['street_area_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_2" value="0" <?php echo ( (isset($_POST['street_area_2']) && $_POST['street_area_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_2" value="n" <?php echo ( (isset($_POST['street_area_2']) && $_POST['street_area_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Damage:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_3" value="1" <?php echo ( (isset($_POST['street_area_3']) && $_POST['street_area_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_3" value="0" <?php echo ( (isset($_POST['street_area_3']) && $_POST['street_area_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_3" value="n" <?php echo ( (isset($_POST['street_area_3']) && $_POST['street_area_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rubish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_4" value="1" <?php echo ( (isset($_POST['street_area_4']) && $_POST['street_area_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_4" value="0" <?php echo ( (isset($_POST['street_area_4']) && $_POST['street_area_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="street_area_4" value="n" <?php echo ( (isset($_POST['street_area_4']) && $_POST['street_area_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['street_area'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Spine and pump track area <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="spine" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dry:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spine_mini_pump_track_0" value="1" <?php echo ( (isset($_POST['spine_mini_pump_track_0']) && $_POST['spine_mini_pump_track_0'] == "1") || !empty($premises->checks[0]['spine_mini_pump_track']) && substr($premises->checks[0]['spine_mini_pump_track'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spine_mini_pump_track_0" value="0" <?php echo ( (isset($_POST['spine_mini_pump_track_0']) && $_POST['spine_mini_pump_track_0'] === "0") || !empty($premises->checks[0]['spine_mini_pump_track']) && substr($premises->checks[0]['spine_mini_pump_track'], 0, 1) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spine_mini_pump_track_0" value="n" <?php echo ( (isset($_POST['spine_mini_pump_track_0']) && $_POST['spine_mini_pump_track_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_1" value="1" <?php echo ( (isset($_POST['spine_mini_pump_track_1']) && $_POST['spine_mini_pump_track_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_1" value="0" <?php echo ( (isset($_POST['spine_mini_pump_track_1']) && $_POST['spine_mini_pump_track_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_1" value="n" <?php echo ( (isset($_POST['spine_mini_pump_track_1']) && $_POST['spine_mini_pump_track_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Screw check:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_2" value="1" <?php echo ( (isset($_POST['spine_mini_pump_track_2']) && $_POST['spine_mini_pump_track_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_2" value="0" <?php echo ( (isset($_POST['spine_mini_pump_track_2']) && $_POST['spine_mini_pump_track_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_2" value="n" <?php echo ( (isset($_POST['spine_mini_pump_track_2']) && $_POST['spine_mini_pump_track_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Damage:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_3" value="1" <?php echo ( (isset($_POST['spine_mini_pump_track_3']) && $_POST['spine_mini_pump_track_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_3" value="0" <?php echo ( (isset($_POST['spine_mini_pump_track_3']) && $_POST['spine_mini_pump_track_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_3" value="n" <?php echo ( (isset($_POST['spine_mini_pump_track_3']) && $_POST['spine_mini_pump_track_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rubish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_4" value="1" <?php echo ( (isset($_POST['spine_mini_pump_track_4']) && $_POST['spine_mini_pump_track_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_4" value="0" <?php echo ( (isset($_POST['spine_mini_pump_track_4']) && $_POST['spine_mini_pump_track_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spine_mini_pump_track_4" value="n" <?php echo ( (isset($_POST['spine_mini_pump_track_4']) && $_POST['spine_mini_pump_track_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spine_mini_pump_track'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
				<fieldset class="col-md-6 col-sm-12">
					<legend>CCTV<a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="cctv" disabled>Populate</a></legend>
				
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Switched on:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="cctv_0" value="1" <?php echo ( (isset($_POST['cctv_0']) && $_POST['cctv_0'] == "1") || !empty($premises->checks[0]['cctv']) && substr($premises->checks[0]['cctv'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="cctv_0" value="1" <?php echo ( (isset($_POST['cctv_0']) && $_POST['cctv_0'] == "1") || !empty($premises->checks[0]['cctv']) && substr($premises->checks[0]['cctv'], 0, 1) == "1") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="cctv_0" value="n" <?php echo ( (isset($_POST['cctv_0']) && $_POST['cctv_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cctv'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Operational:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="cctv_1" value="1" <?php echo ( (isset($_POST['cctv_1']) && $_POST['cctv_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['cctv'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="cctv_1" value="0" <?php echo ( (isset($_POST['cctv_1']) && $_POST['cctv_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['cctv'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="cctv_1" value="n" <?php echo ( (isset($_POST['cctv_1']) && $_POST['cctv_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['cctv'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-12 col-sm-12">
					<legend>Reception <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="reception" disabled>Populate</a></legend>
				
					<div class="col-md-6">
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="reception_0" value="1" <?php echo ( (isset($_POST['reception_0']) && $_POST['reception_0'] == "1") || !empty($premises->checks[0]['reception']) && substr($premises->checks[0]['reception'], 0, 1) == "1") ? "checked": ""?>> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="reception_0" value="1" <?php echo ( (isset($_POST['reception_0']) && $_POST['reception_0'] == "1") || !empty($premises->checks[0]['reception']) && substr($premises->checks[0]['reception'], 0, 1) == "1") ? "checked": ""?>> No	
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="reception_0" value="n" <?php echo ( (isset($_POST['reception_0']) && $_POST['reception_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 0, 1) == "n") ? "checked": ""?>> N/A
								</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Float counted:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_1" value="1" <?php echo ( (isset($_POST['reception_1']) && $_POST['reception_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_1" value="0" <?php echo ( (isset($_POST['reception_1']) && $_POST['reception_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 1, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_1" value="n" <?php echo ( (isset($_POST['reception_1']) && $_POST['reception_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Till operational:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_2" value="1" <?php echo ( (isset($_POST['reception_2']) && $_POST['reception_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_2" value="0" <?php echo ( (isset($_POST['reception_2']) && $_POST['reception_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 2, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_2" value="n" <?php echo ( (isset($_POST['reception_2']) && $_POST['reception_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Tablets operational:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_3" value="1" <?php echo ( (isset($_POST['reception_3']) && $_POST['reception_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_3" value="0" <?php echo ( (isset($_POST['reception_3']) && $_POST['reception_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 3, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_3" value="n" <?php echo ( (isset($_POST['reception_3']) && $_POST['reception_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Membership tested with member <strong>#2</strong>:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_4" value="1" <?php echo ( (isset($_POST['reception_4']) && $_POST['reception_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_4" value="0" <?php echo ( (isset($_POST['reception_4']) && $_POST['reception_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 4, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_4" value="n" <?php echo ( (isset($_POST['reception_4']) && $_POST['reception_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required"><strong>Unique</strong> sessions band selected:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_5" value="1" <?php echo ( (isset($_POST['reception_5']) && $_POST['reception_5'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 5, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_5" value="0" <?php echo ( (isset($_POST['reception_5']) && $_POST['reception_5'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 5, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_5" value="n" <?php echo ( (isset($_POST['reception_5']) && $_POST['reception_5'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 5, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Bins emptied:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_6" value="1" <?php echo ( (isset($_POST['reception_6']) && $_POST['reception_6'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 6, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_6" value="0" <?php echo ( (isset($_POST['reception_6']) && $_POST['reception_6'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 6, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_6" value="n" <?php echo ( (isset($_POST['reception_6']) && $_POST['reception_6'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 6, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Front door opended:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_7" value="1" <?php echo ( (isset($_POST['reception_7']) && $_POST['reception_7'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 7, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_7" value="0" <?php echo ( (isset($_POST['reception_7']) && $_POST['reception_7'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 7, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_7" value="n" <?php echo ( (isset($_POST['reception_7']) && $_POST['reception_7'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 7, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Dishwasher emptied:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_8" value="1" <?php echo ( (isset($_POST['reception_8']) && $_POST['reception_8'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 8, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_8" value="0" <?php echo ( (isset($_POST['reception_8']) && $_POST['reception_8'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 8, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_8" value="n" <?php echo ( (isset($_POST['reception_8']) && $_POST['reception_8'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 8, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Coffie machine operational:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_9" value="1" <?php echo ( (isset($_POST['reception_9']) && $_POST['reception_9'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 9, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_9" value="0" <?php echo ( (isset($_POST['reception_9']) && $_POST['reception_9'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 9, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_9" value="n" <?php echo ( (isset($_POST['reception_9']) && $_POST['reception_9'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 9, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">All lights are on and operational:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_10" value="1" <?php echo ( (isset($_POST['reception_10']) && $_POST['reception_10'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 10, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_10" value="0" <?php echo ( (isset($_POST['reception_10']) && $_POST['reception_10'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 10, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_10" value="n" <?php echo ( (isset($_POST['reception_10']) && $_POST['reception_10'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 10, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Roller shutter up:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_11" value="1" <?php echo ( (isset($_POST['reception_11']) && $_POST['reception_11'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 11, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_11" value="0" <?php echo ( (isset($_POST['reception_11']) && $_POST['reception_11'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 11, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="reception_11" value="n" <?php echo ( (isset($_POST['reception_11']) && $_POST['reception_11'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['reception'], 11, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Specators area <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="spectators" disabled>Populate</a></legend>
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Bins emptied:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spectators_0" value="1" <?php echo ( (isset($_POST['spectators_0']) && $_POST['spectators_0'] == "1") || !empty($premises->checks[0]['spectators']) && substr($premises->checks[0]['spectators'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spectators_0" value="1" <?php echo ( (isset($_POST['spectators_0']) && $_POST['spectators_0'] == "1") || !empty($premises->checks[0]['spectators']) && substr($premises->checks[0]['spectators'], 0, 1) == "1") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="spectators_0" value="n" <?php echo ( (isset($_POST['spectators_0']) && $_POST['spectators_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Bins cleaned:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_1" value="1" <?php echo ( (isset($_POST['spectators_1']) && $_POST['spectators_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_1" value="0" <?php echo ( (isset($_POST['spectators_1']) && $_POST['spectators_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_1" value="n" <?php echo ( (isset($_POST['spectators_1']) && $_POST['spectators_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Tables clean w/ clean water:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_2" value="1" <?php echo ( (isset($_POST['spectators_2']) && $_POST['spectators_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_2" value="0" <?php echo ( (isset($_POST['spectators_2']) && $_POST['spectators_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_2" value="n" <?php echo ( (isset($_POST['spectators_2']) && $_POST['spectators_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Floors cleanded:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_3" value="1" <?php echo ( (isset($_POST['spectators_3']) && $_POST['spectators_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_3" value="0" <?php echo ( (isset($_POST['spectators_3']) && $_POST['spectators_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 3, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_3" value="n" <?php echo ( (isset($_POST['spectators_3']) && $_POST['spectators_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Upstairs radiator switched on <strong(Winter only)</strong>:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_4" value="1" <?php echo ( (isset($_POST['spectators_4']) && $_POST['spectators_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_4" value="0" <?php echo ( (isset($_POST['spectators_4']) && $_POST['spectators_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 4, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_4" value="n" <?php echo ( (isset($_POST['spectators_4']) && $_POST['spectators_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Rental bikes safely stored:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_5" value="1" <?php echo ( (isset($_POST['spectators_5']) && $_POST['spectators_5'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 5, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_5" value="0" <?php echo ( (isset($_POST['spectators_5']) && $_POST['spectators_5'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 5, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_5" value="n" <?php echo ( (isset($_POST['spectators_5']) && $_POST['spectators_5'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 5, 1) == "n") ? "checked": ""?>> N/A</label>
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean and no rubbish:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_6" value="1" <?php echo ( (isset($_POST['spectators_6']) && $_POST['spectators_6'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 6, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_6" value="0" <?php echo ( (isset($_POST['spectators_6']) && $_POST['spectators_6'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 6, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="spectators_6" value="n" <?php echo ( (isset($_POST['spectators_6']) && $_POST['spectators_6'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['spectators'], 6, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
				</fieldset>
			
				<fieldset class="col-md-6 col-sm-12">
					<legend>Toilets <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="toilets" disabled>Populate</a></legend>
			
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Toilets cleaned:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="toilets_0" value="1" <?php echo ( (isset($_POST['toilets_0']) && $_POST['toilets_0'] == "1") || !empty($premises->checks[0]['toilets']) && substr($premises->checks[0]['toilets'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="toilets_0" value="1" <?php echo ( (isset($_POST['toilets_0']) && $_POST['toilets_0'] == "1") || !empty($premises->checks[0]['toilets']) && substr($premises->checks[0]['toilets'], 0, 1) == "1") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="toilets_0" value="n" <?php echo ( (isset($_POST['toilets_0']) && $_POST['toilets_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Toilets anti-bac:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_1" value="1" <?php echo ( (isset($_POST['toilets_1']) && $_POST['toilets_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_1" value="0" <?php echo ( (isset($_POST['toilets_1']) && $_POST['toilets_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_1" value="n" <?php echo ( (isset($_POST['toilets_1']) && $_POST['toilets_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Basins cleaned:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_2" value="1" <?php echo ( (isset($_POST['toilets_2']) && $_POST['toilets_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_2" value="0" <?php echo ( (isset($_POST['toilets_2']) && $_POST['toilets_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_2" value="n" <?php echo ( (isset($_POST['toilets_2']) && $_POST['toilets_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Floors cleaned:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_3" value="1" <?php echo ( (isset($_POST['toilets_3']) && $_POST['toilets_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_3" value="0" <?php echo ( (isset($_POST['toilets_3']) && $_POST['toilets_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_3" value="n" <?php echo ( (isset($_POST['toilets_3']) && $_POST['toilets_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Toilets soap present:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_4" value="1" <?php echo ( (isset($_POST['toilets_4']) && $_POST['toilets_4'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 4, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_4" value="0" <?php echo ( (isset($_POST['toilets_4']) && $_POST['toilets_4'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 4, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_4" value="n" <?php echo ( (isset($_POST['toilets_4']) && $_POST['toilets_4'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 4, 1) == "n") ? "checked": ""?>> N/A</label>
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">3 adequate toilet rolls present:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_5" value="1" <?php echo ( (isset($_POST['toilets_5']) && $_POST['toilets_5'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 5, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_5" value="0" <?php echo ( (isset($_POST['toilets_5']) && $_POST['toilets_5'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 5, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_5" value="n" <?php echo ( (isset($_POST['toilets_5']) && $_POST['toilets_5'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 5, 1) == "n") ? "checked": ""?>> N/A</label>
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Clean and no rubbish:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_6" value="1" <?php echo ( (isset($_POST['toilets_6']) && $_POST['toilets_6'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 6, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_6" value="0" <?php echo ( (isset($_POST['toilets_6']) && $_POST['toilets_6'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 6, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="toilets_6" value="n" <?php echo ( (isset($_POST['toilets_6']) && $_POST['toilets_6'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['toilets'], 6, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
				</fieldset>
			</div>
			<div class="row">
				<fieldset class="col-md-6 col-sm-12">
					<legend>Fire safety <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="fireCheck" disabled>Populate</a></legend>
		
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">All fire exist clear:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="fireChecks_0" value="1" <?php echo ( (isset($_POST['fireChecks_0']) && $_POST['fireChecks_0'] == "1") || !empty($premises->checks[0]['fireChecks']) && substr($premises->checks[0]['fireChecks'], 0, 1) == "1") ? "checked": ""?>> Yes
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="fireChecks_0" value="0" <?php echo ( (isset($_POST['fireChecks_0']) && $_POST['fireChecks_0'] === "0") || !empty($premises->checks[0]['fireChecks']) && substr($premises->checks[0]['fireChecks'], 0, 0) === "0") ? "checked": ""?>> No	
							</label>
							<label class="radio-inline">
								<input type="radio" class="form-control"disabled name="fireChecks_0" value="n" <?php echo ( (isset($_POST['fireChecks_0']) && $_POST['fireChecks_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 0, 1) == "n") ? "checked": ""?>> N/A
							</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Alarm operational:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_1" value="1" <?php echo ( (isset($_POST['fireChecks_1']) && $_POST['fireChecks_1'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_1" value="0" <?php echo ( (isset($_POST['fireChecks_1']) && $_POST['fireChecks_1'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 1, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_1" value="n" <?php echo ( (isset($_POST['fireChecks_1']) && $_POST['fireChecks_1'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Fire alarm operational:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_2" value="1" <?php echo ( (isset($_POST['fireChecks_2']) && $_POST['fireChecks_2'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_2" value="0" <?php echo ( (isset($_POST['fireChecks_2']) && $_POST['fireChecks_2'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 2, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_2" value="n" <?php echo ( (isset($_POST['fireChecks_2']) && $_POST['fireChecks_2'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					<div class="form-group">
						
						<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Check glass on fire alaram unit:</label>
						<div class="col-xs-6 col-ms-6">
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_3" value="1" <?php echo ( (isset($_POST['fireChecks_3']) && $_POST['fireChecks_3'] == "1") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_3" value="0" <?php echo ( (isset($_POST['fireChecks_3']) && $_POST['fireChecks_3'] === "0") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 3, 1) === "0") ? "checked": ""?>> No</label>
							<label class="radio-inline"><input type="radio" class="form-control"disabled name="fireChecks_3" value="n" <?php echo ( (isset($_POST['fireChecks_3']) && $_POST['fireChecks_3'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['fireChecks'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
							
						</div>
					</div>
					
				</fieldset>
			
				<?php if(date('D', strtotime($premises->checks[0]['date'])) == "Thur"):?>
					<fieldset class="col-md-6 col-sm-12">
						<legend>Advance cleaning checks <a href="" id="" class="btn btn-sm btn-default pull-right populate-section" data-section="thursday" disabled>Populate</a></legend>
				
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Park first aid boxes:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="thursday_0" value="1" <?php echo ( (isset($_POST['thursday_0']) && $_POST['thursday_0'] == "1") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 0, 1) == "1") ? "checked": ""?>> Yes
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="thursday_0" value="0" <?php echo ( (isset($_POST['thursday_0']) && $_POST['thursday_0'] === "0") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 0, 1) === "0") ? "checked": ""?>> No	
								</label>
								<label class="radio-inline">
									<input type="radio" class="form-control"disabled name="thursday_0" value="n" <?php echo ( (isset($_POST['thursday_0']) && $_POST['thursday_0'] == "n") || !empty($premises->checks) && substr($premises->checks[0]['thursday'], 0, 1) == "n") ? "checked": ""?>> N/A
								</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Reception first aid boxes:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_1" value="1" <?php echo ( (isset($_POST['thursday_1']) && $_POST['thursday_1'] == "1") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 1, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_1" value="0" <?php echo ( (isset($_POST['thursday_1']) && $_POST['thursday_1'] === "0") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 1, 1) === '0') ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_1" value="n" <?php echo ( (isset($_POST['thursday_1']) && $_POST['thursday_1'] == "n") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 1, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Deep toilet clean:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_2" value="1" <?php echo ( (isset($_POST['thursday_2']) && $_POST['thursday_2'] == "1") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 2, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_2" value="0" <?php echo ( (isset($_POST['thursday_2']) && $_POST['thursday_2'] === "0") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 2, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_2" value="n" <?php echo ( (isset($_POST['thursday_2']) && $_POST['thursday_2'] == "n") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 2, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						<div class="form-group">
							
							<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Toilet sanitary bins emptied and cleaned:</label>
							<div class="col-xs-6 col-ms-6">
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_3" value="1" <?php echo ( (isset($_POST['thursday_3']) && $_POST['thursday_3'] == "1") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 3, 1) == "1") ? "checked": ""?>> Yes</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_3" value="0" <?php echo ( (isset($_POST['thursday_3']) && $_POST['thursday_3'] === "0") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 3, 1) === "0") ? "checked": ""?>> No</label>
								<label class="radio-inline"><input type="radio" class="form-control"disabled name="thursday_3" value="n" <?php echo ( (isset($_POST['thursday_3']) && $_POST['thursday_3'] == "n") || !empty($premises->checks[0]['thursday']) && substr($premises->checks[0]['thursday'], 3, 1) == "n") ? "checked": ""?>> N/A</label>
								
							</div>
						</div>
						
					</fieldset>

				<?php endif;?>	
			</div>
			<div class="row">
				<fieldset class="col-sm-12">
					<legend>Comments</legend>
					<label for="visible" class="col-xs-4 col-ms-6 control-label form-label required">Any comments:</label>
					<div class="col-xs-7 col-ms-6">
						<textarea name="comments" class=" form-control"><?php echo (isset($_POST['comments'])) ? $_POST['comments']: ((isset($premises->checks[0]['comments'])? $premises->checks[0]['comments']: ''));?></textarea>
					</div>


			</div>
		</form>
	</div>
	<div class="col-md-4 col-sm-4">
		<section class="related-content">
			<div class="title">
				<p class="lead">Holiday information</p>
			</div>
			<div class="content">
				<div class="table-responsive">	
					<table class="table table-condensed">
						<tbody>
								<tr><th>Creation date</th><td><?php echo $premises->checks[0]['datetime'];?></td></tr>
								<tr><th>Last updated at</th><td><?php echo ($premises->checks[0]['updated_amount'] == null)? 'Not updated': $premises->checks[0]['last_updated_time'];?></td></tr>
								<tr><th>Last updated by</th><td><?php echo ($premises->checks[0]['last_updated_user'] == null)? 'Not updated': $premises->checks[0]['last_updated_user'];?></td></tr>
								<tr><th>No. of updates</th><td><?php echo ($premises->checks[0]['updated_amount'] == null)? 'Not updated': $premises->checks[0]['updated_amount'];?></td></tr>
						

						</tbody>
					</table>
				</div>
			</div>
		</section>
</div>

</div>
