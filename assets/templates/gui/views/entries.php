<div class="row">
	<div class="sessionChartContainer">
		<p class="lead">Session breakdown</p>
		<?php echo $sessionOverview;?>

	</div>
	<div style="margin: 0 10px;">
		<h2>Entries</h2>

		<?php echo $pagination->body;?>

		<?php echo $pagination->breadcrumb;?>


	</div>
</div>