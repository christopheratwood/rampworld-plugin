<div class="content">
	

	<div class="row">
		<h1><?php echo ucwords(strtolower($holiday->holidays[0]['display_name']));?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fprint&amp;hid=<?php echo $holiday->holidays[0]['holiday_id'];?>" class="btn btn-default mg-10 btn-ms-block">Print timetable</a>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall&amp;hid=<?php echo $holiday->holidays[0]['holiday_id'];?>" class="btn btn-default mg-10 btn-ms-block">Sessions</a>
				<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>

				<?php if(current_user_can('administrator')):?>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=holiday&amp;start_dt=<?php echo $holiday->holidays[0]['start_date'];?>&amp;end_dt=<?php echo$holiday->holidays[0]['end_date'];?>" class="btn btn-default mg-10 btn-ms-block">Generate report</a>
				</div>
				<div class="col-md-3 col-xs-4 col-ms-12 float-right">
					<a class="btn btn-danger mg-10 btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fdelete&amp;hid=<?php echo $_GET['hid'];?>">Permanently delete holiday</a>
				</div>
				<?php else:?>
					</div>
				<?php endif;?>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8" id="holiday-dates">
			<?php if(isset($updated_successfull) && $updated_successfull == true):?>
				<div class="alert alert-success"><strong>Success!</strong> Member details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Member details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['success']) && $_GET['success'] == 'created'):?>
				<div class="alert alert-success"><strong>Success!</strong> New holidays has been created.</div>
			<?php endif?>
			<?php if(isset($_GET['error']) && $_GET['error'] == 'overlap'):?>
				<div class="alert alert-danger"><strong>Could not create holiday!</strong> The holiday dates overlapped with this holiday.</div>
			<?php endif?>
			<form class="form-horizontal" id="editForm" action="#" method="post">

				<div class="form-group">
					
					<label for="name" class="col-xs-3 col-ms-12 control-label form-label required">Display name:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="name" name="display_name" class="form-control "readonly value="<?php echo ucwords(strtolower($holiday->holidays[0]['display_name']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Start date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="start_date" name="start_date" readonly value="<?php echo $holiday->holidays[0]['start_date'];?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
					<div class="col-xs-6 col-ms-12">
						<input type='text' class="form-control" id="end_date" name="end_date" readonly value="<?php echo $holiday->holidays[0]['end_date'];?>" />
					</div>
				</div>
				<div class="form-group">
					
					<label for="visible" class="col-xs-3 col-ms-12 control-label form-label required">Publish:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="visible" name="visible" class="form-control" value="0" disabled <?php echo (($holiday->holidays[0]['visible'] == '0')? 'checked':'');?>> No
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="visible" name="visible" class="form-control" value="1" disabled <?php echo (($holiday->holidays[0]['visible'] == '1')? 'checked':'');?>> Yes
							<input type="hidden" name="update_counter" value="<?php echo $holiday->holidays[0]['updated_amount'];?>">
						</label>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Holiday information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
									<tr><th>Creation date</th><td><?php echo $holiday->holidays[0]['created'];?></td></tr>
									<tr><th>Number of sessions</th><td><?php echo ($holiday->holidays[0]['sessions'] == 0)? 'No sessions set. <a href="'.host.'wp-admin/admin.php?page=holidayship%2Fholidayship.php%2Fholiday%2Fsessions&amp;holiday='.$holiday->holidays[0]["holiday_id"].'">Set here</a>.': $holiday->holidays[0]['sessions'] .' sessions';?></td></tr>
									<tr><th>Number of entries</th><td><?php echo ($holiday->holidays[0]['visible'] == 0)? 'To see statistics, holiday must be public.': $holiday->count;?></td></tr>
									<tr><th>Holiday last updated at</th><td><?php echo ($holiday->holidays[0]['updated_amount'] == null)? 'Not updated': $holiday->holidays[0]['last_updated_time'];?></td></tr>
									<tr><th>Holiday last updated by</th><td><?php echo ($holiday->holidays[0]['updated_amount'] == null)? 'Not updated': $holiday->holidays[0]['last_updated_user'];?></td></tr>
									<tr><th>No. of updates</th><td><?php echo ($holiday->holidays[0]['updated_amount'] == null)? 'Not updated': $holiday->holidays[0]['updated_amount'];?></td></tr>
							

							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	
</div>
