<div class="content">
	<div class="row">
		<div class="col-xs-6 col-ms-12">
			<section class="related-content">
				<div class="title">
					<p class="lead">Search by membership number</p>
				</div>
				<div class="content">
					<div class="form-group col-sm-10 col-xs-12">
						<input type="number" name="" id="membership-number-input" class="form-control entry-lg" autofocus value="<?php echo ((isset($_GET['member_id']) )? $_GET['member_id'] :'');?>">
				
					</div>
					<button class="entry-submit col-sm-2  col-xs-12 pull-right" id="search-member-number">
						<i class="glyphicon glyphicon-arrow-right"></i></button>
				</div>
			</section>
		</div>
		<div class="col-xs-6 col-ms-12">
			<section class="related-content">
				<div class="title">
						<p class="lead">Search by custom properties</p>
					</div>
					<div class="content">
						<div class="form-group col-sm-10 col-xs-12">
							<div class="input-group input-group-lg entry-custom-search" style="width: 100%;">
									<div class="input-group-btn" style="width: 1%;">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;"  data-element="custom-field-1" data-input="custom-input-1">Forename <span class="caret"></span></button>
											<ul class="dropdown-menu">
													<li><a href="#">Forename</a></li>
													<li><a href="#">Surname</a></li>
													<li><a href="#">Date of birth</a></li>
													<li><a href="#">Phone number</a></li>
													<li role="separator" class="divider"></li>
													<li><a href="#">Address line 1</a></li>
													<li><a href="#">Postcode</a></li>
													<li role="separator" class="divider"></li>
													<li><a href="#">Booking number</a></li>
													<li><a href="#">Booking name</a></li>
													<li><a href="#">Booking email</a></li>
											</ul>
											<input type="hidden" id="custom-field-1" value="forename">
									</div><!-- /btn-group -->
									<input type="text" class="form-control" id="custom-input-1" placeholder="Please enter Forename" style="width:100%" value="">
							</div><!-- /input-group -->
							<br>
							<div class="input-group input-group-lg entry-custom-search" style="width: 100%;">
									<div class="input-group-btn" style="width: 1%;">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" margin: 0px;" data-element="custom-field-2" data-input="custom-input-2">Surname <span class="caret"></span></button>
											<ul class="dropdown-menu">
													<li><a href="#">Forename</a></li>
													<li><a href="#">Surname</a></li>
													<li><a href="#">Date of birth</a></li>
													<li><a href="#">Phone number</a></li>
													<li role="separator" class="divider"></li>
													<li><a href="#">Address line 1</a></li>
													<li><a href="#">Postcode</a></li>
													<li role="separator" class="divider"></li>
													<li><a href="#">Booking number</a></li>
													<li><a href="#">Booking name</a></li>
													<li><a href="#">Booking email</a></li>
											</ul>
											<input type="hidden" id="custom-field-2" value="surname">
									</div><!-- /btn-group -->
									<input type="text" class="form-control input-lg" id="custom-input-2" placeholder="Please enter Surname" style="width:100%" value="">
							</div><!-- /input-group -->
						</div>
						<button class="entry-submit col-sm-2 col-xs-12 pull-right" id="search-custom"><i class="glyphicon glyphicon-arrow-right"></i></button>

				</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-9" style="margin-top:20px;">
			<div class="row">
				<div class="col-sm-12">	<p class="lead">Account statuses</p></div>
			
				<div class="status-item col-xs-3">
					<span class="status status-success"></span><p class="status-label">Okay</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-new-member"></span><p class="status-label">Verification</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-booking"></span><p class="status-label">Booking</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-banned"></span><p class="status-label">Banned</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-on-premises"></span><p class="status-label">On premises</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-membership"></span><p class="status-label">Membership</p>
				</div>
				<div class="status-item col-xs-3">
					<span class="status status-notes"></span><p class="status-label">Has notes</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3" style="margin-top:20px;">
			<p class="lead">Actions</p>
			<table class="table">
				<tr><th>Number of members:</th><td><span id="number-of-members">-</span></td></tr>
				<tr><th>Query time: </th><td><span id="query-time">-</span></td></tr>
				<tr><th>Render time: </th><td><span id="render-time">-</span></td></tr>
				<tr><td colspan="2"><a class="btn btn-primary btn-block" id="reset-entry">Reset</a></td></tr>
			</table>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-hover" id="entry-table">
			<thead>
				<tr><th>#</th><th>Forename</th><th>Surname</th><th>Age</th><th>Expertise</th><th>Disipline</th><th>Medical notes</th><th>System notes</th></tr>			
			</thead>
			<tbody id="members">
				<tr><td colspan="8"><p class="lead" style="text-align: center;"><?php echo totalMembersOnPremises;?> members on premises</p></td></tr>
            </tbody>
        </table>
    </div>
		<div class="hidden">
			<input type="radio" value="true" id="reset-on-finish">
</div>