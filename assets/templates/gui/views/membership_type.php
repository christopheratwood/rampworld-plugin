
<div class="content system-dates">
	

	<div class="row">
		<h1>Membership types</h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Ftypes%2Fadd" class="btn btn-default mg-10 btn-ms-block">Add new</a>

				</div>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8">
		<?php if(isset($_GET['errors'])):?>

				<?php if($_GET['errors'] == 'incorrect permissions'):?>
					<div class="alert alert-danger"><strong>Failed!</strong> You do not have the correct permissions.</div>
				<?php elseif($_GET['errors'] == 'couldnt remove'):?>
					<div class="alert alert-danger"><strong>Failed!</strong>Type was not removed.</div>
				<?php endif;?>
			<?php endif;?>
			<?php if(isset($_GET['success'])):?>
				<div class="alert alert-success"><strong>Success!</strong> New Type has been added.</div>
			<?php elseif(isset($_GET['removed'])):?>
				<div class="alert alert-success"><strong>Success!</strong> Type has been removed.</div>
			<?php elseif(isset($updated_successfull) && $updated_successfull == true):?>
				<div class="alert alert-success"><strong>Success!</strong> notes details have been changed.</div>
			<?php elseif(isset($updated_successfull) && $updated_successfull == false):?>
				<div class="alert alert-warning"><strong>Success!</strong> Type has been deleted.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> notes details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			
				<h2>System notes</h2>	
				<?php if(count($mt->types) == 0):?>
					<p >No types set. <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fadd&amp;member=<?php echo $_GET['member'];?>" class="btn btn-link">Add here</a></p>
				<?php else:?>
					<?php foreach($mt->types as $t):?>
						<form class="form-horizontal" id="editForm_<?php echo $t['membership_type_id'];?>" action="#" method="post">
					
						<div class="form-group">
							
							<label for="start_date" class="col-xs-3 col-ms-12 control-label form-label required">Display name:</label>
							<div class="col-xs-6 col-ms-12">
								<input type="text" class="form-control "  id="display_name" readonly name="display_name" value="<?php echo $t['display_name'];?>">
							</div>
						</div>
						<div class="form-group">
							
							<label for="end_date" class="col-xs-3 col-ms-12 control-label form-label required">End date:</label>
							<div class="col-xs-6 col-ms-12">
								<div class='input-group' style="100px">
										<input type='text' class="form-control" readonly name="duration" id='duration'  style="width: 100px" value="<?php echo ($t['duration'] != null) ? $t['duration']: '';?>"/>
										<div class="input-group-btn" style="width: 0%;"><button type="button" class="btn btn-default disabled" style="height: 34px;">weeks</button></div>
								</div>
							
				
							</div>
						</div>
							<div class="form-group" >
								
								<label class="col-xs-3 col-ms-12 control-label form-label">Actions:</label>
								<div class="col-xs-6 col-ms-12">
								<a class="btn btn-warning btn-sm mg-10 btn-ms-block editType" data-type-id="<?php echo $t['membership_type_id'];?>">Change</a>
								<?php if(current_user_can('administrator')):?>
									<a class="btn btn-sm mg-10 btn-danger btn-ms-block" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Ftypes%2Fdelete&amp;mtid=<?php echo $t['membership_type_id'];?>">Remove</a>
								<?php endif;?>
								</div>
							</div>
							<div class="form-group" >
								
								<label class="col-xs-3 col-ms-12 control-label form-label">Metadata:</label>
								<div class="col-xs-6 col-ms-12">
									<table class="table">
										<tbody>
											<tr><th>Created at</th><td><?php echo ($t['created'] != "")?$t['created']: 'Date not set';?></td></tr>
											<tr><th>Last updated at</th><td><?php echo ($t['last_updated_time'] != "")?$t['last_updated_time']: 'Not updated';?></td></tr>
											<tr><th>Last updated by</th><td><?php echo ($t['last_updated_user'] != "")?$t['last_updated_user']: 'Not updated';?></td></tr>
											<tr><th>Number of updates</th><td><?php echo ($t['updated_amount']) != ""?$t['updated_amount']: 'Not updated';?></td></tr>
											<tr><th>Number of passes used</th><td><?php echo ($t['Cnt']) != ""?$t['Cnt']: '0';?></td></tr>
										</tbody>
									</table>
					
								</div>
							</div>

							<input type="hidden" name="mtid" value="<?php echo $t['membership_type_id'];?>">
							<input type="hidden" name="updated_amount" value="<?php echo $t['updated_amount'];?>">
							<hr>
						</form>
					<?php endforeach;?>
				<?php endif;?>
		</div>
		
</div>