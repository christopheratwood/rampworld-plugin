<?php $types=array(1 => 'Ban',2 => 'Temporary ban',3 => 'Anti-social', 4 => 'Other' );?>
<div class="content">
	<div class="row">
		<h1><?php echo ucwords(strtolower($member->members[0]['forename'])) .' '. ucwords(strtolower($member->members[0]['surname']));?></h1>
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-9 col-xs-8 col-ms-12">
				<p class="lead" style="margin-bottom:0; display: inline-block;">Actions</p>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=<?php echo $_GET['member'];?>" class="btn mg-10 btn-default btn-ms-block">Reissue email</a>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fview%2Fall&amp;member=<?php echo $_GET['member'];?>" class="btn mg-10 btn-ms-block btn-default">Member notes</a>
				<a class="btn btn-warning mg-10 btn-ms-block" id="editDetails">Change details</a>
				<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fadd&amp;member=<?php echo $_GET['member'];?>" class="btn mg-10 btn-ms-block btn-default">Add membership pass</a>
				<?php if(current_user_can('administrator')):?>
					<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=member&amp;member=<?php echo $member->members[0]['member_id'];?>" class="btn btn-default mg-10 btn-ms-block">Generate report</a>
					</div>
					<div class="col-md-3 col-xs-4 col-ms-12 float-right">
						<a data-m="<?php echo $member->members[0]['member_id'];?>" class="btn btn-danger mg-10 btn-ms-block" id="deleteMember">Permanently delete member</a>
					</div>

				<?php else:?>
					</div>
				<?php endif;?>
		
			</div>
		</div>

		<div class="col-md-8 col-sm-8">
			<?php if(isset($updated_successfull) && $updated_successfull == true && !is_array($updated_successfull)):?>
				<div class="alert alert-success"><strong>Success!</strong> Member details have been changed.</div>
			<?php elseif(isset($updated_successfull)):?>
				<div class="alert alert-danger"><strong>Error!</strong> Member details did not pass validation.<br>
					<?php foreach($validation->errors as $error){
						echo '<p>'. $error.'</p>';
					}?>
				</div>
			<?php endif?>
			<?php if(isset($_GET['error'])):?>
				<div class="alert alert-danger"><strong>Error!</strong> An error has occured. <?php echo $_GET['error'];?></div>
			<?php endif;?>
			<form class="form-horizontal" id="editForm" action="#" method="post">
				<h2>Personal information</h2>
				<div class="form-group">
					<label for="forename" class="col-xs-3 col-ms-12 control-label form-label required">Forename:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="forename" name="forename" class="form-control "readonly value="<?php echo ucwords(strtolower($member->members[0]['forename']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="surname" class="col-xs-3 col-ms-12 control-label form-label required">Surname:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="surname" name="surname" class="form-control" readonly value="<?php echo ucwords(strtolower($member->members[0]['surname']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="gender" class="col-xs-3 col-ms-12 control-label form-label required">Gender:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="gender" name="gender" class="form-control" value="m" disabled <?php echo (($member->members[0]['gender'] == 'm')? 'checked':'');?>> Male
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="gender" name="gender" class="form-control" value="f" disabled <?php echo (($member->members[0]['gender'] == 'f')? 'checked':'');?>> Female
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="gender" name="gender" class="form-control" value="o" disabled <?php echo (($member->members[0]['gender'] == 'o')? 'checked':'');?>> Other
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-xs-3 col-ms-12 control-label form-label required" data-toggle="popover" data-html="true" data-content="<a href='mailto:<?php echo $member->members[0]['email'];?>'>Click to email</a>">Email:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="email" id="email" name="email" class="form-control" readonly value="<?php echo $member->members[0]['email'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="number" class="col-xs-3 col-ms-12 control-label form-label required">Number:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="number" id="number" name="number" class="form-control" readonly value="<?php echo $member->members[0]['number'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="dob" class="col-xs-3 col-ms-12 control-label form-label required">Date of Birth:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="date" id="dob" name="dob" class="form-control" readonly value="<?php echo $member->members[0]['dob'];?>">
					</div>
				</div>
				<h2>Address</h2>
				<div class="form-group">
					
					<label for="address_line_one" class="col-xs-3 col-ms-12 control-label form-label required">Line one:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="address_line_one" name="address_line_one" class="form-control "readonly value="<?php echo ucwords(strtolower($member->members[0]['address_line_one']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="address_line_two" class="col-xs-3 col-ms-12 control-label form-label">Line two:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="address_line_two" name="address_line_two" class="form-control" readonly value="<?php echo $member->members[0]['address_line_two'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="address_city" class="col-xs-3 col-ms-12 control-label form-label">City:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="address_city" name="address_city" class="form-control" readonly value="<?php echo $member->members[0]['address_city'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="address_county" class="col-xs-3 col-ms-12 control-label form-label">Count:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="address_county" name="address_county" class="form-control" readonly value="<?php echo $member->members[0]['address_county'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="address_postcode" class="col-xs-3 col-ms-12 control-label form-label required">Postcode:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="address_postcode" name="address_postcode" class="form-control" readonly value="<?php echo $member->members[0]['address_postcode'];?>">
					</div>
				</div>
				<h2>Activity Information</h2>
				<div class="form-group">
					<label for="expertise" class="col-xs-3 col-ms-12 control-label form-label required">Expertise:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="0" disabled <?php echo (($member->members[0]['expertise'] == '0')? 'checked':'');?>> Beginner
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="1" disabled <?php echo (($member->members[0]['expertise'] == '1')? 'checked':'');?>> Novice
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="2" disabled <?php echo (($member->members[0]['expertise'] == '2')? 'checked':'');?>> Experienced
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="3" disabled <?php echo (($member->members[0]['expertise'] == '3')? 'checked':'');?>> Advanced
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="4" disabled <?php echo (($member->members[0]['expertise'] == 4)? 'checked':'');?>> Expert
						</label>
						<label class="radio-inline radio-ms-block">
							<input type="radio" id="expertise" class="form-control" name="expertise" value="5" disabled <?php echo (($member->members[0]['expertise'] == '5')? 'checked':'');?>> Professional
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="discipline" class="col-xs-3 col-ms-12 control-label form-label required">Disciplines:</label>
					<div class="col-xs-6 col-ms-12">
						<label class="checkbox-inline checkbox-ms-block checkbox-ms-block">
							<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="BMX" disabled <?php echo ((preg_match('/BMX/', $member->members[0]['discipline']))? 'checked':'');?>> BMX
						</label>
						<label class="radio-inline radio-ms-block">
						<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="SMX" disabled <?php echo ((preg_match('/SMX/', $member->members[0]['discipline']))? 'checked':'');?>> SMX
						</label>
						<label class="checkbox-inline checkbox-ms-block checkbox-ms-block">
							<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="MTB" disabled <?php echo ((preg_match('/MTB/', $member->members[0]['discipline']))? 'checked':'');?>> MTB
						</label>
						<label class="checkbox-inline checkbox-ms-block checkbox-ms-block">
							<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="skateboard" disabled <?php echo ((preg_match('/skateboard/', $member->members[0]['discipline']))? 'checked':'');?>> Skateboard
						</label>
						<label class="checkbox-inline checkbox-ms-block checkbox-ms-block">
								<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="inline" disabled <?php echo ((preg_match('/inline/', $member->members[0]['discipline']))? 'checked':'');?>> Inline
						</label>
						<label class="checkbox-inline checkbox-ms-block checkbox-ms-block">
							<input type="checkbox" id="discipline" class="form-control" name="discipline[]" value="spectator" disabled <?php echo ((preg_match('/spectator/', $member->members[0]['discipline']))? 'checked':'');?>> Spectator
						</label>
					</div>
				</div>
				
				<h2>Parental Information</h2>
				<div class="form-group">
					
					<label for="consent_name" class="col-xs-3 col-ms-12 control-label form-label required">Parental name:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="consent_name" name="consent_name" class="form-control "readonly value="<?php echo ucwords(strtolower((isset($member->consent[0]['name'])?$member->consent[0]['name']: '')));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="consent_email" class="col-xs-3 col-ms-12 control-label form-label required">Parental email:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="email" id="consent_email" name="consent_email" class="form-control "readonly value="<?php echo strtolower((isset($member->consent[0]['email'])?$member->consent[0]['email']: ''));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="consent_number" class="col-xs-3 col-ms-12 control-label form-label required">Parental phone number:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="consent_number" name="consent_number" class="form-control "readonly value="<?php echo(isset($member->consent[0]['number'])?$member->consent[0]['number']: '');?>">
					</div>
				</div>
				<div class="form-group">
					<label for="consent_address_one" class="col-xs-3 col-ms-12 control-label form-label required">Parental Address line 1:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="consent_address_one" name="consent_address_one" class="form-control "readonly value="<?php echo(isset($member->consent[0]['address_line_one'])?$member->consent[0]['address_line_one']: '');?>">
					</div>
				</div>
				<div class="form-group">
					<label for="dob" class="col-xs-3 col-ms-12 control-label form-label required">Parental postcode:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="consent_postcode" name="consent_postcode" class="form-control "readonly value="<?php echo(isset($member->consent[0]['address_postcode'])?$member->consent[0]['address_postcode']: '');?>">
						<input type="hidden" name="cid" value="<?php echo $member->members[0]['consent_id'];?>">
					</div>
				</div>
				<h2>Emergency Information</h2>
				<div class="form-group">
					
					<label for="emergency_name_one" class="col-xs-3 col-ms-12 control-label form-label required">Emergency name one:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="emergency_name_one" name="emergency_name_one" class="form-control "readonly value="<?php echo ucwords(strtolower($member->emergency[0]['name']));?>">
					</div>
				</div>
				<div class="form-group">
					<label for="emergency_number_one" class="col-xs-3 col-ms-12 control-label form-label required">Emergency number one:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="number" id="emergency_number_one" name="emergency_number_one" class="form-control "readonly value="<?php echo $member->emergency[0]['emergency_number'];?>">
						<input type="hidden" name="eoid" value="<?php echo $member->emergency[0]['emergency_id'];?>">
					</div>
				</div>
				<div class="form-group">
					<label for="emergency_name_two" class="col-xs-3 col-ms-12 control-label form-label">Emergency name two:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="emergency_name_two" name="emergency_name_two" class="form-control "readonly value="<?php echo(isset($member->emergency[1]['name'])?$member->emergency[1]['name']: '');?>">

					</div>
				</div>
				<div class="form-group">
					<label for="emergency_number_two" class="col-xs-3 col-ms-12 control-label form-label">Emergency number two:</label>
					<div class="col-xs-6 col-ms-12">
						<input type="text" id="emergency_number_two" name="emergency_number_two" class="form-control "readonly value="<?php echo(isset($member->emergency[1]['emergency_number'])?$member->emergency[1]['emergency_number']: '');?>">
						<input type="hidden" name="etid" value="<?php echo(isset($member->emergency[1]['emergency_id'])?$member->emergency[1]['emergency_id']: '0');?>">
						<input type="hidden" name="update_counter" value="<?php echo $member->members[0]['updated_amount'];?>">
					</div>
				</div>
				<h2>Medical Notes</h2>
				<div class="form-group">
					
					<label for="medical_notes" class="col-xs-3 col-ms-12 control-label form-label">Medical Notes:</label>
					<div class="col-xs-6 col-ms-12">
						<textarea type="text" id="medical_notes" name="medical_notes" class="form-control " readonly><?php echo $member->members[0]['medical_notes'];?></textarea>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4 col-sm-4">
			<section class="related-content">
				<div class="title">
					<p class="lead">Quick actions</p>
				</div>
				<div class="content">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th>Type</th>
									<th>Information</th>
								</thead>
								<tbody>
									<?php
										echo '<tr><td>Phone number</td><td><a href="tel:'.$member->members[0]['number'].'">'.$member->members[0]['number'].'</a></td><tr>';
										echo '<tr><td>Email </td><td><a href="mailto:'.strtolower($member->members[0]['email']).'">'.strtolower($member->members[0]['email']).'</a></td><tr>';
										echo (isset($member->consent[0]['email'])) ?'<tr><td>Consent email</td><td><strong>'.$member->consent[0]['name'].'</strong><br><a href="mailto:'.strtolower($member->consent[0]['email']).'">'.strtolower($member->consent[0]['email']).'</a></td><tr>':'';
										echo (isset($member->consent[0]['number'])) ?'<tr><td>Consent phone number</td><td><strong>'.$member->consent[0]['name'].'</strong><br><a href="mailto:'.$member->consent[0]['number'].'">'.$member->consent[0]['number'].'</a></td><tr>':'';
										echo '<tr><td>Emergency phone number</td><td><strong>'.$member->emergency[0]['name'].'</strong><br><a href="tel:'.$member->emergency[0]['emergency_number'].'">'.$member->emergency[0]['emergency_number'].'</a></td><tr>';
										echo  (isset($member->emergency[1]['emergency_number'])) ?'<tr><td>Emergency phone number</td><td><strong>'.$member->emergency[1]['name'].'</strong><br><a href="tel:'.$member->emergency[1]['emergency_number'].'">'.$member->emergency[1]['emergency_number'].'</a></td><tr>':'';

									?>
							</tbody>
						</table>
					</div>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">System notes <?php echo ((count($note->notes) > 0) ? '('.count($note->notes).')' : '');?></p>
				</div>
				<div class="content">
					<?php
						// generate 
						if(count($note->notes) > 0):?>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed">
									<thead>
										<tr>
											<th>Type</th>
											<th>Date</th>
											<th>Content</th>
										</thead>
										<tbody>
									<?php $limit = (count($note->notes) > 5) ? 5:count($note->notes);
									for($i = 0; $i < ($limit); $i++){
										if(strlen($note->notes[$i]['comments']) >= 20) {
											preg_match('/^.{0,20}(?:.*?)\b/iu', $note->notes[$i]['comments'], $matches);
											$new_string = $matches[0];
										} else {
											$new_string = $note->notes[$i]['comments'];
										}
										$comment = ($new_string === $note->notes[$i]['comments']) ? $note->notes[$i]['comments'] : $new_string . '&hellip;';

										echo '<tr><td>'.$types[$note->notes[$i]['type']].'<td>'.$note->notes[$i]['start_date'].' - '.$note->notes[$i]['end_date'].'</td><td>'.$comment.'</td></tr>';
									}?>
									</tbody>
								</table>
							</div>
							
						<?php else:?>
							<small>No system notes</small>
						<?php endif;?>
						<div class="col-ms-12 text-right">
							<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fnotes%2Fview%2Fall&amp;member=<?php echo $_GET['member'];?>" class="btn btn-sm btn-default">Add/ view all</a>
						</div>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Recent visits <?php echo ((count($entries->entries) > 0) ? '('.count($entries->entries).')' : '');?></p>
				</div>
				<div class="content">
					<?php
						// generate 
						if(count($entries->entries) > 0):?>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed">
									<thead>
										<tr>
											<th>#</th>
											<th>Date</th>
											<th>Entry time</th>
											<th>Exit time</th>
											<th>View</th>
										</thead>
										<tbody>
									<?php $limit = (count($entries->entries) > 5) ? 5:count($entries->entries);
									for($i = 0; $i < ($limit); $i++){
										echo '<tr><td>'.$entries->entries[$i]['entry_id'].'</td><td>'.$entries->entries[$i]['entry_date'].'</td><td>'.$entries->entries[$i]['entry_time'].'</td><td>'.$entries->entries[$i]['exit_time'].'</td><td><a href="#" class="btn btn-sm btn-default">View</a></td></tr>';
									}?>
									</tbody>
								</table>
							</div>
							<?php if(count($entries->entries) > 5):?>
								<div class="col-ms-12 text-right">
									<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&amp;p=1&amp;limit=999&amp;membership_id=<?php echo $member->members[0]['member_id'];?>&amp;start_dt=2017-01-01T00:00&amp;end_dt=<?php echo date('Y-m-d\TH:i');?>" class="btn btn-sm btn-default">View all</a>
								</div>

							<?php endif;?>
						<?php else:?>
							<small>No recent visits</small>
						<?php endif;?>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Recent passes <?php echo ((count($pm->memberships) > 0) ? '('.count($pm->memberships).')' : '');?></p>
				</div>
				<div class="content">
					<?php
						// generate 
						if(count($pm->memberships) > 0):?>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed">
									<thead>
										<tr>
											<th>#</th>
											<th>Type</th>
											<th>Date</th>
											<th>Times</th>
											<th>View</th>
										</thead>
										<tbody>
									<?php $limit = (count($pm->memberships) > 5) ? 5:count($pm->memberships);
									for($i = 0; $i < ($limit); $i++){
										echo '<tr><td>'.$pm->memberships[$i]['paid_membership_id'].'</td><td>'.$pm->memberships[$i]['display_name'].'</td><td>'.$pm->memberships[$i]['start_date'].'</td><td>'.$pm->memberships[$i]['end_date'].'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fsingle&pmid='.$pm->memberships[0]['paid_membership_id'].'" class="btn btn-sm btn-default">View</a></td></tr>';
									}?>
									</tbody>
								</table>
							</div>
							<div class="col-ms-12 text-right">
									<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall&amp;p=1&amp;membership_id=<?php echo $member->members[0]['member_id'];?>" class="btn btn-sm btn-default">View all</a>
								</div>
						<?php else:?>
							<small>No recent passes.</small>
						<?php endif;?>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Recent bookings <?php echo ((count($bookings->bookings) > 0) ? '('.count($bookings->bookings).')' : '');?></p>
				</div>
				<div class="content">
					<?php
						// generate 
						if(count($bookings->bookings) > 0):?>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed">
									<thead>
										<tr>
											<th>#</th>
											<th>Type</th>
											<th>Date</th>
											<th>Times</th>
											<th>View</th>
										</thead>
										<tbody>
									<?php $limit = (count($bookings->bookings) > 5) ? 5:count($bookings->bookings);
									for($i = 0; $i < ($limit); $i++){
										echo '<tr><td>'.$bookings->bookings[$i]['id'].'</td><td>'.(($bookings->bookings[$i]['isPass'] == "true")? 'Pass': 'Session').'</td><td>'.$bookings->bookings[$i]['date'].'</td><td>'.$bookings->bookings[$i]['session'].'</td><td><a href="'. host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&bid='.$bookings->bookings[$i]['id'].'" class="btn btn-sm btn-default">View</a></td></tr>';
									}?>
									</tbody>
								</table>
							</div>
							<?php if(count($bookings->bookings) > 5):?>
								<div class="col-ms-12 text-right">
									<a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&amp;p=1&amp;search_field=member+id&amp;search_query=<?php echo $member->members[0]['member_id'];?>" class="btn btn-sm btn-default">View all</a>

								</div>
							<?php endif;?>
						<?php else:?>
							<small>No recent bookings.</small>
						<?php endif;?>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Recent purchases <?php echo ((count($purchases->purchases) > 0) ? '('.count($purchases->purchases).')' : '');?></p>
				</div>
				<div class="content">
					<?php
						// generate 
						if(count($purchases->purchases) > 0):?>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed">
									<thead>
										<tr>
											<th>#</th>
											<th>Type</th>
											<th>Date</th>
											<th>Times</th>
											<th>View</th>
										</thead>
										<tbody>
									<?php $limit = (count($purchases->purchases) > 5) ? 5:count($purchases->purchases);
									for($i = 0; $i < ($limit); $i++){
										echo '<tr><td>'.$purchases->purchases[$i]['id'].'</td><td>'.(($bookings->bookings[$i]['isPass'] == "true")? 'Pass': 'Session').'</td><td>'.$bookings->bookings[$i]['date'].'</td><td>'.$bookings->bookings[$i]['session'].'</td><td><a href="#" class="btn btn-sm btn-default">View</a></td></tr>';
									}?>
									</tbody>
								</table>
							</div>
						<?php else:?>
							<small>No recent purchases.</small>
						<?php endif;?>
				</div>
			</section>
			<section class="related-content">
				<div class="title">
					<p class="lead">Account information</p>
				</div>
				<div class="content">
					<div class="table-responsive">	
						<table class="table table-condensed">
							<tbody>
								<tr><th>Membership number</th><td><?php echo $member->members[0]['member_id'];?></td></tr>
								<tr><th>Registered on</th><td><?php echo $member->members[0]['registered'];?></td></tr>	
								<tr><th>Terms and Conditions</th><td><a href="<?php echo host. '/wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fterms&tandcid='.$terms->terms[0]['version_number'];?>"><?php echo $terms->terms[0]['version_number'];?></a></td></tr>
								<tr><th>Number of entries</th><td><?php echo count($entries->entries);?></td></tr>		
								<tr><th>Number of bookings</th><td><?php echo count($bookings->bookings);?></td></tr>	
								<tr><th>Account last updated at</th><td><?php echo (($member->members[0]['last_updated_time'] != '')? $member->members[0]['last_updated_time']: 'Not updated');?></td></tr>
								<tr><th>Account last updated by</th><td><?php echo (($member->members[0]['last_updated_user'] != '')? ucwords(strtolower($member->members[0]['last_updated_user'])): 'Not updated');?></td></tr>
								<tr><th>No. of updates</th><td><?php echo ($member->members[0]['updated_amount'] == '') ? 'Not updated': $member->members[0]['updated_amount'];?></td></tr>

							</tbody>
						</table>
					</div>
				</div>
			</section>
	</div>
	
</div>
<div class="modal fade" id="deleteEntryModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="text-danger">Warning!</span> You are about to permanently delete <?php echo $member->members[0]['forename'] .' '.$member->members[0]['surname'],' #'.$member->members[0]['member_id'];?></h5>
        
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" id="warning-notice">
					<strong>Please complete the following checklist before continuing</strong>
				</div>
				<form id="deleteForm">
					<div class="form-group">
						<div class="checkbox">

							<label>
									<input type="checkbox" id="checked_member"> You have ensured you are deleting the correct member. Please check multiple times.
							</label>
						</div>
						<div class="checkbox">
							<label>
									<input type="checkbox" id="notified_member"> You have notified the member you are terminating their account either via email, phone, or in person.
							</label>
						</div>
						<div class="checkbox">
							<label>
									<input type="checkbox" id="notified_administation"> You have notified administation of your action.
							</label>
						</div>
					</div>
				</form>
				<div id="countdown"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" disabled id="removeMemberConfirm">Please complete form.</button>
      </div>
    </div>
  </div>
</div>