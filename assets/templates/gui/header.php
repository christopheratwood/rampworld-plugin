<?php if(array_key_exists("openingMessage__ID", get_defined_constants(true)['user'])):?>
    <div class="top membership" data-message-id="<?php echo openingMessage__ID;?>" data-message="<?php echo openingMessage__message;?>" data-message-member="<?php echo openingMessage__member;?>" data-message-date="<?php echo openingMessage__date;?>">
<?php else:?>
    
    <div class="top membership">
<?php endif;?>
	<div class="checks">
		<p class="check_title">Checks</p>
		<div class="check_item">
			<p class="check_sub_title">Opening</p>
			<?php if (!openingCheckCompleted) :?>
                <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/opening&date=<?php echo date('Y-m-d');?>" class="check_button warning animated"><i class="glyphicon glyphicon-remove"></i></a>
            <?php else :?>
                <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/opening&date=<?php echo date('Y-m-d');?>" class="check_button pass"><i class="glyphicon glyphicon-ok"></i></a>
            <?php endif; ?>
        </div><div class="check_item">
            <p class="check_sub_title">Closing</p>
            <?php if (!closingCheckCompleted) :?>
                <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/closing&date=<?php echo date('Y-m-d');?>" class="check_button warning animated"><i class="glyphicon glyphicon-remove"></i></a>
            <?php else :?>
                <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/closing&date=<?php echo date('Y-m-d');?>" class="check_button pass"><i class="glyphicon glyphicon-ok"></i></a>
            <?php endif ?>
        </div>
    </div>
    <div class="logo hidden-xs hidden-sm"><img src="<?php echo host;?>wp-content/uploads/2016/06/footer_logo.png"></div>
    <span class="membership_entry hidden-xs hidden-sm"><?php echo $page_name;?></span>
    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/entries%2Fview%2Fall">
        <div id="paidCounter" class="small">
            <p class="lead" id="passes_txt"></p>
            <p class="lead" id="sessions_txt"></p>
        </div>
        <input type="hidden" id="paid-pass" value="<?php echo totalPaidPassesToday;?>">
        <input type="hidden" id="paid-sesh" value="<?php echo totalPaidSessionsToday;?>">
        </a>
    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/entries%2Fview%2Fall">
        <div id="entryCounter" class="small">
            <p class="lead" id="number"></p>
            <p class="lead" id="percentage"></p>
        </div>
        <input type="hidden" id="entry-percentage" value="<?php echo ((totalMembersOnPremises / 200 ));?>">
        <input type="hidden" id="entry-amount" value="<?php echo round(totalMembersOnPremises);?>">
        </a>
</div>

<div class="banner <?php echo (isHoliday == '1') ? 'holiday': 'normal';?>">
    <p><?php echo (isHoliday == '1') ? 'Holiday Timetable': 'Usual Timetable';?></p>
</div>
<?php if (openingCheckWarning) :?>
 <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/opening&date=<?php echo date('Y-m-d');?>">
    <div class="banner danger">
        <p>Please complete opening checks!</p>
    </div>
</a>
<?php endif;?>
<?php if (closingCheckWarning) :?>
    <a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership/membership.php/premises-checks/view/closing&date=<?php echo date('Y-m-d');?>">
        <div class="banner danger">
            <p>Please complete closing checks!</p>
        </div>
    </a>
<?php endif;?>

<?php

switch ($menu_name) {
    case 'main':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Entry</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                    <li class="<?php echo ($menu_name == 'entry-listing')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership/membership.php/entries%2Fview%2Fall';?>">Entries<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                
                    <li class="<?php echo ($menu_name == 'members')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                    
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/members/view/all';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                    </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'entries':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Entries</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>

                    <li class="<?php echo ($sub_name == 'view all')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">View all</a></li>
                    
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/members/view/all';?>">Members</a></li>

                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'booking':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Bookings</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                    <li class="<?php echo ($menu_name == 'view')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">View all<?php echo ($menu_name == 'view')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'report':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Reports</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Report templates <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li <?php echo (($report == 'holiday') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall';?>">Holiday</a></li>
                        <li <?php echo (($report == 'member') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Member</a></li>
                        <li <?php echo (($report == 'paid') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%memberships';?>">Paid membership</a></li>
                        <li <?php echo (($report == 'statistics') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fstatistics';?>">Statistics</a></li>
                        <li <?php echo (($report == 'tandc') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports%2Fterms-and-conditions';?>">Terms and conditions</a></li>
                        <li <?php echo (($report == 'time') ? 'class="active"':'');?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Time duration</a></li>
                        </ul>
                    </li>
                
                
                    

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">Booking</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'members':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Members</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>

                    <li class="<?php echo ($sub_menu == 'view_all')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">View all</a></li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                        <li><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'stats':
            ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Statistics</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>

                        <li class="<?php echo ($sub_menu == 'view_all')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Freports';?>">Reports</a></li>
                        
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                            <li><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                        </li>
                        <?php if (current_user_can('administrator')) :?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                                </ul>
                            </li>
                        <?php endif;?>
                    </ul>
                    
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'holiday':
            ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Holiday</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                        <li class="<?php echo ($sub_menu == 'add')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fcreate';?>">Add</a></li>
                        <li class="<?php echo ($sub_menu == 'view')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2Fall';?>">View</a></li>
                        
                        
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                            <li><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                        </li>
                        <?php if (current_user_can('administrator')) :?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                                </ul>
                            </li>
                        <?php endif;?>
                    </ul>
                    
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'sessions':
            ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sessions</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                        <li class="<?php echo ($sub_menu == 'add')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fcreate';?>">Add</a></li>
                        <li class="<?php echo ($sub_menu == 'view')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2Fall';?>">View</a></li>
                        <li class="<?php echo ($sub_menu == 'print')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fprint'.(isset($_GET['hid'])? '&hid='.$_GET['hid']:'') ;?>">Print</a></li>
                        
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                            <li><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                        </li>
                        <?php if (current_user_can('administrator')) :?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li>
                                </ul>
                            </li>
                        <?php endif;?>
                    </ul>
                    
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'premises':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall">Premises checks</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home</a></li>
                    <li <?php echo ($sub_menu == 'view')?'class="active"':'';?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">View</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">Bookings</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;
    case 'tandc':?>
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Terms and Conditions</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home</a></li>
                <li <?php echo ($sub_menu == 'view')?'class="active"':'';?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">View</a></li>
                <li <?php echo ($sub_menu == 'add')?'class="active"':'';?>><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fadd';?>">Add</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">Bookings</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                    
                    </ul>
                </li>
                <?php if (current_user_can('administrator')) :?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li>
                        </ul>
                    </li>
                <?php endif;?>
            </ul>
            
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        </nav>
        <?php
     break;
    case 'notes':
            ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Session notes</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                        <li class="<?php echo ($sub_menu == 'add')? 'active':'';?> "><a>Add</a></li>
                        <li class="<?php echo ($sub_menu == 'view')? 'active':'';?> "><a>View</a></li>
                        
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">Bookings</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2fall';?>">Events</a></li>
                        </ul>
                        </li>
                        <?php if (current_user_can('administrator')) :?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li class="active"><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li>
                                    <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                                </ul>
                            </li>
                        <?php endif;?>
                    </ul>
                    
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        <?php
        break;?>
        <?php case 'memberships':
            ?>
            <nav class="navbar navbar-default">
            <div class="container-fluid">

                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Passes</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
                    <li class="<?php echo ($sub_menu == 'add')? 'active':'';?> "><a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fadd">Add</a></li>
                    <li class="<?php echo ($sub_menu == 'view')? 'active':'';?> "><a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall">View</a></li>
                    
                    <li class="<?php echo ($sub_menu == 'types')? 'active':'';?> "><a href="<?php echo host;?>wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Ftypes%2Fview%2Fall">Types</a></li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall';?>">Bookings</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall';?>">Entries</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall';?>">Members</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                            <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="active"><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <?php break;

    case 'events':?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Events</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?php echo ($menu_name == 'main')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=membership_entry';?>">Home<?php echo ($menu_name == 'main')? ' <span class="sr-only">(current)</span>':'';?></a></li>
               
                    <li class="<?php echo ($sub_menu == 'view')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall';?>">View all</a></li>
                    <li class="<?php echo ($sub_menu == 'add')? 'active':'';?> "><a href="<?php echo host .'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fadd';?>">Add</a></li>
                    
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Other pages <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/bookings/view/all';?>">Booking</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/members/view/all';?>">Members</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fsessions%2Fview%2fall';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fholidays%2Fview%2fall';?>">Holidays</a></li>
                        
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fall';?>">Premises Checks</a></li>
                        <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fpaid-memberships%2Fview%2Fall';?>">Passes</a></li>
                    </ul>
                    </li>
                    <?php if (current_user_can('administrator')) :?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/Statistics';?>">Statistics</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php%2Freports';?>">Reports</a></li>
                                <li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/settings';?>">Settings</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership/membership.php/sessions/view/all';?>">Sessions</a></li><li><a href="<?php echo host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fterms-and-conditions%2Fview%2Fall';?>">Terms and conditions</a></li>
                            </ul>
                        </li>
                    <?php endif;?>
                </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <?php
        break;
}?>