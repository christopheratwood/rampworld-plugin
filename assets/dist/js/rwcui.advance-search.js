define(['datepicker'], function () {
    var roots = {
        live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
        dev: '//127.0.0.1/wp-admin/admin.php'
    };
    var init = function () {
        $('.date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#advance-search-btn').off().on('click', function (e) {
            e.preventDefault();
            var membership_id = (($('#membership_id').val() !== undefined && $('#membership_id').val().length > 0) ? $('#membership_id').val().replace(/, /g, '|').replace(/,/g, '|') : null);
            var forename = (($('#forename').val() !== undefined && $('#forename').val().length > 0) ? $('#forename').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var surname = (($('#surname').val() !== undefined && $('#surname').val().length > 0) ? $('#surname').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var start_age = (($('#start_age').val() !== undefined && $('#start_age').val().length > 0) ? $('#start_age').val() : null);
            var end_age = (($('#end_age').val() !== undefined && $('#end_age').val().length > 0) ? $('#end_age').val() : null);
            var gender = (($('#genders:checked').val() !== undefined && $('#genders:checked').val().length > 0) ? $('#genders:checked').map(function () { return this.value; }).get().join('|') : null);
            var dob = (($('#dob').val() !== undefined && $('#dob').val().length > 0) ? $('#dob').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var email = (($('#email').val() !== undefined && $('#email').val().length > 0) ? $('#email').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var number = (($('#form_number').val() !== undefined && $('#form_number').val().length > 0) ? $('#form_number').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var address_line_one = (($('#address_line_one').val() !== undefined && $('#address_line_one').val().length > 0) ? $('#address_line_one').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var address_line_two = (($('#address_line_two').val() !== undefined && $('#address_line_two').val().length > 0) ? $('#address_line_two').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var address_county = (($('#address_county').val() !== undefined && $('#address_county').val().length > 0) ? $('#address_county').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var address_city = (($('#address_city').val() !== undefined && $('#address_city').val().length > 0) ? $('#address_city').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var address_postcode = (($('#address_postcode').val() !== undefined && $('#address_postcode').val().length > 0) ? $('#address_postcode').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var registered_start_date = (($('#start_date').val() !== undefined && $('#start_date').val().length > 0) ? $('#start_date').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var registered_end_date = (($('#end_date').val() !== undefined && $('#end_date').val().length > 0) ? $('#end_date').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var report_start = (($('#report_start').val() !== undefined && $('#report_start').val().length > 0) ? $('#report_start').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var report_end = (($('#report_end').val() !== undefined && $('#report_end').val().length > 0) ? $('#report_end').val().replace(/, /, '|').replace(/,/g, '|') : null);
            var age_groups = (($('#age_groups:checked').val() !== undefined && $('#age_groups:checked').val().length > 0) ? $('#age_groups:checked').map(function () { return this.value; }).get().join('|') : null);
            var disciplines = (($('#disciplines:checked').val() !== undefined && $('#disciplines:checked').val().length > 0) ? $('#disciplines:checked').map(function () { return this.value; }).get().join('|') : null);
            var expertise = (($('#expertise:checked').val() !== undefined && $('#expertise:checked').val().length > 0) ? $('#expertise:checked').map(function () { return this.value; }).get().join('|') : null);
            window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview%2Fall&form=advance-search" + ((membership_id != null) ? '&membership_id=' + membership_id : '') + "" + ((forename != null) ? '&forename=' + forename : '') + "" + ((surname != null) ? '&surname=' + surname : '') + "" + ((start_age != null) ? '&start_age=' + start_age : '') + "" + ((end_age != null) ? '&end_age=' + end_age : '') + "" + ((gender != null) ? '&gender=' + gender : '') + "" + ((dob != null) ? '&dob=' + dob : '') + "" + ((email != null) ? '&email=' + email : '') + "" + ((number != null) ? '&number=' + number : '') + "" + ((address_line_one != null) ? '&address_line_one=' + address_line_one : '') + "" + ((address_line_two != null) ? '&address_line_two=' + address_line_two : '') + "" + ((address_city != null) ? '&address_city=' + address_city : '') + "" + ((address_county != null) ? '&address_county=' + address_county : '') + "" + ((address_postcode != null) ? '&address_postcode=' + address_postcode : '') + "" + ((registered_start_date != null) ? '&registered_start_date=' + registered_start_date : '') + "" + ((registered_end_date != null) ? '&registered_end_date=' + registered_end_date : '') + "" + ((report_start != null) ? '&report_start=' + report_start : '') + "" + ((report_end != null) ? '&report_end=' + report_end : '') + "" + ((age_groups != null) ? '&age_groups=' + age_groups : '') + "" + ((disciplines != null) ? '&disciplines=' + disciplines : '') + "" + ((expertise != null) ? '&expertise=' + expertise : '');
        });
        $(document).on('change', '#check-all--expertise', function () {
            $('.expertise-item').prop("checked", this.checked);
        });
        $(document).on('change', '#check-all--gender', function () {
            $('.gender-item').prop("checked", this.checked);
        });
        $(document).on('change', '#check-all--discipline', function () {
            $('.discipline-item').prop("checked", this.checked);
        });
        $(document).on('change', '#check-all--age', function () {
            $('.age-item').prop("checked", this.checked);
        });
    };
    return {
        init: init
    };
});
