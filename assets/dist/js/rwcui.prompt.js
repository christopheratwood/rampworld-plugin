define(['impromptu'], function () {
    var processing = false;
    var roots = {
        live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
        dev: '//127.0.0.1/wp-admin/admin.php'
    };
    var state = function (states) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            processing = true;
            $.prompt(states, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: '',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var simple = function (message) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            var prompt = {
                message: {
                    html: message,
                    buttons: [{
                            title: "Cancel",
                            value: 0,
                            classes: 'tworow'
                        }],
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else { }
                    }
                }
            };
        }
        $.prompt.close();
        $.prompt(prompt, {
            close: function (e) {
                processing = false;
            },
            loaded: function (e) {
                setTimeout(function () {
                    $('.time').animate({
                        width: 150,
                        height: 150,
                        top: -25,
                        left: -25,
                        easing: "easeOutBounce"
                    });
                    setInterval(function () {
                        $('.current_time').animate({
                            fontSize: "2em",
                            easing: "easeOutBounce"
                        });
                    }, 10);
                }, 100);
                $('.clean.tiny:not(.loaded)').animate({
                    marginTop: 0,
                    easing: "easeOutBounce",
                    opacity: 1
                });
                $('.clean.tiny').addClass('loaded');
                $('.right:not(.loaded)').animate({
                    marginRight: 0,
                    width: 250,
                    easing: "easeOutBounce",
                    opacity: 1
                });
                $('.right').addClass('loaded');
                setTimeout(function () {
                    $('#seshTitle:not(.loaded)').animate({
                        fontSize: 16,
                        opacity: 1,
                        paddingTop: 20
                    });
                    $('#seshTitle').addClass('loaded');
                    var i = 1;
                    $('.entry_button:not(.loaded)').each(function () {
                        var el = this;
                        setTimeout(function () {
                            $(el).animate({
                                marginTop: 10,
                                easing: "easeOutBounce",
                                opacity: 1
                            });
                        }, i * 40);
                        i++;
                        $(this).addClass("loaded");
                    });
                }, 600);
            },
            classes: {
                box: '',
                fade: '',
                prompt: '',
                close: '',
                title: 'lead',
                message: '',
                buttons: 'entry_buttons',
                button: 'entry_button',
                defaultButton: 'entry_buttona'
            }
        });
        $('body').on('click', '.jqifade', function (e) {
            processing = false;
            $.prompt.close();
        });
        $('div.jqi').css('width', '700px');
    };
    var danger = function (states) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            processing = true;
            $.prompt(states, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'danger',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var full = function () {
        processing = false;
        $.prompt.close();
        if (!processing) {
            var prompt = {
                message: {
                    html: "<div><div class=\"right\"><h1 id=\"seshTitle\">Danger! Premises is now full!</h1></div><div class=\"centerContent\"><div class=\"error-icon\"><i class=\"glyphicon glyphicon-warning-sign\"></i></div><p>The premises is now full. You cannot allow more members to enter the premises.  Please note, this takes into consideration bookings and membership passes.</p><div class=\"alert alert-danger\">You must contact administration to notify that this message has appeared.</div><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall\" class=\"btn btn-primary btn-block\">Check active participants here.</a></div></div>",
                    buttons: [{
                            title: "Cancel",
                            value: 0,
                            classes: 'tworow'
                        }],
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else { }
                    }
                }
            };
            processing = true;
            $.prompt(prompt, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'danger',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var almostFull = function (r) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            var prompt = {
                message: {
                    html: "<div><div class=\"right\"><h1 id=\"seshTitle\">Danger! Only " + r + " places available!</h1></div><div class=\"centerContent\"><div class=\"error-icon\"><i class=\"glyphicon glyphicon-warning-sign\"></i></div><p>Only <strong>" + r + "</strong> places are available! Please prepare to refuse entry. If there is a queue, you <strong>must</strong> notify the customers. Please note, this takes into consideration bookings and membership passes.</p><div class=\"alert alert-danger\">You must contact administration to notify that this message has appeared.</div><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall\" class=\"btn btn-primary btn-block\">Check active participants here.</a></div></div>",
                    buttons: [{
                            title: "Cancel",
                            value: 0,
                            classes: 'tworow'
                        }],
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else { }
                    }
                }
            };
            processing = true;
            $.prompt(prompt, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'danger',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var booking = function (states) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            processing = true;
            $.prompt(states, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'booking',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_button'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var warning = function (message) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            var prompt = {
                message: {
                    html: "<div><div class=\"right\"><h1 id=\"seshTitle\">Warning! </h1></div><div class=\"centerContent\"><div class=\"warning-icon\"><i class=\"glyphicon glyphicon-warning-sign\"></i></div>" + message + "</div></div>",
                    buttons: [{
                            title: "Cancel",
                            value: 0,
                            classes: 'tworow'
                        }],
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else { }
                    }
                }
            };
            processing = true;
            $.prompt(prompt, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'warning',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    var openingMessage = function (message, member, date, id) {
        processing = false;
        $.prompt.close();
        if (!processing) {
            var prompt = {
                message: {
                    html: "<div><div class=\"right\"><h1 id=\"seshTitle\">Warning! </h1></div><div class=\"centerContent\"><div class=\"warning-icon\"><i class=\"glyphicon glyphicon-warning-sign\"></i></div><p class=\"lead\">The following comments have been made during yesterday's closing checks</p><p>If you require any clarification, please contact the user.</p><div class=\"well well-sm\">" + message + "</div><table class=\"table table-responsive\"><thead><tr><th>Messaged created by</th><th>Last updated</th></tr></thead><tbody><tr><td>" + member + "</td><td>" + date + "</td></tr></tbody></table></div></div>",
                    buttons: [{
                            title: "Cancel",
                            value: 0,
                            classes: 'threerow'
                        }, {
                            title: "View",
                            value: 1,
                            classes: 'threerow'
                        }, {
                            title: "Approve",
                            value: 2,
                            classes: 'threerow'
                        }],
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else if (v === 1) {
                            window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fpremises-checks%2Fview%2Fclosing&date=" + date.split(' ')[0];
                        }
                        else if (v === 2) {
                            $.ajax({
                                url: ajaxurl,
                                type: 'post',
                                data: {
                                    action: 'update_check_message',
                                    checkID: id
                                },
                                success: function (response) {
                                    console.log(response);
                                    $.prompt.close();
                                    processing = false;
                                },
                                error: function (e) {
                                    console.log(e);
                                }
                            });
                        }
                    }
                }
            };
            processing = true;
            $.prompt(prompt, {
                close: function (e) {
                    processing = false;
                },
                loaded: function (e) {
                    setTimeout(function () {
                        $('.time').animate({
                            width: 150,
                            height: 150,
                            top: -25,
                            left: -25,
                            easing: "easeOutBounce"
                        });
                        setInterval(function () {
                            $('.current_time').animate({
                                fontSize: "2em",
                                easing: "easeOutBounce"
                            });
                        }, 10);
                    }, 100);
                    $('.clean.tiny:not(.loaded)').animate({
                        marginTop: 0,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.clean.tiny').addClass('loaded');
                    $('.right:not(.loaded)').animate({
                        marginRight: 0,
                        width: 250,
                        easing: "easeOutBounce",
                        opacity: 1
                    });
                    $('.right').addClass('loaded');
                    setTimeout(function () {
                        $('#seshTitle:not(.loaded)').animate({
                            fontSize: 16,
                            opacity: 1,
                            paddingTop: 20
                        });
                        $('#seshTitle').addClass('loaded');
                        var i = 1;
                        $('.entry_button:not(.loaded)').each(function () {
                            var el = this;
                            setTimeout(function () {
                                $(el).animate({
                                    marginTop: 10,
                                    easing: "easeOutBounce",
                                    opacity: 1
                                });
                            }, i * 40);
                            i++;
                            $(this).addClass("loaded");
                        });
                    }, 600);
                },
                classes: {
                    box: '',
                    fade: '',
                    prompt: 'warning',
                    close: '',
                    title: 'lead',
                    message: '',
                    buttons: 'entry_buttons',
                    button: 'entry_button',
                    defaultButton: 'entry_buttona'
                }
            });
            $('div.jqi').css('width', '700px');
            $('body').on('click', '.jqifade', function (e) {
                processing = false;
                $.prompt.close();
            });
            processing = false;
        }
    };
    return {
        simple: simple,
        state: state,
        booking: booking,
        warning: warning,
        danger: danger,
        almostFull: almostFull,
        full: full,
        openingMessage: openingMessage
    };
});
