define(['jquery'], function ($) {
    var checks = {
        member: false,
        admin: false,
        correct: false
    };
    var member_id = null;
    var isCountdown = false;
    var counter = 9;
    var init = function () {
        member_id = $('#deleteMember').data('m');
        $('#deleteMember').on('click', function (e) {
            e.preventDefault();
            $('#deleteEntryModal').modal('toggle');
        });
        $('#deleteEntryModal').on('hidden.bs.modal', function () {
            checks.member = false;
            checks.admin = false;
            checks.correct = false;
            isCountdown = false;
            counter = 9;
            $('#countdown').empty();
            $('#deleteForm').show();
            $('#checked_member').removeAttr('checked');
            $('#notified_member').removeAttr('checked');
            $('#notified_administation').removeAttr('checked');
        });
        $('#checked_member').on('change', function (e) {
            check();
        });
        $('#notified_member').on('change', function (e) {
            check();
        });
        $('#notified_administation').on('change', function (e) {
            check();
        });
        $('body').on('click', '#delete-member-confirm-2281', function () {
            if (isCountdown == true && counter == -1) {
                window.location.href = '//www.rampworldcardiff.co.uk/wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fdelete&member_id=' + member_id;
            }
        });
    };
    var check = function () {
        if ($('#checked_member').is(':checked'))
            checks.correct = true;
        else
            checks.correct = false;
        if ($('#notified_member').is(':checked'))
            checks.member = true;
        else
            checks.member = false;
        if ($('#notified_administation').is(':checked'))
            checks.admin = true;
        else
            checks.admin = false;
        if (checks.correct == true && checks.member == true && checks.admin == true) {
            $('#countdown').hide();
            $('#countdown').html("<div><span style=\"display:block; margin: 10px auto; font-size: 32px; text-align:center;color:#21a1e1;\"><span id=\"remaining\">10</span> Seconds</span></div>");
            $('#warning-notice').fadeOut(500);
            $('#deleteForm').fadeOut(500, function () {
                $('#countdown').fadeIn(500);
            });
            isCountdown = true;
            updateCounter();
        }
    };
    var updateCounter = function () {
        if (isCountdown) {
            var interval_1 = setInterval(function () {
                if (isCountdown) {
                    if (counter < 0) {
                        $('#removeMemberConfirm').text('Complete your action');
                        $('#countdown').html("<div style=\"text-align: center;\"><a class=\"btn btn-danger btn-lg\" id=\"delete-member-confirm-2281\">Delete member</a></div>");
                        clearInterval(interval_1);
                    }
                    else {
                        $('#remaining').text(counter);
                        $('#removeMemberConfirm').text(counter + " seconds");
                        counter--;
                    }
                }
                else {
                    $('#removeMemberConfirm').text('Please complete form.');
                    counter = 9;
                    clearInterval(interval_1);
                }
            }, 1000);
            if (counter == 0) {
                $('#removeMemberConfirm').text('Delete member');
                $('#removeMemberConfirm').removeAttr('disabled');
                $('#countdown').html("<div><span style=\"display:block; margin: 10px auto; font-size: 32px; text-align:center;color:#21a1e1;\">You may remove this member.</span></div>");
            }
        }
        else {
            counter = 9;
        }
    };
    return {
        init: init
    };
});
