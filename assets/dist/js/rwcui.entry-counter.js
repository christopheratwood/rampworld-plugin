define(['percentage'], function () {
    var colours = {
        okay: '#5cb85c',
        warning: '#f0ad4e',
        bad: '#d9534f'
    };
    var config = {
        size: 100
    };
    var counter = {
        percentage: $('#entry-percentage').val(),
        amount: $('#entry-amount').val(),
        passes: $('#paid-pass').val(),
        sessions: $('#paid-sesh').val()
    };
    var init = function () {
        createEntry();
        createPaid();
    };
    var createEntry = function () {
        counter.percentage = $('#entry-percentage').val();
        counter.amount = $('#entry-amount').val();
        var progressBarOptions = {};
        if (counter.amount < 40) {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: counter.percentage,
                fill: {
                    color: colours.okay
                }
            };
        }
        else if (counter.amount < 90) {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: counter.percentage,
                fill: {
                    color: colours.warning
                }
            };
        }
        else {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: counter.percentage,
                fill: {
                    color: colours.bad
                }
            };
        }
        $('#entryCounter').circleProgress(progressBarOptions).on('circle-animation-progress', function (event, progress, stepValue) {
            $(this).find('#number').text(counter.amount);
            $(this).find('#percentage').text((stepValue.toFixed(2) * 100).toFixed(0) + "%");
        });
    };
    var createPaid = function () {
        counter.passes = $('#paid-pass').val();
        counter.sessions = $('#paid-sesh').val();
        var total_p = (parseInt(counter.passes) + parseInt(counter.sessions)) / 220;
        var total = parseInt(counter.passes) + parseInt(counter.sessions);
        var progressBarOptions = {};
        if (total < 40) {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: total_p,
                fill: {
                    color: colours.okay
                }
            };
        }
        else if (total < 90) {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: total_p,
                fill: {
                    color: colours.warning
                }
            };
        }
        else {
            progressBarOptions = {
                startAngle: -1.55,
                size: config.size,
                value: total_p,
                fill: {
                    color: colours.bad
                }
            };
        }
        $('#paidCounter').circleProgress(progressBarOptions).on('circle-animation-progress', function (event, progress, stepValue) {
            $(this).find('#passes_txt').html("<span>Pass</span>" + counter.passes);
            $(this).find('#sessions_txt').html("<span>Book</span>" + counter.sessions);
        });
    };
    return {
        init: init,
        createEntry: createEntry,
        createPaid: createPaid
    };
});
