define(['jquery'], function ($) {
    var add = function (id, text, classes) {
        if (classes === void 0) { classes = ''; }
        $(id).removeClass('hidden');
        $(id).text(text).addClass(classes);
    };
    var remove = function (id) {
        $(id).removeClass('visible');
        $(id).addClass('hidden');
        $(id).text('');
    };
    return {
        add: add,
        remove: remove
    };
});
