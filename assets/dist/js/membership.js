var roots = {
    live: '//www.rampworldcardiff.co.uk/wp-content/plugins/rampworld-membership/assets/dist/thirdparty/',
    dev: '//127.0.0.1/wp-content/plugins/rampworld-membership/assets/dist/thirdparty/'
};
var version = 2.14;
require.config({
    paths: {
        jquery: roots.live + 'js/jquery.min',
        bootstrap: roots.live + 'js/bootstrap.min',
        percentage: roots.live + 'js/circle-percentage.min',
        impromptu: roots.live + 'js/jquery-impromptu.min',
        datatable: roots.live + 'js/datatable.min',
        datepicker: roots.live + 'js/bootstrap-datetimepicker.min',
        moment: roots.live + 'js/moment.min',
        tinymce: roots.live + 'js/tinymce/jquery.tinymce.min',
        chart: roots.live + 'js/charts.min'
    },
    shim: {
        bootstrap: { deps: ['jquery'] },
        percentage: { deps: ['jquery'] },
        impromptu: { deps: ['jquery'] },
        datatable: { deps: ['jquery', 'bootstrap'] },
        chart: {
            exports: 'chart'
        }
    },
    waitSeconds: 200,
});
require(['bootstrap'], function () { console.log('Core Loaded: ' + Date()); console.log('rwcui version:  ' + version); });
;
require(['rwcui.entry-counter', 'rwcui.populate-checks'], function (entryCounter, checks) {
    loadCss(roots.live + "css/jquery-impromptu.min.css");
    entryCounter.init();
    checks.init();
});
;
if (document.querySelector('#search-member-number') || document.querySelector('.change-session-btn')) {
    require(['rwcui.entry-counter', 'rwcui.member', 'rwcui.prompt', 'rwcui.entry'], function (entryCounter, member, prompt, entry) {
        loadCss(roots.live + "css/jquery-impromptu.min.css");
        member.init();
        entry.init();
    });
}
if (document.querySelector('#search-panel') !== null || document.querySelector('#report-dates-panel') !== null || document.querySelector('#table-dates-panel') !== null) {
    require(['rwcui.search'], function (search) {
        search.init();
    });
}
;
if (document.querySelector('#advance-search-panel') !== null) {
    require(['rwcui.advance-search'], function (advanceSearch) {
        advanceSearch.init();
    });
}
if (document.querySelector('#holiday-dates') !== null) {
    require(['jquery', 'datepicker'], function ($) {
        $('#start_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
        $('#end_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
}
if (document.querySelector('.date') !== null) {
    require(['jquery', 'datepicker'], function ($) {
        $('.date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
}
if (document.querySelector('.datetime') !== null) {
    require(['jquery', 'datepicker'], function ($) {
        $('.datetime').datetimepicker({
            format: 'YYYY-MM-DD HH:mm'
        });
    });
}
if (document.querySelector('#session-times') !== null) {
    require(['jquery', 'datepicker'], function ($) {
        $('#start_time').datetimepicker({
            format: 'HH:mm'
        });
        $('#end_time').datetimepicker({
            format: 'HH:mm'
        });
    });
}
if (document.querySelector('.remove-participant-btn') !== null) {
    require(['jquery', 'bootstrap', 'rwcui.loader', 'rwcui.remove-participant'], function ($, bootstrap, loader, removeParticipant) {
        removeParticipant.init();
    });
}
if (document.querySelector('.table-sortable') !== null) {
    require(['jquery', 'datatable'], function ($, dt) {
        loadCss(roots.live + 'css/datatable.min.css');
        $('.table-sortable').DataTable({
            searching: false,
            paging: false,
            info: false,
            order: []
        });
    });
}
if (document.querySelector('#deleteMember') !== null) {
    require(['rwcui.delete-member'], function (deleteMember) {
        deleteMember.init();
    });
}
if (document.querySelector('#editDetails') !== null) {
    require(['jquery'], function ($) {
        $('#editDetails').on('click', function (e) {
            if ($(this).text() == 'Save changes') {
                $('#editForm').submit();
            }
            else {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-primary');
                $(this).text('Save changes');
                $('[readonly]').removeAttr('readonly');
                $('[disabled]').removeAttr('disabled');
                $(this).before('<a class="btn mg-10 btn-warning btn-ms-block" id="cancelEdit">Cancel</a>');
            }
        });
        $('body').on('click', '#cancelEdit', function (e) {
            $('#editDetails').removeClass('btn-primary');
            $('#editDetails').addClass('btn-warning');
            $('#editDetails').text('Change details');
            $('input.form-control ').attr('readonly', 'readonly');
            $('input[type="radio"].form-control ').attr('disabled', 'disabled');
            $('input[type="checkbox"].form-control ').attr('disabled', 'disabled');
            $('select ').attr('disabled', 'disabled');
            $('textarea.form-control ').attr('readonly', 'readonly');
            $(this).remove();
        });
    });
}
if (document.querySelector('#editOpening') !== null) {
    require(['rwcui.populate-checks'], function (populateChecks) {
        populateChecks.opening();
    });
}
;
if (document.querySelector('#editClosing') !== null) {
    require(['rwcui.populate-checks'], function (populateChecks) {
        populateChecks.closing();
    });
}
;
if (document.querySelector('.editNote') !== null) {
    require(['jquery'], function ($) {
        $('.editNote').on('click', function (e) {
            var note_id = $(this).data('note-id');
            if ($(this).text() == 'Save changes') {
                $('#editForm_' + note_id).submit();
            }
            else {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-primary');
                $(this).text('Save changes');
                $('#editForm_' + note_id + ' [readonly]').removeAttr('readonly');
                $('#editForm_' + note_id + ' [disabled]').removeAttr('disabled');
                $(this).before('<a class="btn btn-sm mg-10 btn-warning btn-ms-block cancelEdit" data-note-id="' + note_id + '">Cancel</a>');
            }
        });
        $('body').on('click', '.cancelEdit', function (e) {
            var note_id = $(this).data('note-id');
            $(this).remove();
            $('*[data-note-id="' + note_id + '"]').closest('.editNote').removeClass('btn-primary');
            $('*[data-note-id="' + note_id + '"]').addClass('btn-warning');
            $('*[data-note-id="' + note_id + '"]').text('Change');
            $('#editForm_' + note_id + ' input.form-control ').attr('readonly', 'readonly');
            $('#editForm_' + note_id + ' input[type="radio"].form-control ').attr('disabled', 'disabled');
            $('#editForm_' + note_id + ' input[type="checkbox"].form-control ').attr('disabled', 'disabled');
            $('#editForm_' + note_id + ' textarea.form-control ').attr('readonly', 'readonly');
        });
    });
}
if (document.querySelector('.editType') !== null) {
    require(['jquery'], function ($) {
        $('.editType').on('click', function (e) {
            var type_id = $(this).data('type-id');
            if ($(this).text() == 'Save changes') {
                $('#editForm_' + type_id).submit();
            }
            else {
                $(this).removeClass('btn-warning');
                $(this).addClass('btn-primary');
                $(this).text('Save changes');
                $('#editForm_' + type_id + ' [readonly]').removeAttr('readonly');
                $('#editForm_' + type_id + ' [disabled]').removeAttr('disabled');
                $(this).before('<a class="btn btn-sm mg-10 btn-warning btn-ms-block cancelEdit" data-type-id="' + type_id + '">Cancel</a>');
            }
        });
        $('body').on('click', '.cancelEdit', function (e) {
            var type_id = $(this).data('type-id');
            $(this).remove();
            $('*[data-type-id="' + type_id + '"]').closest('.editNote').removeClass('btn-primary');
            $('*[data-type-id="' + type_id + '"]').addClass('btn-warning');
            $('*[data-type-id="' + type_id + '"]').text('Change');
            $('#editForm_' + type_id + ' input.form-control ').attr('readonly', 'readonly');
            $('#editForm_' + type_id + ' input[type="radio"].form-control ').attr('disabled', 'disabled');
            $('#editForm_' + type_id + ' input[type="checkbox"].form-control ').attr('disabled', 'disabled');
            $('#editForm_' + type_id + ' textarea.form-control ').attr('readonly', 'readonly');
        });
    });
}
if (document.querySelector(".input-group-btn .dropdown-menu li a") !== null) {
    require(['jquery'], function ($) {
        $(".input-group-btn .dropdown-menu li a").click(function (e) {
            var selText = $(this).html();
            var element = $(this).parent().parent().siblings('button').data('element');
            var input = $(this).parent().parent().siblings('button').data('input');
            $(this).parents('.input-group-btn').find('.dropdown-toggle').html(selText + '  <span class="caret"></span>');
            $('#' + element).val(selText.toLowerCase());
            console.log(selText);
            if (selText == 'Date of birth') {
                $('#' + input).after("<input type=\"number\" name=\"day\" id=\"day\" min=\"1\" max=\"32\" placeholder=\"DD\" class=\"small form-control\"><input type=\"number\" name=\"month\" id=\"month\" min=\"1\" max=\"12\" placeholder=\"MM\" class=\"small form-control\"><input type=\"number\" name=\"year\" id=\"year\" min=\"1920\" max=\"2100\" placeholder=\"YYYY\" class=\"small form-control\">");
                $('#' + input).remove();
            }
            else if (selText == 'Phone number') {
                if ($('#day').length) {
                    $('#day').remove();
                    $('#month').remove();
                    $('#year').after("<input type=\"number\" class=\"form-control input-lg border-bottom-6\" id=\"" + input + "\" placeholder=\"Search for phone number\" style=\"width:100%\" value=\"\">");
                    $('#year').remove();
                }
                else {
                    $('#' + input).attr('type', 'number').val('');
                    $('#' + input).attr('placeholder', 'Please enter phone number').val('');
                }
            }
            else {
                if ($('#day').length) {
                    $('#day').remove();
                    $('#month').remove();
                    $('#year').after("<input type=\"text\" class=\"form-control input-lg border-bottom-6\" id=\"" + input + "\" placeholder=\"Please enter " + selText + "\" style=\"width:100%\" value=\"\">");
                    $('#year').remove();
                }
                else {
                    $('#' + input).attr('type', 'text').val('');
                    $('#' + input).attr('placeholder', 'Please enter ' + selText).val('');
                }
            }
        });
    });
}
if (document.querySelector('#removeHidden') !== null) {
    require(['jquery'], function ($) {
        $('#removeHidden').off().on('click', function (e) {
            if (document.querySelector(this).text() == 'Hide') {
                $('tr.visible').addClass('hidden');
                $('tr.visible').removeClass('visible');
            }
            else {
                $('tr.hidden').addClass('visible');
                $('tr.hidden').removeClass('hidden');
                $(this).text('Hide');
            }
        });
    });
}
if (document.querySelector('.sessionChart') !== null) {
    require(['rwcui.chart'], function (chart) {
        chart.init();
    });
}
function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}
