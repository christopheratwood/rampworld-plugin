define(['jquery', 'chart'], function () {
    var roots = {
        live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
        dev: '//127.0.0.1/wp-admin/admin.php'
    };
    var colours = {
        spectator: "#772E7B",
        scooter: "#21a1e1",
        bmx: "#CDEAC0",
        mtb: "#FF934F",
        skateboard: "#F85AE3",
        inline: "#bbb",
        other: "#d9534f"
    };
    var init = function () {
        $(function () {
            $.each($('.sessionChart'), function (i, v) {
                $(this).draw(setData($(this).data('session-data')), $(this).data('session-time'), $(this).data('total'));
            });
        });
    };
    var setData = function (data) {
        var items = [];
        if (data.length > 0) {
            data = data.split(';');
            data.forEach(function (element) {
                items.push({
                    title: element.split(":")[0],
                    value: element.split(":")[1],
                    color: colours[element.split(":")[0]]
                });
            });
        }
        console.log(items);
        return items;
    };
    return {
        init: init
    };
});
