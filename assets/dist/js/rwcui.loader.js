define(['jquery'], function ($) {
    var create = function (element) {
        $('#' + element).addClass('bg-success');
        $('#' + element).html("<div class=\"cs-loader\"><div class=\"cs-loader-inner\"><label>\u25CF</label><label>\u25CF</label><label>\u25CF</label></div></div>");
        fixedCreate();
    };
    var remove = function (element) {
        $('#' + element).removeClass('bg-success');
        $('#' + element).html('<i class="glyphicon glyphicon-arrow-right"></i>');
        fixedDelete();
    };
    var fixedCreate = function (message) {
        if (message === void 0) { message = "Loading"; }
        $('body').append("<div id=\"loading_fixed_notice\" class=\"active\"><p>" + message + "</p></div>");
    };
    var fixedDelete = function () {
        $('#loading_fixed_notice').removeClass('active');
        setTimeout(function () {
            $('#loading_fixed_notice').remove();
        }, 100);
    };
    var toast = function (message, duration) {
        if (duration === void 0) { duration = 500; }
        if ($('#loading_fixed_notice').length) {
            $('#loading_fixed_notice > p').text(message);
            setTimeout(function () {
                $('#loading_fixed_notice').removeClass('active');
                setTimeout(function () {
                    $('#loading_fixed_notice').remove();
                }, 300);
            }, duration);
        }
        else {
            $('body').append("<div id=\"loading_fixed_notice\" class=\"active\"><p>" + message + "</p></div>");
            setTimeout(function () {
                $('#loading_fixed_notice > p').text(message);
                setTimeout(function () {
                    $('#loading_fixed_notice').removeClass('active');
                    setTimeout(function () {
                        $('#loading_fixed_notice').remove();
                    }, 300);
                }, duration);
            }, 300);
        }
    };
    return {
        create: create,
        remove: remove,
        fixedCreate: fixedCreate,
        fixedDelete: fixedDelete,
        toast: toast
    };
});
