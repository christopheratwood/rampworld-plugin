var today = new Date();
define(['rwcui.entry-counter', 'rwcui.prompt', 'rwcui.loader', 'rwcui.error', 'rwcui.member'], function (entryCounter, prompts, loader, error, memberClass) {
    var prompt;
    var roots = {
        live: '//www.rampworldcardiff.co.uk/wp-admin/admin.php',
        dev: '//127.0.0.1/wp-admin/admin.php'
    };
    var sessiontime;
    var processing = false;
    var config = {
        lasttime: null,
        onlyPrivate: null,
        hasMembershipBtn: false,
        entry_stats: {
            active: 0,
            reserved: 0,
            percentage: 0
        },
        date: {
            today: new Date(),
            thisMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1).toLocaleDateString(),
            thisSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 7).toLocaleDateString(),
            nextMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (8 - today.getDay())).toLocaleDateString(),
            nextSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (14 - today.getDay())).toLocaleDateString(),
            twoWeekMonday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (15 - today.getDay())).toLocaleDateString(),
            twoWeekSunday: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (21 - today.getDay())).toLocaleDateString(),
        },
        isFull: false,
        maxAmount: 220,
        premises: {
            onPremises: $('#entry-amount').val(),
            sessions: $('#paid-sesh').val(),
            passes: $('#paid-pass').val()
        },
        buttons: [],
        sessions: [],
        sessoion_atm: false,
        default_html: "<div><div class=\"right\"><h1 id=\"seshTitle\">{{actiontitle}}</h1></div><div class=\"centerContent\"><span class=\"name\">{{name}}</span><div class=\"age-notification\"><span class=\"age\">{{age}}</span><span class=\"message\">years of age</span></div>{{memberinfo}}{{notices}}</div></div>",
        default_booking: "<div><div class=\"right\"><h1 id=\"seshTitle\">{{actiontitle}}</h1></div><div class=\"centerContent\"><span class=\"name\">{{name}}</span><div class=\"age-notification\"><span class=\"age\">{{booking_number}}</span><span class=\"message\">booking No.</span></div><p class=\"lead\">Booking information</p>{{booking_info}}<p class=\"lead\">Session information</p>{{session_info}}<p class=\"lead\">Participants</p><small>Click to process member</small>{{member_info}}</div></div>",
        default_simple_html: "<div><div class=\"right\"><h1 id=\"seshTitle\">{{actiontitle}}</h1></div><div class=\"centerContent\">{{message}}</div></div>",
        default_membership_html: "<div><div class=\"right\"><h1 id=\"seshTitle\">{{actiontitle}}</h1></div><div class=\"centerContent\"><span class=\"name\">{{name}}</span><div class=\"age-notification\"><span class=\"age\">{{age}}</span><span class=\"message\">years of age</span></div><p class=\"lead\">Dates</p><p class=\"lead\">{{startdate}} - {{enddate}}</p><div class=\"alert alert-warning\">Please explain to the customer that the pass ends on the {{enddate}}</div>{{notices}}</div></div>"
    };
    var booking = {
        details: {
            id: null
        },
        total: null
    };
    var init = function () {
        var total = parseInt($('#entry-amount').val()) + parseInt($('#paid-pass').val()) + parseInt($('#paid-sesh').val());
        if (total >= config.maxAmount) {
            config.isFull = true;
            prompts.full();
        }
        else if (total == config.maxAmount - 20)
            prompts.almostFull(config.maxAmount - total);
        else if (total == config.maxAmount - 10)
            prompts.almostFull(config.maxAmount - total);
        else if (total == config.maxAmount - 5)
            prompts.almostFull(config.maxAmount - total);
        setSessions();
        $('.change-session-btn').off().on('click', function (e) {
            e.preventDefault();
            changeSession($(this).data('member-id'), $(this).data('forename'), $(this).data('surname'), $(this).data('expertise'), $(this).data('age'), $(this).data('has-membership'), $(this).data('session-id'), $(this).data('current-times'), false);
        });
        $("body").on('click', '#booking-members tbody tr', function (e) {
            if ($(this).data('verification-required') != '0') {
                verifyBookingMember($(this).data('member-id'), $(this).data('session-id'), $(this).data('booking-end-time'), $(this).data('session-type'));
            }
            else {
                switch ($(this).data('session-type')) {
                    case 'pre-session':
                        setBookingEntry('session', $(this).data('member-id'), $(this).data('session-id'), $(this).data('booking-end-time'), false, false);
                        break;
                    case 'weekpass':
                        setBookingEntry('pass', $(this).data('member-id'), $(this).data('session-id'), $(this).data('booking-end-time'), false, false);
                        break;
                }
            }
        });
    };
    var processBooking = function (bookingID) {
        booking.details.id = bookingID;
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'get_full_booking_details',
                bookingID: booking.details.id,
            },
            success: function (response) {
                if (response !== false) {
                    var booking_response = JSON.parse(response);
                    booking.total = booking_response.members.length;
                    var details = "<table class=\"table\"><tr><th>Phone number</th><th>Email</th></tr><tr><td>" + booking_response.details.number + "</td><td>" + booking_response.details.email + "</td></tr></table>";
                    var session_details = "<table class=\"table\"><tr><th>Start time</th><th>End time</th><th>Session band</th></tr><tr><td>" + booking_response.details.session_start_time + "</td><td>" + booking_response.details.session_end_time + "</td><td>unknown</td></tr></table>";
                    var members = "<div class=\"table-responsive\" id=\"booking-members\" style=\"max-height: 200px\"><table class=\"table\"><thead><th>#</th><th>Name</th><th>Age</th><th>Expertise</th><th>Process</th></thead><tbody>";
                    for (var _i = 0, _a = booking_response.members; _i < _a.length; _i++) {
                        var member = _a[_i];
                        members += "<tr data-session-type=\"" + ((member.pre_session_id != null) ? 'pre-session' : 'weekpass') + "\"data-session-id=\"" + ((member.pre_session_id != null) ? member.pre_session_id : member.paid_pass_id) + "\"data-booking-end-time=\"" + member.session_end_time + "\" data-verification-required=\"" + ((member.staff_verification == 0) ? 1 : 0) + "\"data-member-id=\"" + member.member_id + "\"><td>" + member.member_id + "</td><td>" + member.forename + " " + member.surname + "</td><td>" + member.dob + "</td><td>" + member.expertise + "</td><td><a class=\"btn btn-default disabled\" >Process</a></td></tr>";
                    }
                    members += '</tbody></table></div>';
                    prompt = {
                        day: {
                            html: config.default_booking.replace('{{actiontitle}}', 'Select action')
                                .replace('{{name}}', booking_response.details.name)
                                .replace('{{booking_number}}', bookingID)
                                .replace('{{booking_info}}', details)
                                .replace('{{session_info}}', session_details)
                                .replace('{{member_info}}', members),
                            buttons: [{
                                    title: "Close",
                                    value: false,
                                    classes: ['tworow', 'cancel']
                                }],
                            focus: 1,
                            submit: function (e, v, m, f) {
                            }
                        }
                    };
                    prompts.booking(prompt);
                }
            }
        });
    };
    var verifyBookingMember = function (memberID, ppid, end_time, type) {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                mid: memberID,
                action: 'verify_member_json'
            },
            success: function (response) {
                if (response !== false) {
                    var details = JSON.parse(response);
                    var member = details[0];
                    var confirm_details = "<table class=\"table\" style=\"font-size:14px;\"><tr><th>#</th><td>" + memberID + "</td></tr><tr><th>Phone numner</th><td>" + member.number + "</td></tr><tr><th>Email</th><td>" + member.email + "</td></tr><tr><th>Gender</th><td>" + member.gender + "</td></tr><tr><th>Date of birth</th><td>" + member.dob + "</td></tr><tr><th>Address line one</th><td>" + member.address_line_one + "</td></tr><tr><th>Postcode</th><td>" + member.address_postcode + "</td></tr><tr><th>Medical notes</th><td colspan=\"3\">" + ((member.medical_notes == null) ? '' : member.medical_notes) + "</td></tr></table>";
                    var notices = "<div class=\"alert alert-warning\">Please ensure these details are correct be proceeding.</div>";
                    prompt = {
                        day: {
                            html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                .replace('{{age}}', member.age)
                                .replace('{{name}}', member.forename + "  " + member.surname)
                                .replace('{{memberinfo}}', confirm_details)
                                .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                            buttons: [{
                                    title: "Cancel",
                                    value: false,
                                    classes: ['tworow', 'cancel']
                                }, {
                                    title: "Verify",
                                    value: true,
                                    class: ['tworow']
                                }],
                            focus: 1,
                            submit: function (e, v, m, f) {
                                e.preventDefault();
                                if (v == false) {
                                    $.prompt.close();
                                    processing = false;
                                }
                                else {
                                    if (type == 'pre-session') {
                                        setBookingEntry('session', memberID, ppid, end_time, true, false);
                                    }
                                    else {
                                        setBookingEntry('pass', memberID, ppid, null, true, false);
                                    }
                                }
                            }
                        }
                    };
                    prompts.state(prompt);
                }
            }
        });
    };
    var processEntry = function (memberID, forename, surname, age, notes, is_banned, has_membership, expertise, redirect, already, currentSession, pp) {
        if (notes === void 0) { notes = false; }
        if (is_banned === void 0) { is_banned = false; }
        if (has_membership === void 0) { has_membership = "None"; }
        if (expertise === void 0) { expertise = null; }
        if (redirect === void 0) { redirect = null; }
        if (already === void 0) { already = null; }
        if (pp === void 0) { pp = null; }
        if (Object.keys(config.sessions).length === 0) {
            prompts.warning("<h3 style=\"text-align:center\">No available sessions</h3><div class=\"well\">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>");
        }
        else {
            if (has_membership == null)
                has_membership = "None";
            if (is_banned != false) {
                var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td> " + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                var notices = "<div class=\"alert alert-danger\" style=\"max-height: 120px; overflow-y:auto\"><strong>Danger!</strong> This member is banned from the premises. You are required to refuse entry.</div>";
                prompt = {
                    day: {
                        html: config.default_html.replace('{{actiontitle}}', 'This member is banned')
                            .replace('{{age}}', age).replace('{{name}}', forename + " " + surname)
                            .replace('{{memberinfo}}', table)
                            .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                        buttons: [{
                                title: 'Cancel',
                                value: -1,
                                classes: ['entry_large', "cancel"]
                            }],
                        focus: 1,
                        submit: function (e, v, m, f) {
                            e.preventDefault();
                            if (v == -1) {
                                $.prompt.close();
                                processing = false;
                            }
                        }
                    }
                };
                prompts.danger(prompt);
            }
            else {
                if (isNaN(memberID)) {
                    prompts.simple(config.default_simple_html.replace('{{message}}', '<p class="lead">Member ID must be Integer.</p>').replace('{{actiontitle}}', 'Choose action'));
                }
                else {
                    if (already != null && already != false) {
                        changeSession(memberID, forename, surname, expertise, age, has_membership, currentSession, already, notes);
                    }
                    else {
                        if (has_membership == "None" && pp == null && config.hasMembershipBtn == false) {
                            config.buttons.push({
                                title: 'Membership',
                                value: 'add_membership',
                                classes: ['membership']
                            });
                            config.hasMembershipBtn = true;
                        }
                        if (pp != null) {
                            $.ajax({
                                url: ajaxurl,
                                type: 'post',
                                data: {
                                    action: 'get_booking_details',
                                    memberID: memberID,
                                    ppid: pp
                                },
                                success: function (response) {
                                    if (response !== false) {
                                        var details_1 = JSON.parse(response);
                                        var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                                        var notices = (notes !== false) ? "<div class=\"alert alert-warning\"><p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p><p>" + notes + "</p></div>" : '';
                                        prompt = {
                                            day: {
                                                html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                                    .replace('{{age}}', age)
                                                    .replace('{{name}}', forename + " " + surname)
                                                    .replace('{{memberinfo}}', table)
                                                    .replace('{{notices}}', "<p class=\"lead\">Booking details</p><table class=\"table\" style=\"font-size:14px;\"><thead><tr><th>Start time</th><th>End time</th></tr></thead><tbody><tr><td>" + details_1.session_start_time + "</td><td>" + details_1.session_end_time + "</td></tr></tbody></table>"),
                                                buttons: [{
                                                        title: "Cancel",
                                                        value: false,
                                                        classes: ['tworow', 'cancel']
                                                    }, {
                                                        title: "Next",
                                                        value: true,
                                                        class: ['tworow']
                                                    }],
                                                focus: 1,
                                                submit: function (e, v, m, f) {
                                                    e.preventDefault();
                                                    if (v == false) {
                                                        $.prompt.close();
                                                        processing = false;
                                                    }
                                                    else {
                                                        sessiontime = v;
                                                        setBookingEntry('pass', memberID, pp, details_1.session_end_time);
                                                    }
                                                }
                                            }
                                        };
                                        prompts.state(prompt);
                                    }
                                    else {
                                        var details = JSON.parse(response);
                                        var table = "<div class=\"alert alert-danger\">Booking details coild not be rendered. Please verify with customer. <strong>You must contact administration to manual change this booking status</strong></div><table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                                        var notices = (notes !== false) ? "<div class=\"alert alert-warning\"><p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p><p>" + notes + "</p></div>" : '';
                                        prompt = {
                                            day: {
                                                html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                                    .replace('{{age}}', age)
                                                    .replace('{{name}}', forename + "  " + surname)
                                                    .replace('{{memberinfo}}', table)
                                                    .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                                                buttons: config.buttons,
                                                focus: 1,
                                                submit: function (e, v, m, f) {
                                                    e.preventDefault();
                                                    if (v == false) {
                                                        $.prompt.close();
                                                        processing = false;
                                                    }
                                                    else {
                                                        sessiontime = v;
                                                        setEntry(memberID, sessiontime);
                                                    }
                                                }
                                            }
                                        };
                                        prompts.state(prompt);
                                    }
                                }
                            });
                        }
                        else {
                            if (has_membership != "None" && config.onlyPrivate == false) {
                                var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                                var notices = (notes !== false) ? "<div class=\"alert alert-warning\"><p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p><p>" + notes + "</p></div>" : '';
                                prompt = {
                                    day: {
                                        html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                            .replace('{{age}}', age)
                                            .replace('{{name}}', forename + "  " + surname)
                                            .replace('{{memberinfo}}', table)
                                            .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                                        buttons: [{
                                                title: "Cancel",
                                                value: false,
                                                classes: ['tworow', 'cancel']
                                            }, {
                                                title: "Next",
                                                value: true,
                                                class: ['tworow']
                                            }],
                                        focus: 1,
                                        submit: function (e, v, m, f) {
                                            e.preventDefault();
                                            if (v == false) {
                                                $.prompt.close();
                                                processing = false;
                                            }
                                            else {
                                                setPassEntry(memberID, config.lasttime);
                                            }
                                        }
                                    }
                                };
                                prompts.state(prompt);
                            }
                            else {
                                var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                                var notices = (notes !== false) ? "<div class=\"alert alert-warning\"><p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p><p>" + notes + "</p></div>" : '';
                                prompt = {
                                    day: {
                                        html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                            .replace('{{age}}', age)
                                            .replace('{{name}}', forename + "  " + surname)
                                            .replace('{{memberinfo}}', table)
                                            .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                                        buttons: config.buttons,
                                        focus: 1,
                                        submit: function (e, v, m, f) {
                                            e.preventDefault();
                                            if (v == false) {
                                                $.prompt.close();
                                                processing = false;
                                            }
                                            else if (v == 'add_membership') {
                                                $.prompt.goToState('membership');
                                            }
                                            else {
                                                sessiontime_1 = v;
                                                setEntry(memberID, sessiontime_1);
                                            }
                                        }
                                    },
                                    membership: {
                                        html: config.default_membership_html.replace('{{actiontitle}}', 'Select action')
                                            .replace('{{age}}', age)
                                            .replace('{{name}}', forename + " " + surname)
                                            .replace('{{notices}}', notices).replace('{{startdate}}', config.date.thisMonday)
                                            .replace(/{{enddate}}/g, config.date.thisSunday),
                                        buttons: [{
                                                title: 'Cancel',
                                                value: 0,
                                                classes: 'tworow'
                                            }, {
                                                title: 'Back',
                                                value: -1,
                                                classes: ['tworow', 'cancel']
                                            }, {
                                                title: 'Continue',
                                                value: 2,
                                                classes: 'threerow'
                                            }],
                                        focus: 1,
                                        submit: function (e, v, m, f) {
                                            e.preventDefault();
                                            if (v == 0) {
                                                $.prompt.close();
                                            }
                                            else if (v == -1) {
                                                $.prompt.goToState('day');
                                            }
                                            else {
                                                setWeekPass(memberID, config.lasttime, config.date.thisMonday);
                                            }
                                        }
                                    }
                                };
                                prompts.state(prompt);
                                var sessiontime_1 = '';
                            }
                        }
                    }
                }
            }
        }
    };
    var verifyMemberGUI = function (mid, age, expertise, has_membership, ppid) {
        if (has_membership === void 0) { has_membership = "None"; }
        if (ppid === void 0) { ppid = null; }
        if (Object.keys(config.sessions).length === 0) {
            prompts.warning("<h3 style=\"text-align:center\">No available sessions</h3><div class=\"well\">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>");
        }
        else {
            var data = {
                "mid": mid,
                "action": 'verify_member_json'
            };
            if (config.hasMembershipBtn == false) {
                config.buttons.push({
                    title: 'Membership',
                    value: 'add_membership',
                    classes: ['membership']
                });
                config.hasMembershipBtn = true;
            }
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: data,
                beforeSend: function () {
                    loader.fixedCreate();
                },
                success: function (response) {
                    loader.fixedDelete();
                    var member = JSON.parse(response);
                    member = member[0];
                    var confirm_details = "<table class=\"table\" style=\"font-size:14px;\"><tr><th>#</th><td>" + mid + "</td></tr><tr><th>Phone numner</th><td>" + member.number + "</td></tr><tr><th>Email</th><td>" + member.email + "</td></tr><tr><th>Gender</th><td>" + member.gender + "</td></tr><tr><th>Date of birth</th><td>" + member.dob + "</td></tr><tr><th>Address line one</th><td>" + member.address_line_one + "</td></tr><tr><th>Postcode</th><td>" + member.address_postcode + "</td></tr><tr><th>Medical notes</th><td colspan=\"3\">" + ((member.medical_notes == null) ? '' : member.medical_notes) + "</td></tr></table>";
                    var notices = "<div class=\"alert alert-warning\">Please ensure these details are correct be proceeding.</div>";
                    var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + mid + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table>";
                    if (ppid == null) {
                        if (has_membership != "None") {
                            prompt = {
                                verify: {
                                    html: config.default_html.replace('{{name}}', member.forename + " " + member.surname)
                                        .replace('{{age}}', age)
                                        .replace('{{actiontitle}}', 'Are these details correct?')
                                        .replace('{{memberinfo}}', confirm_details)
                                        .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid + "\" class=\"btn btn-block btn-primary\"> View member</a>"),
                                    buttons: [
                                        {
                                            title: 'Cancel',
                                            value: -1,
                                            classes: ['entry_large', "cancel"]
                                        },
                                        {
                                            title: 'Verify',
                                            value: 1,
                                            classes: ['entry_large']
                                        }
                                    ],
                                    focus: 1,
                                    submit: function (e, v, m, f) {
                                        e.preventDefault();
                                        if (v == -1) {
                                            $.prompt.close();
                                            processing = false;
                                        }
                                        else if (v == 'view') {
                                            window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid;
                                        }
                                        else {
                                            $.prompt.goToState('day');
                                        }
                                    }
                                },
                                day: {
                                    html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                        .replace('{{age}}', age)
                                        .replace('{{name}}', member.forename + "  " + member.surname)
                                        .replace('{{memberinfo}}', table)
                                        .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + member.member_id + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                                    buttons: [{
                                            title: "Cancel",
                                            value: false,
                                            classes: ['tworow', 'cancel']
                                        }, {
                                            title: "Next",
                                            value: true,
                                            class: ['tworow']
                                        }],
                                    focus: 1,
                                    submit: function (e, v, m, f) {
                                        e.preventDefault();
                                        if (v == false) {
                                            $.prompt.close();
                                            processing = false;
                                        }
                                        else {
                                            setPassEntry(member.member_id, config.lasttime, true);
                                        }
                                    }
                                }
                            };
                        }
                        else {
                            prompt = {
                                verify: {
                                    html: config.default_html.replace('{{name}}', member.forename + " " + member.surname)
                                        .replace('{{age}}', age)
                                        .replace('{{actiontitle}}', 'Are these details correct?')
                                        .replace('{{memberinfo}}', confirm_details)
                                        .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid + "\" class=\"btn btn-block btn-primary\"> View member</a>"),
                                    buttons: [
                                        {
                                            title: 'Cancel',
                                            value: -1,
                                            classes: ['entry_large', "cancel"]
                                        },
                                        {
                                            title: 'Verify',
                                            value: 1,
                                            classes: ['entry_large']
                                        }
                                    ],
                                    focus: 1,
                                    submit: function (e, v, m, f) {
                                        e.preventDefault();
                                        if (v == -1) {
                                            $.prompt.close();
                                            processing = false;
                                        }
                                        else if (v == 'view') {
                                            window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid;
                                        }
                                        else {
                                            $.prompt.goToState('entry');
                                        }
                                    }
                                },
                                entry: {
                                    html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                        .replace('{{age}}', age)
                                        .replace('{{name}}', member.forename + " " + member.surname)
                                        .replace('{{memberinfo}}', table)
                                        .replace('{{notices}}', notices)
                                        .replace('{{memberid}}', ''),
                                    buttons: config.buttons,
                                    focus: 1,
                                    submit: function (e, v, m, f) {
                                        e.preventDefault();
                                        if (v == false) {
                                            $.prompt.goToState('verify');
                                        }
                                        else if (v == 'add_membership') {
                                            $.prompt.goToState('membership');
                                        }
                                        else {
                                            sessiontime = v;
                                            setEntry(member.member_id, sessiontime, true);
                                        }
                                    }
                                },
                                membership: {
                                    html: config.default_membership_html.replace('{{actiontitle}}', 'Select action')
                                        .replace('{{age}}', age)
                                        .replace('{{name}}', member.forename + " " + member.surname)
                                        .replace('{{notices}}', notices)
                                        .replace('{{startdate}}', config.date.thisMonday)
                                        .replace(/{{enddate}}/g, config.date.thisSunday),
                                    buttons: [{
                                            title: 'Back',
                                            value: -1,
                                            classes: ['tworow', 'cancel']
                                        }, {
                                            title: 'Continue',
                                            value: 2,
                                            classes: 'threerow'
                                        }],
                                    focus: 1,
                                    submit: function (e, v, m, f) {
                                        e.preventDefault();
                                        if (v == -1) {
                                            $.prompt.goToState('entry');
                                        }
                                        else {
                                            setWeekPass(mid, config.lasttime, config.date.thisMonday, true);
                                        }
                                    }
                                }
                            };
                        }
                        prompts.state(prompt);
                    }
                    else {
                        $.ajax({
                            url: ajaxurl,
                            type: 'post',
                            data: {
                                action: 'get_booking_details',
                                memberID: member.member_id,
                                ppid: ppid
                            },
                            beforeSend: function () {
                                loader.fixedCreate();
                            },
                            success: function (response) {
                                loader.fixedDelete();
                                var details = JSON.parse(response);
                                if (details == false) {
                                    prompt = {
                                        verify: {
                                            html: config.default_html.replace('{{name}}', member.forename + " " + member.surname)
                                                .replace('{{age}}', age)
                                                .replace('{{actiontitle}}', 'Are these details correct?')
                                                .replace('{{memberinfo}}', confirm_details)
                                                .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid + "\" class=\"btn btn-block btn-primary\"> View member</a>"),
                                            buttons: [
                                                {
                                                    title: 'Cancel',
                                                    value: -1,
                                                    classes: ['entry_large', "cancel"]
                                                },
                                                {
                                                    title: 'Verify',
                                                    value: 1,
                                                    classes: ['entry_large']
                                                }
                                            ],
                                            focus: 1,
                                            submit: function (e, v, m, f) {
                                                e.preventDefault();
                                                if (v == -1) {
                                                    $.prompt.close();
                                                    processing = false;
                                                }
                                                else if (v == 'view') {
                                                    window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid;
                                                }
                                                else {
                                                    $.prompt.goToState('day');
                                                }
                                            }
                                        },
                                        day: {
                                            html: config.default_html.replace('{{actiontitle}}', 'Select action')
                                                .replace('{{age}}', age)
                                                .replace('{{name}}', member.forename + " " + member.surname)
                                                .replace('{{memberinfo}}', table)
                                                .replace('{{notices}}', notices)
                                                .replace('{{memberid}}', ''),
                                            buttons: config.buttons,
                                            focus: 1,
                                            submit: function (e, v, m, f) {
                                                e.preventDefault();
                                                if (v == false) {
                                                    $.prompt.close();
                                                    processing = false;
                                                }
                                                else {
                                                    sessiontime = v;
                                                    setBookingEntry(member.member_id, sessiontime, ppid, true);
                                                }
                                            }
                                        }
                                    };
                                    prompts.state(prompt);
                                }
                                else {
                                    prompt = {
                                        verify: {
                                            html: config.default_html.replace('{{name}}', member.forename + " " + member.surname)
                                                .replace('{{age}}', age)
                                                .replace('{{actiontitle}}', 'Are these details correct?')
                                                .replace('{{memberinfo}}', confirm_details)
                                                .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid + "\" class=\"btn btn-block btn-primary\"> View member</a>"),
                                            buttons: [
                                                {
                                                    title: 'Cancel',
                                                    value: -1,
                                                    classes: ['entry_large', "cancel"]
                                                },
                                                {
                                                    title: 'Verify',
                                                    value: 1,
                                                    classes: ['entry_large']
                                                }
                                            ],
                                            focus: 1,
                                            submit: function (e, v, m, f) {
                                                e.preventDefault();
                                                if (v == -1) {
                                                    $.prompt.close();
                                                    processing = false;
                                                }
                                                else if (v == 'view') {
                                                    window.location.href = roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + mid;
                                                }
                                                else {
                                                    $.prompt.goToState('day');
                                                }
                                            }
                                        },
                                        day: {
                                            html: config.default_html.replace('{{name}}', member.forename + " " + member.surname)
                                                .replace('{{age}}', age)
                                                .replace('{{actiontitle}}', 'Are these details correct?')
                                                .replace('{{memberinfo}}', table)
                                                .replace('{{notices}}', "<p class=\"lead\">Booking details</p><table class=\"table\" style=\"font-size:14px;\"><thead><tr><th>Start time</th><th>End time</th></tr></thead><tbody><tr><td>" + details.session_start_time + "</td><td>" + details.session_end_time + "</td></tr></tbody></table>")
                                                .replace('{{memberid}}', member.member_id),
                                            buttons: [{
                                                    title: "Cancel",
                                                    value: false,
                                                    classes: ['tworow', 'cancel']
                                                }, {
                                                    title: "Next",
                                                    value: true,
                                                    class: ['tworow']
                                                }],
                                            focus: 1,
                                            submit: function (e, v, m, f) {
                                                e.preventDefault();
                                                if (v == false) {
                                                    $.prompt.close();
                                                    processing = false;
                                                }
                                                else {
                                                    setBookingEntry(member.member_id, ppid, details.session_end_time, true);
                                                }
                                            }
                                        }
                                    };
                                    prompts.state(prompt);
                                }
                            }
                        });
                        prompts.state(prompt);
                    }
                }
            });
        }
    };
    var changeSession = function (memberID, forename, surname, expertise, age, has_membership, session_id, times, notes) {
        if (notes === void 0) { notes = false; }
        if (Object.keys(config.sessions).length === 0) {
            prompts.warning("<h3 style=\"text-align:center\">No available sessions</h3><div class=\"well\">Please check if this is corret via the session page. If this not correct, please contact adminstration.</div>");
        }
        else {
            var table = "<table class=\"table\" style=\"font-size:14px;\"><tbody><tr><th>Member number</th><td>" + memberID + "</td></tr><tr><th>Expertise</th><td>" + expertise + "</td></tr><tr><th>Membership</th><td>" + has_membership + "</td></tr></tbody></table><div class=\"alert alert-info\">This member is already on the premises. Member is due to entered @ <strong>" + times.split(',')[0] + "</strong> and due to leave @ <strong>" + times.split(',')[1] + "</strong></div>";
            var notices = (notes !== false) ? "<div class=\"alert alert-warning\"><p><strong>Warning</strong> This member has notes associated with his account.  Please read carefully.</p><p>" + notes + "</p></div>" : '';
            prompt = {
                day: {
                    html: config.default_html.replace('{{actiontitle}}', 'Change session time')
                        .replace('{{age}}', age)
                        .replace('{{name}}', forename + " " + surname)
                        .replace('{{memberinfo}}', table)
                        .replace('{{notices}}', notices + " <a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Freports%2Fcreate&amp;report=email_member&amp;member_id=" + memberID + "&amp;membership=1\" class=\"btn btn-block btn-default\" style=\"margin-bottom:20px\">Reissue email</a><a href=\"" + roots.live + "?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member=" + memberID + "\" class=\"btn btn-block btn-primary\">View member</a>"),
                    buttons: config.buttons,
                    focus: 1,
                    submit: function (e, v, m, f) {
                        e.preventDefault();
                        if (v == false) {
                            $.prompt.close();
                            processing = false;
                        }
                        else {
                            sessiontime = v;
                            updateSession(session_id, sessiontime);
                        }
                    }
                }
            };
            prompts.state(prompt);
        }
    };
    var setEntry = function (memberID, exitTime, verify) {
        if (verify === void 0) { verify = false; }
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'set_member_entry',
                memberID: memberID,
                timeOut: exitTime,
                verify: verify
            },
            beforeSend: function (res) {
                loader.fixedCreate();
            },
            success: function (response) {
                loader.fixedDelete();
                if (response == 'passed') {
                    if (memberID != 2) {
                        config.premises.onPremises++;
                        $('#entry-amount').val(parseInt(config.premises.onPremises));
                        $('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
                        entryCounter.createEntry();
                    }
                    $.prompt.close();
                    processing = false;
                    reset();
                }
                else {
                    prompts.warning("<p class=\"lead\">" + response + "</p>");
                    processing = false;
                }
            },
            error: function (error) {
                prompts.warning("<h3 style=\"text-align:center\">Error</h3><p class=\"lead\">" + error + "</p>");
            }
        });
    };
    var setWeekPass = function (memberID, exitTime, week, verify, close) {
        if (verify === void 0) { verify = false; }
        if (close === void 0) { close = true; }
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'set_new_weekpass',
                memberID: memberID,
                week: week,
                time: exitTime,
                verify: verify
            },
            beforeSend: function (res) {
                loader.fixedCreate();
            },
            success: function (response) {
                loader.fixedDelete();
                if (response == 'passed') {
                    if (memberID != 2) {
                        config.premises.onPremises++;
                        $('#entry-amount').val(parseInt(config.premises.onPremises));
                        $('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
                        entryCounter.createEntry();
                    }
                    if (close) {
                        $.prompt.close();
                        reset();
                        processing = false;
                    }
                    else {
                        $("a[data-member-id=\"" + memberID + "\"]").text(' Processed ').removeClass('btn-default').addClass('btn-success');
                        return true;
                    }
                }
                else {
                    prompts.warning("<h3 style=\"text-align:center\">Error</h3><div class=\"well\">Error: " + response + "</div>");
                    processing = false;
                }
            },
            error: function (error) {
                prompts.warning("<h3 style=\"text-align:center\">Error</h3><div class=\"well\">Error: " + error + "</div>");
            }
        });
    };
    var setBookingEntry = function (type, memberID, pp, sessiontime, verify, close) {
        if (verify === void 0) { verify = false; }
        if (close === void 0) { close = true; }
        switch (type) {
            case 'session':
                $.ajax({
                    url: ajaxurl,
                    type: 'post',
                    data: {
                        action: 'set_member_entry_pp',
                        memberID: memberID,
                        ppid: pp,
                        timeOut: sessiontime,
                        verify: verify
                    },
                    beforeSend: function (res) {
                        loader.fixedCreate();
                    },
                    success: function (response) {
                        loader.fixedDelete();
                        if (response == 'passed') {
                            config.premises.onPremises++;
                            config.premises.sessions--;
                            $('#entry-amount').val(parseInt(config.premises.onPremises));
                            $('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
                            entryCounter.createEntry();
                            $('#paid-sesh').val(config.premises.sessions);
                            $('#paid-pass').val(config.premises.passes).triggerHandler('change');
                            entryCounter.createPaid();
                            if (close === true) {
                                $.prompt.close();
                                reset();
                                processing = false;
                            }
                            else {
                                $.prompt.close();
                                booking.total--;
                                if (booking.total > 0) {
                                    processBooking(booking.details.id);
                                }
                                else {
                                    reset();
                                    processing = false;
                                }
                            }
                        }
                        else {
                            $.prompt.close();
                            return true;
                        }
                    },
                    error: function (error) {
                        loader.fixedDelete();
                        prompts.simple(config.default_simple_html.replace('{{message}}', "<p class=\"lead\">" + error + "</p>"))
                            .replace('{{actiontitle}}', 'Choose action');
                    }
                });
                break;
            case 'pass':
                $.ajax({
                    url: ajaxurl,
                    type: 'post',
                    data: {
                        action: 'set_member_entry_booking_pass',
                        passid: pp,
                        memberID: memberID,
                        timeOut: config.lasttime,
                        verify: verify
                    },
                    beforeSend: function (res) {
                        loader.fixedCreate();
                    },
                    success: function (response) {
                        loader.fixedDelete();
                        if (response == 'passed') {
                            config.premises.onPremises++;
                            config.premises.sessions--;
                            $('#entry-amount').val(parseInt(config.premises.onPremises));
                            $('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
                            entryCounter.createEntry();
                            $('#paid-sesh').val(config.premises.sessions);
                            $('#paid-pass').val(config.premises.passes).triggerHandler('change');
                            entryCounter.createPaid();
                            if (close === true) {
                                $.prompt.close();
                                reset();
                                processing = false;
                            }
                            else {
                                $.prompt.close();
                                booking.total--;
                                if (booking.total > 0) {
                                    processBooking(booking.details.id);
                                }
                                else {
                                    reset();
                                    processing = false;
                                }
                            }
                        }
                        else {
                            prompts.simple(config.default_simple_html.replace('{{message}}', "<p class=\"lead\">" + response + "</p>"))
                                .replace('{{actiontitle}}', 'Choose action');
                            processing = false;
                        }
                    },
                    error: function (error) {
                        loader.fixedDelete();
                        prompts.simple(config.default_simple_html.replace('{{message}}', "<p class=\"lead\">" + error + "</p>"))
                            .replace('{{actiontitle}}', 'Choose action');
                    }
                });
        }
    };
    var setPassEntry = function (memberID, sessiontime, verify) {
        if (verify === void 0) { verify = false; }
        updateSessions();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'set_member_entry',
                memberID: memberID,
                timeOut: sessiontime,
                verify: verify
            },
            beforeSend: function (res) {
                loader.fixedCreate();
            },
            success: function (response) {
                loader.fixedDelete();
                if (response == 'passed') {
                    config.premises.onPremises++;
                    config.premises.passes--;
                    $('#entry-amount').val(parseInt(config.premises.onPremises));
                    $('#entry-percentage').val(config.premises.onPremises / config.maxAmount);
                    entryCounter.createEntry();
                    $('#paid-sesh').val(config.premises.sessions);
                    $('#paid-pass').val(config.premises.passes).triggerHandler('change');
                    entryCounter.createPaid();
                    $.prompt.close();
                    processing = false;
                    reset();
                }
                else {
                    prompts.warning("<p class=\"lead\">An error occured.  Please check the member has been added into the system <a href=\"" + roots.live + "?page=rampworld-membership/membership.php/entries%2Fview%2Fall&membership_id=" + memberID + "\" target=\"_blank\">here</a>.</p><div class=\"well\">" + error + "</div>");
                    processing = false;
                }
            },
            error: function (error) {
                prompts.warning("<p class=\"lead\">An error occured.  Please check the member has been added into the system <a href=\"" + roots.live + "?page=rampworld-membership/membership.php/entries%2Fview%2Fall&membership_id=" + memberID + "\" target=\"_blank\">here</a>.</p><div class=\"well\">" + error + "</div>");
            }
        });
    };
    var setSessions = function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'get_todays_sessions'
            },
            beforeSend: function () {
                loader.fixedCreate();
            },
            success: function (response) {
                loader.fixedDelete();
                if (response != "") {
                    config.sessions = JSON.parse(response);
                    config.buttons.push({
                        title: 'Cancel',
                        value: false,
                        classes: ['entry_large', "cancel"]
                    });
                    for (var i = 0; i < config.sessions.length; i++) {
                        if (i % 3 != 0) {
                            config.buttons.push({
                                title: ((config.sessions[i].display_name.toLowerCase().includes('private')) ? config.sessions[i].end_time + " " + config.sessions[i].display_name.replace('Only', '') : config.sessions[i].end_time),
                                value: config.sessions[i].end_time,
                                classes: ['threerow', 'noBorder']
                            });
                        }
                        else {
                            config.buttons.push({
                                title: ((config.sessions[i].display_name.toLowerCase().includes('private')) ? config.sessions[i].end_time + " " + config.sessions[i].display_name.replace('Only', '') : config.sessions[i].end_time),
                                value: config.sessions[i].end_time,
                                classes: 'threerow'
                            });
                        }
                        if (!config.sessions[i].display_name.toLowerCase().includes('private') && (config.sessions[i].end_time > config.lasttime || config.lasttime == null))
                            config.lasttime = config.sessions[i].end_time;
                        if ((config.onlyPrivate == true || config.onlyPrivate == null) && config.sessions[i].display_name.toLowerCase().includes('private')) {
                            config.onlyPrivate = true;
                        }
                        else {
                            config.onlyPrivate = false;
                        }
                    }
                    config.sessoion_atm = true;
                }
                else {
                    config.sessoion_atm = false;
                    prompts.warning("<h3 style=\"text-align: center;\">Could not load session times</h3><div class=\"well\">Please contact administration to notify them of the issue.</div>");
                }
            },
            error: function (e) {
                prompts.warning("<h3 style=\"text-align: center;\">Could not load session times</h3><div class=\"well\">Please contact administration to notify them of the issue.</div>");
            }
        });
    };
    var updateSession = function (session_id, sessiontime) {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'update_member_entry',
                session_id: session_id,
                exit_time: sessiontime
            },
            beforeSend: function (res) {
                loader.fixedCreate();
            },
            success: function (response) {
                loader.fixedDelete();
                if (response == 'passed') {
                    $.prompt.close();
                    reset();
                    processing = false;
                }
                else {
                    prompts.warning("<h3>A database error occured.</h3><div class=\"well\"><strong>Error log</strong><br>" + response + "</div>");
                    processing = false;
                }
            },
            error: function (error) {
                prompts.warning("<h3>A database error occured.</h3><div class=\"well\"><strong>Error log</strong><br>" + error + "</div>");
                processing = false;
            }
        });
    };
    var updateSessions = function () {
        var current_date = new Date();
        var temp_buttons = [];
        var updated = false;
    };
    var reset = function () {
        $('#membership-number-input').val('');
        if ($('#custom-input-1').length == 0) {
            $('#day').remove();
            $('#month').remove();
            $('#year').after('<input type="text" class="form-control input-lg border-bottom-6" id="custom-input-1" placeholder="Search for Forename" style="width:100%" value="">').remove();
        }
        if ($('#custom-input-2').length == 0) {
            $('#day').remove();
            $('#month').remove();
            $('#year').after('<input type="text" class="form-control input-lg border-bottom-6" id="custom-input-2" placeholder="Search for Surname" style="width:100%" value="">').remove();
        }
        $('#custom-input-1').val('');
        $('#custom-input-1').attr('type', 'text');
        $('#custom-input-1').attr('placeholder', 'Please enter Forename');
        $('#custom-field-1').val('forename');
        $('#custom-input-2').val('');
        $('#custom-input-2').attr('type', 'text');
        $('#custom-input-2').attr('placeholder', 'Please enter Surname');
        $('#custom-field-2').val('surname');
        $('#number-of-members').text('-');
        $('#query-time').text('-');
        $('#render-time').text('-');
        $('#members').html("<tr><td colspan=\"8\"><p class=\"lead\" style=\"text-align: center;\">" + $('#entry-amount').val() + " members on premises</p></td></tr>");
        $('.input-group:first-child .input-group-btn .dropdown-toggle').html('Forename <span class="caret"></span>');
        $('.input-group:last-child .input-group-btn .dropdown-toggle').html('Surname <span class="caret"></span>');
        $('.has-feedback').removeClass('has-feedback');
        $('.has-error').removeClass('has-error');
        $('.has-success').removeClass('has-success');
        $('#entry-table thead').html('<tr><th>#</th><th>Forename</th><th>Surname</th><th>Age</th><th>Expertise</th><th>Disipline</th><th>Medical notes</th><th>System notes</th></tr>');
    };
    return {
        init: init,
        processBooking: processBooking,
        processEntry: processEntry,
        verifyMemberGUI: verifyMemberGUI,
        reset: reset
    };
});
