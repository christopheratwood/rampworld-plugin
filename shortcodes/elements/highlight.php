<?php

function highlight($atts, $content = "")
{
    
    $total_links = 20;
    $total_btns = 3;
    $total_sections = 10;
    $sizes = array(3 => "col-md-3 col-sm-4 col-xs-6 col-ms-12",4 => "col-md-4 col-sm-6 col-xs-6 col-ms-12", 6 => "col-xs-6 col-ms-12", 12 => "col-xs-12 col-ms-12");

    $html = '<div class="col-ms-12">'.(isset($atts['link']) ? '<a href="'.$atts['link'].'">':'').'<div class="card card-highlight">';

    if (isset($atts['image-link'])) {
        $html .= '<div class="card-image "><img class="img-responsive " src="'.$atts['image-link'].'"/>';
        if (isset($atts['title'])) {
            $html .= '<h2 class="card-title">'.$atts['title'].'</h2>';
        }
        $html .= '</div>';
        
    };   

    $html .='<div class="card-content "><h2>'.(isset($atts['summary'])?$atts['summary']:'').'</p></div><div class="card-action ">';
    $list_style = (isset($atts['list-style'])) ? $atts['list-style']: 'li';
    if(isset($atts['text'])) {
        $html .= '<p class="lead">'.$atts['text'].'</p>';
    }
    if(isset($atts['before-link'])) {
        $html .= '<p>'.$atts['before-link'].'</p>';
    }
    if (isset($atts['list-1'])) {
        $html .= '<'.$list_style.' class="smart">';
    }
    for ($i = 1; $i <= $total_links; $i++) {
        if (isset($atts['list-'.$i])) {
            $html .= '<li>'.$atts['list-'.$i].'</li>';
        }
    }
    if (isset($atts['list-1'])) {
        $html .= '</'.$list_style.'>';
    }
    if (isset($atts['after-link'])) {
        $html .= '<p>'.$atts['after-link'].'</p>';
    }
    for ($i = 1; $i <= $total_sections; $i++) {
        if (isset($atts['section-'.$i.'-title'])&& isset($atts['section-'.$i.'-text']))  {
            $html .= '<p class="lead">'.$atts['section-'.$i.'-title'].'</p><p>'.$atts['section-'.$i.'-text'].'</p>';
        }
    }
        //buttons
    if (isset($atts['button-1-style']) && isset($atts['text'])) {
        $html .= '<hr>';
    }
    for ($i = 1; $i <= $total_btns; $i++) {
        if (isset($atts['button-'.$i.'-style'])) {
            $html .= '<a class="btn btn-'.$atts['button-'.$i.'-style'].'" href="'.$atts['button-'.$i.'-link'].'">'.$atts['button-'.$i.'-text'].'</a>';
        }
    }
    $html .= $content;
    $html .='</div>'.(isset($atts['link']) ? '</a>':'').'</div></div>';
    return $html;
}

add_shortcode('highlight', 'highlight');