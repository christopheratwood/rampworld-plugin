<?php
use Rampworld\Timetable\Timetable as Timetable;
use Rampworld\Timetable\NonOptionalDates as NonOptionalDates;

require_once __dir__ .'/../../../../themes/rampworld/modules/vendor/autoload.php';
/*
Template Name: Opening Hours
*/
  
function timetable() {
  $timetable = new Timetable();	
  echo $timetable->render();
}
 add_shortcode('timetable', 'timetable');