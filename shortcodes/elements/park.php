<?php
function park($atts, $contents= "") {
    $total_links = 20;
    $total_btns = 3;
    $total_sections = 10;
    $sizes = array(3 => "col-md-3 col-sm-4 col-xs-6 col-ms-12",4 => "col-md-4 col-sm-6 col-xs-6 col-ms-12", 6 => "col-xs-6 col-ms-12", 12 => "col-xs-12 col-ms-12");

    $html = '<div class="col-ms-12">
    <div class="card">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          <img src="'.$atts['image-link'].'" alt="'.$atts['image-alt'].'" style="width: 100%; height: auto;" class="size-large wp-image-960" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="card-content">
            <h2>'.$atts['title'].'</h2>
          </div>
          <div class="card-action">'.$contents.'</div>
        </div>
      </div>
      </div>
      </div>';    
     
    return $html;
}
add_shortcode('park', 'park');

function park_left($atts, $contents= "") {
  $total_links = 20;
  $total_btns = 3;
  $total_sections = 10;
  $sizes = array(3 => "col-md-3 col-sm-4 col-xs-6 col-ms-12",4 => "col-md-4 col-sm-6 col-xs-6 col-ms-12", 6 => "col-xs-6 col-ms-12", 12 => "col-xs-12 col-ms-12");

  $html = '<div class="col-ms-12">
  <div class="card">
    <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12 visible-sm visible-md visible-lg">
        <div class="card-content">
          <h2>'.$atts['title'].'</h2>
        </div>
        <div class="card-action">'.$contents.'</div>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12">
        <img src="'.$atts['image-link'].'" alt="'.$atts['image-alt'].'" style="width: 100%; height: auto;" class="size-large wp-image-960" />
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 visible-xs visible-ms">
        <div class="card-content">
          <h2>'.$atts['title'].'</h2>
        </div>
        <div class="card-action">'.$contents.'</div>
      </div>
    </div>
    </div>
    </div>';    
   
  return $html;
}
add_shortcode('park-left', 'park_left');

function park_360($atts, $contents= "") {
  $total_links = 20;
  $total_btns = 3;
  $total_sections = 10;
  $sizes = array(3 => "col-md-3 col-sm-4 col-xs-6 col-ms-12",4 => "col-md-4 col-sm-6 col-xs-6 col-ms-12", 6 => "col-xs-6 col-ms-12", 12 => "col-xs-12 col-ms-12");

  $html = '<div class="col-ms-12">
  <div class="card">
    <div class="row">
      <div class="col-md-4 col-sm-4 col-xs-12 visible-sm visible-md visible-lg">
        <div class="card-content">
          <h2>'.$atts['title'].'</h2>
        </div>
        <div class="card-action">'.$contents.'</div>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m0!4v1500725073708!6m8!1m7!1sF%3A-tmZz2ZwyRz4%2FWNVY7LWD5zI%2FAAAAAAAADrI%2FbUa0FQPmh7U8LnZ8GdI8O6t4VeH70omAACLIB!2m2!1d51.52518051986609!2d-3.192247822880745!3f285.3164633527862!4f-12.472149918119683!5f0.4000000000000002" style="width: 100%; min-height: 300px; " frameborder="0" allowfullscreen=""></iframe>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12 visible-xs visible-ms">
        <div class="card-content">
          <p>'.$atts['title'].'</p>
        </div>
        <div class="card-action">'.$contents.'</div>
      </div>
    </div>
    </div>
    </div>';    
   
  return $html;
}
add_shortcode('park-360', 'park_360');
